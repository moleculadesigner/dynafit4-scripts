
===================================================
Least squares Minimization - Trust Region Algorithm
===================================================

Optimized parameters:

     1 	 k.1
     2 	 k.-1
     3 	 k.cat
     4 	 r(E)
     5 	 r(E.S1)
     6 	 r(E.P)
     7 	 [E]#1
     8 	 [S]#1
     9 	 offset#1
    10 	 [E]#2
    11 	 [S]#2
    12 	 offset#2
    13 	 [E]#3
    14 	 [S]#3
    15 	 offset#3
    16 	 [E]#4
    17 	 [S]#4
    18 	 offset#4
    19 	 [E]#5
    20 	 [S]#5
    21 	 offset#5
    22 	 [E]#6
    23 	 [S]#6
    24 	 offset#6
    25 	 [E]#7
    26 	 [S]#7
    27 	 offset#7

:: NL2SOL :: DNLAFB
:: Analytic Jacobian; Constrained

________________________________________________________________________

:: Initial Estimate

     i	     Param p(i)	            Min	            Max
     1	   1.40507e-005	         1e-006	         1e+006
     2	   1.45003e-005	         1e-012	         1e+006
     3	        2.77081	         1e-006	         1e+006
     4	           0.31	       3.1e-007	         310000
     5	           0.22	       2.2e-007	         220000
     6	           0.25	       2.5e-007	         250000
     7	              1	       0.909091	            1.1
     8	            0.5	       0.454545	           0.55
     9	         1.1793	       -10.6137	        12.9723
    10	              1	       0.909091	            1.1
    11	              1	       0.909091	            1.1
    12	        1.14325	       -10.6497	        12.9363
    13	              1	       0.909091	            1.1
    14	            1.5	        1.36364	           1.65
    15	        1.10351	       -10.6895	        12.8965
    16	              1	       0.909091	            1.1
    17	              2	        1.81818	            2.2
    18	        1.07103	        -10.722	         12.864
    19	              1	       0.909091	            1.1
    20	            2.5	        2.27273	           2.75
    21	        1.02909	       -10.7639	        12.8221
    22	              1	       0.909091	            1.1
    23	              3	        2.72727	            3.3
    24	        0.97752	       -10.8155	        12.7705
    25	              1	       0.909091	            1.1
    26	              4	        3.63636	            4.4
    27	        0.95791	       -10.8351	        12.7509

:: Iterations

     i	 FEval	   F(i) = SSQ	   Param( 1)	   Param( 2)	   Param( 3)	   Param( 4)	   Param( 5)	   Param( 6)	   Param( 7)	   Param( 8)	   Param( 9)	   Param(10)	   Param(11)	   Param(12)	   Param(13)	   Param(14)	   Param(15)	   Param(16)	   Param(17)	   Param(18)	   Param(19)	   Param(20)	   Param(21)	   Param(22)	   Param(23)	   Param(24)	   Param(25)	   Param(26)	   Param(27)	
     0	     1	       486.98	  1.4051e-005	    1.45e-005	       2.7708	         0.31	         0.22	         0.25	            1	          0.5	       1.1793	            1	            1	       1.1433	            1	          1.5	       1.1035	            1	            2	        1.071	            1	          2.5	       1.0291	            1	            3	      0.97752	            1	            4	      0.95791	
     1	     3	       1.3725	   0.00016721	    0.0011009	       2.7708	     3.1e-007	      0.18323	         0.31	      0.99567	      0.50414	       1.1587	       1.0596	       1.0735	       1.0982	       1.0462	        1.541	       1.0544	       1.0389	       2.0735	       1.0252	      0.99338	       2.3563	       0.9844	       1.0165	       3.0085	      0.94239	      0.97029	       4.0125	      0.91155	
     2	     4	      0.72671	       1e-006	       1e-012	       2.7656	     3.1e-007	      0.11231	      0.45561	      0.96736	      0.47336	       1.1514	      0.95843	      0.97597	       1.1012	       1.0166	       1.5365	       1.0548	       1.0528	       2.1172	       1.0181	       1.0258	       2.5697	      0.97756	       1.0558	       3.2251	       0.9373	       1.0231	        3.863	      0.90813	
     3	     5	      0.51522	       1e-006	    0.0067572	       2.7655	     3.1e-007	      0.11181	      0.42512	      0.96728	      0.47319	       1.1427	      0.95584	      0.97342	       1.1018	       1.0137	       1.5346	       1.0535	       1.0507	       2.1161	        1.011	       1.0244	       2.5692	      0.96935	       1.0561	       3.2252	      0.93029	        1.018	       3.8616	      0.89584	
     4	     7	       0.5152	       1e-006	     0.012495	       2.7655	    0.0046738	      0.11119	      0.38745	      0.96718	      0.47298	       1.1382	      0.95285	      0.97049	       1.0974	       1.0105	       1.5325	       1.0487	       1.0482	       2.1149	       1.0061	       1.0229	       2.5686	      0.96457	       1.0565	       3.2253	      0.92536	        1.012	       3.8601	      0.89107	
     5	     9	      0.51515	       1e-006	     0.022677	       2.7654	     0.013979	      0.10996	      0.31308	      0.96696	      0.47262	       1.1292	      0.94745	      0.96523	       1.0885	       1.0046	       1.5286	       1.0394	       1.0437	       2.1127	      0.99634	       1.0202	       2.5675	      0.95508	       1.0571	       3.2255	      0.91553	       1.0013	       3.8573	      0.88174	
     6	    12	      0.51514	       1e-006	      0.02268	       2.7654	     0.014021	      0.10922	      0.31306	      0.96696	      0.47262	       1.1291	      0.94804	      0.96523	       1.0886	       1.0042	       1.5283	       1.0394	       1.0437	       2.1127	      0.99635	       1.0202	       2.5675	      0.95507	       1.0571	       3.2255	      0.91548	       1.0021	       3.8573	      0.88182	
     7	    16	       0.5151	       1e-006	       0.0323	       2.7652	     0.025467	      0.10771	      0.22283	      0.96663	      0.47227	       1.1181	       0.9428	      0.96019	       1.0778	      0.99853	       1.5246	        1.028	       1.0393	       2.1106	      0.98446	       1.0175	       2.5665	      0.94343	       1.0576	       3.2257	      0.90338	      0.99182	       3.8546	      0.87047	
     8	    18	      0.51497	       1e-006	     0.045008	        2.765	     0.042294	      0.10461	     0.040453	      0.96635	      0.47179	       1.1018	      0.94251	      0.95338	        1.062	      0.99801	       1.5197	       1.0113	       1.0389	       2.1078	      0.96707	       1.0171	       2.5651	      0.92638	       1.0575	       3.2261	      0.88559	      0.99148	       3.8511	        0.854	
     9	    20	      0.51376	    0.0038282	     0.045006	        2.765	     0.042911	      0.10449	     0.036496	      0.96633	      0.47179	       1.1013	      0.94249	      0.95339	       1.0615	      0.99798	       1.5197	       1.0108	       1.0389	       2.1078	      0.96656	       1.0171	       2.5651	      0.92592	       1.0575	       3.2261	      0.88518	      0.99147	       3.8511	      0.85358	
    10	    22	      0.51218	    0.0024318	     0.045007	        2.765	     0.043297	      0.10439	     0.036165	      0.96632	      0.47179	        1.101	      0.94249	      0.95339	       1.0613	      0.99798	       1.5197	       1.0107	       1.0389	       2.1078	      0.96656	       1.0171	       2.5651	      0.92597	       1.0575	       3.2261	       0.8853	      0.99148	       3.8511	      0.85375	
    11	    23	      0.51192	    0.0016865	     0.045006	        2.765	     0.043447	      0.10426	     0.034928	      0.96631	      0.47179	       1.1008	      0.94249	       0.9534	       1.0611	      0.99798	       1.5197	       1.0105	       1.0388	       2.1078	      0.96638	       1.0171	       2.5651	       0.9258	       1.0575	        3.226	      0.88514	      0.99147	       3.8511	      0.85361	
    12	    24	      0.51172	    0.0015462	     0.045011	        2.765	     0.043701	      0.10329	     0.033908	      0.96629	      0.47179	       1.1006	      0.94254	      0.95346	       1.0609	      0.99802	       1.5198	       1.0102	       1.0388	       2.1078	      0.96612	       1.0171	       2.5651	      0.92556	       1.0573	        3.226	       0.8849	      0.99147	       3.8511	       0.8534	
    13	    26	      0.51154	    0.0012957	     0.045008	        2.765	      0.04396	      0.10306	     0.032219	      0.96628	      0.47179	       1.1003	      0.94255	      0.95348	       1.0606	      0.99803	       1.5198	         1.01	       1.0388	       2.1078	      0.96587	        1.017	       2.5651	      0.92532	       1.0573	        3.226	      0.88466	      0.99146	       3.8511	      0.85318	
    14	    28	      0.51131	    0.0010625	     0.044992	        2.765	     0.044604	      0.10108	     0.029361	      0.96625	      0.47178	       1.0997	      0.94284	      0.95379	         1.06	      0.99827	         1.52	       1.0094	       1.0389	       2.1078	      0.96525	       1.0169	       2.5651	      0.92473	       1.0567	       3.2258	      0.88409	      0.99149	       3.8512	      0.85264	
    15	    29	      0.51119	   0.00080343	     0.044977	       2.7651	     0.045227	     0.099313	     0.026201	      0.96623	      0.47178	       1.0991	      0.94319	      0.95416	       1.0594	      0.99856	       1.5202	       1.0087	       1.0389	       2.1078	      0.96461	       1.0167	        2.565	      0.92412	       1.0561	       3.2257	      0.88348	      0.99152	       3.8512	      0.85204	
    16	    30	      0.51108	   0.00070927	     0.044957	       2.7651	     0.045835	     0.097555	     0.023103	      0.96621	      0.47178	       1.0985	      0.94367	      0.95466	       1.0588	      0.99897	       1.5204	       1.0081	        1.039	       2.1079	      0.96397	       1.0164	       2.5649	      0.92351	       1.0552	       3.2254	      0.88288	      0.99158	       3.8512	      0.85144	
    17	    32	      0.51096	    0.0005052	      0.04488	       2.7652	     0.047531	     0.089069	     0.018118	      0.96613	       0.4718	       1.0969	      0.94696	        0.958	       1.0571	       1.0018	       1.5224	       1.0063	       1.0397	       2.1083	      0.96219	       1.0149	       2.5644	      0.92187	       1.0494	       3.2236	      0.88138	      0.99198	       3.8513	      0.84977	
    18	    36	      0.51038	   0.00044715	     0.044838	       2.7655	     0.050352	     0.073199	     0.013292	      0.96555	      0.47253	       1.0942	      0.99768	       1.0091	        1.052	       1.0461	        1.552	       1.0014	       1.0517	       2.1145	      0.95873	      0.99363	       2.5562	      0.92005	      0.96468	       3.1965	      0.88243	      0.99578	       3.8524	      0.84685	
    19	    40	      0.51035	   0.00046019	     0.044838	       2.7655	      0.05036	     0.073189	     0.013262	      0.96554	      0.47253	       1.0942	      0.99891	       1.0091	       1.0519	       1.0474	        1.552	       1.0014	       1.0517	       2.1145	      0.95869	      0.99363	       2.5562	      0.92011	      0.96369	       3.1965	       0.8826	      0.99578	       3.8524	      0.84687	
    20	    41	      0.51032	   0.00045924	     0.044839	       2.7655	      0.05049	     0.072543	     0.012914	      0.96554	      0.47253	       1.0941	      0.99915	       1.0093	       1.0517	       1.0476	       1.5522	       1.0011	       1.0518	       2.1145	      0.95858	      0.99351	       2.5561	      0.92001	      0.96327	       3.1964	      0.88264	      0.99576	       3.8524	      0.84676	
    21	    45	      0.51024	   0.00036492	     0.044852	       2.7656	     0.052467	     0.062014	    0.0076108	      0.96545	      0.47254	       1.0922	       1.0033	       1.0135	       1.0495	        1.051	       1.5545	      0.99886	       1.0524	       2.1148	      0.95647	      0.99147	       2.5554	      0.91815	       0.9563	       3.1944	      0.88108	      0.99544	       3.8524	      0.84482	
    22	    47	      0.51003	   0.00024659	     0.045281	       2.7663	     0.061408	     2.2e-007	  1.4214e-005	       0.9651	      0.47273	       1.0836	       1.0441	       1.0559	       1.0384	       1.0837	       1.5777	      0.98778	       1.0586	       2.1183	      0.94676	      0.97254	       2.5479	       0.9103	      0.90909	       3.1748	      0.87501	      0.98957	       3.8513	      0.83627	
    23	    49	      0.50972	   0.00029181	     0.045282	       2.7663	     0.061451	     2.2e-007	  5.2551e-005	      0.96511	      0.47273	       1.0835	        1.044	       1.0559	       1.0382	       1.0832	       1.5777	      0.98763	       1.0585	       2.1183	      0.94676	      0.97253	       2.5479	      0.91039	      0.90909	       3.1748	      0.87522	      0.98959	       3.8512	      0.83629	
    24	    50	      0.50962	   0.00034055	     0.045443	       2.7663	     0.061292	     2.2e-007	    0.0085663	      0.96514	      0.47277	       1.0837	       1.0492	        1.061	       1.0379	       1.0872	       1.5805	      0.98744	       1.0592	       2.1186	      0.94684	      0.97013	        2.547	      0.91077	      0.90909	       3.1725	      0.87559	      0.98872	        3.851	      0.83656	
    25	    52	      0.50955	   0.00042784	     0.045613	       2.7663	     0.060908	     2.2e-007	     0.019625	      0.96518	      0.47281	       1.0841	       1.0542	        1.066	        1.038	       1.0911	       1.5831	      0.98764	       1.0599	       2.1189	      0.94722	      0.96783	       2.5461	      0.91129	      0.90909	       3.1703	      0.87595	      0.98788	       3.8508	        0.837	
    26	    54	       0.5095	   0.00049384	     0.045647	       2.7663	      0.06075	     2.2e-007	     0.022711	      0.96519	      0.47281	       1.0842	        1.055	       1.0668	       1.0382	       1.0917	       1.5835	      0.98778	         1.06	        2.119	      0.94739	      0.96744	        2.546	      0.91148	      0.90909	         3.17	       0.8761	      0.98774	       3.8508	      0.83718	
    27	    56	      0.50946	   0.00054887	     0.045677	       2.7663	     0.060566	     2.2e-007	     0.025885	      0.96521	      0.47282	       1.0844	       1.0558	       1.0675	       1.0383	       1.0923	       1.5839	      0.98795	       1.0601	        2.119	      0.94758	      0.96711	       2.5459	      0.91168	      0.90909	       3.1696	      0.87628	      0.98761	       3.8507	      0.83737	
    28	    58	       0.5094	   0.00065365	     0.045723	       2.7662	     0.060228	     2.2e-007	     0.031195	      0.96523	      0.47283	       1.0847	       1.0568	       1.0685	       1.0386	       1.0931	       1.5844	      0.98828	       1.0602	       2.1191	      0.94794	      0.96665	       2.5457	      0.91205	      0.90909	       3.1692	      0.87659	      0.98745	       3.8507	      0.83773	
    29	    59	      0.50935	   0.00087899	     0.045754	       2.7661	     0.060928	     2.2e-007	     0.040891	      0.96524	      0.47291	       1.0841	       1.0602	       1.0719	       1.0377	       1.0956	       1.5862	      0.98738	       1.0607	       2.1193	       0.9472	      0.96515	       2.5451	      0.91149	      0.90909	       3.1679	      0.87599	      0.98695	       3.8505	      0.83709	
    30	    60	      0.50916	   0.00094961	     0.045716	        2.766	     0.063695	     2.2e-007	     0.042921	      0.96515	      0.47303	       1.0814	       1.0647	       1.0765	       1.0345	        1.099	       1.5886	      0.98415	       1.0611	       2.1196	      0.94424	      0.96301	       2.5444	      0.90896	      0.90909	       3.1661	      0.87349	      0.98609	       3.8503	      0.83443	
    31	    62	      0.50905	    0.0011513	     0.045726	        2.766	     0.063505	     2.2e-007	      0.04519	      0.96516	      0.47303	       1.0816	       1.0648	       1.0767	       1.0347	       1.0991	       1.5886	      0.98437	       1.0612	       2.1196	      0.94447	      0.96296	       2.5444	      0.90917	      0.90909	       3.1661	      0.87368	      0.98608	       3.8503	      0.83464	
    32	    64	      0.50883	    0.0015649	     0.045743	        2.766	     0.063103	     2.2e-007	     0.049725	      0.96519	      0.47304	        1.082	       1.0651	       1.0769	       1.0351	       1.0993	       1.5888	      0.98483	       1.0612	       2.1196	      0.94493	      0.96288	       2.5443	       0.9096	      0.90909	        3.166	      0.87408	      0.98607	       3.8503	      0.83508	
    33	    66	      0.50853	    0.0020947	     0.045747	        2.766	     0.062962	     2.2e-007	     0.051484	       0.9652	      0.47304	       1.0821	       1.0651	       1.0769	       1.0353	       1.0994	       1.5888	      0.98503	       1.0612	       2.1196	      0.94513	      0.96287	       2.5443	      0.90979	      0.90909	        3.166	      0.87426	      0.98607	       3.8503	      0.83527	
    34	    68	      0.50819	    0.0027455	     0.045751	        2.766	     0.062822	     2.2e-007	     0.053204	      0.96521	      0.47304	       1.0823	       1.0652	       1.0769	       1.0355	       1.0994	       1.5888	      0.98523	       1.0612	       2.1196	      0.94533	      0.96287	       2.5443	      0.90998	      0.90909	        3.166	      0.87444	      0.98608	       3.8503	      0.83547	
    35	    70	      0.50766	    0.0037529	     0.045753	        2.766	     0.062697	     2.2e-007	     0.054854	      0.96522	      0.47304	       1.0824	       1.0652	        1.077	       1.0357	       1.0994	       1.5888	      0.98543	       1.0613	       2.1196	      0.94554	      0.96287	       2.5443	      0.91017	      0.90909	        3.166	      0.87462	      0.98608	       3.8503	      0.83566	
    36	    72	      0.50643	     0.006499	     0.045755	        2.766	     0.062514	     2.2e-007	     0.057636	      0.96524	      0.47304	       1.0827	       1.0652	        1.077	        1.036	       1.0995	       1.5888	      0.98577	       1.0613	       2.1196	      0.94588	      0.96288	       2.5443	      0.91049	      0.90909	       3.1659	      0.87492	       0.9861	       3.8503	      0.83596	
    37	    73	      0.50464	     0.020501	     0.045751	       2.7659	     0.062728	     2.2e-007	     0.061438	      0.96524	      0.47304	       1.0827	       1.0653	       1.0771	       1.0362	       1.0995	       1.5888	      0.98611	       1.0613	       2.1196	      0.94627	      0.96288	       2.5443	      0.91086	      0.90909	       3.1659	      0.87525	      0.98616	       3.8503	      0.83628	
    38	    74	      0.49087	     0.034778	     0.045702	       2.7659	     0.063746	     2.2e-007	     0.059518	       0.9652	      0.47307	       1.0818	       1.0653	       1.0771	       1.0353	       1.0995	       1.5889	      0.98525	       1.0613	       2.1196	      0.94547	      0.96287	       2.5443	      0.91016	      0.90909	        3.166	      0.87463	      0.98617	       3.8503	      0.83568	
    39	    78	      0.41696	      0.16828	     0.047844	       2.7659	     0.070593	     2.2e-007	     0.062817	      0.95643	      0.47343	       1.0773	       1.0578	       1.0802	       1.0319	       1.0923	       1.5903	       0.9824	        1.056	       2.1202	      0.94292	      0.95895	       2.5446	      0.90797	      0.90909	       3.1663	      0.87273	      0.98182	       3.8497	      0.83424	
    40	    79	      0.25829	      0.63554	      0.19391	       2.9183	     0.082969	     2.2e-007	     0.065641	       1.0019	      0.49813	        1.066	          1.1	          1.1	       1.0241	          1.1	         1.65	      0.97908	       1.0974	       2.1502	      0.93872	      0.96518	       2.5513	      0.90709	      0.90909	       3.1584	      0.87325	      0.98182	       3.7608	      0.83575	
    41	    80	      0.15768	       1.1589	      0.22318	       2.9837	     0.093776	     2.2e-007	     0.068372	       1.0619	      0.54857	       1.0546	          1.1	          1.1	       1.0205	          1.1	         1.65	      0.97612	       1.0974	       2.1871	      0.93634	      0.97871	       2.5674	      0.90432	      0.90909	       3.1677	      0.87138	      0.98182	       3.7183	      0.83354	
    42	    81	      0.10668	       1.7074	      0.24036	       3.0795	      0.10198	     2.2e-007	     0.069051	          1.1	         0.55	       1.0455	          1.1	          1.1	        1.019	          1.1	         1.65	      0.97536	       1.0991	        2.189	      0.93581	      0.98161	       2.5886	       0.9038	      0.90909	       3.1669	      0.87091	      0.97895	       3.6364	      0.83283	
    43	    82	     0.085605	       2.1915	      0.24934	       3.1141	      0.10738	     2.2e-007	     0.068749	          1.1	         0.55	       1.0435	          1.1	          1.1	       1.0188	          1.1	         1.65	      0.97567	       1.0991	        2.189	      0.93622	      0.97694	       2.5915	      0.90445	      0.90909	       3.1172	      0.87107	      0.97895	       3.6364	      0.83293	
    44	    83	     0.076311	        2.645	      0.20714	       3.1342	      0.10794	     2.2e-007	     0.065447	          1.1	         0.55	       1.0451	          1.1	          1.1	       1.0223	          1.1	         1.65	      0.97939	       1.0878	          2.2	       0.9408	      0.97209	       2.5647	       0.9079	      0.95643	       2.9797	      0.87063	      0.94951	       3.6364	      0.83774	
    45	    84	     0.071271	       3.0032	     0.059108	       3.1091	      0.10482	     2.2e-007	     0.060541	          1.1	         0.55	       1.0497	          1.1	          1.1	       1.0279	       1.0976	         1.65	      0.98519	        1.074	          2.2	      0.94708	      0.96797	       2.5236	      0.91281	      0.97017	       2.8681	      0.87429	      0.90909	       3.6364	      0.84466	
    46	    85	     0.068583	       3.7754	       1e-012	       2.7539	      0.10458	     0.015036	     0.059413	          1.1	         0.55	       1.0508	          1.1	          1.1	       1.0299	       1.0967	         1.65	      0.98696	       1.0937	          2.2	      0.94699	       1.0095	       2.4914	      0.91108	       1.0314	       2.8532	      0.87142	      0.90909	       3.6364	      0.84503	
    47	    86	     0.065676	        4.086	       1e-012	       2.6047	      0.13437	     0.044137	     0.088275	          1.1	         0.55	       1.0185	          1.1	          1.1	       0.9979	       1.0967	         1.65	      0.95516	       1.0937	          2.2	      0.91545	        1.003	       2.4978	      0.88248	       1.0223	       2.8532	      0.84238	      0.90909	       3.6364	      0.81907	
    48	    88	     0.065106	          4.2	       1e-012	       2.5567	      0.13477	     0.045462	     0.088481	          1.1	         0.55	       1.0182	          1.1	          1.1	      0.99772	       1.0824	         1.65	      0.95631	        1.086	          2.2	      0.91591	      0.99951	       2.4869	      0.88274	       1.0139	       2.7709	      0.84301	      0.90909	       3.6364	      0.81885	
    49	    89	     0.064917	       4.3255	       1e-012	       2.5048	      0.13477	     0.046587	     0.088206	          1.1	         0.55	       1.0181	          1.1	          1.1	      0.99839	       1.0743	         1.65	      0.95763	       1.0876	          2.2	      0.91615	       1.0079	       2.4772	      0.88225	       1.0283	       2.7273	      0.84191	      0.90909	       3.6364	      0.81896	
    50	    90	     0.064716	        4.385	   0.00033326	        2.493	      0.13543	     0.046904	     0.088482	          1.1	         0.55	       1.0177	          1.1	          1.1	      0.99781	       1.0694	         1.65	      0.95764	       1.0822	          2.2	      0.91632	      0.99822	       2.4618	      0.88249	       1.0147	       2.7273	      0.84273	      0.90909	       3.6364	      0.81898	
    51	    91	     0.064574	       4.4084	       1e-012	       2.4852	      0.13525	     0.046849	     0.088324	          1.1	         0.55	        1.018	          1.1	          1.1	      0.99798	       1.0713	         1.65	      0.95759	       1.0825	          2.2	      0.91642	      0.99894	       2.4612	      0.88288	       1.0145	       2.7273	      0.84302	      0.90909	       3.6364	      0.81896	
    52	   119	     0.064574	       4.4084	  2.1879e-010	       2.4852	      0.13525	     0.046849	     0.088324	          1.1	         0.55	        1.018	          1.1	          1.1	      0.99798	       1.0713	         1.65	      0.95759	       1.0825	          2.2	      0.91642	      0.99894	       2.4612	      0.88288	       1.0145	       2.7273	      0.84302	      0.90909	       3.6364	      0.81896	
    53	   121	     0.064574	       4.4084	  2.2732e-010	       2.4852	      0.13525	     0.046849	     0.088324	          1.1	         0.55	        1.018	          1.1	          1.1	      0.99798	       1.0713	         1.65	      0.95759	       1.0825	          2.2	      0.91642	      0.99894	       2.4612	      0.88288	       1.0145	       2.7273	      0.84302	      0.90909	       3.6364	      0.81896	
    54	   123	     0.063963	       4.7952	  1.3907e-007	        2.306	      0.13416	     0.049182	      0.08668	          1.1	         0.55	       1.0196	          1.1	          1.1	      0.99992	       1.0706	         1.65	       0.9595	       1.0855	          2.2	      0.91795	       1.0042	       2.4518	      0.88403	       1.0226	       2.7273	      0.84396	      0.90909	       3.6364	      0.82038	
    55	   124	     0.063821	       5.1721	  2.5194e-007	       2.1517	     0.091745	    0.0087723	     0.043531	          1.1	         0.55	       1.0668	          1.1	          1.1	       1.0475	       1.0601	         1.65	       1.0067	       1.0855	          2.2	      0.96481	       1.0058	        2.443	      0.92721	       1.0278	       2.7273	      0.88763	      0.90909	       3.6364	      0.85956	
    56	   125	     0.063669	       5.2726	  2.6731e-007	       2.1243	     0.083015	     2.2e-007	      0.03442	          1.1	         0.55	       1.0766	          1.1	          1.1	       1.0575	       1.0483	         1.65	       1.0163	       1.0775	          2.2	      0.97505	      0.99871	       2.4373	      0.93676	       1.0239	       2.7273	      0.89747	      0.90909	       3.6364	      0.86784	
    57	   126	     0.063644	       5.2783	       1e-012	       2.1251	      0.08284	     2.2e-007	     0.034336	          1.1	         0.55	       1.0767	          1.1	          1.1	       1.0576	       1.0556	         1.65	       1.0162	        1.083	          2.2	      0.97489	       1.0034	       2.4371	      0.93661	       1.0265	       2.7273	      0.89736	      0.90909	       3.6364	       0.8679	
    58	   128	     0.063639	        5.318	       1e-012	       2.1024	     0.082766	   0.00036642	     0.034296	          1.1	         0.55	       1.0768	          1.1	          1.1	       1.0577	       1.0572	         1.65	       1.0162	       1.0852	          2.2	      0.97485	       1.0062	        2.439	      0.93654	       1.0294	       2.7273	      0.89729	      0.90909	       3.6364	      0.86794	
    59	   129	     0.063638	       5.3571	       1e-012	       2.0908	     0.091635	    0.0094082	     0.043096	          1.1	         0.55	       1.0671	          1.1	          1.1	        1.048	       1.0568	         1.65	       1.0069	       1.0856	          2.2	      0.96529	       1.0068	       2.4376	      0.92766	       1.0304	       2.7273	      0.88819	      0.90909	       3.6364	      0.85993	
    60	   130	     0.063638	       5.3538	       1e-012	       2.0931	     0.088794	    0.0065388	     0.040256	          1.1	         0.55	       1.0702	          1.1	          1.1	       1.0511	       1.0567	         1.65	       1.0099	       1.0856	          2.2	      0.96836	       1.0068	       2.4374	      0.93052	       1.0304	       2.7273	      0.89111	      0.90909	       3.6364	      0.86251	
    61	   132	     0.063638	       5.3537	  9.5473e-009	       2.0928	     0.087863	    0.0056067	     0.039324	          1.1	         0.55	       1.0712	          1.1	          1.1	       1.0522	       1.0566	         1.65	       1.0109	       1.0855	          2.2	      0.96938	       1.0067	       2.4375	      0.93146	       1.0302	       2.7273	      0.89208	      0.90909	       3.6364	      0.86336	
    62	   133	     0.063638	       5.3533	  8.9727e-009	       2.0931	     0.087952	    0.0056892	     0.039412	          1.1	         0.55	       1.0712	          1.1	          1.1	       1.0521	       1.0566	         1.65	       1.0108	       1.0855	          2.2	      0.96928	       1.0066	       2.4374	      0.93137	       1.0302	       2.7273	      0.89199	      0.90909	       3.6364	      0.86328	
    63	   134	     0.063638	       5.3532	  8.8096e-009	       2.0932	     0.087952	    0.0056883	     0.039411	          1.1	         0.55	       1.0712	          1.1	          1.1	       1.0521	       1.0566	         1.65	       1.0108	       1.0855	          2.2	      0.96928	       1.0067	       2.4374	      0.93137	       1.0302	       2.7273	      0.89199	      0.90909	       3.6364	      0.86328	
    64	   135	     0.063638	       5.3532	       1e-012	       2.0932	     0.087952	    0.0056883	     0.039411	          1.1	         0.55	       1.0712	          1.1	          1.1	       1.0521	       1.0566	         1.65	       1.0108	       1.0855	          2.2	      0.96928	       1.0067	       2.4374	      0.93137	       1.0302	       2.7273	      0.89199	      0.90909	       3.6364	      0.86328	
    65	   169	     0.063638	       5.3532	  1.1909e-012	       2.0932	     0.087952	    0.0056883	     0.039411	          1.1	         0.55	       1.0712	          1.1	          1.1	       1.0521	       1.0566	         1.65	       1.0108	       1.0855	          2.2	      0.96928	       1.0067	       2.4374	      0.93137	       1.0302	       2.7273	      0.89199	      0.90909	       3.6364	      0.86328	

:: False convergence

       Squares	      0.0636378
     Func Eval	            169
     Grad Eval	             66

:: Nonlinear parameters

     i	   Final p(i)	  Gradient(i)
     1	      5.35324	  5.1206e-009
     2	 1.19088e-012	       0.6746
     3	      2.09317	    3.64e-009
     4	    0.0879518	 -3.5192e-009
     5	    0.0056883	  -8.093e-007
     6	    0.0394114	 -1.6612e-006
     7	          1.1	   -0.0050011
     8	         0.55	   -0.0088914
     9	      1.07115	  2.7866e-007
    10	          1.1	  -0.00094898
    11	          1.1	  -0.00094899
    12	      1.05208	  1.4212e-007
    13	      1.05661	    8.29e-008
    14	         1.65	   -0.0025309
    15	      1.01083	  1.1001e-006
    16	      1.08551	 -3.6094e-008
    17	          2.2	  -0.00015056
    18	     0.969284	  -1.182e-006
    19	      1.00666	 -5.5953e-008
    20	      2.43741	  1.0568e-009
    21	     0.931372	 -1.3495e-006
    22	      1.03017	 -7.6507e-008
    23	      2.72727	   0.00078777
    24	     0.891989	 -1.5999e-006
    25	     0.909091	   0.00028155
    26	      3.63636	    0.0040101
    27	     0.863278	   2.094e-007
