
===================================================
Least squares Minimization - Trust Region Algorithm
===================================================

Optimized parameters:

     1 	 k.1
     2 	 k.-1
     3 	 k.cat
     4 	 r(E)
     5 	 r(E.S1)
     6 	 r(E.P)
     7 	 [E]#1
     8 	 [S]#1
     9 	 offset#1
    10 	 [E]#2
    11 	 [S]#2
    12 	 offset#2
    13 	 [E]#3
    14 	 [S]#3
    15 	 offset#3
    16 	 [E]#4
    17 	 [S]#4
    18 	 offset#4
    19 	 [E]#5
    20 	 [S]#5
    21 	 offset#5
    22 	 [E]#6
    23 	 [S]#6
    24 	 offset#6
    25 	 [E]#7
    26 	 [S]#7
    27 	 offset#7

:: NL2SOL :: DNLAFB
:: Analytic Jacobian; Constrained

________________________________________________________________________

:: Initial Estimate

     i	     Param p(i)	            Min	            Max
     1	         131890	         1e-006	         1e+006
     2	        3232.61	         1e-012	         1e+006
     3	        141.493	         1e-006	         1e+006
     4	           0.31	       3.1e-007	         310000
     5	           0.22	       2.2e-007	         220000
     6	           0.25	       2.5e-007	         250000
     7	              1	       0.909091	            1.1
     8	            0.5	       0.454545	           0.55
     9	         1.1793	       -10.6137	        12.9723
    10	              1	       0.909091	            1.1
    11	              1	       0.909091	            1.1
    12	        1.14325	       -10.6497	        12.9363
    13	              1	       0.909091	            1.1
    14	            1.5	        1.36364	           1.65
    15	        1.10351	       -10.6895	        12.8965
    16	              1	       0.909091	            1.1
    17	              2	        1.81818	            2.2
    18	        1.07103	        -10.722	         12.864
    19	              1	       0.909091	            1.1
    20	            2.5	        2.27273	           2.75
    21	        1.02909	       -10.7639	        12.8221
    22	              1	       0.909091	            1.1
    23	              3	        2.72727	            3.3
    24	        0.97752	       -10.8155	        12.7705
    25	              1	       0.909091	            1.1
    26	              4	        3.63636	            4.4
    27	        0.95791	       -10.8351	        12.7509

:: Iterations

     i	 FEval	   F(i) = SSQ	   Param( 1)	   Param( 2)	   Param( 3)	   Param( 4)	   Param( 5)	   Param( 6)	   Param( 7)	   Param( 8)	   Param( 9)	   Param(10)	   Param(11)	   Param(12)	   Param(13)	   Param(14)	   Param(15)	   Param(16)	   Param(17)	   Param(18)	   Param(19)	   Param(20)	   Param(21)	   Param(22)	   Param(23)	   Param(24)	   Param(25)	   Param(26)	   Param(27)	
     0	     1	       348.03	  1.3189e+005	       3232.6	       141.49	         0.31	         0.22	         0.25	            1	          0.5	       1.1793	            1	            1	       1.1433	            1	          1.5	       1.1035	            1	            2	        1.071	            1	          2.5	       1.0291	            1	            3	      0.97752	            1	            4	      0.95791	
     1	     2	       16.956	  1.3189e+005	       3232.6	       140.78	     3.1e-007	      0.30266	     2.5e-007	          1.1	      0.45455	       1.0774	          1.1	          1.1	       1.0581	      0.90909	       1.6302	       1.1327	      0.95233	          2.2	       1.0781	       0.9486	       2.5896	       1.0374	       1.0142	       3.0438	      0.98195	      0.97888	        4.009	      0.95635	
     2	     3	       2.2055	  1.3189e+005	       3232.6	       141.44	      0.24624	      0.26949	     2.5e-007	      0.93916	      0.47891	      0.98165	      0.90909	      0.90915	         1.09	      0.90909	       1.3658	       1.0528	       1.0217	       1.8182	       1.0104	      0.99903	       2.4169	      0.96906	       1.0345	       2.9365	      0.93004	      0.98245	       4.1398	      0.89871	
     3	     4	      0.79764	  1.3189e+005	       3232.6	       141.97	      0.15818	      0.22952	  8.4761e-005	      0.90909	      0.54824	       1.0919	      0.96059	      0.90915	       1.0947	      0.90909	       1.3658	       1.0526	       1.0469	       1.8182	         1.01	       1.0197	       2.4169	      0.96843	       1.0298	       2.8498	      0.92943	      0.98245	       4.1101	       0.8951	
     4	     5	      0.65187	  1.3189e+005	       3232.6	        142.5	     0.086044	      0.21935	    0.0023996	      0.90909	         0.55	       1.1089	      0.95398	      0.90909	       1.0949	      0.90909	       1.3636	       1.0503	       1.0339	       1.8182	       1.0074	      0.98953	       2.4158	      0.96592	      0.97621	       2.8494	       0.9269	      0.98234	       4.0694	      0.89246	
     5	     6	      0.59054	  1.3189e+005	       3232.6	       143.02	     0.066767	      0.20933	     0.003058	      0.90909	         0.55	        1.115	      0.95398	      0.90909	       1.0948	      0.90909	       1.3636	       1.0497	       1.0249	       1.8182	       1.0067	      0.97354	       2.4158	      0.96535	      0.94509	       2.8494	      0.92643	      0.98234	       4.0073	       0.8918	
     6	     7	      0.52916	  1.3189e+005	       3232.6	        143.3	      0.06472	      0.19506	    0.0035135	      0.90909	         0.55	       1.1157	      0.95398	      0.90909	       1.0945	      0.90909	       1.3636	       1.0493	       1.0201	       1.8182	       1.0063	      0.96297	       2.4158	      0.96496	      0.92452	       2.8494	      0.92609	      0.98234	       3.9363	      0.89135	
     7	     8	      0.48439	  1.3189e+005	       3232.6	       143.31	     0.069147	      0.18764	       0.0124	      0.90909	         0.55	       1.1094	      0.95398	      0.96612	       1.0895	      0.90909	       1.3636	       1.0414	       1.0709	       1.8182	      0.99703	      0.96454	       2.3595	      0.95641	      0.92452	         2.82	      0.91786	      0.98234	       3.9248	      0.88261	
     8	     9	      0.45818	  1.3189e+005	       3232.6	       143.31	     0.062627	      0.17818	     0.014392	      0.90909	         0.55	       1.1109	       1.0042	      0.96612	       1.0872	      0.90909	       1.3636	       1.0393	       1.0773	       1.8182	      0.99424	      0.97116	       2.3561	      0.95417	      0.92661	       2.8184	      0.91579	      0.98205	       3.9242	      0.88046	
     9	    10	      0.42574	  1.3189e+005	       3232.6	       143.31	     0.085013	      0.16847	      0.01511	      0.91333	         0.55	       1.1021	      0.95507	      0.96612	       1.0869	      0.90909	       1.3636	       1.0386	       1.0638	       1.8182	       0.9936	      0.96601	       2.3511	      0.95357	      0.91925	       2.8161	      0.91527	      0.98165	       3.9234	      0.87981	
    10	    11	      0.39143	  1.3189e+005	       3232.6	        143.3	      0.08054	       0.1499	     0.016271	      0.90909	         0.55	       1.1038	      0.98595	      0.96612	       1.0855	      0.90909	       1.3636	       1.0375	       1.0641	       1.8182	      0.99235	      0.96773	       2.3494	      0.95243	      0.91997	       2.8153	      0.91419	      0.97711	       3.9231	      0.87874	
    11	    12	      0.37201	  1.3189e+005	       3232.6	        143.3	     0.084853	      0.13701	     0.017484	      0.91778	         0.55	       1.1009	      0.98595	      0.99776	       1.0851	      0.90909	       1.3636	       1.0364	       1.0642	       1.8182	      0.99105	      0.96875	       2.3481	      0.95124	      0.91997	       2.8147	      0.91308	      0.97255	       3.9228	      0.87763	
    12	    14	      0.36504	  1.3189e+005	       3232.6	        143.3	     0.084212	      0.13178	      0.01778	      0.91775	         0.55	       1.1011	      0.98848	      0.99216	       1.0842	      0.90909	       1.3636	        1.036	       1.0636	       1.8182	      0.99062	      0.96829	       2.3481	      0.95085	      0.91946	       2.8147	      0.91273	      0.97164	       3.9228	      0.87739	
    13	    15	      0.36229	  1.3189e+005	       3232.6	        143.3	     0.084282	       0.1266	     0.018026	      0.91758	         0.55	        1.101	      0.99391	      0.98298	       1.0837	      0.90909	       1.3636	       1.0359	       1.0632	       1.8182	       0.9904	      0.96796	        2.348	      0.95065	      0.91908	       2.8147	      0.91254	      0.97083	       3.9228	       0.8772	
    14	    16	      0.35748	  1.3189e+005	       3232.6	        143.3	      0.08445	      0.12163	     0.018228	      0.91772	         0.55	       1.1009	      0.99275	      0.99233	       1.0835	      0.90909	       1.3636	       1.0357	       1.0627	       1.8182	      0.99024	      0.96773	        2.348	       0.9505	      0.91879	       2.8147	      0.91239	      0.96998	       3.9228	      0.87702	
    15	    18	      0.35652	  1.3189e+005	       3232.6	        143.3	     0.085009	      0.11928	     0.018452	      0.91759	         0.55	       1.1006	      0.98908	      0.99609	        1.083	      0.90944	       1.3636	       1.0356	       1.0625	       1.8182	      0.99014	      0.96758	        2.348	      0.95041	      0.91861	       2.8147	      0.91231	      0.96959	       3.9228	      0.87694	
    16	    19	      0.35418	  1.3189e+005	       3232.6	        143.3	     0.085475	      0.11252	     0.019261	      0.91744	         0.55	       1.1001	      0.99414	      0.99043	       1.0814	      0.90982	       1.3636	        1.035	       1.0623	       1.8182	      0.98938	      0.96746	       2.3479	      0.94972	      0.91846	       2.8147	      0.91165	      0.96935	       3.9228	      0.87617	
    17	    20	      0.35348	  1.3189e+005	       3232.6	        143.3	     0.092616	      0.10913	     0.018523	      0.91625	         0.55	        1.098	      0.98955	         0.99	       1.0822	      0.91453	       1.3636	       1.0356	       1.0617	       1.8182	      0.99019	      0.96762	       2.3478	      0.95044	      0.91834	       2.8146	      0.91234	      0.96845	       3.9228	      0.87691	
    18	    24	      0.35345	  1.3189e+005	       3232.6	        143.3	     0.092697	      0.10905	     0.018528	      0.91625	         0.55	        1.098	      0.98981	      0.98972	       1.0822	      0.91455	       1.3636	       1.0356	       1.0617	       1.8182	      0.99019	      0.96762	       2.3478	      0.95044	      0.91834	       2.8146	      0.91235	      0.96844	       3.9228	      0.87691	
    19	    25	      0.35344	  1.3189e+005	       3232.6	        143.3	     0.092987	      0.10883	     0.018507	      0.91622	         0.55	       1.0979	      0.98966	      0.98976	       1.0822	      0.91464	       1.3636	       1.0356	       1.0617	       1.8182	      0.99021	      0.96762	       2.3478	      0.95046	      0.91833	       2.8146	      0.91237	      0.96842	       3.9228	      0.87693	
    20	    30	      0.35232	  1.3189e+005	       3232.6	       143.28	      0.11193	      0.10046	     0.012266	      0.90909	         0.55	        1.095	      0.93169	       0.9317	       1.0893	      0.97111	       1.3636	       1.0401	       1.0516	       1.8182	      0.99702	      0.96987	       2.3433	      0.95644	      0.91774	       2.8125	       0.9181	      0.95784	        3.922	      0.88318	
    21	    31	      0.35208	  1.3189e+005	       3232.6	       143.16	      0.11953	     0.084522	     2.5e-007	      0.91105	         0.55	        1.099	      0.93168	       0.9317	       1.1002	       1.0305	       1.3636	       1.0517	       1.0728	       1.8182	       1.0096	       1.0047	       2.3132	      0.96795	      0.95078	       2.7979	      0.92897	      0.98963	       3.9161	       0.8945	
    22	    32	       0.3515	  1.3189e+005	       3232.6	       143.16	      0.11839	     0.083689	     2.5e-007	      0.91105	         0.55	       1.0993	      0.93169	       0.9317	       1.1004	       1.0497	       1.3636	       1.0526	       1.0799	       1.8182	         1.01	       1.0147	       2.3119	      0.96853	      0.96008	       2.7973	      0.92953	      0.99812	       3.9158	      0.89505	
    23	    35	      0.35149	  1.3189e+005	       3232.6	       143.16	      0.11889	     0.083851	     2.5e-007	      0.91097	         0.55	       1.0992	       0.9319	      0.93132	       1.1004	       1.0498	       1.3636	       1.0524	       1.0801	       1.8182	       1.0099	       1.0148	       2.3119	      0.96839	       0.9601	       2.7973	      0.92939	      0.99814	       3.9158	       0.8949	
    24	    37	      0.35145	  1.3189e+005	       3232.6	       143.16	      0.11939	     0.083951	     2.5e-007	       0.9109	         0.55	        1.099	      0.93139	      0.93165	       1.1005	       1.0499	       1.3636	       1.0523	       1.0801	       1.8182	       1.0099	       1.0148	       2.3119	      0.96833	      0.96012	       2.7973	      0.92933	      0.99816	       3.9158	      0.89483	
    25	    38	      0.35141	  1.3189e+005	       3232.6	       143.16	      0.12005	     0.083926	     2.5e-007	      0.91079	         0.55	       1.0988	      0.93143	      0.93135	       1.1004	         1.05	       1.3636	       1.0524	       1.0801	       1.8182	       1.0099	       1.0148	       2.3119	      0.96838	      0.96015	       2.7973	      0.92938	      0.99818	       3.9158	       0.8949	
    26	    39	       0.3514	  1.3189e+005	       3232.6	       143.16	      0.12072	     0.083889	     2.5e-007	      0.91069	         0.55	       1.0986	      0.93122	      0.93129	       1.1005	       1.0501	       1.3636	       1.0524	       1.0801	       1.8182	       1.0099	       1.0148	       2.3119	      0.96839	      0.96018	       2.7972	      0.92939	       0.9982	       3.9158	       0.8949	
    27	    44	      0.35031	  1.3189e+005	       3232.6	       143.12	      0.19538	     0.075703	     2.5e-007	      0.90909	         0.55	       1.0718	      0.90909	      0.90909	       1.1002	       1.0821	       1.3636	       1.0524	          1.1	       1.8182	         1.01	       1.0757	       2.3026	      0.96843	       1.0221	       2.7927	      0.92943	       1.0638	       3.9139	      0.89495	
    28	    45	      0.35024	  1.3189e+005	       3232.6	       143.09	      0.19824	     0.075623	     2.5e-007	      0.90909	         0.55	       1.0709	      0.90909	      0.90909	       1.1002	       1.0821	       1.3636	       1.0523	          1.1	       1.8182	       1.0099	       1.0757	       2.2964	       0.9684	       1.0452	       2.7897	      0.92938	       1.0638	       3.9121	      0.89492	
    29	    46	      0.35022	  1.3189e+005	       3232.6	       143.09	      0.19888	     0.075382	     2.5e-007	      0.90909	         0.55	       1.0707	      0.90909	      0.90909	       1.1002	       1.0849	       1.3636	       1.0523	          1.1	       1.8182	       1.0099	       1.0803	       2.2959	       0.9684	       1.0475	       2.7894	      0.92939	       1.0692	       3.9119	      0.89492	
    30	    49	      0.35009	  1.3189e+005	       3232.6	       143.05	      0.20479	     0.073526	     2.5e-007	      0.90909	         0.55	       1.0686	      0.90909	      0.90909	       1.1002	          1.1	       1.3636	       1.0523	          1.1	       1.8182	         1.01	          1.1	       2.2901	      0.96839	       1.0786	        2.785	      0.92938	        1.092	       3.9101	      0.89492	
    31	    51	      0.35009	  1.3189e+005	       3232.6	       143.04	      0.20546	     0.078282	    0.0043998	      0.90909	         0.55	       1.0659	      0.90909	      0.90909	       1.0962	          1.1	       1.3636	       1.0475	          1.1	       1.8182	       1.0051	          1.1	       2.2902	      0.96354	       1.0781	       2.7853	      0.92461	       1.0962	       3.9105	      0.89009	
    32	    52	      0.35008	  1.3189e+005	       3232.6	       143.04	      0.20583	      0.07823	     0.004387	      0.90909	         0.55	       1.0658	      0.90909	      0.90909	       1.0962	          1.1	       1.3636	       1.0475	          1.1	       1.8182	       1.0051	          1.1	       2.2892	      0.96357	        1.079	       2.7849	      0.92464	       1.0962	       3.9102	      0.89011	
    33	    58	      0.34485	  1.3189e+005	       3232.7	       134.15	      0.31801	     0.061352	     2.5e-007	      0.90909	         0.55	       1.0278	      0.90909	      0.90909	       1.1001	          1.1	       1.3636	       1.0521	          1.1	       1.8182	       1.0097	          1.1	       2.2727	      0.96819	          1.1	       2.7273	      0.92906	       1.0968	       3.6364	      0.89473	
    34	    60	      0.32191	  1.3189e+005	       3232.8	       106.63	      0.32572	     0.058719	     2.5e-007	      0.90909	         0.55	       1.0249	      0.90909	      0.92388	       1.1002	          1.1	       1.3636	       1.0524	          1.1	       1.8182	       1.0119	          1.1	       2.2727	      0.96791	          1.1	       2.7273	      0.92887	       1.0968	       3.6364	      0.89443	
    35	    61	      0.30598	  1.3189e+005	         3234	       67.634	       0.1249	     0.052802	     2.5e-007	      0.90909	         0.55	       1.0966	        1.009	      0.92388	       1.0974	          1.1	       1.3636	       1.0515	          1.1	       1.8182	       1.0091	          1.1	       2.2727	      0.96749	          1.1	       2.7273	      0.92844	      0.99732	       3.6364	       0.8941	
    36	    68	      0.26395	  1.3189e+005	         3234	       67.634	      0.12405	     0.072862	     2.5e-007	      0.93066	         0.55	       1.0965	       1.0197	      0.91399	       1.0892	          1.1	       1.3636	       1.0535	          1.1	       1.8182	       1.0091	          1.1	       2.2727	      0.96764	          1.1	       2.7273	       0.9286	      0.99846	       3.6364	       0.8944	
    37	    69	      0.25421	  1.3189e+005	         3234	       67.633	      0.12306	     0.073352	     2.5e-007	      0.92801	         0.55	       1.0951	      0.99587	      0.93703	       1.0925	          1.1	       1.3636	       1.0514	          1.1	       1.8182	        1.009	       1.0975	       2.2727	      0.96746	       1.0947	       2.7273	      0.92842	      0.99939	       3.6364	      0.89414	
    38	    71	      0.25042	  1.3189e+005	         3234	       67.631	      0.12421	     0.072844	     2.5e-007	      0.92647	         0.55	       1.0949	        0.971	      0.95959	        1.098	          1.1	       1.3636	       1.0514	          1.1	       1.8182	        1.009	       1.0963	       2.2727	      0.96747	       1.0918	       2.7273	      0.92844	       1.0002	       3.6364	      0.89415	
    39	    73	      0.24926	  1.3189e+005	         3234	       67.631	      0.12538	     0.072651	     2.5e-007	      0.92624	         0.55	       1.0945	      0.96442	      0.96584	       1.0994	          1.1	       1.3636	       1.0514	          1.1	       1.8182	        1.009	       1.0962	       2.2727	      0.96748	       1.0915	       2.7273	      0.92844	       1.0003	       3.6364	      0.89416	
    40	    75	      0.24921	  1.3189e+005	         3234	       67.631	      0.12581	     0.072618	     2.5e-007	      0.92617	         0.55	       1.0943	      0.96565	      0.96453	       1.0992	          1.1	       1.3636	       1.0514	          1.1	       1.8182	        1.009	       1.0962	       2.2727	      0.96748	       1.0914	       2.7273	      0.92844	       1.0003	       3.6364	      0.89416	
    41	    77	      0.24915	  1.3189e+005	         3234	       67.631	      0.12605	     0.072609	     2.5e-007	      0.92613	         0.55	       1.0943	        0.965	      0.96512	       1.0993	          1.1	       1.3636	       1.0514	          1.1	       1.8182	        1.009	       1.0962	       2.2727	      0.96748	       1.0914	       2.7273	      0.92844	       1.0003	       3.6364	      0.89416	
    42	    78	      0.24913	  1.3189e+005	         3234	       67.631	      0.12685	     0.072557	     2.5e-007	      0.92601	         0.55	        1.094	      0.96513	      0.96484	       1.0992	          1.1	       1.3636	       1.0514	          1.1	       1.8182	        1.009	       1.0961	       2.2727	      0.96748	       1.0912	       2.7273	      0.92844	       1.0004	       3.6364	      0.89416	
    43	    82	      0.24838	  1.3189e+005	         3234	        67.63	      0.17967	     0.068253	     2.5e-007	      0.92115	         0.55	       1.0744	      0.95876	      0.95927	       1.0991	          1.1	       1.3636	       1.0511	          1.1	       1.8182	       1.0091	       1.0963	       2.2727	       0.9675	       1.0895	       2.7273	      0.92848	       1.0036	       3.6364	      0.89417	
    44	    85	      0.24817	  1.3189e+005	         3234	        67.63	      0.17984	     0.068515	     2.5e-007	      0.92132	         0.55	       1.0746	      0.95906	      0.95888	       1.0991	          1.1	       1.3636	       1.0513	          1.1	       1.8182	       1.0091	       1.0963	       2.2727	      0.96754	       1.0895	       2.7273	      0.92851	       1.0036	       3.6364	      0.89422	
    45	    86	      0.24815	  1.3189e+005	         3234	        67.63	      0.18002	     0.068564	     2.5e-007	      0.92139	         0.55	       1.0747	      0.95852	      0.95931	       1.0991	          1.1	       1.3636	       1.0513	          1.1	       1.8182	       1.0091	       1.0963	       2.2727	      0.96754	       1.0894	       2.7273	      0.92852	       1.0036	       3.6364	      0.89422	
    46	    87	      0.24807	  1.3189e+005	         3234	        67.63	      0.18024	     0.069186	     2.5e-007	      0.92127	         0.55	       1.0747	      0.95855	      0.95892	       1.0989	          1.1	       1.3636	       1.0513	          1.1	       1.8182	       1.0091	       1.0964	       2.2727	      0.96753	       1.0894	       2.7273	      0.92851	       1.0038	       3.6364	      0.89422	
    47	    89	      0.24805	  1.3189e+005	         3234	        67.63	      0.18027	     0.069253	     2.5e-007	      0.92126	         0.55	       1.0747	      0.95874	      0.95868	       1.0989	          1.1	       1.3636	       1.0513	          1.1	       1.8182	       1.0091	       1.0964	       2.2727	      0.96753	       1.0894	       2.7273	      0.92851	       1.0038	       3.6364	      0.89422	
    48	    90	      0.24805	  1.3189e+005	         3234	       67.629	      0.18039	      0.06937	     2.5e-007	      0.92117	         0.55	       1.0747	      0.95852	      0.95862	       1.0989	          1.1	       1.3636	       1.0513	          1.1	       1.8182	       1.0091	       1.0964	       2.2727	      0.96753	       1.0893	       2.7273	      0.92851	       1.0039	       3.6364	      0.89422	
    49	    95	       0.2472	  1.3189e+005	         3234	       67.612	       0.2015	     0.068634	     2.5e-007	      0.91405	         0.55	       1.0682	      0.90909	      0.90912	       1.0988	          1.1	       1.3636	       1.0513	          1.1	       1.8182	       1.0091	       1.0984	       2.2727	      0.96753	       1.0794	       2.7273	      0.92853	       1.0266	       3.6364	      0.89419	
    50	    96	      0.24693	  1.3189e+005	         3234	       67.538	      0.21058	     0.067892	     2.5e-007	      0.91405	         0.55	        1.065	      0.90911	      0.90912	       1.0988	          1.1	       1.3636	       1.0513	          1.1	       1.8182	       1.0091	       1.0984	       2.2727	      0.96755	       1.0689	       2.7273	      0.92857	       1.0639	       3.6364	      0.89414	
    51	   103	      0.19459	  1.3189e+005	         3234	       45.529	      0.19856	     0.059098	     2.5e-007	      0.90909	         0.55	       1.0705	      0.90909	      0.90909	       1.0979	          1.1	       1.3636	        1.051	          1.1	       1.8182	       1.0087	       1.0984	       2.2727	      0.96724	       1.0516	       2.7273	      0.92831	          1.1	       3.6364	      0.89381	
    52	   105	      0.18178	  1.3189e+005	         3234	       40.976	      0.19679	     0.060601	     2.5e-007	      0.90909	         0.55	       1.0707	       0.9091	      0.90909	       1.0977	          1.1	       1.3636	       1.0505	          1.1	       1.8182	       1.0084	       1.0918	       2.2727	      0.96691	       1.0419	       2.7273	      0.92801	          1.1	       3.6364	      0.89345	
    53	   107	      0.15967	  1.3189e+005	         3234	       32.158	      0.17286	     0.060711	     2.5e-007	      0.90909	         0.55	       1.0789	      0.90909	      0.90909	       1.0973	          1.1	       1.3636	         1.05	          1.1	       1.8182	       1.0078	       1.0462	       2.2727	      0.96649	      0.99405	       2.7273	      0.92763	       1.0139	       3.6364	       0.8931	
    54	   109	      0.14695	  1.3189e+005	         3234	       25.157	      0.14616	     0.057591	     2.5e-007	      0.90909	         0.55	       1.0882	      0.90909	      0.90909	        1.097	          1.1	       1.3636	       1.0494	          1.1	       1.8182	       1.0073	       1.0013	       2.2727	      0.96615	      0.94817	       2.7273	      0.92731	      0.96014	       3.6364	      0.89281	
    55	   110	      0.14464	  1.3189e+005	       3234.3	       20.846	      0.13354	     0.053062	     2.5e-007	      0.90909	         0.55	       1.0926	      0.90909	      0.90909	       1.0966	       1.0925	       1.3636	        1.049	       1.0737	       1.8182	        1.007	      0.97151	       2.2727	      0.96587	      0.94817	       2.7273	      0.92694	      0.96014	       3.6364	      0.89247	
    56	   111	      0.14446	  1.3189e+005	       3234.6	       21.707	      0.13563	     0.053763	     2.5e-007	      0.90909	         0.55	       1.0918	      0.90909	      0.90909	       1.0966	       1.0887	       1.3636	       1.0491	       1.0765	       1.9044	        1.007	      0.96697	       2.2727	      0.96591	      0.94817	       2.7273	      0.92696	      0.96014	       3.6364	      0.89249	
    57	   112	      0.14424	  1.3189e+005	       3234.9	       21.619	       0.1286	     0.055315	     2.5e-007	      0.90909	         0.55	       1.0943	      0.90909	      0.90909	       1.0966	       1.0755	       1.3636	       1.0491	       1.0322	       1.8182	        1.007	      0.97499	         2.75	      0.96593	      0.92779	          3.3	      0.92699	      0.93425	       3.8895	      0.89249	
    58	   113	      0.14403	  1.3189e+005	       3237.8	       21.535	      0.11756	     0.057796	     2.5e-007	      0.90909	         0.55	       1.0982	      0.90909	      0.90909	       1.0967	       1.0532	       1.3636	       1.0491	       1.0102	       1.8182	        1.007	      0.91062	         2.75	      0.96591	      0.90909	          3.3	      0.92702	      0.90909	          4.4	      0.89249	
    59	   114	      0.14396	  1.3189e+005	       3244.9	       21.553	      0.12323	     0.056487	     2.5e-007	      0.90909	         0.55	       1.0962	      0.90909	      0.90909	       1.0966	       1.0618	       1.3636	       1.0491	       1.0324	       1.9054	        1.007	      0.92956	         2.75	      0.96591	      0.90909	          3.3	      0.92695	      0.90909	          4.4	      0.89251	
    60	   119	      0.14351	  1.3188e+005	       4122.7	       21.826	       0.1132	     0.056626	     2.5e-007	      0.90909	         0.55	       1.0998	      0.90909	      0.90909	       1.0966	       1.0544	       1.3636	       1.0491	         1.03	       1.9234	        1.007	      0.92968	         2.75	      0.96591	      0.90909	          3.3	      0.92695	      0.90909	          4.4	      0.89252	
    61	   120	       0.1425	  1.3062e+005	       7224.9	       22.708	     0.098356	     0.056235	     2.5e-007	      0.90909	         0.55	        1.105	      0.90909	      0.90909	       1.0963	       1.0398	       1.3636	        1.049	       1.0291	       1.9234	        1.007	      0.93962	         2.75	      0.96594	      0.90909	          3.3	      0.92701	      0.90909	          4.4	      0.89259	
    62	   121	      0.14179	  1.2983e+005	       9992.2	       23.295	     0.094319	     0.056476	     2.5e-007	      0.90909	         0.55	       1.1064	      0.90909	      0.90909	       1.0962	       1.0323	       1.3636	        1.049	       1.0233	       1.9234	        1.007	      0.93861	         2.75	      0.96595	      0.90909	          3.3	      0.92702	      0.90909	          4.4	      0.89262	
    63	   122	      0.14105	  1.2927e+005	        16437	       24.716	     0.081508	     0.057399	   0.00010613	      0.90909	         0.55	       1.1108	      0.90909	      0.90909	        1.096	       1.0154	       1.3636	       1.0488	       1.0058	       1.9234	        1.007	      0.93024	         2.75	      0.96592	      0.90909	          3.3	        0.927	      0.90909	          4.4	      0.89262	
    64	   123	       0.1405	   1.285e+005	        22859	        25.75	     0.076606	     0.058393	   0.00012426	      0.90909	         0.55	       1.1125	      0.90909	      0.90909	       1.0958	       1.0069	       1.3636	       1.0488	       1.0024	       1.9234	       1.0069	      0.93116	         2.75	       0.9659	      0.90909	          3.3	        0.927	      0.90909	          4.4	      0.89262	
    65	   124	      0.14024	  1.2742e+005	        29237	       26.893	     0.073854	     0.059152	   0.00016816	      0.90909	         0.55	       1.1135	      0.90909	      0.90909	       1.0957	      0.99913	       1.3636	       1.0487	      0.99641	       1.9234	       1.0069	      0.92911	         2.75	      0.96588	      0.90909	          3.3	      0.92698	      0.90909	          4.4	      0.89262	
    66	   129	      0.14023	  1.2741e+005	        29239	       26.979	     0.084455	     0.068428	     0.009477	      0.90909	         0.55	       1.1045	      0.90909	      0.90909	       1.0872	      0.99831	       1.3636	       1.0394	      0.99004	       1.8516	      0.99762	      0.93005	         2.75	      0.95724	      0.90909	          3.3	      0.91853	      0.90909	          4.4	      0.88416	
    67	   130	      0.14023	  1.2741e+005	        29241	       26.959	     0.083284	     0.067299	    0.0083413	      0.90909	         0.55	       1.1056	      0.90909	      0.90909	       1.0883	       0.9981	       1.3636	       1.0406	      0.98988	       1.8658	      0.99881	      0.92992	         2.75	      0.95828	      0.90909	          3.3	      0.91956	      0.90909	          4.4	      0.88519	
    68	   131	      0.14023	  1.2741e+005	        29243	        26.95	     0.080576	     0.064564	    0.0056371	      0.90909	         0.55	        1.108	      0.90909	      0.90909	       1.0907	      0.99836	       1.3636	       1.0433	      0.99229	       1.8797	       1.0015	      0.93005	         2.75	       0.9608	      0.90909	          3.3	      0.92202	      0.90909	          4.4	      0.88765	
    69	   132	      0.14023	  1.2741e+005	        29245	       26.955	     0.079709	     0.063698	    0.0047667	      0.90909	         0.55	       1.1088	      0.90909	      0.90909	       1.0915	      0.99836	       1.3636	       1.0442	      0.99174	       1.8738	       1.0023	      0.93006	         2.75	      0.96161	      0.90909	          3.3	      0.92281	      0.90909	          4.4	      0.88844	
    70	   136	      0.14023	  1.2739e+005	        29371	       26.974	      0.31217	      0.29626	      0.23731	      0.90909	         0.55	      0.89745	      0.90909	      0.90909	       0.8801	      0.99824	       1.3636	      0.81199	      0.99188	       1.8765	      0.77171	      0.92999	         2.75	      0.74533	      0.90909	          3.3	       0.7114	      0.90909	          4.4	      0.67704	
    71	   140	       0.1402	  1.2712e+005	        30548	        27.17	       0.3279	      0.31284	      0.25375	      0.90909	         0.55	      0.88275	      0.90909	      0.90909	      0.86514	       0.9971	       1.3636	      0.79588	      0.99079	       1.8763	      0.75564	      0.92942	         2.75	       0.7302	      0.90909	          3.3	      0.69647	      0.90909	          4.4	       0.6621	
    72	   141	      0.14015	  1.2605e+005	        35250	       27.953	      0.35936	      0.34746	       0.2878	      0.90909	         0.55	      0.85269	      0.90909	      0.90909	      0.83413	      0.99255	       1.3636	      0.76308	      0.98711	       1.8811	      0.72285	      0.92725	         2.75	      0.69912	      0.90909	          3.3	      0.66552	      0.90909	          4.4	      0.63116	
    73	   142	      0.14011	  1.2528e+005	        37586	       28.465	      0.35319	      0.34242	      0.28247	      0.90909	         0.55	      0.85782	      0.90909	      0.90909	      0.83894	      0.99255	       1.3636	      0.76851	      0.98543	       1.8811	      0.72872	      0.92702	         2.75	      0.70422	      0.90909	          3.3	      0.67038	      0.90909	          4.4	      0.63603	
    74	   143	       0.1401	  1.2498e+005	        38163	       28.507	        0.368	      0.35719	      0.29719	      0.90909	         0.55	       0.8444	      0.90909	      0.90909	      0.82554	       0.9888	       1.3636	      0.75497	      0.98471	       1.8811	      0.71441	      0.92641	         2.75	      0.69074	      0.90909	          3.3	      0.65699	      0.90909	          4.4	      0.62264	
    75	   144	       0.1401	  1.2471e+005	        38859	       28.637	      0.34243	      0.33198	      0.27189	      0.90909	         0.55	       0.8675	      0.90909	      0.90909	      0.84854	       0.9888	       1.3636	      0.78004	      0.98419	       1.8811	      0.73949	      0.92631	         2.75	      0.71422	      0.90909	          3.3	         0.68	      0.90909	          4.4	      0.64565	
    76	   147	       0.1401	   1.247e+005	        38905	       28.612	      0.35602	      0.34547	       0.2854	      0.90909	         0.55	      0.85519	      0.90909	      0.90909	      0.83626	      0.98787	       1.3636	      0.76695	      0.98521	       1.8909	      0.72591	      0.92627	         2.75	      0.70171	      0.90909	          3.3	      0.66772	      0.90909	          4.4	      0.63337	
    77	   148	       0.1401	  1.2469e+005	        38949	       28.632	      0.35009	      0.33957	      0.27947	      0.90909	         0.55	      0.86057	      0.90909	      0.90909	      0.84164	       0.9879	       1.3636	       0.7728	      0.98475	       1.8861	      0.73186	      0.92627	         2.75	       0.7072	      0.90909	          3.3	       0.6731	      0.90909	          4.4	      0.63875	
    78	   151	       0.1401	   1.245e+005	        39644	       28.739	      0.29063	      0.28047	       0.2203	      0.90909	         0.55	      0.91447	      0.90909	      0.90909	      0.89543	      0.98739	       1.3636	       0.8314	      0.98457	       1.8894	      0.79018	      0.92598	         2.75	      0.76209	      0.90909	          3.3	       0.7269	      0.90909	          4.4	      0.69255	
    79	   154	       0.1401	   1.245e+005	        39644	       28.739	      0.28913	      0.27882	      0.21866	      0.90909	         0.55	      0.91591	      0.90909	      0.90909	      0.89692	      0.98542	       1.3636	      0.83342	      0.98624	        1.896	      0.79142	      0.92585	         2.75	      0.76362	      0.90909	          3.3	      0.72839	      0.90909	          4.4	      0.69404	
    80	   155	       0.1401	   1.245e+005	        39644	       28.743	      0.28902	      0.27884	      0.21867	      0.90909	         0.55	      0.91594	      0.90909	      0.90909	      0.89691	      0.98731	       1.3636	        0.833	      0.98447	       1.8882	       0.7918	      0.92601	         2.75	      0.76358	      0.90909	          3.3	      0.72838	      0.90909	          4.4	      0.69403	
    81	   156	       0.1401	   1.245e+005	        39644	       28.747	      0.28891	      0.27871	      0.21854	      0.90909	         0.55	      0.91605	      0.90909	      0.90909	      0.89703	      0.98723	       1.3636	      0.83314	       0.9845	       1.8889	      0.79192	      0.92602	         2.75	       0.7637	      0.90909	          3.3	       0.7285	      0.90909	          4.4	      0.69415	
    82	   157	       0.1401	   1.245e+005	        39644	       28.746	      0.28893	      0.27873	      0.21856	      0.90909	         0.55	      0.91604	      0.90909	      0.90909	      0.89701	      0.98722	       1.3636	      0.83313	      0.98449	       1.8887	       0.7919	      0.92601	         2.75	      0.76368	      0.90909	          3.3	      0.72848	      0.90909	          4.4	      0.69413	
    83	   159	       0.1401	   1.245e+005	        39644	       28.746	      0.28893	      0.27873	      0.21856	      0.90909	         0.55	      0.91604	      0.90909	      0.90909	      0.89701	      0.98723	       1.3636	      0.83313	      0.98448	       1.8887	       0.7919	      0.92602	         2.75	      0.76368	      0.90909	          3.3	      0.72848	      0.90909	          4.4	      0.69414	
    84	   163	       0.1401	   1.245e+005	        39644	       28.746	      0.28893	      0.27873	      0.21856	      0.90909	         0.55	      0.91604	      0.90909	      0.90909	      0.89701	      0.98723	       1.3636	      0.83313	      0.98448	       1.8887	       0.7919	      0.92602	         2.75	      0.76368	      0.90909	          3.3	      0.72848	      0.90909	          4.4	      0.69413	
    85	   167	       0.1401	   1.245e+005	        39644	       28.746	      0.28893	      0.27873	      0.21856	      0.90909	         0.55	      0.91604	      0.90909	      0.90909	      0.89701	      0.98723	       1.3636	      0.83313	      0.98448	       1.8887	       0.7919	      0.92602	         2.75	      0.76368	      0.90909	          3.3	      0.72848	      0.90909	          4.4	      0.69413	

:: False convergence

       Squares	       0.140098
     Func Eval	            167
     Grad Eval	             85

:: Nonlinear parameters

     i	   Final p(i)	  Gradient(i)
     1	       124504	  1.9272e-010
     2	        39644	 -7.2835e-010
     3	      28.7464	  7.9786e-010
     4	     0.288926	 -3.2786e-009
     5	     0.278728	 -1.9546e-009
     6	     0.218557	   -1.24e-007
     7	     0.909091	    0.0023322
     8	         0.55	    -0.012114
     9	     0.916039	   1.277e-008
    10	     0.909091	     0.001602
    11	     0.909091	     0.001602
    12	     0.897014	  7.8699e-009
    13	     0.987225	  4.8358e-008
    14	      1.36364	   0.00023836
    15	     0.833127	 -8.3441e-008
    16	      0.98448	 -3.1432e-008
    17	      1.88867	  2.1232e-008
    18	     0.791904	 -1.5653e-008
    19	     0.926019	  1.5743e-007
    20	         2.75	 -5.0423e-005
    21	     0.763683	 -5.9374e-008
    22	     0.909091	   0.00098469
    23	          3.3	  -5.658e-005
    24	     0.728483	  3.2759e-009
    25	     0.909091	    0.0010495
    26	          4.4	 -4.4609e-005
    27	     0.694135	  1.9754e-009
