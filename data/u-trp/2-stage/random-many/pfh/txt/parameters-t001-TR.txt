
PARAMETERS AND CONFIDENCE INTERVALS

NO.	SET	           PARAMETER	     INITIAL	    LIMIT-LO	    LIMIT-HI	         FIT	      STDERR	         CV%	      F(SDI)	   CONLIN-LO	   CONLIN-HI	   CONLIN-P%	

  1	  1	                time	           0	
  2	  1	             restart	           0	
  3	  *	                 k.1	     44845.7	      1e-006	      1e+006	       44844	1.91687e+010	1.66886e+008	3.70143e+007	-3.75822e+010	3.75823e+010	          95	
  4	  *	                k.-1	  0.00306808	      1e-012	      1e+006	     4.88599	5.77655e+006	4.61582e+008	1.02376e+008	-1.13255e+007	1.13255e+007	          95	
  5	  *	               k.cat	2.73957e-006	      1e-006	      1e+006	  6.286e-005	   0.0173037	      107472	     23836.7	  -0.0338628	   0.0339886	          95	
  6	  1	                 [E]	           1	    0.909091	         1.1	    0.909091	      223102	 9.5814e+007	2.12509e+007	     -437414	      437416	          95	
  7	  1	                 [S]	         0.5	    0.454545	        0.55	    0.454545	      222410	1.91034e+008	  4.237e+007	     -436058	      436059	          95	
  8	  1	              [E.S1]	           0	
  9	  1	               [E.P]	           0	
 10	  *	                r(E)	        0.31	    3.1e-007	      310000	    0.973384	      402742	1.61538e+008	3.58281e+007	     -789618	      789620	          95	
 11	  1	                r(S)	           0	
 12	  *	             r(E.S1)	        0.22	    2.2e-007	      220000	    0.792992	      388072	1.91062e+008	4.23764e+007	     -760854	      760856	          95	
 13	  *	              r(E.P)	        0.25	    2.5e-007	      250000	2.65781e-007	     137.174	2.01502e+011	4.46919e+010	    -268.944	     268.944	          95	
 14	  1	              offset	      1.1793	     -INFTY	     +INFTY	    0.340153	      193168	2.21714e+008	4.91747e+007	     -378726	      378726	          95	
 15	  1	               delay	           0	
 16	  1	          incubation	           0	
 17	  1	                [E]i	           0	
 18	  1	                [S]i	           0	
 19	  1	             [E.S1]i	           0	
 20	  1	              [E.P]i	           0	
 21	  1	            dilution	           0	
 22	  1	           intensive	           0	
 23	  2	                time	           0	
 24	  2	             restart	           0	
 25	  2	                 [E]	           1	    0.909091	         1.1	         1.1	      367845	1.30558e+008	 2.8957e+007	     -721198	      721200	          95	
 26	  2	                 [S]	           1	    0.909091	         1.1	    0.909091	      444915	1.91074e+008	 4.2379e+007	     -872302	      872304	          95	
 27	  2	              [E.S1]	           0	
 28	  2	               [E.P]	           0	
 29	  2	                r(S)	           0	
 30	  2	              offset	     1.14325	     -INFTY	     +INFTY	    0.195779	      157826	3.14734e+008	6.98059e+007	     -309434	      309434	          95	
 31	  2	               delay	           0	
 32	  2	          incubation	           0	
 33	  2	                [E]i	           0	
 34	  2	                [S]i	           0	
 35	  2	             [E.S1]i	           0	
 36	  2	              [E.P]i	           0	
 37	  2	            dilution	           0	
 38	  2	           intensive	           0	
 39	  3	                time	           0	
 40	  3	             restart	           0	
 41	  3	                 [E]	           1	    0.909091	         1.1	         1.1	      538306	 1.9106e+008	4.23758e+007	-1.0554e+006	1.05541e+006	          95	
 42	  3	                 [S]	         1.5	     1.36364	        1.65	        1.65	      341601	8.08291e+007	1.79274e+007	     -669743	      669746	          95	
 43	  3	              [E.S1]	           0	
 44	  3	               [E.P]	           0	
 45	  3	                r(S)	           0	
 46	  3	              offset	     1.10351	     -INFTY	     +INFTY	    0.182129	     222.954	      477935	      106003	    -436.943	     437.307	          95	
 47	  3	               delay	           0	
 48	  3	          incubation	           0	
 49	  3	                [E]i	           0	
 50	  3	                [S]i	           0	
 51	  3	             [E.S1]i	           0	
 52	  3	              [E.P]i	           0	
 53	  3	            dilution	           0	
 54	  3	           intensive	           0	
 55	  4	                time	           0	
 56	  4	             restart	           0	
 57	  4	                 [E]	           1	    0.909091	         1.1	         1.1	      538298	1.91057e+008	4.23751e+007	-1.05539e+006	1.05539e+006	          95	
 58	  4	                 [S]	           2	     1.81818	         2.2	     1.81818	      312654	6.71365e+007	1.48904e+007	     -612989	      612992	          95	
 59	  4	              [E.S1]	           0	
 60	  4	               [E.P]	           0	
 61	  4	                r(S)	           0	
 62	  4	              offset	     1.07103	     -INFTY	     +INFTY	    0.139656	     221.937	      620445	      137611	    -434.991	      435.27	          95	
 63	  4	               delay	           0	
 64	  4	          incubation	           0	
 65	  4	                [E]i	           0	
 66	  4	                [S]i	           0	
 67	  4	             [E.S1]i	           0	
 68	  4	              [E.P]i	           0	
 69	  4	            dilution	           0	
 70	  4	           intensive	           0	
 71	  5	                time	           0	
 72	  5	             restart	           0	
 73	  5	                 [E]	           1	    0.909091	         1.1	         1.1	      538292	1.91055e+008	4.23747e+007	-1.05538e+006	1.05538e+006	          95	
 74	  5	                 [S]	         2.5	     2.27273	        2.75	     2.27273	      268448	4.61154e+007	1.02281e+007	     -526319	      526324	          95	
 75	  5	              [E.S1]	           0	
 76	  5	               [E.P]	           0	
 77	  5	                r(S)	           0	
 78	  5	              offset	     1.02909	     -INFTY	     +INFTY	   0.0980946	      235.45	      937101	      207843	    -461.527	     461.723	          95	
 79	  5	               delay	           0	
 80	  5	          incubation	           0	
 81	  5	                [E]i	           0	
 82	  5	                [S]i	           0	
 83	  5	             [E.S1]i	           0	
 84	  5	              [E.P]i	           0	
 85	  5	            dilution	           0	
 86	  5	           intensive	           0	
 87	  6	                time	           0	
 88	  6	             restart	           0	
 89	  6	                 [E]	           1	    0.909091	         1.1	    0.909091	      444868	1.91054e+008	4.23745e+007	     -872209	      872210	          95	
 90	  6	                 [S]	           3	     2.72727	         3.3	     2.72727	      342044	 4.8965e+007	1.08601e+007	     -670610	      670616	          95	
 91	  6	              [E.S1]	           0	
 92	  6	               [E.P]	           0	
 93	  6	                r(S)	           0	
 94	  6	              offset	     0.97752	     -INFTY	     +INFTY	    0.210274	     205.962	      382414	       84817	      -403.6	     404.021	          95	
 95	  6	               delay	           0	
 96	  6	          incubation	           0	
 97	  6	                [E]i	           0	
 98	  6	                [S]i	           0	
 99	  6	             [E.S1]i	           0	
100	  6	              [E.P]i	           0	
101	  6	            dilution	           0	
102	  6	           intensive	           0	
103	  7	                time	           0	
104	  7	             restart	           0	
105	  7	                 [E]	           1	    0.909091	         1.1	         1.1	      538290	1.91054e+008	4.23745e+007	-1.05537e+006	1.05538e+006	          95	
106	  7	                 [S]	           4	     3.63636	         4.4	     3.63636	      539815	5.79575e+007	1.28546e+007	-1.05836e+006	1.05837e+006	          95	
107	  7	              [E.S1]	           0	
108	  7	               [E.P]	           0	
109	  7	                r(S)	           0	
110	  7	              offset	     0.95791	     -INFTY	     +INFTY	   0.0246293	     257.621	4.08377e+006	      905754	    -505.067	     505.117	          95	
111	  7	               delay	           0	
112	  7	          incubation	           0	
113	  7	                [E]i	           0	
114	  7	                [S]i	           0	
115	  7	             [E.S1]i	           0	
116	  7	              [E.P]i	           0	
117	  7	            dilution	           0	
118	  7	           intensive	           0	
