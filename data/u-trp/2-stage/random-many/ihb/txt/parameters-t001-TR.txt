
PARAMETERS AND CONFIDENCE INTERVALS

NO.	SET	           PARAMETER	     INITIAL	    LIMIT-LO	    LIMIT-HI	         FIT	      STDERR	         CV%	      F(SDI)	   CONLIN-LO	   CONLIN-HI	   CONLIN-P%	

  1	  1	                time	           0	
  2	  1	             restart	           0	
  3	  *	                 k.1	      15.132	      1e-006	      1e+006	     5.57612	     10257.8	      718216	      451963	    -20105.9	     20117.1	          95	
  4	  *	                k.-1	1.01924e-006	      1e-012	      1e+006	 1.5638e-011	7.54369e-005	1.88337e+009	1.18518e+009	-0.000147902	 0.000147902	          95	
  5	  *	               k.cat	 0.000111155	      1e-006	      1e+006	     2.08045	   0.0803837	     15.0849	     9.49273	     1.92285	     2.23805	          95	
  6	  1	                 [E]	           1	    0.909091	         1.1	         1.1	     2023.57	      718220	      451965	    -3966.32	     3968.52	          95	
  7	  1	                 [S]	         0.5	    0.454545	        0.55	        0.55	     1011.78	      718217	      451963	    -1983.15	     1984.25	          95	
  8	  1	              [E.S1]	           0	
  9	  1	               [E.P]	           0	
 10	  *	                r(E)	        0.31	    3.1e-007	      310000	    0.272776	2.38849e+007	3.41862e+010	2.15129e+010	-4.68289e+007	4.68289e+007	          95	
 11	  1	                r(S)	           0	
 12	  *	             r(E.S1)	        0.22	    2.2e-007	      220000	    0.185084	2.38849e+007	5.03833e+010	3.17054e+010	-4.68289e+007	4.68289e+007	          95	
 13	  *	              r(E.P)	        0.25	    2.5e-007	      250000	    0.218969	2.38849e+007	4.25866e+010	2.67991e+010	-4.68289e+007	4.68289e+007	          95	
 14	  1	              offset	      1.1793	     -INFTY	     +INFTY	    0.870413	2.62734e+007	1.17848e+010	7.41602e+009	-5.15118e+007	5.15118e+007	          95	
 15	  1	               delay	           0	
 16	  1	          incubation	           0	
 17	  1	                [E]i	           0	
 18	  1	                [S]i	           0	
 19	  1	             [E.S1]i	           0	
 20	  1	              [E.P]i	           0	
 21	  1	            dilution	           0	
 22	  1	           intensive	           0	
 23	  2	                time	           0	
 24	  2	             restart	           0	
 25	  2	                 [E]	           1	    0.909091	         1.1	    0.909091	     1672.36	      718215	      451962	    -3277.93	     3279.74	          95	
 26	  2	                 [S]	           1	    0.909091	         1.1	         1.1	     2023.56	      718216	      451963	     -3966.3	      3968.5	          95	
 27	  2	              [E.S1]	           0	
 28	  2	               [E.P]	           0	
 29	  2	                r(S)	           0	
 30	  2	              offset	     1.14325	     -INFTY	     +INFTY	    0.898109	2.17136e+007	9.43918e+009	5.93993e+009	-4.25717e+007	4.25717e+007	          95	
 31	  2	               delay	           0	
 32	  2	          incubation	           0	
 33	  2	                [E]i	           0	
 34	  2	                [S]i	           0	
 35	  2	             [E.S1]i	           0	
 36	  2	              [E.P]i	           0	
 37	  2	            dilution	           0	
 38	  2	           intensive	           0	
 39	  3	                time	           0	
 40	  3	             restart	           0	
 41	  3	                 [E]	           1	    0.909091	         1.1	    0.960586	     1767.09	      718215	      451962	     -3463.6	     3465.53	          95	
 42	  3	                 [S]	         1.5	     1.36364	        1.65	        1.65	     3035.34	      718216	      451963	    -5949.45	     5952.75	          95	
 43	  3	              [E.S1]	           0	
 44	  3	               [E.P]	           0	
 45	  3	                r(S)	           0	
 46	  3	              offset	     1.10351	     -INFTY	     +INFTY	     0.84201	2.29435e+007	1.06384e+010	6.69457e+009	-4.49832e+007	4.49832e+007	          95	
 47	  3	               delay	           0	
 48	  3	          incubation	           0	
 49	  3	                [E]i	           0	
 50	  3	                [S]i	           0	
 51	  3	             [E.S1]i	           0	
 52	  3	              [E.P]i	           0	
 53	  3	            dilution	           0	
 54	  3	           intensive	           0	
 55	  4	                time	           0	
 56	  4	             restart	           0	
 57	  4	                 [E]	           1	    0.909091	         1.1	     1.00086	     1841.17	      718215	      451962	    -3608.81	     3610.81	          95	
 58	  4	                 [S]	           2	     1.81818	         2.2	         2.2	     4047.11	      718215	      451962	    -7932.58	     7936.98	          95	
 59	  4	              [E.S1]	           0	
 60	  4	               [E.P]	           0	
 61	  4	                r(S)	           0	
 62	  4	              offset	     1.07103	     -INFTY	     +INFTY	    0.792584	2.39054e+007	1.17756e+010	 7.4102e+009	-4.6869e+007	 4.6869e+007	          95	
 63	  4	               delay	           0	
 64	  4	          incubation	           0	
 65	  4	                [E]i	           0	
 66	  4	                [S]i	           0	
 67	  4	             [E.S1]i	           0	
 68	  4	              [E.P]i	           0	
 69	  4	            dilution	           0	
 70	  4	           intensive	           0	
 71	  5	                time	           0	
 72	  5	             restart	           0	
 73	  5	                 [E]	           1	    0.909091	         1.1	    0.936257	     1722.33	      718215	      451962	    -3375.88	     3377.75	          95	
 74	  5	                 [S]	         2.5	     2.27273	        2.75	     2.46594	     4536.33	      718216	      451963	    -8891.49	     8896.42	          95	
 75	  5	              [E.S1]	           0	
 76	  5	               [E.P]	           0	
 77	  5	                r(S)	           0	
 78	  5	              offset	     1.02909	     -INFTY	     +INFTY	    0.765756	2.23624e+007	1.14015e+010	7.17477e+009	-4.38438e+007	4.38439e+007	          95	
 79	  5	               delay	           0	
 80	  5	          incubation	           0	
 81	  5	                [E]i	           0	
 82	  5	                [S]i	           0	
 83	  5	             [E.S1]i	           0	
 84	  5	              [E.P]i	           0	
 85	  5	            dilution	           0	
 86	  5	           intensive	           0	
 87	  6	                time	           0	
 88	  6	             restart	           0	
 89	  6	                 [E]	           1	    0.909091	         1.1	    0.956483	     1759.54	      718215	      451962	    -3448.81	     3450.72	          95	
 90	  6	                 [S]	           3	     2.72727	         3.3	     2.72727	     5017.09	      718217	      451963	     -9833.8	     9839.25	          95	
 91	  6	              [E.S1]	           0	
 92	  6	               [E.P]	           0	
 93	  6	                r(S)	           0	
 94	  6	              offset	     0.97752	     -INFTY	     +INFTY	    0.722786	2.28455e+007	1.23402e+010	7.76553e+009	-4.4791e+007	 4.4791e+007	          95	
 95	  6	               delay	           0	
 96	  6	          incubation	           0	
 97	  6	                [E]i	           0	
 98	  6	                [S]i	           0	
 99	  6	             [E.S1]i	           0	
100	  6	              [E.P]i	           0	
101	  6	            dilution	           0	
102	  6	           intensive	           0	
103	  7	                time	           0	
104	  7	             restart	           0	
105	  7	                 [E]	           1	    0.909091	         1.1	    0.909091	     1672.36	      718215	      451962	    -3277.93	     3279.75	          95	
106	  7	                 [S]	           4	     3.63636	         4.4	     3.63636	     6689.45	      718216	      451963	    -13111.7	       13119	          95	
107	  7	              [E.S1]	           0	
108	  7	               [E.P]	           0	
109	  7	                r(S)	           0	
110	  7	              offset	     0.95791	     -INFTY	     +INFTY	    0.699882	2.17136e+007	1.21126e+010	 7.6223e+009	-4.25717e+007	4.25717e+007	          95	
111	  7	               delay	           0	
112	  7	          incubation	           0	
113	  7	                [E]i	           0	
114	  7	                [S]i	           0	
115	  7	             [E.S1]i	           0	
116	  7	              [E.P]i	           0	
117	  7	            dilution	           0	
118	  7	           intensive	           0	
