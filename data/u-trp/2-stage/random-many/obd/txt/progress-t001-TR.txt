
===================================================
Least squares Minimization - Trust Region Algorithm
===================================================

Optimized parameters:

     1 	 k.1
     2 	 k.-1
     3 	 k.cat
     4 	 r(E)
     5 	 r(E.S1)
     6 	 r(E.P)
     7 	 [E]#1
     8 	 [S]#1
     9 	 offset#1
    10 	 [E]#2
    11 	 [S]#2
    12 	 offset#2
    13 	 [E]#3
    14 	 [S]#3
    15 	 offset#3
    16 	 [E]#4
    17 	 [S]#4
    18 	 offset#4
    19 	 [E]#5
    20 	 [S]#5
    21 	 offset#5
    22 	 [E]#6
    23 	 [S]#6
    24 	 offset#6
    25 	 [E]#7
    26 	 [S]#7
    27 	 offset#7

:: NL2SOL :: DNLAFB
:: Analytic Jacobian; Constrained

________________________________________________________________________

:: Initial Estimate

     i	     Param p(i)	            Min	            Max
     1	        127.842	         1e-006	         1e+006
     2	        7.17155	         1e-012	         1e+006
     3	   1.28234e-005	         1e-006	         1e+006
     4	           0.31	       3.1e-007	         310000
     5	           0.22	       2.2e-007	         220000
     6	           0.25	       2.5e-007	         250000
     7	              1	       0.909091	            1.1
     8	            0.5	       0.454545	           0.55
     9	         1.1793	       -10.6137	        12.9723
    10	              1	       0.909091	            1.1
    11	              1	       0.909091	            1.1
    12	        1.14325	       -10.6497	        12.9363
    13	              1	       0.909091	            1.1
    14	            1.5	        1.36364	           1.65
    15	        1.10351	       -10.6895	        12.8965
    16	              1	       0.909091	            1.1
    17	              2	        1.81818	            2.2
    18	        1.07103	        -10.722	         12.864
    19	              1	       0.909091	            1.1
    20	            2.5	        2.27273	           2.75
    21	        1.02909	       -10.7639	        12.8221
    22	              1	       0.909091	            1.1
    23	              3	        2.72727	            3.3
    24	        0.97752	       -10.8155	        12.7705
    25	              1	       0.909091	            1.1
    26	              4	        3.63636	            4.4
    27	        0.95791	       -10.8351	        12.7509

:: Iterations

     i	 FEval	   F(i) = SSQ	   Param( 1)	   Param( 2)	   Param( 3)	   Param( 4)	   Param( 5)	   Param( 6)	   Param( 7)	   Param( 8)	   Param( 9)	   Param(10)	   Param(11)	   Param(12)	   Param(13)	   Param(14)	   Param(15)	   Param(16)	   Param(17)	   Param(18)	   Param(19)	   Param(20)	   Param(21)	   Param(22)	   Param(23)	   Param(24)	   Param(25)	   Param(26)	   Param(27)	
     0	     1	        302.2	       127.84	       7.1715	  1.2823e-005	         0.31	         0.22	         0.25	            1	          0.5	       1.1793	            1	            1	       1.1433	            1	          1.5	       1.1035	            1	            2	        1.071	            1	          2.5	       1.0291	            1	            3	      0.97752	            1	            4	      0.95791	
     1	     2	       20.505	       127.91	       6.3262	       1e-006	      0.30765	      0.11205	     2.5e-007	      0.99238	      0.50245	      0.96771	      0.99845	      0.99862	       1.0142	       1.0016	       1.4882	      0.99333	       1.0013	       1.9916	      0.96593	       1.0018	       2.4942	      0.93176	      0.90909	       2.9956	      0.91075	       1.0585	       3.9971	       0.8519	
     2	     4	      0.87146	       127.91	       6.2966	    0.0011154	      0.22116	     0.069404	     2.5e-007	      0.98452	      0.49215	      0.99374	      0.98046	       1.0508	       1.0042	          1.1	       1.4571	      0.98439	      0.99641	       1.9775	      0.94004	       1.0059	        2.487	      0.90733	      0.90909	       2.9921	      0.87429	       1.0628	       3.9899	      0.82526	
     3	     5	       0.3891	        127.9	       6.2819	   0.00046722	      0.17117	     0.072041	     2.5e-007	      0.93327	      0.54545	       1.0361	      0.99004	      0.99728	       1.0095	          1.1	       1.4571	       0.9633	       1.0238	       1.9192	      0.93194	       1.0063	        2.452	      0.89369	      0.90909	       2.9701	      0.86289	        1.051	       3.9763	       0.8187	
     4	     6	      0.38066	        127.9	       6.2478	   0.00043795	       0.1715	     0.072671	     2.5e-007	      0.90909	         0.55	       1.0389	      0.97114	      0.95024	       1.0099	          1.1	       1.4494	      0.96232	       1.0507	       1.8182	      0.92826	       1.0104	       2.3784	      0.89239	      0.90909	       2.9243	      0.86212	       1.0349	       3.9482	      0.81901	
     5	     7	       0.3722	       127.89	        6.226	   0.00041099	       0.1742	     0.072908	     2.5e-007	      0.90909	         0.55	       1.0356	      0.95444	      0.92949	       1.0106	          1.1	       1.3875	      0.96035	       1.0805	       1.8182	      0.92535	       1.0137	       2.3262	      0.89162	      0.90909	       2.8925	      0.86173	        1.025	       3.9293	      0.81938	
     6	     8	      0.37084	       127.88	       6.1841	   0.00039014	      0.17674	      0.07385	     2.5e-007	      0.90909	         0.55	       1.0331	      0.92488	      0.90909	       1.0117	          1.1	       1.3636	      0.95743	       1.0932	       1.8182	      0.92321	       1.0194	       2.3097	      0.89011	      0.90909	       2.8137	      0.86073	       1.0066	       3.8825	      0.81973	
     7	    10	      0.36945	       127.88	       6.1837	   0.00039387	      0.17711	     0.073916	    0.0002299	      0.90909	         0.55	       1.0323	      0.92423	      0.90909	       1.0122	          1.1	       1.3636	       0.9581	          1.1	       1.8182	      0.92324	       1.0194	       2.3083	      0.88948	      0.91486	       2.8126	       0.8605	       1.0064	       3.8822	      0.81985	
     8	    12	      0.36886	       127.88	       6.1807	    0.0004059	      0.17724	     0.074072	    0.0023291	      0.90909	         0.55	       1.0321	      0.91788	      0.90909	       1.0131	          1.1	       1.3636	      0.95805	          1.1	       1.8182	      0.92292	       1.0188	       2.2984	      0.88961	      0.91396	       2.8067	      0.86032	       1.0043	        3.879	      0.81984	
     9	    13	      0.36837	       127.87	       6.1653	   0.00046494	      0.17781	     0.074527	      0.01316	      0.90909	         0.55	       1.0315	      0.90909	      0.90909	       1.0139	          1.1	       1.3636	      0.95765	          1.1	       1.8182	      0.92231	       1.0194	       2.2984	      0.88928	      0.90909	       2.7784	       0.8602	      0.99674	       3.8634	      0.81994	
    10	    14	      0.36814	       127.86	       6.1289	   0.00070237	      0.17804	     0.074739	     0.045665	      0.90909	         0.55	       1.0313	      0.90909	      0.90909	       1.0138	          1.1	       1.3636	      0.95752	          1.1	       1.8182	      0.92201	       1.0235	       2.2984	      0.88884	      0.90909	       2.7784	         0.86	      0.98621	       3.8245	      0.82051	
    11	    16	       0.3678	       127.86	        6.128	   0.00093624	      0.17805	     0.074786	     0.047519	      0.90909	         0.55	       1.0313	      0.90909	      0.90909	       1.0138	          1.1	       1.3636	      0.95758	          1.1	       1.8182	      0.92192	       1.0233	       2.2959	      0.88887	      0.90909	        2.777	      0.85996	      0.98584	       3.8238	      0.82051	
    12	    17	      0.36753	       127.86	       6.1263	    0.0011828	      0.17689	     0.074737	     0.050748	      0.90909	         0.55	       1.0318	      0.90909	      0.90909	       1.0142	          1.1	       1.3636	      0.95827	          1.1	       1.8182	      0.92197	       1.0236	       2.2919	      0.88906	      0.90909	       2.7746	      0.86009	      0.98523	       3.8224	       0.8207	
    13	    19	      0.36741	       127.86	        6.125	    0.0014657	      0.17628	     0.074187	     0.055848	      0.90909	         0.55	       1.0323	      0.90909	      0.90909	       1.0148	          1.1	       1.3636	      0.95892	          1.1	       1.8182	      0.92259	       1.0239	       2.2889	       0.8896	      0.90909	       2.7728	      0.86058	      0.98482	       3.8214	      0.82127	
    14	    21	      0.36733	       127.86	       6.1246	    0.0017195	      0.17605	     0.073928	     0.058153	      0.90909	         0.55	       1.0326	      0.90909	      0.90909	        1.015	          1.1	       1.3636	      0.95922	          1.1	       1.8182	      0.92288	        1.024	        2.288	      0.88985	      0.90909	       2.7722	      0.86081	       0.9847	       3.8211	      0.82152	
    15	    23	      0.36717	       127.86	       6.1239	    0.0022093	      0.17552	     0.073395	     0.062915	      0.90909	         0.55	       1.0331	      0.90909	      0.90909	       1.0155	          1.1	       1.3636	      0.95984	          1.1	       1.8182	      0.92347	       1.0241	       2.2864	      0.89037	      0.90909	       2.7712	      0.86127	       0.9845	       3.8206	      0.82203	
    16	    25	        0.367	       127.86	       6.1227	     0.003251	      0.17453	     0.072746	     0.069096	      0.90909	         0.55	       1.0338	      0.90909	      0.90909	       1.0163	          1.1	       1.3636	      0.96064	          1.1	       1.8182	      0.92415	       1.0244	       2.2836	      0.89093	      0.90909	       2.7696	      0.86178	       0.9843	       3.8196	      0.82258	
    17	    26	      0.36669	       127.86	       6.1211	     0.003978	      0.17414	     0.072801	     0.069171	      0.90909	         0.55	        1.034	      0.90909	      0.90909	       1.0163	          1.1	       1.3636	      0.96064	          1.1	       1.8182	      0.92407	       1.0247	       2.2797	      0.89079	      0.90909	       2.7672	      0.86166	      0.98411	       3.8183	      0.82246	
    18	    28	      0.36647	       127.86	        6.121	    0.0052866	      0.17436	     0.072589	     0.070824	      0.90909	         0.55	       1.0341	      0.90909	      0.90909	       1.0166	          1.1	       1.3636	      0.96095	          1.1	       1.8182	      0.92437	       1.0247	       2.2794	      0.89103	       0.9091	       2.7671	      0.86187	       0.9841	       3.8182	      0.82266	
    19	    30	      0.36629	       127.86	       6.1209	    0.0067146	      0.17434	     0.072393	      0.07215	      0.90909	         0.55	       1.0342	      0.90909	      0.90909	       1.0168	          1.1	       1.3636	       0.9612	          1.1	       1.8182	      0.92455	       1.0248	       2.2792	      0.89116	       0.9091	       2.7669	      0.86196	      0.98407	       3.8181	      0.82275	
    20	    31	      0.36611	       127.86	       6.1191	    0.0094521	      0.17387	     0.072241	     0.073985	      0.90909	         0.55	       1.0345	      0.90909	      0.90909	       1.0172	          1.1	       1.3636	      0.96153	          1.1	       1.8182	      0.92471	       1.0255	       2.2752	      0.89115	      0.90922	       2.7645	      0.86197	      0.98368	       3.8167	      0.82277	
    21	    32	      0.36603	       127.86	       6.1174	     0.010338	      0.17383	     0.072269	     0.073944	      0.90909	         0.55	       1.0345	      0.90909	      0.90909	       1.0173	          1.1	       1.3636	      0.96152	          1.1	       1.8182	      0.92468	       1.0258	       2.2752	       0.8911	      0.90933	       2.7621	      0.86192	      0.98313	       3.8152	      0.82277	
    22	    38	      0.36444	       127.74	       5.8406	     0.010534	       0.1747	     0.073693	     0.075421	      0.90909	         0.55	       1.0337	      0.90909	      0.90909	       1.0164	          1.1	       1.3636	       0.9602	          1.1	       1.8182	      0.92325	       1.0563	       2.2727	       0.8873	      0.92337	       2.7564	      0.85958	       0.9612	       3.6364	      0.82287	
    23	    39	      0.36275	       127.53	       5.4224	     0.010717	       0.1732	     0.072914	      0.07456	      0.90909	         0.55	       1.0351	      0.90909	      0.90909	        1.018	          1.1	       1.3636	      0.96162	          1.1	       1.8182	      0.92447	       1.0858	       2.2727	      0.88599	      0.93723	       2.7273	       0.8593	      0.97472	       3.6364	      0.82266	
    24	    42	      0.33094	       123.82	       1e-012	     0.015517	      0.15228	     0.066595	     0.064779	      0.90909	         0.55	       1.0513	      0.90909	      0.90909	        1.035	          1.1	       1.3636	      0.97884	          1.1	       1.8182	       0.9378	          1.1	       2.2727	      0.89647	       1.0504	       2.7273	      0.85973	       1.0951	       3.6364	      0.82214	
    25	    44	      0.19541	       28.045	       1e-012	       1e-006	      0.13469	     0.067434	     0.064481	      0.90909	         0.55	       1.0566	      0.94105	      0.90909	       1.0352	          1.1	       1.3636	      0.97385	          1.1	       1.8182	      0.93622	       1.0996	       2.2727	      0.89506	       1.0049	       2.7273	      0.86246	          1.1	       3.6364	        0.821	
    26	    45	      0.17126	       12.382	       1e-012	       1e-006	      0.12207	     0.066378	     0.064481	      0.90909	         0.55	        1.059	      0.94105	      0.90909	       1.0305	          1.1	       1.3636	      0.97472	       1.0767	       1.8182	      0.93553	       1.0996	       2.2727	       0.8932	       1.0049	       2.7273	      0.86131	          1.1	       3.6364	        0.821	
    27	    48	      0.16027	       12.399	    0.0013192	       1e-006	      0.12139	      0.06662	     0.065617	      0.90909	         0.55	       1.0582	       1.0717	      0.90909	       1.0166	      0.98144	       1.5171	      0.98297	       1.0326	       1.8518	      0.93883	       1.0755	       2.2982	      0.89504	      0.99033	       2.7352	      0.86196	       1.0703	       3.6364	      0.82033	
    28	    50	      0.14541	       12.441	      0.26983	       1e-006	      0.13056	     0.067285	     0.070059	       1.0714	      0.54334	       1.0337	          1.1	      0.90909	       1.0051	      0.93651	        1.631	      0.98357	      0.96108	       1.9703	      0.94063	      0.98441	       2.3813	      0.89841	      0.94229	       2.7461	      0.86332	       1.0641	       3.6364	      0.82129	
    29	    52	      0.13749	       12.444	      0.32264	     0.020297	      0.13128	     0.065989	     0.070456	       1.0975	      0.53785	        1.029	          1.1	      0.90909	       1.0059	      0.94021	       1.6202	      0.98418	       0.9552	       1.9857	      0.94178	      0.96153	       2.3904	      0.90055	      0.94305	       2.7494	      0.86337	       1.0625	       3.6364	      0.82141	
    30	    53	      0.13326	       12.447	      0.31791	     0.065412	      0.13189	     0.065618	     0.069576	          1.1	      0.54846	       1.0286	          1.1	      0.90909	       1.0061	      0.94456	       1.6276	      0.98393	      0.95833	       1.9912	      0.94151	      0.95896	       2.3918	      0.90067	      0.94874	        2.744	      0.86287	       1.0607	       3.6364	      0.82139	
    31	    54	      0.11791	       12.447	      0.31336	       0.1316	      0.13244	     0.064649	     0.071499	          1.1	         0.55	       1.0282	          1.1	      0.90909	       1.0061	      0.94689	       1.6288	      0.98361	      0.95946	       1.9926	      0.94133	      0.95766	       2.3918	      0.90072	      0.95078	       2.7416	      0.86271	       1.0594	       3.6364	      0.82152	
    32	    55	      0.10744	       12.448	      0.27333	      0.25801	      0.13345	     0.064348	     0.071983	          1.1	         0.55	       1.0272	          1.1	      0.90909	       1.0057	      0.95337	       1.6261	      0.98276	      0.96611	       1.9911	      0.94048	      0.95993	       2.3865	      0.90019	      0.96027	       2.7302	      0.86166	       1.0527	       3.6364	      0.82161	
    33	    56	     0.097163	       12.444	      0.21463	      0.40639	      0.13448	     0.063602	     0.072978	          1.1	         0.55	       1.0264	          1.1	      0.90909	       1.0052	      0.96067	       1.6171	      0.98187	      0.97535	       1.9841	      0.93947	      0.96513	       2.3721	      0.89948	      0.97399	       2.7302	      0.86037	       1.0463	       3.6364	      0.82172	
    34	    57	     0.091352	       12.431	      0.11838	      0.52074	       0.1347	     0.062891	     0.073088	          1.1	         0.55	       1.0264	          1.1	      0.90909	       1.0054	      0.97248	       1.5952	      0.98103	      0.99214	       1.9615	      0.93825	       0.9765	       2.3292	      0.89863	      0.99539	       2.7302	      0.85886	       1.0448	       3.6364	      0.82181	
    35	    59	     0.083375	       12.112	       1e-012	       0.9378	      0.13319	     0.060977	     0.072696	          1.1	         0.55	       1.0275	          1.1	      0.90909	       1.0064	      0.98886	        1.551	      0.97998	       1.0202	       1.9123	       0.9363	       1.0182	       2.3181	      0.89577	       1.0547	       2.7273	       0.8547	       1.0591	       3.6364	      0.82097	
    36	    60	     0.080728	       11.894	  1.3733e-005	      0.86936	       0.1333	     0.060564	     0.073465	          1.1	         0.55	       1.0272	          1.1	      0.90909	       1.0059	      0.99746	       1.5639	      0.97918	       1.0325	       1.9021	        0.935	       1.0317	       2.3181	      0.89411	       1.0786	       2.7273	      0.85262	       1.0531	       3.6364	      0.82104	
    37	    61	      0.07898	       11.711	       1e-012	      0.96757	      0.13381	     0.059687	      0.07365	          1.1	         0.55	       1.0269	          1.1	      0.90909	       1.0055	       1.0011	       1.5522	      0.97839	       1.0356	       1.8977	      0.93453	       1.0344	       2.3181	      0.89407	       1.0786	       2.7273	      0.85252	       1.0531	       3.6364	      0.82104	
    38	    64	     0.074521	       10.651	       1e-012	       1.1186	      0.13539	     0.060703	     0.076416	          1.1	         0.55	       1.0246	          1.1	      0.90909	       1.0027	      0.99853	       1.5843	      0.97589	        1.033	       1.9441	      0.93193	       1.0043	       2.2727	      0.89328	       1.0604	       2.7273	      0.85081	       1.0034	       3.6364	      0.82167	
    39	    66	     0.072286	       10.252	       1e-012	       1.2723	       0.1363	     0.058609	     0.076857	          1.1	         0.55	       1.0241	          1.1	      0.90909	       1.0023	       0.9914	        1.583	      0.97617	       1.0304	       1.9575	      0.93187	       1.0006	       2.2727	      0.89334	       1.0593	       2.7273	      0.85062	      0.99831	       3.6364	      0.82186	
    40	    69	      0.06698	       8.5878	       1e-012	        1.475	      0.13614	     0.056997	     0.078326	          1.1	         0.55	       1.0233	          1.1	      0.90909	       1.0007	      0.99118	       1.6457	      0.97486	       1.0248	       2.0329	      0.93095	       0.9712	       2.2727	      0.89412	       1.0242	       2.7273	      0.85177	      0.94539	       3.6364	      0.82447	
    41	    71	     0.065102	        7.292	  1.6746e-005	       1.6498	      0.13523	     0.054281	      0.07919	          1.1	         0.55	       1.0234	       1.0959	      0.90909	            1	      0.95094	         1.65	      0.97727	       1.0211	       2.0632	      0.93061	      0.98379	       2.3593	      0.89322	       1.0113	       2.7273	       0.8507	      0.94539	       3.6364	       0.8238	
    42	    72	     0.063679	       7.2864	  1.3333e-005	       1.6527	      0.13894	     0.057362	     0.082944	          1.1	         0.55	       1.0191	       1.0959	      0.90909	      0.99628	      0.95094	         1.65	      0.97373	       1.0161	       2.0689	      0.92711	      0.97594	       2.3607	      0.88954	       1.0056	       2.7273	      0.84874	      0.93714	       3.6364	      0.82102	
    43	    73	      0.06237	       6.4598	       1e-012	        1.859	      0.13816	     0.054389	      0.08321	          1.1	         0.55	       1.0193	       1.0959	      0.90909	      0.99592	      0.97092	         1.65	      0.97164	      0.99477	       2.0689	      0.92861	      0.94811	       2.3901	      0.89169	      0.97813	       2.7273	       0.8508	      0.93714	       3.6364	      0.82094	
    44	    75	     0.062064	       5.8816	       1e-012	       2.0184	      0.18021	     0.094002	      0.12617	          1.1	         0.55	      0.97244	          1.1	      0.90909	      0.94789	      0.96095	         1.65	      0.93076	      0.98834	       2.0732	      0.88644	      0.94561	       2.4313	      0.85131	      0.97101	       2.7273	      0.80937	      0.93613	       3.6364	      0.78079	
    45	    77	     0.061554	       5.8783	       1e-012	       2.0123	       0.1842	     0.097234	      0.12977	          1.1	         0.55	      0.96822	          1.1	      0.90909	      0.94425	      0.94918	         1.65	      0.92931	      0.98644	       2.1254	      0.88366	      0.94256	        2.435	      0.84839	      0.96547	       2.7273	      0.80695	      0.90909	       3.6364	      0.78089	
    46	    82	     0.061552	       5.8783	  6.5364e-005	       2.0123	       0.1842	     0.097235	      0.12976	          1.1	         0.55	      0.96822	          1.1	      0.90909	      0.94425	      0.94918	         1.65	      0.92931	      0.98644	       2.1254	      0.88366	      0.94255	        2.435	      0.84839	      0.96546	       2.7273	      0.80694	      0.90909	       3.6364	       0.7809	
    47	    83	     0.061548	       5.8783	  6.4019e-005	       2.0123	       0.1842	     0.097238	      0.12976	          1.1	         0.55	      0.96823	          1.1	      0.90909	      0.94423	      0.94918	         1.65	       0.9293	      0.98643	       2.1254	      0.88364	      0.94256	        2.435	      0.84839	      0.96546	       2.7273	      0.80695	      0.90909	       3.6364	      0.78091	
    48	    84	     0.061543	       5.8783	  4.8571e-005	       2.0123	      0.18421	     0.097244	      0.12975	          1.1	         0.55	      0.96823	          1.1	      0.90909	       0.9442	      0.94919	         1.65	       0.9293	      0.98642	       2.1254	      0.88363	      0.94256	        2.435	       0.8484	      0.96547	       2.7273	      0.80695	      0.90909	       3.6364	      0.78092	
    49	    85	     0.061538	       5.8783	       1e-012	       2.0123	      0.18426	      0.09726	      0.12977	          1.1	         0.55	      0.96821	          1.1	      0.90909	      0.94414	      0.94921	         1.65	      0.92927	      0.98637	       2.1254	      0.88359	      0.94253	        2.435	       0.8484	      0.96545	       2.7273	      0.80695	      0.90909	       3.6364	      0.78092	
    50	   104	     0.061538	       5.8783	  1.1429e-009	       2.0123	      0.18426	      0.09726	      0.12977	          1.1	         0.55	      0.96821	          1.1	      0.90909	      0.94414	      0.94921	         1.65	      0.92927	      0.98637	       2.1254	      0.88359	      0.94253	        2.435	       0.8484	      0.96545	       2.7273	      0.80695	      0.90909	       3.6364	      0.78092	
    51	   105	     0.061538	       5.8783	  1.0739e-009	       2.0123	      0.18426	      0.09726	      0.12977	          1.1	         0.55	      0.96821	          1.1	      0.90909	      0.94414	      0.94921	         1.65	      0.92927	      0.98637	       2.1254	      0.88359	      0.94253	        2.435	       0.8484	      0.96545	       2.7273	      0.80695	      0.90909	       3.6364	      0.78092	
    52	   106	     0.061538	       5.8783	   2.814e-010	       2.0123	      0.18426	      0.09726	      0.12977	          1.1	         0.55	      0.96821	          1.1	      0.90909	      0.94414	      0.94921	         1.65	      0.92927	      0.98637	       2.1254	      0.88359	      0.94253	        2.435	       0.8484	      0.96545	       2.7273	      0.80695	      0.90909	       3.6364	      0.78092	
    53	   114	     0.061538	       5.8783	       1e-012	       2.0123	      0.18426	     0.097262	      0.12977	          1.1	         0.55	      0.96821	          1.1	      0.90909	      0.94413	      0.94921	         1.65	      0.92927	      0.98636	       2.1254	      0.88359	      0.94253	        2.435	       0.8484	      0.96545	       2.7273	      0.80695	      0.90909	       3.6364	      0.78093	
    54	   134	     0.061538	       5.8783	  9.0799e-011	       2.0123	      0.18426	     0.097262	      0.12977	          1.1	         0.55	      0.96821	          1.1	      0.90909	      0.94413	      0.94921	         1.65	      0.92927	      0.98636	       2.1254	      0.88359	      0.94253	        2.435	       0.8484	      0.96545	       2.7273	      0.80695	      0.90909	       3.6364	      0.78093	
    55	   137	     0.061538	       5.8783	  8.9763e-011	       2.0123	      0.18426	     0.097262	      0.12977	          1.1	         0.55	      0.96821	          1.1	      0.90909	      0.94413	      0.94921	         1.65	      0.92927	      0.98636	       2.1254	      0.88359	      0.94253	        2.435	       0.8484	      0.96545	       2.7273	      0.80695	      0.90909	       3.6364	      0.78093	
    56	   140	     0.061538	       5.8783	  8.9763e-011	       2.0123	      0.18426	     0.097262	      0.12977	          1.1	         0.55	      0.96821	          1.1	      0.90909	      0.94413	      0.94921	         1.65	      0.92927	      0.98636	       2.1254	      0.88359	      0.94253	        2.435	       0.8484	      0.96545	       2.7273	      0.80695	      0.90909	       3.6364	      0.78093	

:: False convergence

       Squares	      0.0615379
     Func Eval	            140
     Grad Eval	             56

:: Nonlinear parameters

     i	   Final p(i)	  Gradient(i)
     1	      5.87833	   0.00023056
     2	 8.97629e-011	    0.0022545
     3	      2.01225	  3.8877e-005
     4	      0.18426	   -0.0030707
     5	     0.097262	   -0.0034787
     6	     0.129771	 -8.9591e-005
     7	          1.1	   -0.0060201
     8	         0.55	   -0.0044307
     9	     0.968208	   -0.0018686
    10	          1.1	   -0.0060354
    11	     0.909091	    0.0050211
    12	     0.944133	    0.0090888
    13	     0.949209	  -0.00085639
    14	         1.65	   -0.0019753
    15	     0.929272	   0.00030475
    16	     0.986365	   0.00085156
    17	      2.12543	  -0.00087736
    18	     0.883587	     0.008611
    19	     0.942535	  -0.00016275
    20	      2.43498	 -5.8237e-005
    21	     0.848401	   -0.0049727
    22	     0.965447	  -0.00029322
    23	      2.72727	   0.00077472
    24	     0.806952	   -0.0047855
    25	     0.909091	   0.00076153
    26	      3.63636	    0.0039718
    27	     0.780934	    -0.015463
