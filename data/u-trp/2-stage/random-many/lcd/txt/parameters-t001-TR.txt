
PARAMETERS AND CONFIDENCE INTERVALS

NO.	SET	           PARAMETER	     INITIAL	    LIMIT-LO	    LIMIT-HI	         FIT	      STDERR	         CV%	      F(SDI)	   CONLIN-LO	   CONLIN-HI	   CONLIN-P%	

  1	  1	                time	           0	
  2	  1	             restart	           0	
  3	  *	                 k.1	     1.18874	      1e-006	      1e+006	     33.8177	      652814	7.53663e+006	3.19533e+006	-1.27988e+006	1.27994e+006	          95	
  4	  *	                k.-1	      2.9243	      1e-012	      1e+006	1.14216e-010	   0.0610708	2.08755e+011	8.85068e+010	   -0.119736	    0.119736	          95	
  5	  *	               k.cat	     14.2726	      1e-006	      1e+006	     28.3582	      2.7327	     37.6223	     15.9509	     23.0005	      33.716	          95	
  6	  1	                 [E]	           1	    0.909091	         1.1	     0.94004	     18146.5	7.53664e+006	3.19534e+006	    -35577.1	       35579	          95	
  7	  1	                 [S]	         0.5	    0.454545	        0.55	        0.55	     10617.1	7.53663e+006	3.19534e+006	    -20815.5	     20816.6	          95	
  8	  1	              [E.S1]	           0	
  9	  1	               [E.P]	           0	
 10	  *	                r(E)	        0.31	    3.1e-007	      310000	   0.0591903	     1146.28	7.56088e+006	3.20561e+006	    -2247.34	     2247.46	          95	
 11	  1	                r(S)	           0	
 12	  *	             r(E.S1)	        0.22	    2.2e-007	      220000	    0.049194	     953.843	7.57003e+006	 3.2095e+006	    -1870.06	     1870.16	          95	
 13	  *	              r(E.P)	        0.25	    2.5e-007	      250000	    2.5e-007	     77.6647	1.21287e+011	5.14227e+010	     -152.27	      152.27	          95	
 14	  1	              offset	      1.1793	     -INFTY	     +INFTY	     1.11648	     73.0081	     25530.2	     10824.1	    -142.024	     144.257	          95	
 15	  1	               delay	           0	
 16	  1	          incubation	           0	
 17	  1	                [E]i	           0	
 18	  1	                [S]i	           0	
 19	  1	             [E.S1]i	           0	
 20	  1	              [E.P]i	           0	
 21	  1	            dilution	           0	
 22	  1	           intensive	           0	
 23	  2	                time	           0	
 24	  2	             restart	           0	
 25	  2	                 [E]	           1	    0.909091	         1.1	         1.1	     21234.3	7.53663e+006	3.19533e+006	    -41630.9	     41633.1	          95	
 26	  2	                 [S]	           1	    0.909091	         1.1	    0.909091	       17549	7.53663e+006	3.19534e+006	    -34405.7	     34407.5	          95	
 27	  2	              [E.S1]	           0	
 28	  2	               [E.P]	           0	
 29	  2	                r(S)	           0	
 30	  2	              offset	     1.14325	     -INFTY	     +INFTY	     1.08518	     85.4313	     30736.1	     13031.3	    -166.412	     168.582	          95	
 31	  2	               delay	           0	
 32	  2	          incubation	           0	
 33	  2	                [E]i	           0	
 34	  2	                [S]i	           0	
 35	  2	             [E.S1]i	           0	
 36	  2	              [E.P]i	           0	
 37	  2	            dilution	           0	
 38	  2	           intensive	           0	
 39	  3	                time	           0	
 40	  3	             restart	           0	
 41	  3	                 [E]	           1	    0.909091	         1.1	    0.948303	     18305.9	7.53663e+006	3.19534e+006	    -35889.8	     35891.7	          95	
 42	  3	                 [S]	         1.5	     1.36364	        1.65	     1.47582	     28489.1	7.53663e+006	3.19534e+006	    -55854.3	     55857.3	          95	
 43	  3	              [E.S1]	           0	
 44	  3	               [E.P]	           0	
 45	  3	                r(S)	           0	
 46	  3	              offset	     1.10351	     -INFTY	     +INFTY	     1.04887	     73.6494	     27414.5	       11623	    -143.348	     145.446	          95	
 47	  3	               delay	           0	
 48	  3	          incubation	           0	
 49	  3	                [E]i	           0	
 50	  3	                [S]i	           0	
 51	  3	             [E.S1]i	           0	
 52	  3	              [E.P]i	           0	
 53	  3	            dilution	           0	
 54	  3	           intensive	           0	
 55	  4	                time	           0	
 56	  4	             restart	           0	
 57	  4	                 [E]	           1	    0.909091	         1.1	    0.948012	     18300.3	7.53663e+006	3.19534e+006	    -35878.7	     35880.6	          95	
 58	  4	                 [S]	           2	     1.81818	         2.2	     2.00737	     38750.1	7.53664e+006	3.19534e+006	    -75971.5	     75975.5	          95	
 59	  4	              [E.S1]	           0	
 60	  4	               [E.P]	           0	
 61	  4	                r(S)	           0	
 62	  4	              offset	     1.07103	     -INFTY	     +INFTY	     1.00706	     73.6269	     28543.9	     12101.9	    -143.346	      145.36	          95	
 63	  4	               delay	           0	
 64	  4	          incubation	           0	
 65	  4	                [E]i	           0	
 66	  4	                [S]i	           0	
 67	  4	             [E.S1]i	           0	
 68	  4	              [E.P]i	           0	
 69	  4	            dilution	           0	
 70	  4	           intensive	           0	
 71	  5	                time	           0	
 72	  5	             restart	           0	
 73	  5	                 [E]	           1	    0.909091	         1.1	    0.909091	       17549	7.53663e+006	3.19534e+006	    -34405.7	     34407.6	          95	
 74	  5	                 [S]	         2.5	     2.27273	        2.75	        2.75	     53085.8	7.53664e+006	3.19534e+006	     -104077	      104083	          95	
 75	  5	              [E.S1]	           0	
 76	  5	               [E.P]	           0	
 77	  5	                r(S)	           0	
 78	  5	              offset	     1.02909	     -INFTY	     +INFTY	    0.966081	     70.6042	     28533.1	     12097.3	    -137.461	     139.393	          95	
 79	  5	               delay	           0	
 80	  5	          incubation	           0	
 81	  5	                [E]i	           0	
 82	  5	                [S]i	           0	
 83	  5	             [E.S1]i	           0	
 84	  5	              [E.P]i	           0	
 85	  5	            dilution	           0	
 86	  5	           intensive	           0	
 87	  6	                time	           0	
 88	  6	             restart	           0	
 89	  6	                 [E]	           1	    0.909091	         1.1	    0.909091	       17549	7.53663e+006	3.19534e+006	    -34405.7	     34407.5	          95	
 90	  6	                 [S]	           3	     2.72727	         3.3	         3.3	     63702.9	7.53664e+006	3.19534e+006	     -124893	      124899	          95	
 91	  6	              [E.S1]	           0	
 92	  6	               [E.P]	           0	
 93	  6	                r(S)	           0	
 94	  6	              offset	     0.97752	     -INFTY	     +INFTY	    0.927207	     70.6042	     29729.4	     12604.5	      -137.5	     139.354	          95	
 95	  6	               delay	           0	
 96	  6	          incubation	           0	
 97	  6	                [E]i	           0	
 98	  6	                [S]i	           0	
 99	  6	             [E.S1]i	           0	
100	  6	              [E.P]i	           0	
101	  6	            dilution	           0	
102	  6	           intensive	           0	
103	  7	                time	           0	
104	  7	             restart	           0	
105	  7	                 [E]	           1	    0.909091	         1.1	    0.917475	     17710.8	7.53663e+006	3.19534e+006	      -34723	     34724.9	          95	
106	  7	                 [S]	           4	     3.63636	         4.4	         4.4	     84937.3	7.53664e+006	3.19534e+006	     -166524	      166533	          95	
107	  7	              [E.S1]	           0	
108	  7	               [E.P]	           0	
109	  7	                r(S)	           0	
110	  7	              offset	     0.95791	     -INFTY	     +INFTY	    0.892943	     71.2554	     31154.9	     13208.9	    -138.811	     140.597	          95	
111	  7	               delay	           0	
112	  7	          incubation	           0	
113	  7	                [E]i	           0	
114	  7	                [S]i	           0	
115	  7	             [E.S1]i	           0	
116	  7	              [E.P]i	           0	
117	  7	            dilution	           0	
118	  7	           intensive	           0	
