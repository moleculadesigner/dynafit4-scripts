
PARAMETERS AND CONFIDENCE INTERVALS

NO.	SET	           PARAMETER	     INITIAL	    LIMIT-LO	    LIMIT-HI	         FIT	      STDERR	         CV%	      F(SDI)	   CONLIN-LO	   CONLIN-HI	   CONLIN-P%	

  1	  1	                time	           0	
  2	  1	             restart	           0	
  3	  *	                 k.1	4.62104e-005	      1e-006	      1e+006	     20.7129	     15019.9	      283112	     62812.5	    -29427.3	     29468.7	          95	
  4	  *	                k.-1	     8978.19	      1e-012	      1e+006	     8978.15	     85642.2	     3724.21	      826.27	     -158932	      176889	          95	
  5	  *	               k.cat	    0.258083	      1e-006	      1e+006	     1.79063	     1142.51	      249107	     55268.1	    -2238.22	      2241.8	          95	
  6	  1	                 [E]	           1	    0.909091	         1.1	    0.909091	     667.434	      286638	     63594.8	    -1307.66	     1309.48	          95	
  7	  1	                 [S]	         0.5	    0.454545	        0.55	    0.454545	     333.504	      286455	     63554.3	    -653.416	     654.325	          95	
  8	  1	              [E.S1]	           0	
  9	  1	               [E.P]	           0	
 10	  *	                r(E)	        0.31	    3.1e-007	      310000	     4.63672	     5451.67	      459040	      101845	    -10683.9	     10693.2	          95	
 11	  1	                r(S)	           0	
 12	  *	             r(E.S1)	        0.22	    2.2e-007	      220000	    2.2e-007	     91.2981	1.62021e+011	3.59468e+010	        -179	         179	          95	
 13	  *	              r(E.P)	        0.25	    2.5e-007	      250000	     4.59519	     5424.95	      460919	      102262	    -10631.6	     10640.8	          95	
 14	  1	              offset	      1.1793	     -INFTY	     +INFTY	    -3.06732	     2707.89	      344671	           0	    -5312.17	     5306.03	          95	
 15	  1	               delay	           0	
 16	  1	          incubation	           0	
 17	  1	                [E]i	           0	
 18	  1	                [S]i	           0	
 19	  1	             [E.S1]i	           0	
 20	  1	              [E.P]i	           0	
 21	  1	            dilution	           0	
 22	  1	           intensive	           0	
 23	  2	                time	           0	
 24	  2	             restart	           0	
 25	  2	                 [E]	           1	    0.909091	         1.1	         1.1	     2200.08	      780867	      173247	    -4312.38	     4314.58	          95	
 26	  2	                 [S]	           1	    0.909091	         1.1	         1.1	     2689.68	      954640	      211801	    -5272.29	     5274.49	          95	
 27	  2	              [E.S1]	           0	
 28	  2	               [E.P]	           0	
 29	  2	                r(S)	           0	
 30	  2	              offset	     1.14325	     -INFTY	     +INFTY	    -3.98412	     12031.7	1.17903e+006	           0	    -23593.3	     23585.4	          95	
 31	  2	               delay	           0	
 32	  2	          incubation	           0	
 33	  2	                [E]i	           0	
 34	  2	                [S]i	           0	
 35	  2	             [E.S1]i	           0	
 36	  2	              [E.P]i	           0	
 37	  2	            dilution	           0	
 38	  2	           intensive	           0	
 39	  3	                time	           0	
 40	  3	             restart	           0	
 41	  3	                 [E]	           1	    0.909091	         1.1	         1.1	      808.82	      287072	     63691.2	    -1584.68	     1586.88	          95	
 42	  3	                 [S]	         1.5	     1.36364	        1.65	        1.65	     1211.04	      286555	     63576.4	    -2372.73	     2376.03	          95	
 43	  3	              [E.S1]	           0	
 44	  3	               [E.P]	           0	
 45	  3	                r(S)	           0	
 46	  3	              offset	     1.10351	     -INFTY	     +INFTY	    -4.02588	     3261.83	      316324	           0	    -6399.18	     6391.13	          95	
 47	  3	               delay	           0	
 48	  3	          incubation	           0	
 49	  3	                [E]i	           0	
 50	  3	                [S]i	           0	
 51	  3	             [E.S1]i	           0	
 52	  3	              [E.P]i	           0	
 53	  3	            dilution	           0	
 54	  3	           intensive	           0	
 55	  4	                time	           0	
 56	  4	             restart	           0	
 57	  4	                 [E]	           1	    0.909091	         1.1	         1.1	     809.218	      287214	     63722.6	    -1585.46	     1587.66	          95	
 58	  4	                 [S]	           2	     1.81818	         2.2	         2.2	     1614.89	      286584	     63582.9	    -3163.96	     3168.36	          95	
 59	  4	              [E.S1]	           0	
 60	  4	               [E.P]	           0	
 61	  4	                r(S)	           0	
 62	  4	              offset	     1.07103	     -INFTY	     +INFTY	    -4.06226	     3258.36	      313158	           0	    -6392.41	     6384.29	          95	
 63	  4	               delay	           0	
 64	  4	          incubation	           0	
 65	  4	                [E]i	           0	
 66	  4	                [S]i	           0	
 67	  4	             [E.S1]i	           0	
 68	  4	              [E.P]i	           0	
 69	  4	            dilution	           0	
 70	  4	           intensive	           0	
 71	  5	                time	           0	
 72	  5	             restart	           0	
 73	  5	                 [E]	           1	    0.909091	         1.1	     1.09984	     809.451	      287339	     63750.4	    -1585.91	     1588.11	          95	
 74	  5	                 [S]	         2.5	     2.27273	        2.75	        2.75	     2018.95	      286633	     63593.7	    -3955.62	     3961.12	          95	
 75	  5	              [E.S1]	           0	
 76	  5	               [E.P]	           0	
 77	  5	                r(S)	           0	
 78	  5	              offset	     1.02909	     -INFTY	     +INFTY	     -4.0973	     3254.26	      310089	           0	    -6384.41	     6376.21	          95	
 79	  5	               delay	           0	
 80	  5	          incubation	           0	
 81	  5	                [E]i	           0	
 82	  5	                [S]i	           0	
 83	  5	             [E.S1]i	           0	
 84	  5	              [E.P]i	           0	
 85	  5	            dilution	           0	
 86	  5	           intensive	           0	
 87	  6	                time	           0	
 88	  6	             restart	           0	
 89	  6	                 [E]	           1	    0.909091	         1.1	         1.1	     809.978	      287484	     63782.5	    -1586.95	     1589.15	          95	
 90	  6	                 [S]	           3	     2.72727	         3.3	         3.3	     2423.09	      286674	     63602.7	    -4747.42	     4754.02	          95	
 91	  6	              [E.S1]	           0	
 92	  6	               [E.P]	           0	
 93	  6	                r(S)	           0	
 94	  6	              offset	     0.97752	     -INFTY	     +INFTY	    -4.13158	     3250.83	      307192	           0	    -6377.72	     6369.46	          95	
 95	  6	               delay	           0	
 96	  6	          incubation	           0	
 97	  6	                [E]i	           0	
 98	  6	                [S]i	           0	
 99	  6	             [E.S1]i	           0	
100	  6	              [E.P]i	           0	
101	  6	            dilution	           0	
102	  6	           intensive	           0	
103	  7	                time	           0	
104	  7	             restart	           0	
105	  7	                 [E]	           1	    0.909091	         1.1	         1.1	     810.253	      287581	     63804.1	    -1587.49	     1589.69	          95	
106	  7	                 [S]	           4	     3.63636	         4.4	     3.63636	     2670.36	      286705	     63609.7	    -5231.89	     5239.16	          95	
107	  7	              [E.S1]	           0	
108	  7	               [E.P]	           0	
109	  7	                r(S)	           0	
110	  7	              offset	     0.95791	     -INFTY	     +INFTY	    -4.16281	     3248.12	      304634	           0	    -6372.45	     6364.12	          95	
111	  7	               delay	           0	
112	  7	          incubation	           0	
113	  7	                [E]i	           0	
114	  7	                [S]i	           0	
115	  7	             [E.S1]i	           0	
116	  7	              [E.P]i	           0	
117	  7	            dilution	           0	
118	  7	           intensive	           0	
