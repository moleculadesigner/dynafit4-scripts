
===================================================
Least squares Minimization - Trust Region Algorithm
===================================================

Optimized parameters:

     1 	 k.1
     2 	 k.-1
     3 	 k.cat
     4 	 r(E)
     5 	 r(E.S1)
     6 	 r(E.P)
     7 	 [E]#1
     8 	 [S]#1
     9 	 offset#1
    10 	 [E]#2
    11 	 [S]#2
    12 	 offset#2
    13 	 [E]#3
    14 	 [S]#3
    15 	 offset#3
    16 	 [E]#4
    17 	 [S]#4
    18 	 offset#4
    19 	 [E]#5
    20 	 [S]#5
    21 	 offset#5
    22 	 [E]#6
    23 	 [S]#6
    24 	 offset#6
    25 	 [E]#7
    26 	 [S]#7
    27 	 offset#7

:: NL2SOL :: DNLAFB
:: Analytic Jacobian; Constrained

________________________________________________________________________

:: Initial Estimate

     i	     Param p(i)	            Min	            Max
     1	   5.05241e-005	         1e-006	         1e+006
     2	     0.00389569	         1e-012	         1e+006
     3	    0.000270814	         1e-006	         1e+006
     4	           0.31	       3.1e-007	         310000
     5	           0.22	       2.2e-007	         220000
     6	           0.25	       2.5e-007	         250000
     7	              1	       0.909091	            1.1
     8	            0.5	       0.454545	           0.55
     9	         1.1793	       -10.6137	        12.9723
    10	              1	       0.909091	            1.1
    11	              1	       0.909091	            1.1
    12	        1.14325	       -10.6497	        12.9363
    13	              1	       0.909091	            1.1
    14	            1.5	        1.36364	           1.65
    15	        1.10351	       -10.6895	        12.8965
    16	              1	       0.909091	            1.1
    17	              2	        1.81818	            2.2
    18	        1.07103	        -10.722	         12.864
    19	              1	       0.909091	            1.1
    20	            2.5	        2.27273	           2.75
    21	        1.02909	       -10.7639	        12.8221
    22	              1	       0.909091	            1.1
    23	              3	        2.72727	            3.3
    24	        0.97752	       -10.8155	        12.7705
    25	              1	       0.909091	            1.1
    26	              4	        3.63636	            4.4
    27	        0.95791	       -10.8351	        12.7509

:: Iterations

     i	 FEval	   F(i) = SSQ	   Param( 1)	   Param( 2)	   Param( 3)	   Param( 4)	   Param( 5)	   Param( 6)	   Param( 7)	   Param( 8)	   Param( 9)	   Param(10)	   Param(11)	   Param(12)	   Param(13)	   Param(14)	   Param(15)	   Param(16)	   Param(17)	   Param(18)	   Param(19)	   Param(20)	   Param(21)	   Param(22)	   Param(23)	   Param(24)	   Param(25)	   Param(26)	   Param(27)	
     0	     1	       486.63	  5.0524e-005	    0.0038957	   0.00027081	         0.31	         0.22	         0.25	            1	          0.5	       1.1793	            1	            1	       1.1433	            1	          1.5	       1.1035	            1	            2	        1.071	            1	          2.5	       1.0291	            1	            3	      0.97752	            1	            4	      0.95791	
     1	     2	       98.736	    0.0073549	       1.3202	       1e-006	      0.30967	      0.38414	      0.23463	      0.90909	      0.50456	       1.0171	       1.0006	       1.0007	      0.94063	       1.0004	       1.5003	      0.89342	       1.0001	          2.2	      0.85947	       0.9998	         2.75	      0.81942	      0.99945	       2.9998	      0.77858	      0.99991	       4.1388	      0.78282	
     2	     3	       2.3732	       1e-006	      0.92406	      0.12663	      0.79022	     2.2e-007	      0.23464	      0.90909	      0.45455	      0.44723	      0.98357	      0.98707	      0.33626	      0.99659	       1.4544	      0.28429	      0.93741	          2.2	      0.26143	       0.9998	         2.75	      0.20082	      0.99945	       2.9417	      0.14856	      0.99991	        4.263	      0.14913	
     3	     4	      0.67461	   0.00083849	       0.9309	     0.056943	      0.76431	     2.2e-007	       0.2381	      0.90909	      0.45703	      0.45877	      0.99536	      0.98978	      0.34757	       1.0046	       1.4559	      0.29343	      0.96367	          2.2	        0.279	       1.0069	         2.75	      0.20828	       1.0164	       2.9405	      0.15955	       1.0017	       4.2627	      0.14241	
     4	     7	      0.67461	   0.00083849	       0.9309	     0.056943	      0.76431	     2.2e-007	       0.2381	      0.90909	      0.45703	      0.45877	      0.99536	      0.98978	      0.34757	       1.0046	       1.4559	      0.29343	      0.96367	          2.2	        0.279	       1.0069	         2.75	      0.20828	       1.0164	       2.9405	      0.15955	       1.0017	       4.2627	      0.14241	

:: Singular convergence

       Squares	       0.674614
     Func Eval	              7
     Grad Eval	              4

:: Nonlinear parameters

     i	   Final p(i)	  Gradient(i)
     1	  0.000838488	      -40.865
     2	     0.930898	     0.025494
     3	    0.0569428	     -0.23462
     4	     0.764308	       22.378
     5	     2.2e-007	     0.025552
     6	     0.238104	     0.028414
     7	     0.909091	       4.2872
     8	     0.457034	   -0.0048589
     9	     0.458766	       5.6125
    10	     0.995356	       2.2808
    11	     0.989783	   -0.0045056
    12	     0.347571	         2.99
    13	      1.00464	       2.6418
    14	      1.45591	   -0.0040322
    15	     0.293433	       3.4641
    16	     0.963672	       1.0845
    17	          2.2	   -0.0011459
    18	     0.279003	       1.4224
    19	       1.0069	       2.4424
    20	         2.75	   -0.0020763
    21	     0.208278	        3.203
    22	      1.01636	        1.379
    23	      2.94051	  -0.00028496
    24	     0.159554	       1.8054
    25	      1.00165	       3.3773
    26	      4.26271	   -0.0029808
    27	     0.142405	       4.4354
