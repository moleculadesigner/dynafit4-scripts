
===================================================
Least squares Minimization - Trust Region Algorithm
===================================================

Optimized parameters:

     1 	 k.1
     2 	 k.-1
     3 	 k.cat
     4 	 r(E)
     5 	 r(E.S1)
     6 	 r(E.P)
     7 	 [E]#1
     8 	 [S]#1
     9 	 offset#1
    10 	 [E]#2
    11 	 [S]#2
    12 	 offset#2
    13 	 [E]#3
    14 	 [S]#3
    15 	 offset#3
    16 	 [E]#4
    17 	 [S]#4
    18 	 offset#4
    19 	 [E]#5
    20 	 [S]#5
    21 	 offset#5
    22 	 [E]#6
    23 	 [S]#6
    24 	 offset#6
    25 	 [E]#7
    26 	 [S]#7
    27 	 offset#7

:: NL2SOL :: DNLAFB
:: Analytic Jacobian; Constrained

________________________________________________________________________

:: Initial Estimate

     i	     Param p(i)	            Min	            Max
     1	   1.30781e-005	         1e-006	         1e+006
     2	       0.134827	         1e-012	         1e+006
     3	       0.314379	         1e-006	         1e+006
     4	           0.31	       3.1e-007	         310000
     5	           0.22	       2.2e-007	         220000
     6	           0.25	       2.5e-007	         250000
     7	              1	       0.909091	            1.1
     8	            0.5	       0.454545	           0.55
     9	         1.1793	       -10.6137	        12.9723
    10	              1	       0.909091	            1.1
    11	              1	       0.909091	            1.1
    12	        1.14325	       -10.6497	        12.9363
    13	              1	       0.909091	            1.1
    14	            1.5	        1.36364	           1.65
    15	        1.10351	       -10.6895	        12.8965
    16	              1	       0.909091	            1.1
    17	              2	        1.81818	            2.2
    18	        1.07103	        -10.722	         12.864
    19	              1	       0.909091	            1.1
    20	            2.5	        2.27273	           2.75
    21	        1.02909	       -10.7639	        12.8221
    22	              1	       0.909091	            1.1
    23	              3	        2.72727	            3.3
    24	        0.97752	       -10.8155	        12.7705
    25	              1	       0.909091	            1.1
    26	              4	        3.63636	            4.4
    27	        0.95791	       -10.8351	        12.7509

:: Iterations

     i	 FEval	   F(i) = SSQ	   Param( 1)	   Param( 2)	   Param( 3)	   Param( 4)	   Param( 5)	   Param( 6)	   Param( 7)	   Param( 8)	   Param( 9)	   Param(10)	   Param(11)	   Param(12)	   Param(13)	   Param(14)	   Param(15)	   Param(16)	   Param(17)	   Param(18)	   Param(19)	   Param(20)	   Param(21)	   Param(22)	   Param(23)	   Param(24)	   Param(25)	   Param(26)	   Param(27)	
     0	     1	       487.01	  1.3078e-005	      0.13483	      0.31438	         0.31	         0.22	         0.25	            1	          0.5	       1.1793	            1	            1	       1.1433	            1	          1.5	       1.1035	            1	            2	        1.071	            1	          2.5	       1.0291	            1	            3	      0.97752	            1	            4	      0.95791	
     1	     3	       115.97	     0.018114	        7.008	       1e-006	     3.1e-007	      0.25423	      0.12556	       1.0866	         0.55	       1.3213	          1.1	          1.1	       1.2332	          1.1	       1.5784	       1.2042	       1.0949	       2.0738	       1.1952	      0.90909	       2.4882	       1.1466	          1.1	       3.0142	       1.1531	      0.95295	       3.8798	       1.0592	
     2	     4	       24.854	       1e-006	       6.6724	       1e-006	     3.1e-007	      0.29195	     0.007647	      0.90909	      0.45455	       1.2268	       1.0178	       1.0402	       1.1621	          1.1	       1.5784	       1.1097	       1.0949	       2.0738	       1.0742	      0.90909	       2.4882	       1.0726	          1.1	       3.0142	       1.0331	      0.95295	       3.8798	      0.97693	
     3	     5	      0.51497	       1e-006	       6.6728	       1e-006	     3.1e-007	      0.28245	    0.0076469	      0.90909	      0.45455	       1.1427	       1.0175	       1.0399	       1.1018	       1.0996	       1.5781	       1.0535	       1.0945	       2.0736	        1.011	      0.90909	        2.488	      0.96938	       1.0995	       3.0141	      0.93032	      0.95235	       3.8797	      0.89586	
     4	     6	      0.51497	       1e-006	        7.038	       1e-006	     3.1e-007	     2.2e-007	    0.0074833	      0.90909	      0.45455	       1.1427	       0.9689	       1.0002	       1.1018	       1.0537	       1.3636	       1.0535	       1.0564	       1.8582	        1.011	      0.90909	       2.3405	      0.96938	       1.0722	       2.8532	      0.93032	      0.93511	       3.7435	      0.89586	
     5	     7	      0.51497	       1e-006	       7.0453	       1e-006	     3.1e-007	     2.2e-007	     2.5e-007	      0.90909	      0.46308	       1.1427	      0.96202	      0.98267	       1.1018	       1.0292	       1.3636	       1.0535	       1.0703	        1.865	        1.011	      0.90909	       2.3438	      0.96938	          1.1	       2.8417	      0.93032	       1.0285	       3.7483	      0.89586	
     6	     8	      0.51497	       7.1654	       7.0453	       1e-006	     3.1e-007	     2.2e-007	     2.5e-007	      0.90909	      0.46309	       1.1427	      0.96202	      0.98268	       1.1018	       1.0292	       1.3636	       1.0535	       1.0703	        1.865	        1.011	      0.90909	       2.3438	      0.96938	          1.1	       2.8417	      0.93032	       1.0285	       3.7483	      0.89586	
     7	     9	      0.33862	       7.4299	       6.4945	       1e-006	     0.038576	     2.2e-007	     2.5e-007	      0.90909	      0.46309	       1.1146	      0.96202	      0.98268	       1.0775	       1.0292	       1.3636	       1.0307	       1.0703	       2.1931	      0.99059	      0.90909	       2.4729	      0.95452	          1.1	        3.155	      0.91551	       1.0285	       3.7483	      0.89286	
     8	    11	      0.20877	         8.21	       4.8018	       1e-006	     0.093392	     2.2e-007	     2.5e-007	      0.90909	      0.46318	       1.0766	      0.96208	      0.98269	       1.0476	       1.0292	       1.3636	        1.004	       1.0703	       2.1931	      0.97337	      0.90909	       2.4729	      0.94124	          1.1	        3.155	      0.90141	       1.0285	       3.7483	      0.87293	
     9	    14	      0.17946	       8.2187	       4.7963	       1e-006	     0.097029	    0.0033807	   0.00051379	      0.93097	      0.54481	       1.0768	       1.0046	       1.0227	        1.046	      0.93728	        1.379	       1.0063	       1.0561	       2.1966	      0.97685	      0.92924	       2.4633	      0.94494	       1.0781	       3.1581	      0.90162	       0.9706	       3.7479	      0.87218	
    10	    15	      0.15904	       8.2248	       4.7838	       1e-006	     0.095719	     0.005367	    0.0011179	      0.94116	         0.55	       1.0768	       1.0333	       1.0634	        1.045	      0.98693	       1.3866	       1.0055	       1.0438	        2.198	      0.97513	      0.92714	       2.4828	      0.94232	       1.0667	       3.1537	      0.90107	      0.92925	       3.7473	      0.87256	
    11	    16	      0.15049	       8.2292	        4.767	       1e-006	     0.096196	     0.005495	    0.0021178	      0.98828	         0.55	       1.0724	        1.073	          1.1	       1.0435	       1.0308	       1.4115	       1.0044	       1.0196	       2.1941	      0.97521	      0.90909	       2.4909	      0.94189	       1.0019	       3.1457	        0.903	      0.90909	       3.7464	      0.87389	
    12	    18	      0.14756	       8.2369	       4.7441	       1e-006	     0.096789	    0.0056878	    0.0037327	       1.0513	         0.55	       1.0668	          1.1	          1.1	        1.041	       1.0585	       1.4423	       1.0034	       1.0046	       2.1848	      0.97535	      0.90909	         2.52	      0.94133	      0.96078	       3.1245	      0.90389	      0.90909	       3.7459	      0.87401	
    13	    20	      0.14683	       8.2589	       4.7003	       1e-006	     0.096731	    0.0058752	     0.011603	          1.1	         0.55	       1.0628	          1.1	          1.1	        1.041	       1.0585	       1.4771	       1.0044	      0.98573	       2.1676	      0.97601	      0.90909	         2.52	      0.94111	      0.96078	       3.1336	      0.90402	      0.90909	       3.7425	      0.87406	
    14	    22	      0.14638	       8.2926	       4.6386	       1e-006	     0.096785	    0.0060679	     0.020726	          1.1	         0.55	       1.0628	          1.1	          1.1	        1.041	       1.0676	        1.575	       1.0062	      0.97243	        2.162	      0.97672	      0.90909	        2.543	      0.94115	      0.95973	       3.1448	      0.90418	      0.90909	       3.7431	      0.87411	
    15	    23	      0.14469	       8.3325	       4.4232	       1e-006	     0.095702	    0.0062324	     0.054152	          1.1	         0.55	       1.0641	          1.1	          1.1	       1.0425	          1.1	       1.5454	       1.0044	      0.95328	       2.1747	      0.97852	      0.90909	       2.6252	      0.94247	      0.90909	       3.1435	      0.90651	      0.90909	       3.7389	      0.87468	
    16	    24	      0.13936	       8.3658	       4.0378	    0.0018065	     0.095562	     0.006395	     0.053931	          1.1	         0.55	       1.0647	          1.1	          1.1	       1.0433	          1.1	       1.5337	       1.0052	      0.94939	       2.1737	      0.97913	      0.90909	       2.6692	      0.94304	      0.90909	        3.137	      0.90663	      0.90909	       3.6994	      0.87459	
    17	    27	      0.13917	       8.3657	        4.036	     0.002663	     0.096743	    0.0069818	     0.045879	          1.1	         0.55	       1.0636	          1.1	          1.1	       1.0423	          1.1	       1.5336	       1.0044	      0.94862	       2.1739	      0.97833	      0.90909	       2.6696	      0.94217	      0.90909	       3.1371	      0.90574	      0.90909	       3.6956	      0.87365	
    18	    28	      0.13882	       8.3653	        4.029	    0.0042032	     0.097215	    0.0076337	     0.041014	          1.1	         0.55	       1.0632	          1.1	          1.1	       1.0419	          1.1	       1.5332	       1.0039	      0.94658	       2.1749	      0.97791	      0.90909	       2.6715	      0.94163	      0.90909	       3.1381	      0.90516	      0.90909	       3.6931	        0.873	
    19	    29	      0.13866	       8.3648	       4.0221	    0.0062231	     0.097622	    0.0080645	     0.037367	          1.1	         0.55	       1.0628	          1.1	          1.1	       1.0416	          1.1	       1.5334	       1.0036	      0.94535	        2.176	      0.97756	      0.90909	       2.6732	      0.94114	      0.90909	       3.1387	      0.90459	      0.90909	       3.6897	      0.87235	
    20	    30	      0.13853	        8.364	       4.0139	    0.0068063	     0.097553	    0.0080582	     0.037837	          1.1	         0.55	       1.0629	          1.1	          1.1	       1.0417	          1.1	       1.5337	       1.0037	      0.94419	       2.1773	      0.97765	      0.90909	       2.6751	      0.94116	      0.90909	       3.1393	      0.90458	      0.90909	       3.6855	      0.87231	
    21	    35	      0.13717	       8.3642	       3.5232	     0.007178	     0.095939	     0.010183	     0.037708	          1.1	         0.55	       1.0646	          1.1	          1.1	       1.0433	          1.1	       1.5507	       1.0052	      0.94352	          2.2	      0.97854	      0.90909	       2.7443	      0.94167	      0.90909	        3.166	      0.90458	      0.90909	       3.6364	      0.87185	
    22	    36	      0.13525	       9.1277	       1.8479	    0.0073679	     0.093118	     0.017314	     0.036201	          1.1	         0.55	       1.0689	          1.1	          1.1	       1.0483	       1.0007	       1.5507	       1.0151	      0.92984	          2.2	      0.98112	      0.90909	       2.7443	      0.94268	      0.90909	        3.166	      0.90495	      0.90909	       3.6364	      0.87159	
    23	    38	      0.13451	       9.1738	       1.8503	    0.0094423	      0.09519	     0.018804	       0.0358	          1.1	         0.55	        1.067	          1.1	       1.0506	       1.0456	      0.96471	       1.5859	       1.0152	      0.93406	          2.2	      0.97932	      0.90909	         2.75	      0.94095	      0.90909	       3.1598	      0.90317	      0.90909	       3.6364	      0.86982	
    24	    39	      0.13425	       9.2043	       1.8391	     0.010709	     0.095014	     0.018679	     0.035745	          1.1	         0.55	       1.0672	          1.1	       1.0671	       1.0462	      0.96772	       1.5859	       1.0153	      0.92914	          2.2	      0.97953	      0.90909	         2.75	        0.941	      0.90909	       3.1575	      0.90315	      0.90909	       3.6364	      0.86978	
    25	    42	      0.13409	       9.2044	       1.8391	     0.012575	     0.095063	     0.018559	     0.035474	          1.1	         0.55	       1.0673	          1.1	       1.0671	       1.0463	      0.96773	        1.586	       1.0154	      0.92913	          2.2	      0.97942	      0.90909	         2.75	      0.94086	      0.90909	       3.1575	      0.90298	      0.90909	       3.6364	      0.86957	
    26	    44	      0.13402	       9.2044	       1.8391	     0.014434	     0.095097	     0.018666	     0.035032	          1.1	         0.55	       1.0672	          1.1	       1.0671	       1.0464	      0.96773	       1.5861	       1.0154	      0.92913	          2.2	      0.97936	      0.90909	         2.75	      0.94076	      0.90909	       3.1575	      0.90288	      0.90909	       3.6364	      0.86945	
    27	    45	      0.13392	        9.205	       1.8385	     0.016226	      0.09504	     0.018645	     0.034883	          1.1	         0.55	       1.0673	          1.1	       1.0678	       1.0465	      0.96762	       1.5898	       1.0155	       0.9294	          2.2	      0.97924	      0.90909	         2.75	      0.94064	      0.90909	       3.1572	      0.90272	      0.90909	       3.6364	      0.86929	
    28	    47	      0.13359	       9.2229	       1.8141	     0.018695	     0.094883	     0.018899	     0.034639	          1.1	         0.55	       1.0674	          1.1	       1.0767	       1.0469	      0.97056	         1.65	        1.016	      0.93558	          2.2	       0.9789	      0.90909	         2.75	      0.94046	      0.90909	       3.1393	      0.90247	      0.90909	       3.6364	      0.86904	
    29	    49	      0.13248	       10.087	       1.1172	     0.023531	     0.094878	     0.022077	      0.03386	          1.1	         0.55	       1.0688	          1.1	      0.95143	        1.046	      0.94373	         1.65	       1.0188	      0.94045	          2.2	      0.97936	      0.90909	         2.75	      0.94049	      0.90909	       2.9048	      0.90164	      0.90909	       3.6364	      0.86849	
    30	    50	      0.13031	       10.483	      0.93535	     0.044082	       0.4186	      0.34647	      0.35612	          1.1	         0.55	      0.71317	          1.1	      0.95143	      0.69118	      0.94373	         1.65	      0.71369	      0.94045	          2.2	      0.67479	      0.90909	         2.75	      0.64578	      0.90909	       2.9048	      0.60701	      0.90909	       3.6364	      0.57356	
    31	    51	      0.11848	       10.545	      0.45522	      0.13464	      0.44118	      0.37252	      0.38117	          1.1	         0.55	      0.68796	          1.1	      0.95143	      0.66772	      0.94373	         1.65	      0.69104	      0.94045	          2.2	      0.65123	      0.90909	       2.3275	      0.62165	      0.90909	       2.9048	      0.58363	      0.90909	       3.6364	      0.54986	
    32	    52	      0.09317	       9.8578	      0.36114	      0.82499	      0.44176	      0.36822	      0.38055	          1.1	         0.55	      0.68766	          1.1	      0.95143	      0.66817	      0.94373	         1.65	      0.69107	      0.98381	       2.0569	      0.63428	      0.90909	       2.2775	      0.62181	      0.90909	       2.9048	      0.58371	      0.90909	       3.6364	      0.54999	
    33	    53	     0.074415	       9.4364	       1e-012	       1.0907	      0.44166	      0.36655	      0.38302	          1.1	         0.55	      0.68733	          1.1	      0.95143	      0.66715	      0.98121	         1.65	      0.67659	      0.99487	       2.0049	      0.63005	      0.90909	       2.2775	      0.62201	       1.0006	       2.9048	      0.54913	      0.90909	       3.6364	      0.55035	
    34	    54	     0.071442	       8.9376	       1e-012	        1.191	      0.44228	       0.3648	        0.383	          1.1	         0.55	      0.68698	          1.1	      0.90909	      0.66354	      0.95403	         1.65	      0.68701	      0.98468	        2.045	      0.63401	      0.90909	       2.2744	      0.62195	      0.98904	       2.8252	      0.55331	      0.90909	       3.6364	      0.55032	
    35	    55	     0.064783	       7.4027	  4.1495e-006	        1.616	      0.48317	      0.40124	      0.42579	          1.1	         0.55	      0.64123	          1.1	      0.90909	      0.61823	        0.948	         1.65	       0.6488	      0.99504	       2.1062	      0.58827	      0.90909	       2.2727	      0.58271	      0.98901	       2.8191	      0.51132	      0.90909	       3.6364	      0.51175	
    36	    56	     0.064155	       7.4025	  8.2675e-006	        1.616	      0.48322	       0.4003	      0.42589	          1.1	         0.55	      0.64109	          1.1	      0.90909	      0.61792	      0.94804	         1.65	      0.64885	      0.99479	       2.1063	      0.58774	      0.90909	       2.2728	       0.5832	      0.98892	       2.8187	      0.51114	      0.90909	       3.6364	      0.51157	
    37	    57	     0.062242	        6.227	       1e-012	       1.8852	      0.48165	      0.39656	      0.42623	          1.1	         0.55	      0.64163	          1.1	      0.90909	      0.61783	      0.95347	         1.65	      0.64601	      0.98342	       2.1063	       0.5923	      0.90909	         2.38	      0.58306	      0.96643	       2.8187	      0.52042	      0.90909	       3.6364	      0.51138	
    38	    94	     0.062242	        6.227	  9.3212e-006	       1.8852	      0.48165	      0.39656	      0.42623	          1.1	         0.55	      0.64163	          1.1	      0.90909	      0.61783	      0.95347	         1.65	      0.64601	      0.98342	       2.1063	       0.5923	      0.90909	         2.38	      0.58306	      0.96642	       2.8187	      0.52041	      0.90909	       3.6364	      0.51139	

:: False convergence

       Squares	      0.0622422
     Func Eval	             94
     Grad Eval	             39

:: Nonlinear parameters

     i	   Final p(i)	  Gradient(i)
     1	      6.22696	  -0.00044792
     2	 9.32122e-006	    0.0036943
     3	      1.88525	    0.0013029
     4	      0.48165	       0.1219
     5	     0.396559	      0.22443
     6	     0.426232	     0.064072
     7	          1.1	     0.014761
     8	         0.55	   -0.0060329
     9	     0.641625	     0.042779
    10	          1.1	     0.010727
    11	     0.909091	    0.0051228
    12	     0.617834	     0.039624
    13	     0.953473	     0.025192
    14	         1.65	   -0.0027188
    15	     0.646014	     0.057923
    16	     0.983421	     0.027268
    17	      2.10625	   -0.0015823
    18	     0.592301	     0.063592
    19	     0.909091	      0.02658
    20	      2.38002	  -0.00060059
    21	     0.583058	     0.065833
    22	     0.966423	     0.029162
    23	      2.81871	     0.001041
    24	     0.520415	     0.069891
    25	     0.909091	     0.036315
    26	      3.63636	    0.0035691
    27	     0.511388	     0.082066
