
PARAMETERS AND CONFIDENCE INTERVALS

NO.	SET	           PARAMETER	     INITIAL	    LIMIT-LO	    LIMIT-HI	         FIT	      STDERR	         CV%	      F(SDI)	   CONLIN-LO	   CONLIN-HI	   CONLIN-P%	

  1	  1	                time	           0	
  2	  1	             restart	           0	
  3	  *	                 k.1	7.27052e-006	      1e-006	      1e+006	  0.00249393	     33036.3	5.17176e+009	1.13029e+009	      -64771	     64771.1	          95	
  4	  *	                k.-1	    0.163877	      1e-012	      1e+006	  0.00266298	      879369	1.28924e+011	2.81764e+010	-1.72409e+006	1.72409e+006	          95	
  5	  *	               k.cat	     1472.22	      1e-006	      1e+006	     1472.22	2.28719e+007	6.06545e+006	 1.3256e+006	-4.48414e+007	4.48443e+007	          95	
  6	  1	                 [E]	           1	    0.909091	         1.1	    0.920822	1.21977e+007	5.17171e+009	1.13028e+009	-2.39148e+007	2.39148e+007	          95	
  7	  1	                 [S]	         0.5	    0.454545	        0.55	    0.454545	6.02115e+006	5.17171e+009	1.13028e+009	-1.18051e+007	1.18051e+007	          95	
  8	  1	              [E.S1]	           0	
  9	  1	               [E.P]	           0	
 10	  *	                r(E)	        0.31	    3.1e-007	      310000	    0.288679	3.82395e+006	5.17166e+009	1.13027e+009	-7.49726e+006	7.49726e+006	          95	
 11	  1	                r(S)	           0	
 12	  *	             r(E.S1)	        0.22	    2.2e-007	      220000	2.20021e-007	      100.93	1.79097e+011	3.91416e+010	    -197.883	     197.883	          95	
 13	  *	              r(E.P)	        0.25	    2.5e-007	      250000	    0.277333	3.67365e+006	5.17166e+009	1.13027e+009	-7.20258e+006	7.20258e+006	          95	
 14	  1	              offset	      1.1793	     -INFTY	     +INFTY	    0.877079	     4461.19	1.98584e+006	      434005	    -8745.75	     8747.51	          95	
 15	  1	               delay	           0	
 16	  1	          incubation	           0	
 17	  1	                [E]i	           0	
 18	  1	                [S]i	           0	
 19	  1	             [E.S1]i	           0	
 20	  1	              [E.P]i	           0	
 21	  1	            dilution	           0	
 22	  1	           intensive	           0	
 23	  2	                time	           0	
 24	  2	             restart	           0	
 25	  2	                 [E]	           1	    0.909091	         1.1	         1.1	6.06896e+008	2.15404e+011	4.70765e+010	-1.18988e+009	1.18988e+009	          95	
 26	  2	                 [S]	           1	    0.909091	         1.1	         1.1	6.06553e+008	2.15283e+011	  4.705e+010	-1.18921e+009	1.18921e+009	          95	
 27	  2	              [E.S1]	           0	
 28	  2	               [E.P]	           0	
 29	  2	                r(S)	           0	
 30	  2	              offset	     1.14325	     -INFTY	     +INFTY	    0.784869	1.75098e+008	8.70996e+010	1.90356e+010	-3.43298e+008	3.43298e+008	          95	
 31	  2	               delay	           0	
 32	  2	          incubation	           0	
 33	  2	                [E]i	           0	
 34	  2	                [S]i	           0	
 35	  2	             [E.S1]i	           0	
 36	  2	              [E.P]i	           0	
 37	  2	            dilution	           0	
 38	  2	           intensive	           0	
 39	  3	                time	           0	
 40	  3	             restart	           0	
 41	  3	                 [E]	           1	    0.909091	         1.1	         1.1	1.45712e+007	5.17171e+009	1.13028e+009	-2.85683e+007	2.85683e+007	          95	
 42	  3	                 [S]	         1.5	     1.36364	        1.65	        1.65	2.18568e+007	5.17171e+009	1.13028e+009	-4.28525e+007	4.28525e+007	          95	
 43	  3	              [E.S1]	           0	
 44	  3	               [E.P]	           0	
 45	  3	                r(S)	           0	
 46	  3	              offset	     1.10351	     -INFTY	     +INFTY	    0.736772	     5329.27	2.82401e+006	      617189	    -10447.9	     10449.3	          95	
 47	  3	               delay	           0	
 48	  3	          incubation	           0	
 49	  3	                [E]i	           0	
 50	  3	                [S]i	           0	
 51	  3	             [E.S1]i	           0	
 52	  3	              [E.P]i	           0	
 53	  3	            dilution	           0	
 54	  3	           intensive	           0	
 55	  4	                time	           0	
 56	  4	             restart	           0	
 57	  4	                 [E]	           1	    0.909091	         1.1	         1.1	1.45712e+007	5.17171e+009	1.13028e+009	-2.85683e+007	2.85683e+007	          95	
 58	  4	                 [S]	           2	     1.81818	         2.2	     1.82162	2.41302e+007	5.17171e+009	1.13028e+009	-4.73097e+007	4.73097e+007	          95	
 59	  4	              [E.S1]	           0	
 60	  4	               [E.P]	           0	
 61	  4	                r(S)	           0	
 62	  4	              offset	     1.07103	     -INFTY	     +INFTY	    0.694352	     5329.25	2.99653e+006	      654893	    -10447.9	     10449.3	          95	
 63	  4	               delay	           0	
 64	  4	          incubation	           0	
 65	  4	                [E]i	           0	
 66	  4	                [S]i	           0	
 67	  4	             [E.S1]i	           0	
 68	  4	              [E.P]i	           0	
 69	  4	            dilution	           0	
 70	  4	           intensive	           0	
 71	  5	                time	           0	
 72	  5	             restart	           0	
 73	  5	                 [E]	           1	    0.909091	         1.1	    0.909091	1.20423e+007	5.17171e+009	1.13028e+009	-2.36102e+007	2.36102e+007	          95	
 74	  5	                 [S]	         2.5	     2.27273	        2.75	     2.27273	3.01057e+007	5.17171e+009	1.13028e+009	-5.90254e+007	5.90254e+007	          95	
 75	  5	              [E.S1]	           0	
 76	  5	               [E.P]	           0	
 77	  5	                r(S)	           0	
 78	  5	              offset	     1.02909	     -INFTY	     +INFTY	    0.707871	     4404.31	2.42916e+006	      530893	     -8634.4	     8635.81	          95	
 79	  5	               delay	           0	
 80	  5	          incubation	           0	
 81	  5	                [E]i	           0	
 82	  5	                [S]i	           0	
 83	  5	             [E.S1]i	           0	
 84	  5	              [E.P]i	           0	
 85	  5	            dilution	           0	
 86	  5	           intensive	           0	
 87	  6	                time	           0	
 88	  6	             restart	           0	
 89	  6	                 [E]	           1	    0.909091	         1.1	    0.909091	1.20423e+007	5.17171e+009	1.13028e+009	-2.36102e+007	2.36102e+007	          95	
 90	  6	                 [S]	           3	     2.72727	         3.3	     2.72727	3.61269e+007	5.17171e+009	1.13028e+009	-7.08305e+007	7.08305e+007	          95	
 91	  6	              [E.S1]	           0	
 92	  6	               [E.P]	           0	
 93	  6	                r(S)	           0	
 94	  6	              offset	     0.97752	     -INFTY	     +INFTY	    0.668962	     4404.32	2.57045e+006	      561772	    -8634.46	     8635.79	          95	
 95	  6	               delay	           0	
 96	  6	          incubation	           0	
 97	  6	                [E]i	           0	
 98	  6	                [S]i	           0	
 99	  6	             [E.S1]i	           0	
100	  6	              [E.P]i	           0	
101	  6	            dilution	           0	
102	  6	           intensive	           0	
103	  7	                time	           0	
104	  7	             restart	           0	
105	  7	                 [E]	           1	    0.909091	         1.1	    0.909091	1.20423e+007	5.17171e+009	1.13028e+009	-2.36102e+007	2.36102e+007	          95	
106	  7	                 [S]	           4	     3.63636	         4.4	     3.63636	4.81692e+007	5.17171e+009	1.13028e+009	-9.44407e+007	9.44407e+007	          95	
107	  7	              [E.S1]	           0	
108	  7	               [E.P]	           0	
109	  7	                r(S)	           0	
110	  7	              offset	     0.95791	     -INFTY	     +INFTY	    0.634784	     4404.34	2.70886e+006	      592022	    -8634.53	      8635.8	          95	
111	  7	               delay	           0	
112	  7	          incubation	           0	
113	  7	                [E]i	           0	
114	  7	                [S]i	           0	
115	  7	             [E.S1]i	           0	
116	  7	              [E.P]i	           0	
117	  7	            dilution	           0	
118	  7	           intensive	           0	
