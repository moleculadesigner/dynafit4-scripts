
===================================================
Least squares Minimization - Trust Region Algorithm
===================================================

Optimized parameters:

     1 	 k.1
     2 	 k.-1
     3 	 k.cat
     4 	 r(E)
     5 	 r(E.S1)
     6 	 r(E.P)
     7 	 [E]#1
     8 	 [S]#1
     9 	 offset#1
    10 	 [E]#2
    11 	 [S]#2
    12 	 offset#2
    13 	 [E]#3
    14 	 [S]#3
    15 	 offset#3
    16 	 [E]#4
    17 	 [S]#4
    18 	 offset#4
    19 	 [E]#5
    20 	 [S]#5
    21 	 offset#5
    22 	 [E]#6
    23 	 [S]#6
    24 	 offset#6
    25 	 [E]#7
    26 	 [S]#7
    27 	 offset#7

:: NL2SOL :: DNLAFB
:: Analytic Jacobian; Constrained

________________________________________________________________________

:: Initial Estimate

     i	     Param p(i)	            Min	            Max
     1	        3384.45	         1e-006	         1e+006
     2	        8963.85	         1e-012	         1e+006
     3	        3.04002	         1e-006	         1e+006
     4	           0.31	       3.1e-007	         310000
     5	           0.22	       2.2e-007	         220000
     6	           0.25	       2.5e-007	         250000
     7	              1	       0.909091	            1.1
     8	            0.5	       0.454545	           0.55
     9	         1.1793	       -10.6137	        12.9723
    10	              1	       0.909091	            1.1
    11	              1	       0.909091	            1.1
    12	        1.14325	       -10.6497	        12.9363
    13	              1	       0.909091	            1.1
    14	            1.5	        1.36364	           1.65
    15	        1.10351	       -10.6895	        12.8965
    16	              1	       0.909091	            1.1
    17	              2	        1.81818	            2.2
    18	        1.07103	        -10.722	         12.864
    19	              1	       0.909091	            1.1
    20	            2.5	        2.27273	           2.75
    21	        1.02909	       -10.7639	        12.8221
    22	              1	       0.909091	            1.1
    23	              3	        2.72727	            3.3
    24	        0.97752	       -10.8155	        12.7705
    25	              1	       0.909091	            1.1
    26	              4	        3.63636	            4.4
    27	        0.95791	       -10.8351	        12.7509

:: Iterations

     i	 FEval	   F(i) = SSQ	   Param( 1)	   Param( 2)	   Param( 3)	   Param( 4)	   Param( 5)	   Param( 6)	   Param( 7)	   Param( 8)	   Param( 9)	   Param(10)	   Param(11)	   Param(12)	   Param(13)	   Param(14)	   Param(15)	   Param(16)	   Param(17)	   Param(18)	   Param(19)	   Param(20)	   Param(21)	   Param(22)	   Param(23)	   Param(24)	   Param(25)	   Param(26)	   Param(27)	
     0	     1	       370.65	       3384.4	       8963.8	         3.04	         0.31	         0.22	         0.25	            1	          0.5	       1.1793	            1	            1	       1.1433	            1	          1.5	       1.1035	            1	            2	        1.071	            1	          2.5	       1.0291	            1	            3	      0.97752	            1	            4	      0.95791	
     1	     2	       84.887	       3384.5	       8963.8	       4.7466	      0.30457	       0.1265	       0.0137	       1.0652	      0.50399	       1.1404	       1.0138	       1.0112	       1.1204	      0.98272	       1.5712	       1.0827	      0.94415	       2.0771	       1.0525	      0.91771	       2.5716	       1.0176	      0.90909	       3.0739	      0.97737	      0.92407	       4.0541	      0.94361	
     2	     3	       27.618	       3384.5	       8963.8	       6.6333	      0.19319	     0.088432	    0.0049004	       1.0972	      0.51351	       1.1263	      0.92068	          1.1	       1.1393	      0.98104	       1.5251	       1.0676	      0.94037	       2.0142	        1.032	      0.91328	       2.4894	      0.99396	      0.90909	       3.0381	      0.95477	      0.93409	       3.9563	      0.91991	
     3	     4	       4.9045	       3384.6	       8963.8	       8.4739	      0.10248	     0.047912	     2.5e-007	          1.1	      0.51801	       1.1397	      0.90909	          1.1	       1.1102	      0.95465	       1.5328	       1.0525	      0.90944	        2.022	       1.0097	      0.91226	       2.5895	      0.95932	      0.90909	       3.1453	      0.92706	      0.93364	       4.0346	      0.89241	
     4	     5	       1.5021	       3384.6	       8963.8	       10.278	     0.081138	     0.018282	     2.5e-007	          1.1	      0.51801	       1.1232	      0.90909	          1.1	       1.0972	      0.92735	       1.5328	       1.0504	      0.90944	        2.022	        1.008	      0.91226	       2.5895	       0.9652	      0.90909	       3.1453	      0.92731	      0.93364	       4.2693	      0.89326	
     5	     6	      0.58381	       3384.6	       8963.8	       12.109	     0.056847	     0.020485	     2.5e-007	          1.1	      0.51801	       1.1222	      0.90909	          1.1	        1.096	      0.92735	       1.5328	       1.0489	      0.90944	        2.022	        1.007	      0.91226	       2.5895	      0.96554	      0.90909	       3.1453	      0.92693	      0.94006	       4.2693	      0.89287	
     6	     7	      0.35953	       3384.6	       8963.8	       13.483	     0.044038	     0.024721	     2.5e-007	          1.1	      0.51801	        1.122	      0.90909	          1.1	       1.0956	      0.92735	       1.5328	       1.0484	      0.90944	        2.022	       1.0066	      0.91226	       2.5895	      0.96534	      0.90909	       3.1453	      0.92661	      0.92894	       4.2693	      0.89261	
     7	     8	      0.29983	       3384.6	       8963.8	       14.542	     0.037962	     0.026936	     2.5e-007	          1.1	      0.51801	       1.1218	      0.90909	          1.1	       1.0954	      0.92735	       1.5328	       1.0481	      0.90944	        2.022	       1.0064	      0.91226	       2.5895	      0.96522	      0.90909	       3.1453	      0.92646	      0.92894	       4.2693	      0.89232	
     8	     9	      0.27559	       3384.6	       8963.8	       15.626	     0.034089	     0.033586	     2.5e-007	          1.1	      0.51801	       1.1215	      0.90909	          1.1	       1.0953	      0.92735	       1.5328	        1.048	      0.90944	        2.022	       1.0064	      0.91226	       2.5895	      0.96518	      0.90909	       3.1453	      0.92641	      0.92894	       4.2693	      0.89221	
     9	    12	      0.23246	       3384.6	       8963.8	       21.216	     0.035636	     0.035069	     2.5e-007	          1.1	      0.51801	       1.1203	      0.90909	          1.1	       1.0961	      0.92631	       1.5328	        1.049	      0.90944	        2.022	       1.0072	      0.91226	       2.5895	      0.96596	      0.90909	       3.1453	      0.92712	      0.92894	       4.2693	      0.89284	
    10	    14	      0.18851	       3384.7	       8963.7	       31.956	      0.04409	     0.033818	     2.5e-007	          1.1	      0.54194	       1.1166	      0.90909	          1.1	       1.0964	      0.92631	       1.5328	       1.0493	      0.90944	        2.022	       1.0075	      0.91226	       2.5895	      0.96623	      0.90909	       3.1453	      0.92742	      0.92894	       4.2693	      0.89319	
    11	    15	      0.15058	       3384.8	       8963.7	       44.892	     0.050337	     0.060712	     2.5e-007	          1.1	      0.54194	       1.1115	      0.90909	          1.1	       1.0958	      0.92631	       1.5328	       1.0487	      0.90944	        2.022	       1.0069	      0.91226	       2.5895	      0.96577	      0.90909	       3.1453	        0.927	      0.92894	       4.2693	      0.89282	
    12	    16	      0.14308	       3385.5	       8963.5	       58.306	     0.057541	     0.064167	     2.5e-007	          1.1	         0.55	         1.11	      0.90909	          1.1	       1.0962	      0.98492	         1.65	       1.0493	      0.97704	          2.2	       1.0073	       0.9179	       2.6729	      0.96627	      0.90909	       3.1453	      0.92748	      0.93637	       4.2693	      0.89322	
    13	    17	      0.14023	       3398.9	       8957.7	       58.967	     0.058933	     0.068471	     2.5e-007	          1.1	         0.55	       1.1075	      0.90909	          1.1	       1.0959	      0.96203	         1.65	       1.0489	      0.97288	          2.2	       1.0071	      0.91684	       2.7132	      0.96616	      0.90909	       3.1453	      0.92737	      0.93637	       4.2693	      0.89321	
    14	    18	      0.14004	       3413.1	       8951.9	       59.701	     0.060107	     0.069258	     2.5e-007	          1.1	         0.55	       1.1067	      0.90909	          1.1	       1.0958	      0.96341	         1.65	       1.0488	      0.97288	          2.2	       1.0071	      0.91713	       2.7129	      0.96615	      0.90909	       3.1453	      0.92736	      0.93637	       4.2693	       0.8932	
    15	    20	      0.13994	       3418.9	       8949.6	       60.291	     0.061153	     0.069017	     2.5e-007	       1.0954	         0.55	       1.1063	      0.90909	          1.1	       1.0958	      0.96236	         1.65	       1.0488	      0.97248	          2.2	       1.0071	      0.90983	       2.7252	       0.9662	      0.90909	       3.0308	      0.92732	      0.93323	       4.1198	      0.89324	
    16	    21	      0.13973	       3425.1	       8947.2	       61.901	     0.062476	     0.066231	     2.5e-007	       1.0876	         0.55	        1.106	      0.90909	          1.1	       1.0959	       0.9741	         1.65	       1.0488	      0.98907	          2.2	       1.0071	      0.92515	       2.5827	      0.96614	      0.90909	       2.7722	      0.92727	      0.93308	       3.6364	      0.89315	
    17	    22	      0.13971	       3440.2	       8941.4	       62.191	     0.062844	      0.06562	     2.5e-007	        1.061	         0.55	       1.1075	      0.90909	          1.1	       1.0959	      0.97667	         1.65	       1.0488	      0.98306	       2.1337	       1.0071	      0.92832	       2.5971	      0.96615	      0.90909	       2.8017	       0.9273	      0.93614	       3.6364	      0.89314	
    18	    26	      0.13926	       4064.6	       8691.6	       56.171	     0.064222	     0.062053	     2.5e-007	       1.0507	         0.55	       1.1075	      0.90909	          1.1	        1.096	      0.98482	         1.65	        1.049	      0.98007	       2.0695	       1.0071	      0.92357	       2.4987	      0.96616	      0.90909	       2.7319	      0.92731	       0.9492	       3.6364	      0.89315	
    19	    28	      0.13913	       4480.9	       8498.4	       51.422	     0.065155	     0.061366	     2.5e-007	      0.99655	         0.55	       1.1105	      0.90909	          1.1	       1.0962	      0.98587	         1.65	        1.049	        0.982	       2.1194	       1.0071	      0.93243	       2.6717	      0.96617	      0.90909	       2.8875	      0.92753	      0.90909	       3.6364	      0.89303	
    20	    29	      0.13871	       4887.9	       8286.4	       48.321	     0.064851	     0.062337	     2.5e-007	       1.0635	         0.55	       1.1064	      0.90909	          1.1	        1.096	       0.9766	         1.65	       1.0489	      0.98421	        2.187	       1.0071	      0.92962	       2.7152	      0.96614	      0.90909	       2.9699	       0.9273	      0.91884	       3.6364	      0.89306	
    21	    30	      0.13849	       5290.6	         8066	       45.877	     0.063922	     0.062932	     2.5e-007	      0.98701	         0.55	       1.1117	      0.90909	          1.1	       1.0961	      0.98123	         1.65	       1.0489	       0.9812	       2.1706	       1.0071	      0.92518	       2.7185	      0.96614	      0.90909	       3.0185	      0.92727	      0.91276	       3.6364	      0.89305	
    22	    31	      0.13828	       5669.3	       7807.9	       43.682	      0.06477	     0.062009	     2.5e-007	      0.98907	         0.55	       1.1111	      0.90909	          1.1	       1.0961	      0.97524	       1.6144	       1.0489	      0.97873	       2.1536	       1.0071	       0.9263	         2.75	      0.96614	      0.90909	       3.0713	      0.92729	      0.90909	       3.6364	      0.89301	
    23	    32	      0.13793	       6973.4	       6590.7	       35.539	     0.066622	     0.060383	     2.5e-007	      0.93801	         0.55	       1.1136	      0.90909	          1.1	       1.0963	      0.96953	       1.5887	       1.0489	      0.97585	       2.1787	       1.0071	      0.91484	         2.75	       0.9661	      0.90909	          3.3	      0.92728	      0.90909	       4.0713	        0.893	
    24	    33	      0.13753	       8198.4	       5922.9	       34.362	     0.067926	     0.060731	     2.5e-007	      0.90909	         0.55	       1.1155	      0.90909	          1.1	       1.0965	      0.95774	       1.4728	       1.0489	      0.97726	        2.161	       1.0071	      0.91083	         2.75	       0.9661	      0.90909	          3.3	      0.92725	      0.90909	          4.4	      0.89294	
    25	    34	      0.13746	       7382.8	       4260.3	       32.015	     0.070939	      0.05897	     2.5e-007	      0.90909	         0.55	       1.1142	      0.90909	          1.1	       1.0964	      0.95636	       1.4614	       1.0489	      0.96862	       2.0363	       1.0071	      0.91787	         2.75	       0.9661	      0.90909	          3.3	       0.9272	      0.90909	          4.4	      0.89296	
    26	    35	      0.13743	       7382.8	       4260.3	       32.012	     0.071619	     0.059199	     2.5e-007	      0.90909	         0.55	        1.114	      0.90909	          1.1	       1.0965	      0.95626	       1.4539	       1.0489	      0.96824	       2.0395	       1.0071	      0.91607	         2.75	      0.96608	      0.90909	          3.3	      0.92721	      0.90909	          4.4	      0.89296	
    27	    36	       0.1374	       8717.1	       5676.8	       32.694	     0.070018	     0.058988	     2.5e-007	      0.90909	         0.55	       1.1144	      0.90909	          1.1	       1.0964	      0.96635	       1.5297	       1.0489	      0.98111	        2.177	       1.0071	      0.91638	         2.75	      0.96606	      0.90909	          3.3	       0.9272	      0.90909	          4.4	      0.89294	
    28	    37	      0.13738	       9511.7	       5796.7	       32.093	      0.07153	     0.058751	     2.5e-007	      0.90909	         0.55	       1.1139	      0.90909	          1.1	       1.0964	      0.95587	       1.4643	       1.0488	      0.97438	       2.0944	       1.0071	      0.91733	         2.75	      0.96607	      0.90909	          3.3	       0.9272	      0.90909	          4.4	      0.89294	
    29	    38	      0.13736	        11133	       6881.4	       32.183	     0.071729	     0.058567	     2.5e-007	      0.90909	         0.55	       1.1138	      0.90909	          1.1	       1.0964	       0.9635	       1.5138	       1.0489	      0.97925	       2.1462	       1.0071	      0.91783	         2.75	      0.96607	      0.90909	          3.3	       0.9272	      0.90909	          4.4	      0.89294	
    30	    40	      0.13736	        11146	       6874.6	       32.261	      0.07191	     0.058534	  8.1572e-006	      0.90909	         0.55	       1.1138	      0.90909	          1.1	       1.0964	      0.95708	       1.4755	       1.0488	       0.9765	       2.1113	       1.0071	      0.91889	         2.75	      0.96606	      0.90909	          3.3	      0.92721	      0.90909	          4.4	      0.89295	
    31	    41	      0.13736	        11275	         6930	       32.153	     0.072138	     0.058314	     2.5e-007	      0.90909	         0.55	       1.1137	      0.90909	          1.1	       1.0964	      0.95975	       1.4912	       1.0489	      0.97851	        2.127	       1.0071	      0.91954	         2.75	      0.96607	      0.90909	          3.3	      0.92721	      0.91223	          4.4	      0.89294	
    32	    44	      0.13734	        13387	       8205.9	       32.166	     0.072637	     0.058116	     2.5e-007	      0.90909	         0.55	       1.1135	      0.90909	          1.1	       1.0964	      0.95984	       1.4923	       1.0489	      0.97986	       2.1308	       1.0071	      0.92103	         2.75	      0.96606	      0.90909	          3.3	      0.92722	      0.91491	          4.4	      0.89293	
    33	    45	      0.13729	        19324	        11347	       31.779	     0.074847	     0.057138	     2.5e-007	      0.90909	         0.55	       1.1127	      0.90909	          1.1	       1.0964	      0.95691	       1.4727	       1.0489	      0.98363	       2.1168	       1.0071	      0.92865	         2.75	      0.96606	      0.90909	          3.3	      0.92725	      0.92468	          4.4	      0.89293	
    34	    46	      0.13725	        27322	        15813	       31.779	     0.075897	     0.056564	     2.5e-007	      0.90909	         0.55	       1.1123	      0.90909	          1.1	       1.0964	      0.95913	       1.4837	       1.0489	      0.98736	       2.1271	       1.0071	       0.9327	         2.75	      0.96606	      0.90909	          3.3	      0.92726	      0.93057	          4.4	      0.89293	
    35	    47	      0.13718	        36057	        19947	       31.917	     0.090342	     0.041788	     2.5e-007	      0.90909	         0.55	       1.1072	      0.90909	          1.1	       1.0964	       1.0032	       1.5058	       1.0489	       1.0826	       2.1433	       1.0071	       1.0565	         2.75	      0.96607	       1.0539	          3.3	      0.92725	          1.1	          4.4	      0.89294	
    36	    48	      0.13708	        38353	        21596	       32.293	     0.089218	     0.043474	     2.5e-007	      0.90909	         0.55	       1.1076	      0.90909	          1.1	       1.0964	      0.99948	       1.5065	       1.0488	       1.0791	       2.1547	       1.0071	       1.0538	         2.75	      0.96607	       1.0564	          3.3	      0.92726	          1.1	          4.4	      0.89297	
    37	    49	      0.13707	        40384	        22753	        32.28	     0.089308	     0.043502	     2.5e-007	      0.90909	         0.55	       1.1076	      0.90909	          1.1	       1.0964	      0.99706	       1.4995	       1.0488	       1.0754	       2.1382	       1.0071	       1.0535	         2.75	      0.96607	       1.0558	          3.3	      0.92726	          1.1	          4.4	      0.89296	
    38	    52	      0.13701	        58242	        31449	       31.934	      0.09789	     0.048675	      0.00588	      0.90909	         0.55	       1.1012	      0.90909	          1.1	        1.091	      0.99953	       1.4994	        1.043	       1.0805	       2.1267	       1.0007	       1.0632	         2.75	      0.95987	       1.0671	          3.3	      0.92104	          1.1	          4.4	      0.88651	
    39	    53	      0.13697	        75886	        39135	       31.596	      0.10614	     0.055307	     0.012719	      0.90909	         0.55	       1.0945	      0.90909	          1.1	       1.0848	      0.99621	       1.4839	       1.0361	       1.0806	        2.118	      0.99332	       1.0654	         2.75	      0.95253	       1.0698	          3.3	      0.91366	          1.1	          4.4	      0.87898	
    40	    54	      0.13697	        75884	        39141	       31.607	      0.10306	     0.053683	     0.010539	      0.90909	         0.55	       1.0968	      0.90909	          1.1	       1.0868	      0.99351	       1.4826	       1.0384	       1.0843	       2.1597	      0.99563	       1.0581	         2.75	      0.95493	       1.0604	          3.3	      0.91609	          1.1	          4.4	      0.88138	
    41	    55	      0.13696	        87554	        47016	       32.044	     0.091414	     0.043161	  7.6731e-006	      0.90909	         0.55	       1.1068	      0.90909	          1.1	       1.0964	      0.99685	       1.4953	       1.0488	        1.078	       2.1334	       1.0071	       1.0578	         2.75	      0.96606	       1.0606	          3.3	      0.92725	          1.1	          4.4	      0.89297	
    42	    56	      0.13695	        87716	        46934	       31.949	     0.091551	     0.043145	     2.5e-007	      0.90909	         0.55	       1.1068	      0.90909	          1.1	       1.0964	      0.99482	       1.4881	       1.0488	       1.0805	       2.1435	       1.0071	       1.0584	         2.75	      0.96607	       1.0613	          3.3	      0.92725	          1.1	          4.4	      0.89298	
    43	    57	      0.13695	  1.0581e+005	        55207	       31.714	     0.092617	     0.043061	     2.5e-007	      0.90909	         0.55	       1.1064	      0.90909	          1.1	       1.0964	      0.99497	       1.4873	       1.0488	       1.0805	       2.1392	       1.0071	       1.0592	         2.75	      0.96606	       1.0619	          3.3	      0.92725	          1.1	          4.4	      0.89297	
    44	    58	      0.13694	  1.2331e+005	        64681	       31.809	     0.092363	     0.043061	     2.5e-007	      0.90909	         0.55	       1.1065	      0.90909	          1.1	       1.0964	       0.9946	       1.4864	       1.0488	       1.0806	       2.1409	       1.0071	        1.059	         2.75	      0.96607	       1.0618	          3.3	      0.92725	          1.1	          4.4	      0.89297	
    45	    59	      0.13694	  1.3031e+005	        68014	       31.747	     0.092538	     0.043034	     2.5e-007	      0.90909	         0.55	       1.1064	      0.90909	          1.1	       1.0964	      0.99444	       1.4857	       1.0488	       1.0806	       2.1399	       1.0071	       1.0592	         2.75	      0.96606	        1.062	          3.3	      0.92725	          1.1	          4.4	      0.89297	
    46	    60	      0.13694	  1.4923e+005	        77903	       31.755	     0.092544	     0.043036	     2.5e-007	      0.90909	         0.55	       1.1064	      0.90909	          1.1	       1.0964	      0.99446	       1.4858	       1.0488	       1.0806	       2.1402	       1.0071	       1.0592	         2.75	      0.96606	        1.062	          3.3	      0.92725	          1.1	          4.4	      0.89297	
    47	    61	      0.13694	  1.5599e+005	        81020	        31.71	     0.094435	      0.04476	    0.0017384	      0.90909	         0.55	       1.1048	      0.90909	          1.1	       1.0948	      0.99412	       1.4844	       1.0471	       1.0807	       2.1395	       1.0052	       1.0594	         2.75	      0.96422	       1.0622	          3.3	       0.9254	          1.1	          4.4	      0.89106	
    48	    62	      0.13694	  1.6622e+005	        86682	       31.744	     0.094517	     0.044966	    0.0019415	      0.90909	         0.55	       1.1046	      0.90909	          1.1	       1.0946	      0.99445	       1.4857	       1.0469	       1.0808	       2.1407	        1.005	       1.0593	         2.75	      0.96401	       1.0621	          3.3	      0.92519	          1.1	          4.4	      0.89084	
    49	    63	      0.13694	  1.7088e+005	        88981	       31.733	     0.092627	     0.043026	     2.5e-007	      0.90909	         0.55	       1.1064	      0.90909	          1.1	       1.0964	      0.99419	       1.4848	       1.0488	       1.0806	       2.1399	       1.0071	       1.0593	         2.75	      0.96606	       1.0621	          3.3	      0.92725	          1.1	          4.4	      0.89297	
    50	    64	      0.13694	  1.8926e+005	        98663	       31.741	     0.092595	     0.043026	     2.5e-007	      0.90909	         0.55	       1.1064	      0.90909	          1.1	       1.0964	      0.99438	       1.4855	       1.0488	       1.0807	       2.1406	       1.0071	       1.0593	         2.75	      0.96606	       1.0621	          3.3	      0.92725	          1.1	          4.4	      0.89297	
    51	    65	      0.13694	  2.3625e+005	  1.2307e+005	       31.736	     0.092631	     0.043026	     2.5e-007	      0.90909	         0.55	       1.1064	      0.90909	          1.1	       1.0964	      0.99418	       1.4848	       1.0488	       1.0806	       2.1397	       1.0071	       1.0593	         2.75	      0.96606	       1.0621	          3.3	      0.92725	          1.1	          4.4	      0.89297	
    52	    66	      0.13694	  2.9676e+005	  1.5453e+005	       31.732	     0.092641	     0.043024	     2.5e-007	      0.90909	         0.55	       1.1064	      0.90909	          1.1	       1.0964	      0.99427	       1.4851	       1.0488	       1.0807	       2.1401	       1.0071	       1.0593	         2.75	      0.96606	       1.0621	          3.3	      0.92725	          1.1	          4.4	      0.89297	
    53	    68	      0.13694	  2.9676e+005	  1.5453e+005	       31.733	     0.092636	     0.043024	     2.5e-007	      0.90909	         0.55	       1.1064	      0.90909	          1.1	       1.0964	      0.99421	       1.4849	       1.0488	       1.0806	       2.1399	       1.0071	       1.0593	         2.75	      0.96606	       1.0621	          3.3	      0.92725	          1.1	          4.4	      0.89297	
    54	    70	      0.13694	  2.9676e+005	  1.5453e+005	       31.732	     0.092636	     0.043024	     2.5e-007	      0.90909	         0.55	       1.1064	      0.90909	          1.1	       1.0964	      0.99424	        1.485	       1.0488	       1.0807	         2.14	       1.0071	       1.0593	         2.75	      0.96606	       1.0621	          3.3	      0.92725	          1.1	          4.4	      0.89297	
    55	    71	      0.13694	  2.9676e+005	  1.5453e+005	       31.733	     0.092637	     0.043026	  1.6934e-006	      0.90909	         0.55	       1.1064	      0.90909	          1.1	       1.0964	      0.99424	        1.485	       1.0488	       1.0807	         2.14	       1.0071	       1.0593	         2.75	      0.96606	       1.0621	          3.3	      0.92725	          1.1	          4.4	      0.89297	

:: Singular convergence

       Squares	       0.136936
     Func Eval	             71
     Grad Eval	             55

:: Nonlinear parameters

     i	   Final p(i)	  Gradient(i)
     1	       296756	  1.2638e-012
     2	       154533	 -2.2986e-012
     3	      31.7326	  6.2375e-009
     4	    0.0926375	 -4.5374e-006
     5	    0.0430256	 -5.7648e-006
     6	 1.69336e-006	 -1.5114e-005
     7	     0.909091	    0.0017938
     8	         0.55	   -0.0082668
     9	      1.10637	 -3.5293e-006
    10	     0.909091	    0.0080406
    11	          1.1	   -0.0031089
    12	      1.09636	 -6.3331e-006
    13	     0.994237	 -3.5758e-007
    14	      1.48499	  2.1812e-007
    15	      1.04884	 -4.7223e-006
    16	      1.08066	 -1.1027e-007
    17	      2.13996	  3.7437e-008
    18	      1.00706	 -3.4605e-006
    19	      1.05932	  7.2968e-008
    20	         2.75	 -9.1004e-005
    21	     0.966063	 -2.3789e-006
    22	      1.06212	  4.3127e-008
    23	          3.3	 -3.8579e-005
    24	     0.927249	 -2.0012e-006
    25	          1.1	  -0.00051483
    26	          4.4	 -6.7997e-006
    27	     0.892972	 -1.4718e-006
