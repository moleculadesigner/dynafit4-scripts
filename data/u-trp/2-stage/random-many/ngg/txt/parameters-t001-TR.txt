
PARAMETERS AND CONFIDENCE INTERVALS

NO.	SET	           PARAMETER	     INITIAL	    LIMIT-LO	    LIMIT-HI	         FIT	      STDERR	         CV%	      F(SDI)	   CONLIN-LO	   CONLIN-HI	   CONLIN-P%	

  1	  1	                time	           0	
  2	  1	             restart	           0	
  3	  *	                 k.1	    0.189218	      1e-006	      1e+006	     32.6085	      782736	9.37167e+006	3.97195e+006	-1.5346e+006	1.53467e+006	          95	
  4	  *	                k.-1	   0.0800549	      1e-012	      1e+006	1.58918e-009	    0.340252	8.35909e+010	 3.5428e+010	     -0.6671	      0.6671	          95	
  5	  *	               k.cat	     146.928	      1e-006	      1e+006	     28.4511	     3.38874	      46.502	     19.7087	     21.8071	     35.0951	          95	
  6	  1	                 [E]	           1	    0.909091	         1.1	    0.947279	     22738.6	9.37169e+006	3.97196e+006	    -44580.4	     44582.3	          95	
  7	  1	                 [S]	         0.5	    0.454545	        0.55	        0.55	     13202.2	9.37168e+006	3.97195e+006	    -25883.8	     25884.9	          95	
  8	  1	              [E.S1]	           0	
  9	  1	               [E.P]	           0	
 10	  *	                r(E)	        0.31	    3.1e-007	      310000	    0.158717	 2.2968e+007	5.64979e+010	2.39452e+010	-4.50312e+007	4.50312e+007	          95	
 11	  1	                r(S)	           0	
 12	  *	             r(E.S1)	        0.22	    2.2e-007	      220000	    0.146856	 2.2968e+007	6.10611e+010	2.58792e+010	-4.50312e+007	4.50312e+007	          95	
 13	  *	              r(E.P)	        0.25	    2.5e-007	      250000	   0.0992422	 2.2968e+007	9.03565e+010	3.82954e+010	-4.50312e+007	4.50312e+007	          95	
 14	  1	              offset	      1.1793	     -INFTY	     +INFTY	     1.02192	2.17571e+007	8.31222e+009	3.52293e+009	-4.26572e+007	4.26572e+007	          95	
 15	  1	               delay	           0	
 16	  1	          incubation	           0	
 17	  1	                [E]i	           0	
 18	  1	                [S]i	           0	
 19	  1	             [E.S1]i	           0	
 20	  1	              [E.P]i	           0	
 21	  1	            dilution	           0	
 22	  1	           intensive	           0	
 23	  2	                time	           0	
 24	  2	             restart	           0	
 25	  2	                 [E]	           1	    0.909091	         1.1	         1.1	     26404.5	9.37168e+006	3.97195e+006	    -51767.6	     51769.8	          95	
 26	  2	                 [S]	           1	    0.909091	         1.1	    0.909091	     21821.9	9.37168e+006	3.97195e+006	    -42783.2	       42785	          95	
 27	  2	              [E.S1]	           0	
 28	  2	               [E.P]	           0	
 29	  2	                r(S)	           0	
 30	  2	              offset	     1.14325	     -INFTY	     +INFTY	    0.975925	2.52648e+007	1.01072e+010	 4.2837e+009	-4.95344e+007	4.95344e+007	          95	
 31	  2	               delay	           0	
 32	  2	          incubation	           0	
 33	  2	                [E]i	           0	
 34	  2	                [S]i	           0	
 35	  2	             [E.S1]i	           0	
 36	  2	              [E.P]i	           0	
 37	  2	            dilution	           0	
 38	  2	           intensive	           0	
 39	  3	                time	           0	
 40	  3	             restart	           0	
 41	  3	                 [E]	           1	    0.909091	         1.1	     0.94774	     22749.6	9.37168e+006	3.97195e+006	    -44602.1	       44604	          95	
 42	  3	                 [S]	         1.5	     1.36364	        1.65	     1.50369	     36094.8	9.37169e+006	3.97196e+006	    -70766.2	     70769.2	          95	
 43	  3	              [E.S1]	           0	
 44	  3	               [E.P]	           0	
 45	  3	                r(S)	           0	
 46	  3	              offset	     1.10351	     -INFTY	     +INFTY	    0.954869	2.17677e+007	8.90024e+009	3.77215e+009	-4.26779e+007	4.26779e+007	          95	
 47	  3	               delay	           0	
 48	  3	          incubation	           0	
 49	  3	                [E]i	           0	
 50	  3	                [S]i	           0	
 51	  3	             [E.S1]i	           0	
 52	  3	              [E.P]i	           0	
 53	  3	            dilution	           0	
 54	  3	           intensive	           0	
 55	  4	                time	           0	
 56	  4	             restart	           0	
 57	  4	                 [E]	           1	    0.909091	         1.1	    0.951464	       22839	9.37168e+006	3.97195e+006	    -44777.3	     44779.2	          95	
 58	  4	                 [S]	           2	     1.81818	         2.2	     2.01171	     48289.4	9.37168e+006	3.97196e+006	    -94674.4	     94678.4	          95	
 59	  4	              [E.S1]	           0	
 60	  4	               [E.P]	           0	
 61	  4	                r(S)	           0	
 62	  4	              offset	     1.07103	     -INFTY	     +INFTY	    0.912641	2.18533e+007	9.34865e+009	3.96219e+009	-4.28456e+007	4.28456e+007	          95	
 63	  4	               delay	           0	
 64	  4	          incubation	           0	
 65	  4	                [E]i	           0	
 66	  4	                [S]i	           0	
 67	  4	             [E.S1]i	           0	
 68	  4	              [E.P]i	           0	
 69	  4	            dilution	           0	
 70	  4	           intensive	           0	
 71	  5	                time	           0	
 72	  5	             restart	           0	
 73	  5	                 [E]	           1	    0.909091	         1.1	    0.915345	       21972	9.37168e+006	3.97195e+006	    -43077.5	     43079.3	          95	
 74	  5	                 [S]	         2.5	     2.27273	        2.75	        2.75	     66011.3	9.37169e+006	3.97196e+006	     -129419	      129425	          95	
 75	  5	              [E.S1]	           0	
 76	  5	               [E.P]	           0	
 77	  5	                r(S)	           0	
 78	  5	              offset	     1.02909	     -INFTY	     +INFTY	     0.87525	2.10237e+007	9.37797e+009	3.97462e+009	-4.12191e+007	4.12191e+007	          95	
 79	  5	               delay	           0	
 80	  5	          incubation	           0	
 81	  5	                [E]i	           0	
 82	  5	                [S]i	           0	
 83	  5	             [E.S1]i	           0	
 84	  5	              [E.P]i	           0	
 85	  5	            dilution	           0	
 86	  5	           intensive	           0	
 87	  6	                time	           0	
 88	  6	             restart	           0	
 89	  6	                 [E]	           1	    0.909091	         1.1	    0.909091	     21821.9	9.37168e+006	3.97195e+006	    -42783.2	       42785	          95	
 90	  6	                 [S]	           3	     2.72727	         3.3	     3.10395	     74507.7	9.37169e+006	3.97196e+006	     -146077	      146083	          95	
 91	  6	              [E.S1]	           0	
 92	  6	               [E.P]	           0	
 93	  6	                r(S)	           0	
 94	  6	              offset	     0.97752	     -INFTY	     +INFTY	    0.836968	  2.088e+007	9.73991e+009	4.12802e+009	-4.09375e+007	4.09375e+007	          95	
 95	  6	               delay	           0	
 96	  6	          incubation	           0	
 97	  6	                [E]i	           0	
 98	  6	                [S]i	           0	
 99	  6	             [E.S1]i	           0	
100	  6	              [E.P]i	           0	
101	  6	            dilution	           0	
102	  6	           intensive	           0	
103	  7	                time	           0	
104	  7	             restart	           0	
105	  7	                 [E]	           1	    0.909091	         1.1	    0.909091	     21821.9	9.37168e+006	3.97195e+006	    -42783.2	       42785	          95	
106	  7	                 [S]	           4	     3.63636	         4.4	     3.63741	     87312.9	 9.3717e+006	3.97196e+006	     -171182	      171190	          95	
107	  7	              [E.S1]	           0	
108	  7	               [E.P]	           0	
109	  7	                r(S)	           0	
110	  7	              offset	     0.95791	     -INFTY	     +INFTY	    0.802659	  2.088e+007	1.01562e+010	4.30447e+009	-4.09375e+007	4.09375e+007	          95	
111	  7	               delay	           0	
112	  7	          incubation	           0	
113	  7	                [E]i	           0	
114	  7	                [S]i	           0	
115	  7	             [E.S1]i	           0	
116	  7	              [E.P]i	           0	
117	  7	            dilution	           0	
118	  7	           intensive	           0	
