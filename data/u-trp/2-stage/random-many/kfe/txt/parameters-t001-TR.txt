
PARAMETERS AND CONFIDENCE INTERVALS

NO.	SET	           PARAMETER	     INITIAL	    LIMIT-LO	    LIMIT-HI	         FIT	      STDERR	         CV%	      F(SDI)	   CONLIN-LO	   CONLIN-HI	   CONLIN-P%	

  1	  1	                time	           0	
  2	  1	             restart	           0	
  3	  *	                 k.1	    0.384137	      1e-006	      1e+006	     6.10913	     13564.3	      866860	      543735	    -26588.1	     26600.3	          95	
  4	  *	                k.-1	7.68453e-005	      1e-012	      1e+006	5.21242e-010	   0.0471753	3.53352e+010	2.21639e+010	  -0.0924921	   0.0924921	          95	
  5	  *	               k.cat	   0.0516337	      1e-006	      1e+006	     1.94067	   0.0751586	     15.1203	     9.48414	     1.79331	     2.08802	          95	
  6	  1	                 [E]	           1	    0.909091	         1.1	         1.1	     2442.36	      866862	      543736	    -4787.41	     4789.61	          95	
  7	  1	                 [S]	         0.5	    0.454545	        0.55	        0.55	     1221.18	      866861	      543735	     -2393.7	      2394.8	          95	
  8	  1	              [E.S1]	           0	
  9	  1	               [E.P]	           0	
 10	  *	                r(E)	        0.31	    3.1e-007	      310000	    0.084943	     195.099	      896725	      562467	    -382.426	     382.596	          95	
 11	  1	                r(S)	           0	
 12	  *	             r(E.S1)	        0.22	    2.2e-007	      220000	2.20001e-007	     49.5549	8.79416e+010	 5.5161e+010	    -97.1575	     97.1575	          95	
 13	  *	              r(E.P)	        0.25	    2.5e-007	      250000	   0.0306672	     84.2949	1.07315e+006	      673127	    -165.238	       165.3	          95	
 14	  1	              offset	      1.1793	     -INFTY	     +INFTY	     1.07738	     54.5104	     19753.4	     12390.3	    -105.796	     107.951	          95	
 15	  1	               delay	           0	
 16	  1	          incubation	           0	
 17	  1	                [E]i	           0	
 18	  1	                [S]i	           0	
 19	  1	             [E.S1]i	           0	
 20	  1	              [E.P]i	           0	
 21	  1	            dilution	           0	
 22	  1	           intensive	           0	
 23	  2	                time	           0	
 24	  2	             restart	           0	
 25	  2	                 [E]	           1	    0.909091	         1.1	    0.909091	     2018.48	      866860	      543734	    -3956.53	     3958.34	          95	
 26	  2	                 [S]	           1	    0.909091	         1.1	         1.1	     2442.36	      866860	      543734	     -4787.4	      4789.6	          95	
 27	  2	              [E.S1]	           0	
 28	  2	               [E.P]	           0	
 29	  2	                r(S)	           0	
 30	  2	              offset	     1.14325	     -INFTY	     +INFTY	     1.06947	     45.0499	     16445.9	     10315.6	    -87.2556	     89.3945	          95	
 31	  2	               delay	           0	
 32	  2	          incubation	           0	
 33	  2	                [E]i	           0	
 34	  2	                [S]i	           0	
 35	  2	             [E.S1]i	           0	
 36	  2	              [E.P]i	           0	
 37	  2	            dilution	           0	
 38	  2	           intensive	           0	
 39	  3	                time	           0	
 40	  3	             restart	           0	
 41	  3	                 [E]	           1	    0.909091	         1.1	    0.969234	     2152.01	      866860	      543734	    -4218.28	     4220.22	          95	
 42	  3	                 [S]	         1.5	     1.36364	        1.65	        1.65	     3663.54	      866860	      543734	     -7181.1	      7184.4	          95	
 43	  3	              [E.S1]	           0	
 44	  3	               [E.P]	           0	
 45	  3	                r(S)	           0	
 46	  3	              offset	     1.10351	     -INFTY	     +INFTY	     1.02267	     48.0303	     18336.3	     11501.4	    -93.1457	      95.191	          95	
 47	  3	               delay	           0	
 48	  3	          incubation	           0	
 49	  3	                [E]i	           0	
 50	  3	                [S]i	           0	
 51	  3	             [E.S1]i	           0	
 52	  3	              [E.P]i	           0	
 53	  3	            dilution	           0	
 54	  3	           intensive	           0	
 55	  4	                time	           0	
 56	  4	             restart	           0	
 57	  4	                 [E]	           1	    0.909091	         1.1	     1.01244	     2247.95	      866860	      543734	    -4406.32	     4408.35	          95	
 58	  4	                 [S]	           2	     1.81818	         2.2	     2.16498	     4806.95	      866860	      543734	    -9422.37	      9426.7	          95	
 59	  4	              [E.S1]	           0	
 60	  4	               [E.P]	           0	
 61	  4	                r(S)	           0	
 62	  4	              offset	     1.07103	     -INFTY	     +INFTY	    0.980578	     50.1714	     19975.9	     12529.8	    -97.3857	     99.3469	          95	
 63	  4	               delay	           0	
 64	  4	          incubation	           0	
 65	  4	                [E]i	           0	
 66	  4	                [S]i	           0	
 67	  4	             [E.S1]i	           0	
 68	  4	              [E.P]i	           0	
 69	  4	            dilution	           0	
 70	  4	           intensive	           0	
 71	  5	                time	           0	
 72	  5	             restart	           0	
 73	  5	                 [E]	           1	    0.909091	         1.1	    0.952712	     2115.33	      866859	      543734	    -4146.37	     4148.27	          95	
 74	  5	                 [S]	         2.5	     2.27273	        2.75	     2.42957	     5394.44	      866860	      543734	    -10573.9	     10578.8	          95	
 75	  5	              [E.S1]	           0	
 76	  5	               [E.P]	           0	
 77	  5	                r(S)	           0	
 78	  5	              offset	     1.02909	     -INFTY	     +INFTY	    0.941445	     47.2115	     19578.8	     12280.7	    -91.6217	     93.5046	          95	
 79	  5	               delay	           0	
 80	  5	          incubation	           0	
 81	  5	                [E]i	           0	
 82	  5	                [S]i	           0	
 83	  5	             [E.S1]i	           0	
 84	  5	              [E.P]i	           0	
 85	  5	            dilution	           0	
 86	  5	           intensive	           0	
 87	  6	                time	           0	
 88	  6	             restart	           0	
 89	  6	                 [E]	           1	    0.909091	         1.1	    0.978268	     2172.07	      866860	      543734	     -4257.6	     4259.55	          95	
 90	  6	                 [S]	           3	     2.72727	         3.3	     2.73806	     6079.37	      866859	      543734	    -11916.5	       11922	          95	
 91	  6	              [E.S1]	           0	
 92	  6	               [E.P]	           0	
 93	  6	                r(S)	           0	
 94	  6	              offset	     0.97752	     -INFTY	     +INFTY	    0.902214	      48.478	     20978.2	     13158.5	    -94.1439	     95.9483	          95	
 95	  6	               delay	           0	
 96	  6	          incubation	           0	
 97	  6	                [E]i	           0	
 98	  6	                [S]i	           0	
 99	  6	             [E.S1]i	           0	
100	  6	              [E.P]i	           0	
101	  6	            dilution	           0	
102	  6	           intensive	           0	
103	  7	                time	           0	
104	  7	             restart	           0	
105	  7	                 [E]	           1	    0.909091	         1.1	    0.934131	     2074.07	      866859	      543734	     -4065.5	     4067.37	          95	
106	  7	                 [S]	           4	     3.63636	         4.4	     3.63636	     8073.91	      866860	      543734	    -15826.1	     15833.4	          95	
107	  7	              [E.S1]	           0	
108	  7	               [E.P]	           0	
109	  7	                r(S)	           0	
110	  7	              offset	     0.95791	     -INFTY	     +INFTY	     0.87023	     46.2907	     20767.9	     13026.6	    -89.8876	     91.6281	          95	
111	  7	               delay	           0	
112	  7	          incubation	           0	
113	  7	                [E]i	           0	
114	  7	                [S]i	           0	
115	  7	             [E.S1]i	           0	
116	  7	              [E.P]i	           0	
117	  7	            dilution	           0	
118	  7	           intensive	           0	
