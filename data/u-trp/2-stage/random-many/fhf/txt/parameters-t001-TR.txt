
PARAMETERS AND CONFIDENCE INTERVALS

NO.	SET	           PARAMETER	     INITIAL	    LIMIT-LO	    LIMIT-HI	         FIT	      STDERR	         CV%	      F(SDI)	   CONLIN-LO	   CONLIN-HI	   CONLIN-P%	

  1	  1	                time	           0	
  2	  1	             restart	           0	
  3	  *	                 k.1	    0.387494	      1e-006	      1e+006	     14.5608	     7567.21	      202900	     82726.8	    -14821.7	     14850.9	          95	
  4	  *	                k.-1	     1.72118	      1e-012	      1e+006	      2.7015	     26388.3	3.81363e+006	 1.5549e+006	    -51734.3	     51739.7	          95	
  5	  *	               k.cat	      280119	      1e-006	      1e+006	      280131	1.51412e+008	      211023	     86038.9	-2.96579e+008	2.97139e+008	          95	
  6	  1	                 [E]	           1	    0.909091	         1.1	         1.1	     570.924	      202636	     82619.5	    -1118.26	     1120.46	          95	
  7	  1	                 [S]	         0.5	    0.454545	        0.55	        0.55	     285.228	      202471	     82551.9	     -558.67	      559.77	          95	
  8	  1	              [E.S1]	           0	
  9	  1	               [E.P]	           0	
 10	  *	                r(E)	        0.31	    3.1e-007	      310000	   0.0631119	     24040.1	1.48716e+008	6.06349e+007	    -47133.2	     47133.3	          95	
 11	  1	                r(S)	           0	
 12	  *	             r(E.S1)	        0.22	    2.2e-007	      220000	    2.2e-007	      116.31	2.06409e+011	8.41575e+010	    -228.039	     228.039	          95	
 13	  *	              r(E.P)	        0.25	    2.5e-007	      250000	 0.000281108	     24037.7	3.33852e+010	1.36119e+010	    -47128.5	     47128.5	          95	
 14	  1	              offset	      1.1793	     -INFTY	     +INFTY	     1.10454	     26441.5	9.34627e+006	3.81068e+006	    -51840.2	     51842.5	          95	
 15	  1	               delay	           0	
 16	  1	          incubation	           0	
 17	  1	                [E]i	           0	
 18	  1	                [S]i	           0	
 19	  1	             [E.S1]i	           0	
 20	  1	              [E.P]i	           0	
 21	  1	            dilution	           0	
 22	  1	           intensive	           0	
 23	  2	                time	           0	
 24	  2	             restart	           0	
 25	  2	                 [E]	           1	    0.909091	         1.1	         1.1	     570.577	      202514	     82569.4	    -1117.58	     1119.78	          95	
 26	  2	                 [S]	           1	    0.909091	         1.1	    0.909091	     471.452	      202471	       82552	    -923.422	      925.24	          95	
 27	  2	              [E.S1]	           0	
 28	  2	               [E.P]	           0	
 29	  2	                r(S)	           0	
 30	  2	              offset	     1.14325	     -INFTY	     +INFTY	     1.08298	     26441.5	9.53235e+006	3.88655e+006	    -51840.3	     51842.4	          95	
 31	  2	               delay	           0	
 32	  2	          incubation	           0	
 33	  2	                [E]i	           0	
 34	  2	                [S]i	           0	
 35	  2	             [E.S1]i	           0	
 36	  2	              [E.P]i	           0	
 37	  2	            dilution	           0	
 38	  2	           intensive	           0	
 39	  3	                time	           0	
 40	  3	             restart	           0	
 41	  3	                 [E]	           1	    0.909091	         1.1	     0.98107	     508.563	      202385	     82516.8	    -996.111	     998.073	          95	
 42	  3	                 [S]	         1.5	     1.36364	        1.65	        1.65	     855.775	      202492	     82560.6	    -1676.19	     1679.49	          95	
 43	  3	              [E.S1]	           0	
 44	  3	               [E.P]	           0	
 45	  3	                r(S)	           0	
 46	  3	              offset	     1.10351	     -INFTY	     +INFTY	     1.04854	     23582.7	8.78096e+006	 3.5802e+006	    -46235.3	     46237.4	          95	
 47	  3	               delay	           0	
 48	  3	          incubation	           0	
 49	  3	                [E]i	           0	
 50	  3	                [S]i	           0	
 51	  3	             [E.S1]i	           0	
 52	  3	              [E.P]i	           0	
 53	  3	            dilution	           0	
 54	  3	           intensive	           0	
 55	  4	                time	           0	
 56	  4	             restart	           0	
 57	  4	                 [E]	           1	    0.909091	         1.1	     1.04332	     540.828	      202384	     82516.3	    -1059.31	     1061.39	          95	
 58	  4	                 [S]	           2	     1.81818	         2.2	       2.149	     1114.48	      202474	     82553.1	     -2182.9	      2187.2	          95	
 59	  4	              [E.S1]	           0	
 60	  4	               [E.P]	           0	
 61	  4	                r(S)	           0	
 62	  4	              offset	     1.07103	     -INFTY	     +INFTY	     1.00682	       25079	9.72499e+006	 3.9651e+006	      -49169	       49171	          95	
 63	  4	               delay	           0	
 64	  4	          incubation	           0	
 65	  4	                [E]i	           0	
 66	  4	                [S]i	           0	
 67	  4	             [E.S1]i	           0	
 68	  4	              [E.P]i	           0	
 69	  4	            dilution	           0	
 70	  4	           intensive	           0	
 71	  5	                time	           0	
 72	  5	             restart	           0	
 73	  5	                 [E]	           1	    0.909091	         1.1	    0.983717	     510.003	      202411	     82527.7	    -998.931	      1000.9	          95	
 74	  5	                 [S]	         2.5	     2.27273	        2.75	     2.39091	     1240.05	      202492	     82560.4	    -2428.85	     2433.63	          95	
 75	  5	              [E.S1]	           0	
 76	  5	               [E.P]	           0	
 77	  5	                r(S)	           0	
 78	  5	              offset	     1.02909	     -INFTY	     +INFTY	     0.96588	     23646.4	9.55814e+006	3.89707e+006	    -46360.2	     46362.1	          95	
 79	  5	               delay	           0	
 80	  5	          incubation	           0	
 81	  5	                [E]i	           0	
 82	  5	                [S]i	           0	
 83	  5	             [E.S1]i	           0	
 84	  5	              [E.P]i	           0	
 85	  5	            dilution	           0	
 86	  5	           intensive	           0	
 87	  6	                time	           0	
 88	  6	             restart	           0	
 89	  6	                 [E]	           1	    0.909091	         1.1	    0.998587	      517.88	      202477	     82554.4	    -1014.36	     1016.36	          95	
 90	  6	                 [S]	           3	     2.72727	         3.3	     2.72727	     1414.46	      202485	     82557.9	    -2770.47	     2775.92	          95	
 91	  6	              [E.S1]	           0	
 92	  6	               [E.P]	           0	
 93	  6	                r(S)	           0	
 94	  6	              offset	     0.97752	     -INFTY	     +INFTY	    0.927174	     24003.8	1.01077e+007	4.12112e+006	      -47061	     47062.9	          95	
 95	  6	               delay	           0	
 96	  6	          incubation	           0	
 97	  6	                [E]i	           0	
 98	  6	                [S]i	           0	
 99	  6	             [E.S1]i	           0	
100	  6	              [E.P]i	           0	
101	  6	            dilution	           0	
102	  6	           intensive	           0	
103	  7	                time	           0	
104	  7	             restart	           0	
105	  7	                 [E]	           1	    0.909091	         1.1	         1.1	     571.428	      202816	     82692.5	    -1119.24	     1121.44	          95	
106	  7	                 [S]	           4	     3.63636	         4.4	     3.63636	     1885.49	      202437	     82538.2	    -3693.07	     3700.34	          95	
107	  7	              [E.S1]	           0	
108	  7	               [E.P]	           0	
109	  7	                r(S)	           0	
110	  7	              offset	     0.95791	     -INFTY	     +INFTY	    0.893193	     26441.5	1.15578e+007	4.71236e+006	    -51840.5	     51842.3	          95	
111	  7	               delay	           0	
112	  7	          incubation	           0	
113	  7	                [E]i	           0	
114	  7	                [S]i	           0	
115	  7	             [E.S1]i	           0	
116	  7	              [E.P]i	           0	
117	  7	            dilution	           0	
118	  7	           intensive	           0	
