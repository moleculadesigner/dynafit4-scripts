
PARAMETERS AND CONFIDENCE INTERVALS

NO.	SET	           PARAMETER	     INITIAL	    LIMIT-LO	    LIMIT-HI	         FIT	      STDERR	         CV%	      F(SDI)	   CONLIN-LO	   CONLIN-HI	   CONLIN-P%	

  1	  1	                time	           0	
  2	  1	             restart	           0	
  3	  *	                 k.1	     1.90214	      1e-006	      1e+006	     15.0623	2.95751e+008	7.66599e+009	1.67599e+009	-5.79851e+008	5.79851e+008	          95	
  4	  *	                k.-1	       68238	      1e-012	      1e+006	       68238	2.65197e+007	      151731	     33172.6	-5.19264e+007	5.20629e+007	          95	
  5	  *	               k.cat	2.86546e-005	      1e-006	      1e+006	       9.507	      732483	3.00806e+007	6.57644e+006	-1.4361e+006	1.43612e+006	          95	
  6	  1	                 [E]	           1	    0.909091	         1.1	    0.909091	1.78576e+007	7.66918e+009	1.67669e+009	-3.50118e+007	3.50118e+007	          95	
  7	  1	                 [S]	         0.5	    0.454545	        0.55	    0.454545	8.92881e+006	7.66918e+009	1.67669e+009	-1.75059e+007	1.75059e+007	          95	
  8	  1	              [E.S1]	           0	
  9	  1	               [E.P]	           0	
 10	  *	                r(E)	        0.31	    3.1e-007	      310000	     2.59901	5.10754e+007	7.67247e+009	1.67741e+009	-1.00139e+008	1.00139e+008	          95	
 11	  1	                r(S)	           0	
 12	  *	             r(E.S1)	        0.22	    2.2e-007	      220000	    2.2e-007	     113.662	 2.0171e+011	4.40992e+010	    -222.847	     222.847	          95	
 13	  *	              r(E.P)	        0.25	    2.5e-007	      250000	     2.58424	5.07852e+007	7.67249e+009	1.67742e+009	-9.95697e+007	9.95697e+007	          95	
 14	  1	              offset	      1.1793	     -INFTY	     +INFTY	    -1.21959	      181198	5.80056e+007	           0	     -355258	      355256	          95	
 15	  1	               delay	           0	
 16	  1	          incubation	           0	
 17	  1	                [E]i	           0	
 18	  1	                [S]i	           0	
 19	  1	             [E.S1]i	           0	
 20	  1	              [E.P]i	           0	
 21	  1	            dilution	           0	
 22	  1	           intensive	           0	
 23	  2	                time	           0	
 24	  2	             restart	           0	
 25	  2	                 [E]	           1	    0.909091	         1.1	         1.1	 1.6033e+008	5.69056e+010	1.24411e+010	-3.14344e+008	3.14344e+008	          95	
 26	  2	                 [S]	           1	    0.909091	         1.1	         1.1	 1.5789e+008	5.60395e+010	1.22518e+010	-3.0956e+008	 3.0956e+008	          95	
 27	  2	              [E.S1]	           0	
 28	  2	               [E.P]	           0	
 29	  2	                r(S)	           0	
 30	  2	              offset	     1.14325	     -INFTY	     +INFTY	    -1.75579	 4.0969e+008	9.10995e+010	           0	-8.0324e+008	 8.0324e+008	          95	
 31	  2	               delay	           0	
 32	  2	          incubation	           0	
 33	  2	                [E]i	           0	
 34	  2	                [S]i	           0	
 35	  2	             [E.S1]i	           0	
 36	  2	              [E.P]i	           0	
 37	  2	            dilution	           0	
 38	  2	           intensive	           0	
 39	  3	                time	           0	
 40	  3	             restart	           0	
 41	  3	                 [E]	           1	    0.909091	         1.1	         1.1	2.16077e+007	7.66918e+009	1.67669e+009	-4.23642e+007	4.23642e+007	          95	
 42	  3	                 [S]	         1.5	     1.36364	        1.65	        1.65	3.24116e+007	7.66918e+009	1.67669e+009	-6.35464e+007	6.35464e+007	          95	
 43	  3	              [E.S1]	           0	
 44	  3	               [E.P]	           0	
 45	  3	                r(S)	           0	
 46	  3	              offset	     1.10351	     -INFTY	     +INFTY	    -1.80354	      219072	4.74233e+007	           0	     -429515	      429511	          95	
 47	  3	               delay	           0	
 48	  3	          incubation	           0	
 49	  3	                [E]i	           0	
 50	  3	                [S]i	           0	
 51	  3	             [E.S1]i	           0	
 52	  3	              [E.P]i	           0	
 53	  3	            dilution	           0	
 54	  3	           intensive	           0	
 55	  4	                time	           0	
 56	  4	             restart	           0	
 57	  4	                 [E]	           1	    0.909091	         1.1	     1.04891	2.06043e+007	7.66918e+009	1.67669e+009	-4.03968e+007	4.03968e+007	          95	
 58	  4	                 [S]	           2	     1.81818	         2.2	     1.93929	3.80943e+007	7.66918e+009	1.67669e+009	-7.46879e+007	7.46879e+007	          95	
 59	  4	              [E.S1]	           0	
 60	  4	               [E.P]	           0	
 61	  4	                r(S)	           0	
 62	  4	              offset	     1.07103	     -INFTY	     +INFTY	    -1.71306	      208875	4.76042e+007	           0	     -409523	      409519	          95	
 63	  4	               delay	           0	
 64	  4	          incubation	           0	
 65	  4	                [E]i	           0	
 66	  4	                [S]i	           0	
 67	  4	             [E.S1]i	           0	
 68	  4	              [E.P]i	           0	
 69	  4	            dilution	           0	
 70	  4	           intensive	           0	
 71	  5	                time	           0	
 72	  5	             restart	           0	
 73	  5	                 [E]	           1	    0.909091	         1.1	    0.909091	1.78576e+007	7.66918e+009	1.67669e+009	-3.50118e+007	3.50118e+007	          95	
 74	  5	                 [S]	         2.5	     2.27273	        2.75	     2.27273	4.46441e+007	7.66918e+009	1.67669e+009	-8.75294e+007	8.75294e+007	          95	
 75	  5	              [E.S1]	           0	
 76	  5	               [E.P]	           0	
 77	  5	                r(S)	           0	
 78	  5	              offset	     1.02909	     -INFTY	     +INFTY	    -1.39125	      181001	5.07936e+007	           0	     -354873	      354870	          95	
 79	  5	               delay	           0	
 80	  5	          incubation	           0	
 81	  5	                [E]i	           0	
 82	  5	                [S]i	           0	
 83	  5	             [E.S1]i	           0	
 84	  5	              [E.P]i	           0	
 85	  5	            dilution	           0	
 86	  5	           intensive	           0	
 87	  6	                time	           0	
 88	  6	             restart	           0	
 89	  6	                 [E]	           1	    0.909091	         1.1	    0.909091	1.78576e+007	7.66918e+009	1.67669e+009	-3.50118e+007	3.50118e+007	          95	
 90	  6	                 [S]	           3	     2.72727	         3.3	     2.72727	5.35729e+007	7.66918e+009	1.67669e+009	-1.05035e+008	1.05035e+008	          95	
 91	  6	              [E.S1]	           0	
 92	  6	               [E.P]	           0	
 93	  6	                r(S)	           0	
 94	  6	              offset	     0.97752	     -INFTY	     +INFTY	    -1.42994	      180978	4.94131e+007	           0	     -354828	      354826	          95	
 95	  6	               delay	           0	
 96	  6	          incubation	           0	
 97	  6	                [E]i	           0	
 98	  6	                [S]i	           0	
 99	  6	             [E.S1]i	           0	
100	  6	              [E.P]i	           0	
101	  6	            dilution	           0	
102	  6	           intensive	           0	
103	  7	                time	           0	
104	  7	             restart	           0	
105	  7	                 [E]	           1	    0.909091	         1.1	    0.909091	1.78576e+007	7.66918e+009	1.67669e+009	-3.50118e+007	3.50118e+007	          95	
106	  7	                 [S]	           4	     3.63636	         4.4	     3.63636	7.14306e+007	7.66918e+009	1.67669e+009	-1.40047e+008	1.40047e+008	          95	
107	  7	              [E.S1]	           0	
108	  7	               [E.P]	           0	
109	  7	                r(S)	           0	
110	  7	              offset	     0.95791	     -INFTY	     +INFTY	    -1.46369	      180926	4.82599e+007	           0	     -354727	      354724	          95	
111	  7	               delay	           0	
112	  7	          incubation	           0	
113	  7	                [E]i	           0	
114	  7	                [S]i	           0	
115	  7	             [E.S1]i	           0	
116	  7	              [E.P]i	           0	
117	  7	            dilution	           0	
118	  7	           intensive	           0	
