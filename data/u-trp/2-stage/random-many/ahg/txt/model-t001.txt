=====
Model
=====

--------------
Model equation
--------------

The fitting function (model equation) for each individual data set is

F(t) = F_0 + r_1 c_1(t) + r_2 c_2(t) + ... + r_n_S c_n_S(t)

where 

F(t)		the experimental signal observed at time t
F_0		offset on the signal axis (a property of the instrument)
n_S		number of unique molecular species participating
                in the reaction mechanism
c_i(t)		the concentration of the  ith species at time t
r_i		the molar response coefficient of the ith species

In this case, n_S = 4.
The molecular species participating in the mechanism are:
.E, S, E.S1, E.P

The concentrations of these molecular species at time t are computed from their
initial concentrations (at time zero, t = 0) by solving an initial-value problem
defined by a system of simultaneous first-order Ordinary Differential Equations (ODEs)
listed below.

----------
ODE system
----------

Initial value problem: Given the concentrations of molecular species at t = 0,
compute the concentrations at an arbitrary later time t > 0 by solving the following
system of ODEs.  The solution is obtained by numerical integration.

d[E]/dt = 	 - k.1[E][S] + k.-1[E.S1]
d[S]/dt = 	 - k.1[E][S] + k.-1[E.S1]
d[E.S1]/dt = 	 + k.1[E][S] - k.-1[E.S1] - k.cat[E.S1]
d[E.P]/dt = 	 + k.cat[E.S1]
