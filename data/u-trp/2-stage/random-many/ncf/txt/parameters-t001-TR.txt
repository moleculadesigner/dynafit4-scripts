
PARAMETERS AND CONFIDENCE INTERVALS

NO.	SET	           PARAMETER	     INITIAL	    LIMIT-LO	    LIMIT-HI	         FIT	      STDERR	         CV%	      F(SDI)	   CONLIN-LO	   CONLIN-HI	   CONLIN-P%	

  1	  1	                time	           0	
  2	  1	             restart	           0	
  3	  *	                 k.1	4.70135e-006	      1e-006	      1e+006	     8.32838	     3460.31	      162213	     36331.5	    -6775.96	     6792.62	          95	
  4	  *	                k.-1	     2534.73	      1e-012	      1e+006	     1613.22	      2838.7	     687.002	      153.87	    -3952.35	      7178.8	          95	
  5	  *	               k.cat	   0.0218705	      1e-006	      1e+006	    0.240899	     20.8653	       33816	      7573.9	    -40.6677	     41.1495	          95	
  6	  1	                 [E]	           1	    0.909091	         1.1	         1.1	     466.018	      165402	     37045.8	    -912.576	     914.776	          95	
  7	  1	                 [S]	         0.5	    0.454545	        0.55	    0.454545	     192.465	      165313	     37025.7	    -376.892	     377.801	          95	
  8	  1	              [E.S1]	           0	
  9	  1	               [E.P]	           0	
 10	  *	                r(E)	        0.31	    3.1e-007	      310000	     4.66022	     2096.19	      175613	     39332.7	    -4105.14	     4114.46	          95	
 11	  1	                r(S)	           0	
 12	  *	             r(E.S1)	        0.22	    2.2e-007	      220000	    2.2e-007	     102.296	1.81538e+011	4.06597e+010	    -200.561	     200.561	          95	
 13	  *	              r(E.P)	        0.25	    2.5e-007	      250000	      4.5603	     2054.69	      175908	     39398.7	    -4023.87	     4032.99	          95	
 14	  1	              offset	      1.1793	     -INFTY	     +INFTY	    -3.97091	     467.993	     46013.1	           0	    -921.519	     913.577	          95	
 15	  1	               delay	           0	
 16	  1	          incubation	           0	
 17	  1	                [E]i	           0	
 18	  1	                [S]i	           0	
 19	  1	             [E.S1]i	           0	
 20	  1	              [E.P]i	           0	
 21	  1	            dilution	           0	
 22	  1	           intensive	           0	
 23	  2	                time	           0	
 24	  2	             restart	           0	
 25	  2	                 [E]	           1	    0.909091	         1.1	         1.1	 6.9096e+007	2.45241e+010	5.49275e+009	-1.3547e+008	 1.3547e+008	          95	
 26	  2	                 [S]	           1	    0.909091	         1.1	         1.1	6.90961e+007	2.45241e+010	5.49275e+009	-1.3547e+008	 1.3547e+008	          95	
 27	  2	              [E.S1]	           0	
 28	  2	               [E.P]	           0	
 29	  2	                r(S)	           0	
 30	  2	              offset	     1.14325	     -INFTY	     +INFTY	    -3.99433	3.22003e+008	3.14737e+010	           0	-6.3132e+008	 6.3132e+008	          95	
 31	  2	               delay	           0	
 32	  2	          incubation	           0	
 33	  2	                [E]i	           0	
 34	  2	                [S]i	           0	
 35	  2	             [E.S1]i	           0	
 36	  2	              [E.P]i	           0	
 37	  2	            dilution	           0	
 38	  2	           intensive	           0	
 39	  3	                time	           0	
 40	  3	             restart	           0	
 41	  3	                 [E]	           1	    0.909091	         1.1	         1.1	     466.061	      165418	     37049.2	    -912.661	     914.861	          95	
 42	  3	                 [S]	         1.5	     1.36364	        1.65	        1.65	      698.73	      165332	     37030.1	    -1368.28	     1371.58	          95	
 43	  3	              [E.S1]	           0	
 44	  3	               [E.P]	           0	
 45	  3	                r(S)	           0	
 46	  3	              offset	     1.10351	     -INFTY	     +INFTY	    -4.02828	     466.325	     45196.1	           0	    -918.307	      910.25	          95	
 47	  3	               delay	           0	
 48	  3	          incubation	           0	
 49	  3	                [E]i	           0	
 50	  3	                [S]i	           0	
 51	  3	             [E.S1]i	           0	
 52	  3	              [E.P]i	           0	
 53	  3	            dilution	           0	
 54	  3	           intensive	           0	
 55	  4	                time	           0	
 56	  4	             restart	           0	
 57	  4	                 [E]	           1	    0.909091	         1.1	         1.1	     466.067	      165420	     37049.7	    -912.672	     914.872	          95	
 58	  4	                 [S]	           2	     1.81818	         2.2	         2.2	      931.74	      165350	     37034.1	    -1824.57	     1828.97	          95	
 59	  4	              [E.S1]	           0	
 60	  4	               [E.P]	           0	
 61	  4	                r(S)	           0	
 62	  4	              offset	     1.07103	     -INFTY	     +INFTY	    -4.05677	      465.83	     44831.1	           0	    -917.366	     909.253	          95	
 63	  4	               delay	           0	
 64	  4	          incubation	           0	
 65	  4	                [E]i	           0	
 66	  4	                [S]i	           0	
 67	  4	             [E.S1]i	           0	
 68	  4	              [E.P]i	           0	
 69	  4	            dilution	           0	
 70	  4	           intensive	           0	
 71	  5	                time	           0	
 72	  5	             restart	           0	
 73	  5	                 [E]	           1	    0.909091	         1.1	         1.1	     466.088	      165427	     37051.4	    -912.714	     914.914	          95	
 74	  5	                 [S]	         2.5	     2.27273	        2.75	        2.75	     1164.76	      165362	     37036.8	    -2280.89	     2286.39	          95	
 75	  5	              [E.S1]	           0	
 76	  5	               [E.P]	           0	
 77	  5	                r(S)	           0	
 78	  5	              offset	     1.02909	     -INFTY	     +INFTY	    -4.08474	     465.364	     44479.6	           0	     -916.48	      908.31	          95	
 79	  5	               delay	           0	
 80	  5	          incubation	           0	
 81	  5	                [E]i	           0	
 82	  5	                [S]i	           0	
 83	  5	             [E.S1]i	           0	
 84	  5	              [E.P]i	           0	
 85	  5	            dilution	           0	
 86	  5	           intensive	           0	
 87	  6	                time	           0	
 88	  6	             restart	           0	
 89	  6	                 [E]	           1	    0.909091	         1.1	    0.909091	     385.225	      165440	     37054.1	    -754.364	     756.183	          95	
 90	  6	                 [S]	           3	     2.72727	         3.3	         3.3	     1397.72	      165363	       37037	    -2737.08	     2743.68	          95	
 91	  6	              [E.S1]	           0	
 92	  6	               [E.P]	           0	
 93	  6	                r(S)	           0	
 94	  6	              offset	     0.97752	     -INFTY	     +INFTY	    -3.23538	      383.99	     46336.9	           0	    -756.089	     749.618	          95	
 95	  6	               delay	           0	
 96	  6	          incubation	           0	
 97	  6	                [E]i	           0	
 98	  6	                [S]i	           0	
 99	  6	             [E.S1]i	           0	
100	  6	              [E.P]i	           0	
101	  6	            dilution	           0	
102	  6	           intensive	           0	
103	  7	                time	           0	
104	  7	             restart	           0	
105	  7	                 [E]	           1	    0.909091	         1.1	    0.909091	     385.235	      165444	     37055.1	    -754.384	     756.202	          95	
106	  7	                 [S]	           4	     3.63636	         4.4	     3.63636	     1540.26	      165371	     37038.8	    -3016.21	     3023.48	          95	
107	  7	              [E.S1]	           0	
108	  7	               [E.P]	           0	
109	  7	                r(S)	           0	
110	  7	              offset	     0.95791	     -INFTY	     +INFTY	    -3.26325	     383.723	     45909.2	           0	    -755.592	     749.065	          95	
111	  7	               delay	           0	
112	  7	          incubation	           0	
113	  7	                [E]i	           0	
114	  7	                [S]i	           0	
115	  7	             [E.S1]i	           0	
116	  7	              [E.P]i	           0	
117	  7	            dilution	           0	
118	  7	           intensive	           0	
