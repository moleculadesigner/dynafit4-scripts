
SCRIPT

NOTE: Comments were removed.

[task]
task = fit
data = progress
algorithm = TR
[mechanism]
E + S <==> E.S1 : k.1  k.-1
E.S1 ---> E.P : k.cat
[constants]
k.1    = 620483.1518504139 ? (1e-6 .. 1e6)
k.-1   = 971290.4337228156 ? (1e-12 .. 1e6)
k.cat  = 371073.37820405315 ? (1e-6 .. 1e6)
[concentrations]
[responses]
E    = 0.31 ?
E.S1 = 0.22 ?
E.P  = 0.25 ?
[data]
maximum 100.0
plot logarithmic
directory ./hsmug/series/
sheet U-Trp-R243A.1.csv
column 2
conc S = 0.5 ?
conc E = 1   ?
offset = auto ?
label = 0.5 uM
column 3
conc S = 1.0 ?
conc E = 1   ?
offset = auto ?
label = 1.0 uM
column 4
conc S = 1.5 ?
conc E = 1   ?
offset = auto ?
label = 1.5 uM
column 5
conc S = 2.0 ?
conc E = 1   ?
offset = auto ?
label = 2.0 uM
column 6
conc S = 2.5 ?
conc E = 1   ?
offset = auto ?
label = 2.5 uM
column 7
conc S = 3.0 ?
conc E = 1   ?
offset = auto ?
label = 3.0 uM
column 8
conc S = 4.0 ?
conc E = 1   ?
offset = auto ?
label = 4.0 uM
[output]
directory ./hsmug/out/r243a/u-trp/2-stage/random-many/kec
[settings]
{DynaFit}
RandomizationSeed = 0
{Constraints}
Concentrations = 1.1
{ConfidenceIntervals}
SquaresIncreasePercent = 10
MaxRefitIterations = 5
{Marquardt}
RobustFit = y
{Filter}
ReadEveryNthPoint = 50
{Output}
BlackBackground = n
WriteTeX = n
[end]

SETTINGS

{DynaFit}
   RandomizationSeed                = 4357         ; | 0 for system time
   DefaultFittingAlgorithm          = trust-region ; | marquardt
   PointsParametersRatio            = 10

{ODESolver}
   Iterations                       = 1000
   AbsoluteError                    = 1.e-14
   RelativeError                    = 1.e-8

{TrustRegion}
   IterationsPerParameter           = 100
   FunctionCallsPerParameter        = 200
   ConstrainedFit                   = y
   EqualizeDatasets                 = n
   RobustFit                        = n

{Marquardt}
   IterationsPerParameter           = 100
   RestartPerturbation              = 0.1
   Restarts                         = 2
   RestartsConfidence               = 1
   RobustFit                        = n
   EqualizeDatasets                 = n
   FixRedundantParameters           = y

{ConfidenceIntervals}
   LevelPercent                     = 95
   OnlyConstants                    = n
   JointProbability                 = n
   MaxSteps                         = 30
   SquaresIncreasePercent           = 0 ; | 10 for continuous assays
   RefitImprovedLower               = y
   RefitImprovedHigher              = n
   MaxRefitIterations               = 2

{DifferentialEvolution}
   PopulationSizeFixed              = 0
   PopulationSizeMinimal            = 1000
   PopulationSizePerParameter       = 5
   PopulationSizePerOrderOfMag      = 3
   MinimumGenerationsPerParameter   = 5
   MaximumGenerationsPerParameter   = 100
   MaximumEvolutions                = 6
   MinimumEvolutions                = 3
   RandomSeed                       = 1234
   RootMeanSquareMin                = 0
   RootMeanSquareMax                = 0
   OutputTextFileType               = csv ; | txt

{Constraints}
   Constants                        = 1000000
   Responses                        = 1000000
   Concentrations                   = 1000
   AllParametersConstrained         = y
   AllParametersRelativeBound       = 1000000

{Filter}
   XMin                             = 0
   XMax                             = 0
   XShift                           = 0
   YMin                             = 0
   YMax                             = 0
   XFirstMesh                       = 0
   PointsPerDataset                 = 0
   ExponentialSpacing               = n
   ReadEveryNthPoint                = 0
   SkipFirstNPoints                 = 0
   TimeInitialRate                  = 1
   PrintInitialRate                 = y
   SmoothData                       = n
   SmoothingMethod                  = savitzky-golay ; | average
   SavitzkyGolayWindow              = 10
   SavitzkyGolayDegree              = 4
   ExtrapolationMethod              = quadratic ; | linear
   SmoothingPasses                  = 4
   AverageReplicates                = n
   ZeroBaselineSignal               = n

{PieceWiseLinearFit}
   Points                           = 0
   Segments                         = 4
   Time                             = 0
   Overlap                          = n

{Output}
   Autocorrelations                 = n
   BlackBackground                  = y
   ColorEPS                         = y
   ConfidenceBands                  = n
   PredictionBands                  = n
   GlobalModelInterpolation         = y
   IncludeXZero                     = n
   IncludeYZero                     = n
   InitialRateDigits                = 4
   PlotRatesData                    = n
   PlotRatesLogarithmic             = n
   PlotStateLogarithmic             = n
   ResidualRange                    = 0   
   ResidualsEPS                     = y
   SignificantDigits                = 2
   StartDefaultBrowser              = n
   UseDefaultDirectory              = y
   WriteEPS                         = y
   WriteTeX                         = y
   WriteTXT                         = y
   XAxisLabel                       =
   XAxisUnit                        =
   YAxisLabel                       =

{MonteCarlo}
   PerformInitialFit                = y
   Runs                             = 1000
   RandomizationMethod              = simulate  ;  | shuffle | shift
   Distribution                     = normal    ;  | cauchy | logistic | uniform
   StandardDeviationSource          = fit       ;  | data | explicit
   StandardDeviation                = 1.2
   SignificantDigits                = 4
   HistogramBuckets                 = 20
   TruncateMeanPercent              = 5
   ColorOutput                      = y
   RandomizationSeed                = 1267
   ConcentrationErrorPercent        = 0
   OriginalEstimates                = n
   ConfidenceLevel                  = 100

{EstimateScan}
   EstimatesMax                     = 10000
   ReportSizeMax                    = 1000
   RefineEstimates                  = 10

{ExponentialFit}
   Degree                           = 4
   Automatic                        = y
   AllowOscillations                = n
   TinyAmplitudes                   = 0 ; | 0.000001

{OptimalDesign}
   Algorithm                        = AS ; | DE | BFGS
   Function                         = D  ; | T  | E  | V  ; see manual

{ModelSelection}
   PrioritizeCriterion              = akaike ; | bayesian
   ConfidenceIntervalRangeMax       = 10000
   CoefficientOfVariationMax        = 100
   CoefficientOfVariationSearchMax  = 200
   InformationCriterionDeltaMax     = 5
   InformationCriterionWeightMin    = 0.01
   RelativeSquaresMax               = 1.05
   OnlyConstants                    = y

