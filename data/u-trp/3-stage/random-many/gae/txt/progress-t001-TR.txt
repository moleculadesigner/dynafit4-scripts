
===================================================
Least squares Minimization - Trust Region Algorithm
===================================================

Optimized parameters:

     1 	 k.1
     2 	 k.-1
     3 	 k.2
     4 	 k.-2
     5 	 k.cat
     6 	 r(E)
     7 	 r(E.S1)
     8 	 r(E.S2)
     9 	 r(E.P)
    10 	 [E]#1
    11 	 [S]#1
    12 	 [E]#2
    13 	 [S]#2
    14 	 [E]#3
    15 	 [S]#3
    16 	 [E]#4
    17 	 [S]#4
    18 	 [E]#5
    19 	 [S]#5
    20 	 [E]#6
    21 	 [S]#6
    22 	 [E]#7
    23 	 [S]#7

:: NL2SOL :: DNLAFB
:: Analytic Jacobian; Constrained

________________________________________________________________________

:: Initial Estimate

     i	     Param p(i)	            Min	            Max
     1	     3.767e-006	         1e-006	         1e+006
     2	     1.249e-005	         1e-006	         1e+006
     3	         216100	         1e-006	         1e+006
     4	     4.864e-006	         1e-006	         1e+006
     5	       0.001928	         1e-006	         1e+006
     6	           0.85	       8.5e-007	         850000
     7	           0.83	       8.3e-007	         830000
     8	           0.75	       7.5e-007	         750000
     9	           0.81	       8.1e-007	         810000
    10	              1	       0.909091	            1.1
    11	            0.5	       0.454545	           0.55
    12	              1	       0.909091	            1.1
    13	              1	       0.909091	            1.1
    14	              1	       0.909091	            1.1
    15	            1.5	        1.36364	           1.65
    16	              1	       0.909091	            1.1
    17	              2	        1.81818	            2.2
    18	              1	       0.909091	            1.1
    19	            2.5	        2.27273	           2.75
    20	              1	       0.909091	            1.1
    21	              3	        2.72727	            3.3
    22	              1	       0.909091	            1.1
    23	              4	        3.63636	            4.4

:: Iterations

     i	 FEval	   F(i) = SSQ	   Param( 1)	   Param( 2)	   Param( 3)	   Param( 4)	   Param( 5)	   Param( 6)	   Param( 7)	   Param( 8)	   Param( 9)	   Param(10)	   Param(11)	   Param(12)	   Param(13)	   Param(14)	   Param(15)	   Param(16)	   Param(17)	   Param(18)	   Param(19)	   Param(20)	   Param(21)	   Param(22)	   Param(23)	
     0	     1	       160.28	   3.767e-006	   1.249e-005	   2.161e+005	   4.864e-006	     0.001928	         0.85	         0.83	         0.75	         0.81	            1	          0.5	            1	            1	            1	          1.5	            1	            2	            1	          2.5	            1	            3	            1	            4	
     1	     2	       14.446	     0.022152	    0.0057351	   2.161e+005	  3.7403e-006	       0.3777	      0.76476	         0.83	      0.74899	      0.81256	       1.0187	       0.5024	       1.0077	       1.0042	       1.0034	       1.5013	      0.99819	       1.9989	      0.99812	       2.4988	      0.99807	       2.9982	      0.99622	       4.0004	
     2	     4	       3.2318	    0.0056319	    0.0057351	   2.161e+005	       1e-006	      0.37415	      0.74062	         0.83	       0.7114	      0.79296	       1.0321	      0.50207	       1.0154	       1.0039	       1.0065	          1.5	      0.99769	        1.999	      0.99722	       2.4988	      0.99648	       2.9982	      0.99443	       4.0003	
     3	     5	       2.4152	    0.0054551	    0.0057351	   2.161e+005	       1e-006	      0.37567	      0.72315	         0.83	      0.74724	      0.81705	        1.051	      0.50212	       1.0309	        1.004	       1.0189	       1.5001	       1.0111	        1.999	        1.006	       2.4988	       1.0024	       2.9983	      0.99707	       4.0003	
     4	     6	       2.3769	    0.0040311	    0.0057351	   2.161e+005	  2.3159e-005	      0.37711	      0.72744	         0.83	      0.69334	      0.83131	       1.0465	       0.5015	       1.0268	       1.0029	       1.0152	       1.4998	       1.0078	       1.9993	       1.0027	       2.4992	      0.99923	       2.9987	      0.99416	       4.0001	
     5	     8	       2.3487	    0.0072188	    0.0057351	   2.161e+005	  2.3451e-005	      0.37571	       0.7319	         0.83	      0.64566	      0.81466	       1.0401	      0.50131	       1.0205	       1.0025	        1.009	       1.4998	       1.0017	       1.9996	      0.99664	       2.4993	      0.99318	       2.9989	      0.98819	       3.9998	
     6	    10	       2.3079	     0.014912	    0.0057351	   2.161e+005	  2.2818e-005	      0.37495	      0.73333	         0.83	      0.63939	      0.80848	       1.0384	      0.50132	        1.019	       1.0025	       1.0077	       1.4998	       1.0005	       1.9997	      0.99567	       2.4994	      0.99239	       2.9989	      0.98772	       3.9998	
     7	    12	       2.2425	      0.02796	     0.005733	   2.161e+005	       1e-006	      0.37069	      0.73664	         0.83	      0.62623	      0.78951	       1.0336	      0.50169	        1.015	       1.0029	       1.0043	       1.5003	      0.99811	            2	      0.99346	       2.4995	      0.99042	       2.9989	      0.98657	       3.9992	
     8	    14	       2.1793	     0.050629	    0.0057545	   2.161e+005	       1e-006	      0.38479	      0.73975	         0.83	      0.63141	      0.77405	       1.0308	      0.50261	       1.0129	       1.0041	       1.0028	       1.5012	      0.99687	       2.0005	      0.99312	       2.4996	      0.99084	       2.9987	      0.98802	       3.9984	
     9	    16	       2.1314	     0.072915	    0.0057531	   2.161e+005	       1e-006	      0.39981	      0.74018	         0.83	      0.64741	       0.7667	       1.0307	      0.50366	       1.0132	       1.0054	       1.0035	       1.5021	      0.99774	        2.001	       0.9942	       2.4996	       0.9921	       2.9985	      0.98955	       3.9976	
    10	    18	        2.087	     0.097267	    0.0057529	   2.161e+005	       1e-006	      0.41048	      0.74044	         0.83	      0.65843	      0.76007	       1.0306	      0.50448	       1.0134	       1.0065	       1.0038	       1.5028	      0.99821	       2.0014	      0.99477	       2.4997	      0.99276	       2.9984	      0.99036	        3.997	
    11	    19	       2.0234	      0.15042	    0.0056998	   2.161e+005	       1e-006	      0.43201	      0.74049	         0.83	      0.68157	      0.74792	        1.031	       0.5064	       1.0141	       1.0089	       1.0048	       1.5046	       0.9994	       2.0023	      0.99608	       2.4999	      0.99416	       2.9982	       0.9919	       3.9959	
    12	    21	       1.9193	      0.20789	    0.0056998	   2.161e+005	       1e-006	      0.45158	        0.741	         0.83	      0.68328	       0.7474	       1.0308	      0.50862	       1.0142	       1.0116	       1.0051	       1.5065	      0.99988	       2.0035	      0.99667	       2.5003	      0.99485	       2.9983	      0.99274	       3.9951	
    13	    22	        1.839	      0.38959	    0.0056232	   2.161e+005	    0.0010792	      0.51894	      0.74181	         0.83	      0.70431	      0.73593	       1.0309	      0.51894	       1.0154	        1.024	       1.0068	       1.5155	        1.002	       2.0087	      0.99901	       2.5019	      0.99739	       2.9983	      0.99553	       3.9915	
    14	    23	       1.5024	      0.56835	    0.0056228	   2.161e+005	     0.001079	      0.59581	       0.7438	         0.83	      0.69388	      0.73979	       1.0297	      0.53553	       1.0152	        1.043	       1.0073	       1.5281	       1.0028	        2.016	       1.0001	       2.5045	      0.99877	       2.9994	      0.99716	       3.9893	
    15	    25	       1.1595	       1.3901	    0.0055365	   2.161e+005	     0.090987	       1.1015	      0.74819	      0.83028	      0.70601	      0.72748	       1.0295	         0.55	       1.0191	          1.1	       1.0117	       1.5737	       1.0078	        2.074	       1.0049	       2.4653	       1.0032	        2.968	       1.0001	       3.9005	
    16	    26	      0.50073	       2.4329	    0.0056637	   2.161e+005	     0.090985	        1.433	      0.75349	      0.83074	      0.69502	      0.73035	       1.0276	         0.55	       1.0192	          1.1	       1.0143	         1.65	       1.0113	          2.2	       1.0081	       2.5405	       1.0065	       3.0408	       1.0034	       3.8777	
    17	    28	      0.30545	       2.5821	    0.0064753	   2.161e+005	     0.090985	       1.5882	      0.76638	      0.83076	      0.68506	      0.73552	       1.0149	         0.55	       1.0122	          1.1	       1.0092	         1.65	       1.0068	          2.2	       1.0039	       2.5672	       1.0027	        3.059	       1.0002	       3.8782	
    18	    30	      0.19447	       3.2951	     0.016451	   2.161e+005	     0.089539	        1.936	      0.78212	      0.83125	      0.69383	      0.74336	      0.99836	         0.55	      0.99846	          1.1	      0.99596	         1.65	      0.99342	          2.2	      0.99066	       2.6044	      0.98884	       2.9984	      0.98592	       3.6364	
    19	    32	      0.17135	       3.6522	      0.01337	   2.161e+005	     0.087889	       2.0766	      0.78073	      0.83171	      0.68796	      0.73866	       1.0027	         0.55	       1.0043	          1.1	       1.0022	         1.65	      0.99972	          2.2	      0.99709	       2.6296	      0.99504	       2.9441	      0.99238	       3.6364	
    20	    33	        0.165	       4.0181	    0.0097742	   2.161e+005	     0.080683	       2.0366	      0.80417	      0.83267	      0.71075	      0.75953	      0.97404	         0.55	      0.97625	          1.1	      0.97391	         1.65	      0.97124	          2.2	      0.96841	       2.5912	      0.96615	       2.8442	      0.96388	       3.6364	
    21	    34	      0.15468	       4.3739	    0.0097525	   2.161e+005	     0.062429	       1.9029	      0.82087	      0.83456	      0.72796	      0.77456	      0.95618	         0.55	      0.95872	          1.1	      0.95621	         1.65	      0.95341	          2.2	      0.95034	       2.5382	      0.94803	       2.7647	      0.94601	       3.6364	
    22	    36	      0.15403	       4.4081	    0.0096418	   2.161e+005	     0.062346	       1.8994	      0.82242	      0.83469	      0.72995	      0.77627	      0.95427	         0.55	      0.95666	          1.1	       0.9541	         1.65	      0.95128	          2.2	      0.94819	       2.5342	      0.94587	       2.7583	      0.94385	       3.6364	
    23	    39	       0.1493	       4.8983	      0.10378	   2.161e+005	     0.065441	       1.7311	      0.81966	      0.84347	      0.73065	      0.77283	       0.9584	         0.55	      0.96134	          1.1	      0.95874	         1.65	      0.95571	          2.2	      0.95226	       2.4749	      0.95003	       2.7273	        0.948	       3.6364	
    24	    40	      0.14757	       5.4477	      0.10174	   2.161e+005	     0.066573	       1.5599	      0.81359	      0.95275	      0.72762	      0.76591	      0.96638	         0.55	       0.9703	          1.1	      0.96756	         1.65	      0.96441	          2.2	      0.96079	       2.4458	      0.95864	       2.7273	      0.95654	       3.6364	
    25	    41	      0.14743	       5.4648	      0.51938	   2.161e+005	     0.051622	       1.5784	      0.82043	       1.3635	      0.73338	      0.77232	      0.95846	         0.55	       0.9623	          1.1	      0.95952	         1.65	      0.95638	          2.2	      0.95277	       2.4411	      0.95066	       2.7273	      0.94858	       3.6364	
    26	    42	      0.14735	       5.4827	      0.50095	   2.161e+005	      0.19119	        1.576	      0.82277	        1.933	      0.73556	      0.77456	      0.95586	         0.55	      0.95966	          1.1	      0.95685	         1.65	      0.95359	       2.1775	      0.95007	       2.4348	      0.94799	       2.7273	      0.94591	       3.6364	
    27	    46	      0.14594	       5.4894	      0.54995	   2.161e+005	       1.6228	       1.5737	      0.83528	       38.183	      0.74862	      0.78803	      0.94067	         0.55	      0.94354	          1.1	      0.94037	         1.65	      0.93694	       2.1707	      0.93346	       2.4337	      0.93136	       2.7273	       0.9292	       3.6364	
    28	    47	      0.14511	       5.4831	       1.5963	   2.161e+005	        1.527	       1.5761	      0.84813	       74.448	      0.76146	      0.80184	      0.92576	         0.55	      0.92778	          1.1	      0.92429	         1.65	      0.92073	       2.1643	      0.91727	       2.4297	      0.91518	       2.7273	      0.91295	       3.6364	
    29	    48	      0.14436	       5.4668	       1e-006	   2.161e+005	       1e-006	       1.5764	      0.84801	       94.515	      0.76331	      0.80276	      0.92558	         0.55	      0.92718	          1.1	      0.92358	         1.65	      0.91992	       2.1632	      0.91643	        2.429	      0.91431	       2.7273	      0.91201	       3.6364	
    30	    49	      0.14407	       5.5223	       1e-006	   2.161e+005	    0.0021639	       1.5648	      0.84959	       115.95	      0.76578	      0.80516	      0.92341	         0.55	      0.92463	          1.1	      0.92082	         1.65	      0.91737	       2.1594	      0.91358	       2.4273	      0.91144	       2.7273	      0.90909	       3.6364	
    31	    50	      0.14396	       5.4586	       1e-006	  2.1613e+005	      0.76658	       1.5855	       0.8494	       122.67	      0.76485	      0.80522	      0.92342	         0.55	      0.92457	          1.1	      0.92078	         1.65	      0.91708	       2.1378	      0.91356	       2.4266	      0.91143	       2.7273	      0.90909	       3.6364	
    32	    63	      0.14394	       5.4586	    0.0066459	  2.1613e+005	      0.76658	       1.5855	      0.84953	       122.67	      0.76478	      0.80536	      0.92331	         0.55	      0.92448	          1.1	      0.92069	         1.65	      0.91686	       2.1379	      0.91348	       2.4266	      0.91135	       2.7273	      0.90909	       3.6364	
    33	    64	      0.14392	       5.4588	    0.0066388	  2.1613e+005	      0.76658	       1.5862	      0.84945	       122.67	      0.76475	      0.80525	       0.9234	         0.55	      0.92456	          1.1	      0.92077	         1.65	      0.91697	       2.1413	      0.91355	       2.4262	      0.91143	       2.7273	      0.90909	       3.6364	
    34	    66	       0.1439	       5.4584	    0.0079086	  2.1613e+005	      0.76658	       1.5869	      0.84945	       122.67	      0.76476	      0.80524	      0.92339	         0.55	      0.92456	          1.1	      0.92077	         1.65	      0.91703	       2.1557	      0.91355	       2.4252	      0.91143	       2.7273	      0.90909	       3.6364	
    35	    67	      0.14389	       5.4493	    0.0036346	  2.1613e+005	      0.76645	       1.5875	      0.84944	       122.67	      0.76474	      0.80523	      0.92339	         0.55	      0.92455	          1.1	      0.92076	         1.65	      0.91708	       2.1668	      0.91356	       2.4269	      0.91143	       2.7273	      0.90909	       3.6364	
    36	    69	      0.14389	       5.4487	       1e-006	  2.1613e+005	      0.76643	       1.5876	      0.84944	       122.67	      0.76474	      0.80523	      0.92339	         0.55	      0.92455	          1.1	      0.92076	         1.65	      0.91708	       2.1665	      0.91356	        2.427	      0.91143	       2.7273	      0.90909	       3.6364	
    37	    88	      0.14389	       5.4487	  3.1672e-005	  2.1613e+005	      0.76643	       1.5876	      0.84944	       122.67	      0.76474	      0.80523	      0.92339	         0.55	      0.92455	          1.1	      0.92076	         1.65	      0.91708	       2.1665	      0.91356	        2.427	      0.91143	       2.7273	      0.90909	       3.6364	

:: False convergence

       Squares	       0.143894
     Func Eval	             88
     Grad Eval	             38

:: Nonlinear parameters

     i	   Final p(i)	  Gradient(i)
     1	      5.44865	  1.2773e-005
     2	 3.16721e-005	   -0.0039011
     3	       216127	  2.1228e-010
     4	     0.766432	  2.5203e-006
     5	      1.58759	 -5.0634e-006
     6	     0.849442	   0.00044768
     7	      122.667	 -3.8498e-007
     8	     0.764742	   0.00066788
     9	     0.805232	  -0.00072739
    10	     0.923394	 -5.9155e-005
    11	         0.55	      -0.0345
    12	     0.924551	  -0.00040543
    13	          1.1	    -0.019074
    14	     0.920764	 -6.7568e-005
    15	         1.65	   -0.0073534
    16	     0.917079	   0.00070063
    17	      2.16655	 -2.1813e-006
    18	     0.913556	  5.3604e-005
    19	        2.427	 -3.2923e-006
    20	     0.911426	  2.3642e-005
    21	      2.72727	    0.0025412
    22	     0.909091	    0.0054416
    23	      3.63636	     0.011092
