
PARAMETERS AND CONFIDENCE INTERVALS

NO.	SET	           PARAMETER	     INITIAL	    LIMIT-LO	    LIMIT-HI	         FIT	      STDERR	         CV%	      F(SDI)	   CONLIN-LO	   CONLIN-HI	   CONLIN-P%	

  1	  1	                time	           0	
  2	  1	             restart	           0	
  3	  *	                 k.1	      0.1217	      1e-006	      1e+006	     22.3102	     17441.5	      602040	      267915	    -44911.1	     44955.7	          99	
  4	  *	                k.-1	     0.03361	      1e-006	      1e+006	     2.20595	    0.507021	         177	     78.7672	    0.899747	     3.51215	          99	
  5	  *	                 k.2	       1.086	      1e-006	      1e+006	     9.15899	    0.517485	     43.5105	     19.3627	     7.82582	     10.4921	          99	
  6	  *	                k.-2	    0.004824	      1e-006	      1e+006	     0.82944	    0.264124	     245.226	     109.128	    0.148995	     1.50989	          99	
  7	  *	               k.cat	       125.1	      1e-006	      1e+006	     2.02764	   0.0811805	     30.8322	     13.7207	      1.8185	     2.23678	          99	
  8	  1	                 [E]	           1	    0.909091	         1.1	    0.924128	     722.462	      602041	      267915	     -1860.3	     1862.15	          99	
  9	  1	                 [S]	         0.5	    0.454545	        0.55	        0.55	     429.977	      602041	      267915	    -1107.17	     1108.27	          99	
 10	  1	              [E.S1]	           0	
 11	  1	              [E.S2]	           0	
 12	  1	               [E.P]	           0	
 13	  *	                r(E)	        0.85	    8.5e-007	      850000	    0.855481	     668.795	      602041	      267915	    -1722.11	     1723.83	          99	
 14	  1	                r(S)	           0	
 15	  *	             r(E.S1)	        0.83	    8.3e-007	      830000	    0.823943	     644.139	      602041	      267915	    -1658.63	     1660.27	          99	
 16	  *	             r(E.S2)	        0.75	    7.5e-007	      750000	    0.756187	      591.17	      602041	      267915	    -1522.23	     1523.75	          99	
 17	  *	              r(E.P)	        0.81	    8.1e-007	      810000	    0.803542	     628.191	      602041	      267915	    -1617.56	     1619.17	          99	
 18	  1	              offset	     0.38165	
 19	  1	               delay	           0	
 20	  1	          incubation	           0	
 21	  1	                [E]i	           0	
 22	  1	                [S]i	           0	
 23	  1	             [E.S1]i	           0	
 24	  1	             [E.S2]i	           0	
 25	  1	              [E.P]i	           0	
 26	  1	            dilution	           0	
 27	  1	           intensive	           0	
 28	  2	                time	           0	
 29	  2	             restart	           0	
 30	  2	                 [E]	           1	    0.909091	         1.1	    0.925658	     723.657	      602041	      267915	    -1863.38	     1865.23	          99	
 31	  2	                 [S]	           1	    0.909091	         1.1	    0.909091	     710.706	      602041	      267915	    -1830.03	     1831.85	          99	
 32	  2	              [E.S1]	           0	
 33	  2	              [E.S2]	           0	
 34	  2	               [E.P]	           0	
 35	  2	                r(S)	           0	
 36	  2	              offset	    0.357428	
 37	  2	               delay	           0	
 38	  2	          incubation	           0	
 39	  2	                [E]i	           0	
 40	  2	                [S]i	           0	
 41	  2	             [E.S1]i	           0	
 42	  2	             [E.S2]i	           0	
 43	  2	              [E.P]i	           0	
 44	  2	            dilution	           0	
 45	  2	           intensive	           0	
 46	  3	                time	           0	
 47	  3	             restart	           0	
 48	  3	                 [E]	           1	    0.909091	         1.1	    0.922465	     721.161	      602041	      267915	    -1856.95	      1858.8	          99	
 49	  3	                 [S]	         1.5	     1.36364	        1.65	     1.36364	     1066.06	      602041	      267915	    -2745.05	     2747.78	          99	
 50	  3	              [E.S1]	           0	
 51	  3	              [E.S2]	           0	
 52	  3	               [E.P]	           0	
 53	  3	                r(S)	           0	
 54	  3	              offset	    0.316368	
 55	  3	               delay	           0	
 56	  3	          incubation	           0	
 57	  3	                [E]i	           0	
 58	  3	                [S]i	           0	
 59	  3	             [E.S1]i	           0	
 60	  3	             [E.S2]i	           0	
 61	  3	              [E.P]i	           0	
 62	  3	            dilution	           0	
 63	  3	           intensive	           0	
 64	  4	                time	           0	
 65	  4	             restart	           0	
 66	  4	                 [E]	           1	    0.909091	         1.1	    0.918251	     717.867	      602041	      267915	    -1848.47	     1850.31	          99	
 67	  4	                 [S]	           2	     1.81818	         2.2	     1.86146	     1455.25	      602041	      267915	    -3747.19	     3750.91	          99	
 68	  4	              [E.S1]	           0	
 69	  4	              [E.S2]	           0	
 70	  4	               [E.P]	           0	
 71	  4	                r(S)	           0	
 72	  4	              offset	    0.278203	
 73	  4	               delay	           0	
 74	  4	          incubation	           0	
 75	  4	                [E]i	           0	
 76	  4	                [S]i	           0	
 77	  4	             [E.S1]i	           0	
 78	  4	             [E.S2]i	           0	
 79	  4	              [E.P]i	           0	
 80	  4	            dilution	           0	
 81	  4	           intensive	           0	
 82	  5	                time	           0	
 83	  5	             restart	           0	
 84	  5	                 [E]	           1	    0.909091	         1.1	     0.91502	     715.341	      602041	      267915	    -1841.97	      1843.8	          99	
 85	  5	                 [S]	         2.5	     2.27273	        2.75	     2.37042	     1853.15	      602042	      267916	    -4771.76	      4776.5	          99	
 86	  5	              [E.S1]	           0	
 87	  5	              [E.S2]	           0	
 88	  5	               [E.P]	           0	
 89	  5	                r(S)	           0	
 90	  5	              offset	    0.239307	
 91	  5	               delay	           0	
 92	  5	          incubation	           0	
 93	  5	                [E]i	           0	
 94	  5	                [S]i	           0	
 95	  5	             [E.S1]i	           0	
 96	  5	             [E.S2]i	           0	
 97	  5	              [E.P]i	           0	
 98	  5	            dilution	           0	
 99	  5	           intensive	           0	
100	  6	                time	           0	
101	  6	             restart	           0	
102	  6	                 [E]	           1	    0.909091	         1.1	    0.912544	     713.406	      602041	      267915	    -1836.98	     1838.81	          99	
103	  6	                 [S]	           3	     2.72727	         3.3	     2.72727	     2132.11	      602040	      267915	    -5490.09	     5495.55	          99	
104	  6	              [E.S1]	           0	
105	  6	              [E.S2]	           0	
106	  6	               [E.P]	           0	
107	  6	                r(S)	           0	
108	  6	              offset	    0.202509	
109	  6	               delay	           0	
110	  6	          incubation	           0	
111	  6	                [E]i	           0	
112	  6	                [S]i	           0	
113	  6	             [E.S1]i	           0	
114	  6	             [E.S2]i	           0	
115	  6	              [E.P]i	           0	
116	  6	            dilution	           0	
117	  6	           intensive	           0	
118	  7	                time	           0	
119	  7	             restart	           0	
120	  7	                 [E]	           1	    0.909091	         1.1	    0.909091	     710.706	      602041	      267915	    -1830.03	     1831.85	          99	
121	  7	                 [S]	           4	     3.63636	         4.4	     3.63636	     2842.83	      602042	      267916	    -7320.15	     7327.42	          99	
122	  7	              [E.S1]	           0	
123	  7	              [E.S2]	           0	
124	  7	               [E.P]	           0	
125	  7	                r(S)	           0	
126	  7	              offset	    0.172856	
127	  7	               delay	           0	
128	  7	          incubation	           0	
129	  7	                [E]i	           0	
130	  7	                [S]i	           0	
131	  7	             [E.S1]i	           0	
132	  7	             [E.S2]i	           0	
133	  7	              [E.P]i	           0	
134	  7	            dilution	           0	
135	  7	           intensive	           0	
