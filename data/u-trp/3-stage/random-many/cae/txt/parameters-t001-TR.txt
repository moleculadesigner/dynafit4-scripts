
PARAMETERS AND CONFIDENCE INTERVALS

NO.	SET	           PARAMETER	     INITIAL	    LIMIT-LO	    LIMIT-HI	         FIT	      STDERR	         CV%	      F(SDI)	   CONLIN-LO	   CONLIN-HI	   CONLIN-P%	

  1	  1	                time	           0	
  2	  1	             restart	           0	
  3	  *	                 k.1	       33.16	      1e-006	      1e+006	       5.684	     182.632	     24743.7	     9258.02	    -464.817	     476.185	          99	
  4	  *	                k.-1	       1.303	      1e-006	      1e+006	1.05441e-006	8.83717e-005	       64543	     24149.2	-0.000226612	  0.00022872	          99	
  5	  *	                 k.2	       3.511	      1e-006	      1e+006	     22.0283	     1701.16	     59471.3	     22251.6	    -4360.55	     4404.61	          99	
  6	  *	                k.-2	      151800	      1e-006	      1e+006	      151800	1.94012e+007	     98423.9	     36825.9	-4.98302e+007	5.01338e+007	          99	
  7	  *	               k.cat	       11090	      1e-006	      1e+006	     11104.9	1.44974e+006	      100536	       37616	-3.72377e+006	3.74598e+006	          99	
  8	  1	                 [E]	           1	    0.909091	         1.1	    0.991147	     31.8444	     24742.2	     9257.44	    -81.0473	     83.0296	          99	
  9	  1	                 [S]	         0.5	    0.454545	        0.55	        0.55	     17.6704	     24741.6	     9257.22	     -44.973	      46.073	          99	
 10	  1	              [E.S1]	           0	
 11	  1	              [E.S2]	           0	
 12	  1	               [E.P]	           0	
 13	  *	                r(E)	        0.85	    8.5e-007	      850000	    0.793256	     25.4864	     24742.2	     9257.45	    -64.8656	     66.4521	          99	
 14	  1	                r(S)	           0	
 15	  *	             r(E.S1)	        0.83	    8.3e-007	      830000	    0.709724	     22.8117	     24752.1	     9261.15	    -58.0585	     59.4779	          99	
 16	  *	             r(E.S2)	        0.75	    7.5e-007	      750000	7.76223e-005	     5922.91	5.87613e+010	2.19859e+010	    -15258.8	     15258.8	          99	
 17	  *	              r(E.P)	        0.81	    8.1e-007	      810000	    0.746264	     23.9766	     24742.2	     9257.46	     -61.023	     62.5155	          99	
 18	  1	              offset	     0.38165	
 19	  1	               delay	           0	
 20	  1	          incubation	           0	
 21	  1	                [E]i	           0	
 22	  1	                [S]i	           0	
 23	  1	             [E.S1]i	           0	
 24	  1	             [E.S2]i	           0	
 25	  1	              [E.P]i	           0	
 26	  1	            dilution	           0	
 27	  1	           intensive	           0	
 28	  2	                time	           0	
 29	  2	             restart	           0	
 30	  2	                 [E]	           1	    0.909091	         1.1	    0.995797	     31.9938	     24742.2	     9257.45	    -81.4276	     83.4192	          99	
 31	  2	                 [S]	           1	    0.909091	         1.1	         1.1	     35.3435	     24743.4	     9257.91	    -89.9529	     92.1529	          99	
 32	  2	              [E.S1]	           0	
 33	  2	              [E.S2]	           0	
 34	  2	               [E.P]	           0	
 35	  2	                r(S)	           0	
 36	  2	              offset	    0.357428	
 37	  2	               delay	           0	
 38	  2	          incubation	           0	
 39	  2	                [E]i	           0	
 40	  2	                [S]i	           0	
 41	  2	             [E.S1]i	           0	
 42	  2	             [E.S2]i	           0	
 43	  2	              [E.P]i	           0	
 44	  2	            dilution	           0	
 45	  2	           intensive	           0	
 46	  3	                time	           0	
 47	  3	             restart	           0	
 48	  3	                 [E]	           1	    0.909091	         1.1	    0.993196	     31.9103	     24742.2	     9257.46	     -81.215	     83.2014	          99	
 49	  3	                 [S]	         1.5	     1.36364	        1.65	        1.65	      53.019	     24745.2	     9258.58	    -134.939	     138.239	          99	
 50	  3	              [E.S1]	           0	
 51	  3	              [E.S2]	           0	
 52	  3	               [E.P]	           0	
 53	  3	                r(S)	           0	
 54	  3	              offset	    0.316368	
 55	  3	               delay	           0	
 56	  3	          incubation	           0	
 57	  3	                [E]i	           0	
 58	  3	                [S]i	           0	
 59	  3	             [E.S1]i	           0	
 60	  3	             [E.S2]i	           0	
 61	  3	              [E.P]i	           0	
 62	  3	            dilution	           0	
 63	  3	           intensive	           0	
 64	  4	                time	           0	
 65	  4	             restart	           0	
 66	  4	                 [E]	           1	    0.909091	         1.1	    0.989952	      31.806	     24742.2	     9257.46	    -80.9497	     82.9296	          99	
 67	  4	                 [S]	           2	     1.81818	         2.2	      2.1927	     70.4559	     24744.7	     9258.39	    -179.318	     183.703	          99	
 68	  4	              [E.S1]	           0	
 69	  4	              [E.S2]	           0	
 70	  4	               [E.P]	           0	
 71	  4	                r(S)	           0	
 72	  4	              offset	    0.278203	
 73	  4	               delay	           0	
 74	  4	          incubation	           0	
 75	  4	                [E]i	           0	
 76	  4	                [S]i	           0	
 77	  4	             [E.S1]i	           0	
 78	  4	             [E.S2]i	           0	
 79	  4	              [E.P]i	           0	
 80	  4	            dilution	           0	
 81	  4	           intensive	           0	
 82	  5	                time	           0	
 83	  5	             restart	           0	
 84	  5	                 [E]	           1	    0.909091	         1.1	    0.986311	     31.6891	     24742.2	     9257.46	     -80.652	     82.6246	          99	
 85	  5	                 [S]	         2.5	     2.27273	        2.75	     2.45215	     78.7937	       24745	     9258.52	    -200.538	     205.443	          99	
 86	  5	              [E.S1]	           0	
 87	  5	              [E.S2]	           0	
 88	  5	               [E.P]	           0	
 89	  5	                r(S)	           0	
 90	  5	              offset	    0.239307	
 91	  5	               delay	           0	
 92	  5	          incubation	           0	
 93	  5	                [E]i	           0	
 94	  5	                [S]i	           0	
 95	  5	             [E.S1]i	           0	
 96	  5	             [E.S2]i	           0	
 97	  5	              [E.P]i	           0	
 98	  5	            dilution	           0	
 99	  5	           intensive	           0	
100	  6	                time	           0	
101	  6	             restart	           0	
102	  6	                 [E]	           1	    0.909091	         1.1	    0.984078	     31.6173	     24742.2	     9257.46	    -80.4694	     82.4375	          99	
103	  6	                 [S]	           3	     2.72727	         3.3	     2.72727	     87.6318	     24744.4	     9258.26	    -223.032	     228.487	          99	
104	  6	              [E.S1]	           0	
105	  6	              [E.S2]	           0	
106	  6	               [E.P]	           0	
107	  6	                r(S)	           0	
108	  6	              offset	    0.202509	
109	  6	               delay	           0	
110	  6	          incubation	           0	
111	  6	                [E]i	           0	
112	  6	                [S]i	           0	
113	  6	             [E.S1]i	           0	
114	  6	             [E.S2]i	           0	
115	  6	              [E.P]i	           0	
116	  6	            dilution	           0	
117	  6	           intensive	           0	
118	  7	                time	           0	
119	  7	             restart	           0	
120	  7	                 [E]	           1	    0.909091	         1.1	    0.981924	     31.5481	     24742.2	     9257.46	    -80.2932	     82.2571	          99	
121	  7	                 [S]	           4	     3.63636	         4.4	     3.63636	     116.843	     24744.4	      9258.3	    -297.377	      304.65	          99	
122	  7	              [E.S1]	           0	
123	  7	              [E.S2]	           0	
124	  7	               [E.P]	           0	
125	  7	                r(S)	           0	
126	  7	              offset	    0.172856	
127	  7	               delay	           0	
128	  7	          incubation	           0	
129	  7	                [E]i	           0	
130	  7	                [S]i	           0	
131	  7	             [E.S1]i	           0	
132	  7	             [E.S2]i	           0	
133	  7	              [E.P]i	           0	
134	  7	            dilution	           0	
135	  7	           intensive	           0	
