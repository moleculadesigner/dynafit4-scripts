
PARAMETERS AND CONFIDENCE INTERVALS

NO.	SET	           PARAMETER	     INITIAL	    LIMIT-LO	    LIMIT-HI	         FIT	      STDERR	         CV%	      F(SDI)	   CONLIN-LO	   CONLIN-HI	   CONLIN-P%	

  1	  1	                time	           0	
  2	  1	             restart	           0	
  3	  *	                 k.1	     0.07767	      1e-006	      1e+006	     5.49059	     824.434	      115633	     43353.7	    -2118.44	     2129.42	          99	
  4	  *	                k.-1	    0.007615	      1e-006	      1e+006	      1e-006	    0.018386	1.41589e+007	5.30856e+006	  -0.0473656	   0.0473676	          99	
  5	  *	                 k.2	     0.01177	      1e-006	      1e+006	     1.57563	     3.65738	     1787.56	     670.202	    -7.84663	     10.9979	          99	
  6	  *	                k.-2	  2.746e-006	      1e-006	      1e+006	   0.0437501	     730.793	1.28635e+007	4.82286e+006	    -1882.65	     1882.74	          99	
  7	  *	               k.cat	       316.7	      1e-006	      1e+006	     316.679	     2234.71	     5434.32	     2037.47	    -5440.46	     6073.81	          99	
  8	  1	                 [E]	           1	    0.909091	         1.1	    0.954388	     143.305	      115633	     43353.8	    -368.233	     370.142	          99	
  9	  1	                 [S]	         0.5	    0.454545	        0.55	        0.55	     82.5846	      115632	     43353.7	    -212.207	     213.307	          99	
 10	  1	              [E.S1]	           0	
 11	  1	              [E.S2]	           0	
 12	  1	               [E.P]	           0	
 13	  *	                r(E)	        0.85	    8.5e-007	      850000	     0.82405	     123.735	      115633	     43353.8	    -317.945	     319.593	          99	
 14	  1	                r(S)	           0	
 15	  *	             r(E.S1)	        0.83	    8.3e-007	      830000	    0.740664	     111.214	      115633	     43353.8	    -285.772	     287.253	          99	
 16	  *	             r(E.S2)	        0.75	    7.5e-007	      750000	    7.5e-007	     11.0229	1.13183e+010	4.24351e+009	    -28.3976	     28.3976	          99	
 17	  *	              r(E.P)	        0.81	    8.1e-007	      810000	    0.775809	     116.491	      115633	     43353.8	    -299.332	     300.883	          99	
 18	  1	              offset	     0.38165	
 19	  1	               delay	           0	
 20	  1	          incubation	           0	
 21	  1	                [E]i	           0	
 22	  1	                [S]i	           0	
 23	  1	             [E.S1]i	           0	
 24	  1	             [E.S2]i	           0	
 25	  1	              [E.P]i	           0	
 26	  1	            dilution	           0	
 27	  1	           intensive	           0	
 28	  2	                time	           0	
 29	  2	             restart	           0	
 30	  2	                 [E]	           1	    0.909091	         1.1	    0.958155	     143.871	      115633	     43353.8	    -369.686	     371.603	          99	
 31	  2	                 [S]	           1	    0.909091	         1.1	         1.1	     165.169	      115632	     43353.7	    -424.414	     426.614	          99	
 32	  2	              [E.S1]	           0	
 33	  2	              [E.S2]	           0	
 34	  2	               [E.P]	           0	
 35	  2	                r(S)	           0	
 36	  2	              offset	    0.357428	
 37	  2	               delay	           0	
 38	  2	          incubation	           0	
 39	  2	                [E]i	           0	
 40	  2	                [S]i	           0	
 41	  2	             [E.S1]i	           0	
 42	  2	             [E.S2]i	           0	
 43	  2	              [E.P]i	           0	
 44	  2	            dilution	           0	
 45	  2	           intensive	           0	
 46	  3	                time	           0	
 47	  3	             restart	           0	
 48	  3	                 [E]	           1	    0.909091	         1.1	    0.955329	     143.447	      115633	     43353.8	    -368.596	     370.507	          99	
 49	  3	                 [S]	         1.5	     1.36364	        1.65	        1.65	     247.753	      115632	     43353.6	    -636.619	     639.919	          99	
 50	  3	              [E.S1]	           0	
 51	  3	              [E.S2]	           0	
 52	  3	               [E.P]	           0	
 53	  3	                r(S)	           0	
 54	  3	              offset	    0.316368	
 55	  3	               delay	           0	
 56	  3	          incubation	           0	
 57	  3	                [E]i	           0	
 58	  3	                [S]i	           0	
 59	  3	             [E.S1]i	           0	
 60	  3	             [E.S2]i	           0	
 61	  3	              [E.P]i	           0	
 62	  3	            dilution	           0	
 63	  3	           intensive	           0	
 64	  4	                time	           0	
 65	  4	             restart	           0	
 66	  4	                 [E]	           1	    0.909091	         1.1	    0.952056	     142.955	      115633	     43353.8	    -367.333	     369.237	          99	
 67	  4	                 [S]	           2	     1.81818	         2.2	     2.17742	     326.945	      115632	     43353.3	    -840.108	     844.463	          99	
 68	  4	              [E.S1]	           0	
 69	  4	              [E.S2]	           0	
 70	  4	               [E.P]	           0	
 71	  4	                r(S)	           0	
 72	  4	              offset	    0.278203	
 73	  4	               delay	           0	
 74	  4	          incubation	           0	
 75	  4	                [E]i	           0	
 76	  4	                [S]i	           0	
 77	  4	             [E.S1]i	           0	
 78	  4	             [E.S2]i	           0	
 79	  4	              [E.P]i	           0	
 80	  4	            dilution	           0	
 81	  4	           intensive	           0	
 82	  5	                time	           0	
 83	  5	             restart	           0	
 84	  5	                 [E]	           1	    0.909091	         1.1	    0.948541	     142.427	      115633	     43353.8	    -365.977	     367.874	          99	
 85	  5	                 [S]	         2.5	     2.27273	        2.75	     2.43349	     365.391	      115630	     43352.9	    -938.899	     943.766	          99	
 86	  5	              [E.S1]	           0	
 87	  5	              [E.S2]	           0	
 88	  5	               [E.P]	           0	
 89	  5	                r(S)	           0	
 90	  5	              offset	    0.239307	
 91	  5	               delay	           0	
 92	  5	          incubation	           0	
 93	  5	                [E]i	           0	
 94	  5	                [S]i	           0	
 95	  5	             [E.S1]i	           0	
 96	  5	             [E.S2]i	           0	
 97	  5	              [E.P]i	           0	
 98	  5	            dilution	           0	
 99	  5	           intensive	           0	
100	  6	                time	           0	
101	  6	             restart	           0	
102	  6	                 [E]	           1	    0.909091	         1.1	    0.946463	     142.115	      115633	     43353.8	    -365.175	     367.068	          99	
103	  6	                 [S]	           3	     2.72727	         3.3	     2.72727	      409.51	      115632	     43353.6	    -1052.26	     1057.72	          99	
104	  6	              [E.S1]	           0	
105	  6	              [E.S2]	           0	
106	  6	               [E.P]	           0	
107	  6	                r(S)	           0	
108	  6	              offset	    0.202509	
109	  6	               delay	           0	
110	  6	          incubation	           0	
111	  6	                [E]i	           0	
112	  6	                [S]i	           0	
113	  6	             [E.S1]i	           0	
114	  6	             [E.S2]i	           0	
115	  6	              [E.P]i	           0	
116	  6	            dilution	           0	
117	  6	           intensive	           0	
118	  7	                time	           0	
119	  7	             restart	           0	
120	  7	                 [E]	           1	    0.909091	         1.1	     0.94438	     141.803	      115633	     43353.8	    -364.372	      366.26	          99	
121	  7	                 [S]	           4	     3.63636	         4.4	     3.63636	     546.007	      115631	     43353.1	       -1403	     1410.28	          99	
122	  7	              [E.S1]	           0	
123	  7	              [E.S2]	           0	
124	  7	               [E.P]	           0	
125	  7	                r(S)	           0	
126	  7	              offset	    0.172856	
127	  7	               delay	           0	
128	  7	          incubation	           0	
129	  7	                [E]i	           0	
130	  7	                [S]i	           0	
131	  7	             [E.S1]i	           0	
132	  7	             [E.S2]i	           0	
133	  7	              [E.P]i	           0	
134	  7	            dilution	           0	
135	  7	           intensive	           0	
