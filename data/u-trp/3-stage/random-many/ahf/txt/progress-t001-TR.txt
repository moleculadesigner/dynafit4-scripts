
===================================================
Least squares Minimization - Trust Region Algorithm
===================================================

Optimized parameters:

     1 	 k.1
     2 	 k.-1
     3 	 k.2
     4 	 k.-2
     5 	 k.cat
     6 	 r(E)
     7 	 r(E.S1)
     8 	 r(E.S2)
     9 	 r(E.P)
    10 	 [E]#1
    11 	 [S]#1
    12 	 [E]#2
    13 	 [S]#2
    14 	 [E]#3
    15 	 [S]#3
    16 	 [E]#4
    17 	 [S]#4
    18 	 [E]#5
    19 	 [S]#5
    20 	 [E]#6
    21 	 [S]#6
    22 	 [E]#7
    23 	 [S]#7

:: NL2SOL :: DNLAFB
:: Analytic Jacobian; Constrained

________________________________________________________________________

:: Initial Estimate

     i	     Param p(i)	            Min	            Max
     1	          249.8	         1e-006	         1e+006
     2	         406600	         1e-006	         1e+006
     3	         690100	         1e-006	         1e+006
     4	          7.325	         1e-006	         1e+006
     5	          19.72	         1e-006	         1e+006
     6	           0.85	       8.5e-007	         850000
     7	           0.83	       8.3e-007	         830000
     8	           0.75	       7.5e-007	         750000
     9	           0.81	       8.1e-007	         810000
    10	              1	       0.909091	            1.1
    11	            0.5	       0.454545	           0.55
    12	              1	       0.909091	            1.1
    13	              1	       0.909091	            1.1
    14	              1	       0.909091	            1.1
    15	            1.5	        1.36364	           1.65
    16	              1	       0.909091	            1.1
    17	              2	        1.81818	            2.2
    18	              1	       0.909091	            1.1
    19	            2.5	        2.27273	           2.75
    20	              1	       0.909091	            1.1
    21	              3	        2.72727	            3.3
    22	              1	       0.909091	            1.1
    23	              4	        3.63636	            4.4

:: Iterations

     i	 FEval	   F(i) = SSQ	   Param( 1)	   Param( 2)	   Param( 3)	   Param( 4)	   Param( 5)	   Param( 6)	   Param( 7)	   Param( 8)	   Param( 9)	   Param(10)	   Param(11)	   Param(12)	   Param(13)	   Param(14)	   Param(15)	   Param(16)	   Param(17)	   Param(18)	   Param(19)	   Param(20)	   Param(21)	   Param(22)	   Param(23)	
     0	     1	       68.247	        249.8	   4.066e+005	   6.901e+005	        7.325	        19.72	         0.85	         0.83	         0.75	         0.81	            1	          0.5	            1	            1	            1	          1.5	            1	            2	            1	          2.5	            1	            3	            1	            4	
     1	     2	       1.0675	       249.81	   4.066e+005	   6.901e+005	       7.3865	       19.255	      0.73349	      0.83231	      0.75353	      0.69425	       1.0608	      0.45455	       1.0666	       1.0564	       1.0607	       1.3999	       1.0557	       1.8182	       1.0519	       2.3466	       1.0298	          3.3	       1.0456	       4.0242	
     2	     3	       0.6754	       249.88	   4.066e+005	   6.901e+005	       7.1016	       17.509	      0.73496	      0.89707	       0.7453	      0.68789	       1.0675	         0.55	       1.0685	        0.917	       1.0647	       1.3636	       1.0577	       1.8182	       1.0529	       2.5738	       1.0479	          3.3	       1.0456	       4.3817	
     3	     4	      0.63768	        249.9	   4.066e+005	   6.901e+005	       6.9878	       17.242	      0.73616	       0.9187	      0.74194	      0.68667	       1.0663	         0.55	       1.0733	       1.0016	       1.0655	       1.3636	       1.0586	       1.8182	       1.0538	       2.7393	       1.0503	          3.3	       1.0456	       4.3817	
     4	     6	       0.5986	       249.89	   4.066e+005	   6.901e+005	       6.9781	       17.721	      0.74635	      0.91759	      0.73566	       0.6874	       1.0586	         0.55	       1.0699	      0.99707	       1.0644	       1.3636	       1.0582	       1.8182	       1.0527	       2.7393	       1.0501	          3.3	       1.0429	       4.3817	
     5	     7	      0.59489	       249.89	   4.066e+005	   6.901e+005	       6.9797	       17.861	      0.74758	      0.91688	      0.73663	      0.68828	       1.0572	         0.55	       1.0684	      0.99449	        1.063	       1.3636	       1.0562	       1.8182	       1.0514	       2.7393	       1.0481	          3.3	       1.0429	       4.3964	
     6	     9	      0.59376	       249.89	   4.066e+005	   6.901e+005	       6.9798	       17.868	      0.75618	      0.91686	      0.74577	      0.69673	       1.0451	         0.55	       1.0557	       0.9891	         1.05	       1.3636	       1.0432	       1.8182	       1.0385	       2.7402	       1.0351	          3.3	       1.0302	       4.3976	
     7	    11	      0.59254	       249.89	   4.066e+005	   6.901e+005	       6.9798	       17.876	      0.76484	      0.91685	      0.75442	      0.70479	       1.0337	         0.55	       1.0436	      0.97763	       1.0381	       1.3636	       1.0313	       1.8182	       1.0267	        2.741	       1.0233	          3.3	       1.0185	       4.3983	
     8	    13	      0.59137	       249.89	   4.066e+005	   6.901e+005	       6.9799	       17.884	      0.77358	      0.91683	       0.7631	      0.71288	       1.0224	         0.55	       1.0317	      0.96547	       1.0263	       1.3636	       1.0196	       1.8182	        1.015	       2.7419	       1.0117	          3.3	       1.0069	        4.399	
     9	    15	      0.59008	       249.89	   4.066e+005	   6.901e+005	       6.9803	       17.918	      0.78765	      0.91677	      0.77685	      0.72575	       1.0047	         0.55	       1.0128	      0.94216	       1.0079	       1.3636	       1.0013	       1.8182	      0.99682	       2.7459	      0.99356	          3.3	      0.98888	          4.4	
    10	    17	      0.58848	       249.89	   4.066e+005	   6.901e+005	       6.9804	       17.924	      0.79452	      0.91676	      0.78394	      0.73227	      0.99641	         0.55	       1.0042	      0.93649	      0.99923	       1.3636	      0.99268	       1.8182	       0.9882	       2.7465	      0.98497	          3.3	      0.98029	          4.4	
    11	    19	      0.58766	       249.89	   4.066e+005	   6.901e+005	       6.9805	       17.929	      0.80121	      0.91675	      0.79061	       0.7385	      0.98837	         0.55	      0.99573	       0.9286	      0.99083	       1.3636	      0.98432	       1.8182	      0.97988	       2.7471	      0.97667	          3.3	      0.97203	          4.4	
    12	    20	      0.58705	       249.89	   4.066e+005	   6.901e+005	       6.9809	       17.967	      0.81861	      0.91673	      0.80796	      0.75473	      0.96764	         0.55	      0.97395	      0.90909	      0.96916	       1.3636	      0.96277	       1.8182	      0.95842	         2.75	      0.95528	          3.3	      0.95077	          4.4	
    13	    22	      0.58479	       249.89	   4.066e+005	   6.901e+005	        6.981	       17.975	      0.82697	      0.91674	      0.81671	      0.76289	      0.95828	         0.55	      0.96452	      0.90909	      0.95919	       1.3636	      0.95286	       1.8182	      0.94854	         2.75	      0.94543	          3.3	      0.94094	          4.4	
    14	    23	      0.58424	       249.89	   4.066e+005	   6.901e+005	       6.9811	       17.987	      0.83494	      0.91678	      0.82502	      0.77069	       0.9492	         0.55	      0.95536	      0.90909	      0.94953	       1.3636	      0.94325	       1.8182	      0.93897	         2.75	      0.93589	          3.3	      0.93144	          4.4	
    15	    28	      0.57407	       249.82	   4.066e+005	   6.901e+005	       7.1781	       20.654	      0.84183	      0.94717	      0.83923	       0.7804	      0.93973	         0.55	      0.94472	      0.90909	      0.93806	       1.3636	      0.93181	       1.8182	      0.92756	         2.75	      0.92451	          3.3	      0.92011	          4.4	
    16	    29	      0.57251	       249.07	   4.066e+005	   6.901e+005	       9.6342	       21.488	      0.84639	       1.2621	      0.84642	      0.78586	      0.93411	         0.55	      0.93858	      0.90909	      0.93169	       1.3636	      0.92547	       1.8182	      0.92125	         2.75	      0.91822	          3.3	      0.91385	          4.4	
    17	    32	      0.56987	       245.37	   4.066e+005	   6.901e+005	       19.944	        22.19	      0.84228	       2.1842	      0.84256	      0.78201	      0.93845	         0.55	      0.94267	      0.90909	      0.93616	       1.3636	      0.93002	       1.8182	      0.92585	         2.75	      0.92283	          3.3	      0.91845	          4.4	
    18	    33	      0.56433	       213.84	   4.066e+005	   6.901e+005	        51.73	       24.291	      0.82815	     8.3e-007	      0.82423	      0.76513	      0.95586	         0.55	      0.96082	      0.90909	        0.956	       1.3636	      0.95007	       1.8182	      0.94607	         2.75	      0.94305	          3.3	      0.93866	          4.4	
    19	    34	      0.56085	       169.73	   4.066e+005	   6.901e+005	       61.298	        25.89	      0.81997	     8.3e-007	      0.81609	      0.75819	      0.96471	         0.55	      0.96915	      0.90909	      0.96488	       1.3636	       0.9591	       1.8182	      0.95527	         2.75	      0.95228	          3.3	      0.94793	          4.4	
    20	    35	      0.56015	       126.56	   4.066e+005	   6.901e+005	       52.647	       27.727	      0.82694	     8.3e-007	      0.82477	      0.76594	      0.95568	         0.55	      0.95939	      0.90909	      0.95467	       1.3636	      0.94912	       1.8182	      0.94544	         2.75	      0.94252	          3.3	      0.93826	          4.4	
    21	    39	      0.55958	       126.56	   4.066e+005	   6.901e+005	       52.646	       27.729	      0.81388	     8.3e-007	      0.81099	      0.75383	      0.97044	         0.55	      0.97404	      0.90909	      0.97002	       1.3745	      0.96431	       1.8198	      0.96062	         2.75	      0.95767	          3.3	      0.95335	          4.4	
    22	    40	      0.55901	       126.56	   4.066e+005	   6.901e+005	       52.643	       27.732	      0.80987	     8.3e-007	      0.80685	      0.74974	      0.97556	         0.55	      0.97927	      0.90909	       0.9756	       1.4045	      0.96982	        1.842	      0.96608	         2.75	      0.96311	          3.3	      0.95877	          4.4	
    23	    42	      0.55894	       126.56	   4.066e+005	   6.901e+005	       52.643	       27.733	      0.80636	     8.3e-007	      0.80291	      0.74631	      0.97976	         0.55	      0.98349	      0.90909	      0.98007	       1.4055	      0.97425	       1.8427	       0.9705	         2.75	      0.96752	          3.3	      0.96317	          4.4	
    24	    43	       0.5589	       126.56	   4.066e+005	   6.901e+005	       52.643	       27.733	      0.80303	     8.3e-007	      0.79935	      0.74303	      0.98382	         0.55	      0.98759	      0.90909	      0.98439	       1.4091	      0.97854	       1.8447	      0.97478	         2.75	       0.9718	          3.3	      0.96742	          4.4	
    25	    44	      0.55889	       126.55	   4.066e+005	   6.901e+005	       52.641	       27.737	      0.80163	     8.3e-007	      0.79788	      0.74164	      0.98555	         0.55	      0.98935	      0.90909	      0.98627	       1.4169	       0.9804	       1.8534	      0.97662	         2.75	      0.97363	          3.3	      0.96925	          4.4	
    26	    45	      0.55889	       126.55	   4.066e+005	   6.901e+005	       52.635	       27.745	      0.80098	     8.3e-007	       0.7972	        0.741	      0.98636	         0.55	      0.99016	      0.90909	      0.98712	       1.4172	      0.98126	       1.8616	      0.97746	         2.75	      0.97447	          3.3	      0.97009	          4.4	
    27	    52	      0.55491	       87.384	   4.066e+005	   6.901e+005	       17.475	       27.139	      0.80152	     8.3e-007	      0.79641	      0.74211	      0.98525	         0.55	      0.98883	      0.90909	      0.98562	       1.4096	      0.97981	       1.8587	      0.97603	         2.75	      0.97305	          3.3	      0.96869	          4.4	
    28	    54	      0.55244	       65.862	   4.066e+005	   6.901e+005	       1e-006	       25.825	      0.80787	     8.3e-007	      0.80307	      0.74907	      0.97708	         0.55	      0.98048	      0.90909	      0.97632	       1.3636	      0.97066	       1.8182	      0.96689	         2.75	      0.96391	          3.3	      0.95955	          4.4	
    29	    56	      0.55175	       62.032	   4.066e+005	   6.901e+005	       1e-006	       27.939	      0.79602	     8.3e-007	      0.79121	       0.7376	        0.991	         0.55	      0.99408	      0.90909	      0.99146	        1.477	      0.98533	       1.8443	      0.98166	         2.75	       0.9787	          3.3	      0.97435	          4.4	
    30	    57	       0.5507	       58.266	   4.066e+005	   6.901e+005	       1e-006	       25.589	      0.77984	     8.3e-007	      0.76941	      0.72217	        1.011	         0.55	       1.0141	      0.90909	       1.0122	       1.4603	       1.0063	       1.9244	       1.0025	         2.75	       0.9995	          3.3	       0.9951	          4.4	
    31	    58	      0.54917	        55.39	   4.066e+005	   6.901e+005	       1e-006	       26.644	       0.7822	     8.3e-007	        0.773	      0.72492	       1.0079	         0.55	       1.0108	      0.90909	       1.0089	       1.4984	       1.0029	       1.9352	      0.99915	         2.75	      0.99619	          3.3	      0.99182	          4.4	
    32	    59	      0.54916	        55.03	   4.066e+005	   6.901e+005	       1e-006	       26.503	      0.77841	     8.3e-007	      0.76888	       0.7212	       1.0128	         0.55	       1.0157	      0.90909	       1.0141	       1.5064	       1.0081	        1.956	       1.0043	         2.75	       1.0013	          3.3	      0.99691	          4.4	
    33	    72	      0.54915	        55.03	   4.066e+005	   6.901e+005	   0.00070499	       26.503	      0.77842	     8.3e-007	      0.76889	      0.72123	       1.0128	         0.55	       1.0157	      0.90909	       1.0141	       1.5064	       1.0081	        1.956	       1.0043	         2.75	       1.0013	          3.3	      0.99691	          4.4	
    34	    73	      0.54915	        55.03	   4.066e+005	   6.901e+005	   0.00056881	       26.503	      0.77851	     8.3e-007	      0.76898	       0.7213	       1.0127	         0.55	       1.0155	      0.90909	        1.014	       1.5064	        1.008	        1.956	       1.0042	         2.75	       1.0012	          3.3	      0.99681	          4.4	
    35	    74	      0.54915	        55.03	   4.066e+005	   6.901e+005	   0.00028783	       26.503	      0.77857	     8.3e-007	      0.76904	      0.72135	       1.0127	         0.55	       1.0155	      0.90909	       1.0139	       1.5064	       1.0079	        1.956	       1.0041	         2.75	       1.0011	          3.3	      0.99673	          4.4	
    36	    75	      0.54915	        55.03	   4.066e+005	   6.901e+005	       1e-006	       26.503	      0.77858	     8.3e-007	      0.76905	      0.72137	       1.0126	         0.55	       1.0155	      0.90909	       1.0139	       1.5064	       1.0079	        1.956	       1.0041	         2.75	       1.0011	          3.3	      0.99671	          4.4	
    37	    88	      0.54915	        55.03	   4.066e+005	   6.901e+005	  1.0322e-006	       26.503	      0.77858	     8.3e-007	      0.76905	      0.72137	       1.0126	         0.55	       1.0155	      0.90909	       1.0139	       1.5064	       1.0079	        1.956	       1.0041	         2.75	       1.0011	          3.3	      0.99671	          4.4	
    38	    89	      0.54915	        55.03	   4.066e+005	   6.901e+005	  1.0322e-006	       26.503	      0.77858	     8.3e-007	      0.76905	      0.72137	       1.0126	         0.55	       1.0155	      0.90909	       1.0139	       1.5064	       1.0079	        1.956	       1.0041	         2.75	       1.0011	          3.3	      0.99671	          4.4	

:: False convergence

       Squares	       0.549153
     Func Eval	             89
     Grad Eval	             38

:: Nonlinear parameters

     i	   Final p(i)	  Gradient(i)
     1	      55.0296	  3.4954e-007
     2	       406600	 -1.8483e-011
     3	       690096	  2.3734e-011
     4	 1.03221e-006	     -0.12463
     5	      26.5027	 -9.9091e-006
     6	     0.778581	 -7.7638e-005
     7	     8.3e-007	  1.2151e-005
     8	     0.769052	 -7.2572e-005
     9	     0.721365	 -5.2187e-005
    10	      1.01264	  9.9168e-005
    11	         0.55	    -0.060089
    12	      1.01546	   0.00010646
    13	     0.909091	     0.037932
    14	      1.01389	  9.2089e-005
    15	      1.50643	 -7.6689e-006
    16	      1.00791	   0.00014017
    17	      1.95602	  1.1477e-005
    18	      1.00407	   0.00010077
    19	         2.75	  -0.00012279
    20	      1.00109	   0.00010039
    21	          3.3	 -7.8238e-005
    22	     0.996714	  9.9997e-005
    23	          4.4	  -0.00039316
