
===================================================
Least squares Minimization - Trust Region Algorithm
===================================================

Optimized parameters:

     1 	 k.1
     2 	 k.-1
     3 	 k.2
     4 	 k.-2
     5 	 k.cat
     6 	 r(E)
     7 	 r(E.S1)
     8 	 r(E.S2)
     9 	 r(E.P)
    10 	 [E]#1
    11 	 [S]#1
    12 	 [E]#2
    13 	 [S]#2
    14 	 [E]#3
    15 	 [S]#3
    16 	 [E]#4
    17 	 [S]#4
    18 	 [E]#5
    19 	 [S]#5
    20 	 [E]#6
    21 	 [S]#6
    22 	 [E]#7
    23 	 [S]#7

:: NL2SOL :: DNLAFB
:: Analytic Jacobian; Constrained

________________________________________________________________________

:: Initial Estimate

     i	     Param p(i)	            Min	            Max
     1	        0.02485	         1e-006	         1e+006
     2	          106.3	         1e-006	         1e+006
     3	           1188	         1e-006	         1e+006
     4	          203.5	         1e-006	         1e+006
     5	          98.66	         1e-006	         1e+006
     6	           0.85	       8.5e-007	         850000
     7	           0.83	       8.3e-007	         830000
     8	           0.75	       7.5e-007	         750000
     9	           0.81	       8.1e-007	         810000
    10	              1	       0.909091	            1.1
    11	            0.5	       0.454545	           0.55
    12	              1	       0.909091	            1.1
    13	              1	       0.909091	            1.1
    14	              1	       0.909091	            1.1
    15	            1.5	        1.36364	           1.65
    16	              1	       0.909091	            1.1
    17	              2	        1.81818	            2.2
    18	              1	       0.909091	            1.1
    19	            2.5	        2.27273	           2.75
    20	              1	       0.909091	            1.1
    21	              3	        2.72727	            3.3
    22	              1	       0.909091	            1.1
    23	              4	        3.63636	            4.4

:: Iterations

     i	 FEval	   F(i) = SSQ	   Param( 1)	   Param( 2)	   Param( 3)	   Param( 4)	   Param( 5)	   Param( 6)	   Param( 7)	   Param( 8)	   Param( 9)	   Param(10)	   Param(11)	   Param(12)	   Param(13)	   Param(14)	   Param(15)	   Param(16)	   Param(17)	   Param(18)	   Param(19)	   Param(20)	   Param(21)	   Param(22)	   Param(23)	
     0	     1	       154.58	      0.02485	        106.3	         1188	        203.5	        98.66	         0.85	         0.83	         0.75	         0.81	            1	          0.5	            1	            1	            1	          1.5	            1	            2	            1	          2.5	            1	            3	            1	            4	
     1	     2	       154.58	      0.02485	        106.3	         1188	        203.5	        98.66	         0.85	         0.83	         0.75	         0.81	            1	          0.5	            1	            1	            1	          1.5	            1	            2	            1	          2.5	            1	            3	            1	            4	

:: Singular convergence

       Squares	        154.581
     Func Eval	              2
     Grad Eval	              1

:: Nonlinear parameters

     i	   Final p(i)	  Gradient(i)
     1	      0.02485	      -95.626
     2	        106.3	    0.0047787
     3	         1188	  -0.00042905
     4	        203.5	    0.0016822
     5	        98.66	    -0.003125
     6	         0.85	       1292.1
     7	         0.83	      0.13517
     8	         0.75	      0.52858
     9	         0.81	       67.458
    10	            1	       131.47
    11	          0.5	     -0.16584
    12	            1	       151.91
    13	            1	     -0.18359
    14	            1	       163.61
    15	          1.5	     -0.18311
    16	            1	       170.73
    17	            2	     -0.17827
    18	            1	       175.34
    19	          2.5	     -0.17386
    20	            1	       178.29
    21	            3	     -0.16783
    22	            1	       182.17
    23	            4	     -0.15864
