
===================================================
Least squares Minimization - Trust Region Algorithm
===================================================

Optimized parameters:

     1 	 k.1
     2 	 k.-1
     3 	 k.2
     4 	 k.-2
     5 	 k.cat
     6 	 r(E)
     7 	 r(E.S1)
     8 	 r(E.S2)
     9 	 r(E.P)
    10 	 [E]#1
    11 	 [S]#1
    12 	 [E]#2
    13 	 [S]#2
    14 	 [E]#3
    15 	 [S]#3
    16 	 [E]#4
    17 	 [S]#4
    18 	 [E]#5
    19 	 [S]#5
    20 	 [E]#6
    21 	 [S]#6
    22 	 [E]#7
    23 	 [S]#7

:: NL2SOL :: DNLAFB
:: Analytic Jacobian; Constrained

________________________________________________________________________

:: Initial Estimate

     i	     Param p(i)	            Min	            Max
     1	     2.341e-005	         1e-006	         1e+006
     2	          42390	         1e-006	         1e+006
     3	          8.212	         1e-006	         1e+006
     4	        0.00909	         1e-006	         1e+006
     5	        0.05411	         1e-006	         1e+006
     6	           0.85	       8.5e-007	         850000
     7	           0.83	       8.3e-007	         830000
     8	           0.75	       7.5e-007	         750000
     9	           0.81	       8.1e-007	         810000
    10	              1	       0.909091	            1.1
    11	            0.5	       0.454545	           0.55
    12	              1	       0.909091	            1.1
    13	              1	       0.909091	            1.1
    14	              1	       0.909091	            1.1
    15	            1.5	        1.36364	           1.65
    16	              1	       0.909091	            1.1
    17	              2	        1.81818	            2.2
    18	              1	       0.909091	            1.1
    19	            2.5	        2.27273	           2.75
    20	              1	       0.909091	            1.1
    21	              3	        2.72727	            3.3
    22	              1	       0.909091	            1.1
    23	              4	        3.63636	            4.4

:: Iterations

     i	 FEval	   F(i) = SSQ	   Param( 1)	   Param( 2)	   Param( 3)	   Param( 4)	   Param( 5)	   Param( 6)	   Param( 7)	   Param( 8)	   Param( 9)	   Param(10)	   Param(11)	   Param(12)	   Param(13)	   Param(14)	   Param(15)	   Param(16)	   Param(17)	   Param(18)	   Param(19)	   Param(20)	   Param(21)	   Param(22)	   Param(23)	
     0	     1	       160.29	   2.341e-005	        42390	        8.212	      0.00909	      0.05411	         0.85	         0.83	         0.75	         0.81	            1	          0.5	            1	            1	            1	          1.5	            1	            2	            1	          2.5	            1	            3	            1	            4	
     1	     2	       104.52	       1e-006	        42390	       8.2124	    0.0023008	     0.050289	      0.82506	      0.82988	      0.74423	      0.80885	       1.0223	      0.50001	       1.0113	            1	       1.0059	          1.5	      0.99731	            2	       0.9977	          2.5	      0.99796	            3	      0.99538	       4.0001	
     2	     3	       42.953	       1e-006	        42390	       8.2124	    0.0022925	     0.050283	      0.79179	      0.82988	      0.74423	      0.80885	       1.0255	      0.50001	        1.012	            1	       1.0062	          1.5	      0.99606	            2	      0.99624	          2.5	      0.99623	            3	      0.99286	       4.0001	
     3	     4	        16.87	       1e-006	        42390	       8.2124	    0.0022904	     0.050281	       0.7687	      0.82988	      0.74423	      0.80885	        1.031	      0.50001	       1.0136	            1	       1.0052	          1.5	      0.99516	            2	      0.99488	          2.5	      0.99494	            3	      0.99107	       4.0001	
     4	     5	       6.7531	       1e-006	        42390	       8.2124	    0.0022897	     0.050281	      0.75311	      0.82988	      0.74423	      0.80885	        1.034	      0.50001	       1.0148	            1	       1.0052	          1.5	      0.99481	            2	       0.9944	          2.5	      0.99411	            3	      0.98982	       4.0001	
     5	     6	       2.4129	       1e-006	        42390	       8.2124	    0.0022897	     0.050281	      0.73591	      0.82988	      0.74423	      0.80885	       1.0346	      0.50001	       1.0152	            1	       1.0038	          1.5	      0.99655	            2	      0.99175	          2.5	      0.98847	            3	      0.98368	       4.0001	
     6	     7	       2.4128	     0.035471	        42390	       8.2124	    0.0022897	     0.050281	      0.73588	      0.82988	      0.74423	      0.80885	       1.0346	      0.50001	       1.0152	            1	       1.0038	          1.5	      0.99663	            2	      0.99173	          2.5	      0.98837	            3	      0.98357	       4.0001	
     7	    10	       2.4105	      0.39793	        42390	        8.214	       1e-006	      0.20528	      0.72902	      0.82956	          1.1	       0.9461	       1.0443	      0.49945	       1.0247	      0.99911	       1.0132	       1.5002	       1.0059	       2.0011	        1.001	       2.5013	      0.99758	       3.0016	      0.99272	       4.0011	
     8	    31	       2.4105	      0.39793	        42390	        8.214	       1e-006	      0.20528	      0.72902	      0.82956	          1.1	       0.9461	       1.0443	      0.49945	       1.0247	      0.99911	       1.0132	       1.5002	       1.0059	       2.0011	        1.001	       2.5013	      0.99758	       3.0016	      0.99272	       4.0011	

:: False convergence

       Squares	        2.41052
     Func Eval	             31
     Grad Eval	              8

:: Nonlinear parameters

     i	   Final p(i)	  Gradient(i)
     1	     0.397926	   -0.0026376
     2	        42390	  2.4749e-008
     3	      8.21396	  -0.00012787
     4	       1e-006	    0.0055163
     5	      0.20528	    0.0016451
     6	      0.72902	     0.050631
     7	     0.829565	  8.7104e-006
     8	      1.09996	  -0.00068773
     9	     0.946101	   -0.0036769
    10	      1.04428	    -0.066085
    11	     0.499455	   0.00014536
    12	       1.0247	    -0.040779
    13	     0.999112	   0.00023119
    14	      1.01318	    -0.018403
    15	      1.50021	  5.6525e-005
    16	      1.00591	   0.00058445
    17	       2.0011	 -9.3114e-005
    18	      1.00096	     0.024662
    19	      2.50126	   -0.0001277
    20	     0.997575	     0.047514
    21	      3.00157	  -0.00017658
    22	     0.992722	     0.090109
    23	      4.00108	   -0.0001007
