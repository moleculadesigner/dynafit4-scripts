
PARAMETERS AND CONFIDENCE INTERVALS

NO.	SET	           PARAMETER	     INITIAL	    LIMIT-LO	    LIMIT-HI	         FIT	      STDERR	         CV%	      F(SDI)	   CONLIN-LO	   CONLIN-HI	   CONLIN-P%	

  1	  1	                time	           0	
  2	  1	             restart	           0	
  3	  *	                 k.1	      305400	      1e-006	      1e+006	      305400	3.37166e+007	     85019.3	     15671.8	-8.65562e+007	 8.7167e+007	          99	
  4	  *	                k.-1	  1.118e-006	      1e-006	      1e+006	   0.0257314	     5.99926	      179547	     33096.3	    -15.4298	     15.4812	          99	
  5	  *	                 k.2	       24790	      1e-006	      1e+006	       24790	      926693	     28787.4	     5306.47	-2.36258e+006	2.41216e+006	          99	
  6	  *	                k.-2	  6.165e-006	      1e-006	      1e+006	   0.0683331	      173.83	1.95901e+006	      361110	    -447.758	     447.895	          99	
  7	  *	               k.cat	      0.7815	      1e-006	      1e+006	     18.8522	    0.285637	      11.668	     2.15079	     18.1163	     19.5881	          99	
  8	  1	                 [E]	           1	    0.909091	         1.1	     1.03411	     112.767	     83977.2	     15479.8	     -289.48	     291.549	          99	
  9	  1	                 [S]	         0.5	    0.454545	        0.55	        0.55	     60.0228	     84042.1	     15491.7	    -154.083	     155.183	          99	
 10	  1	              [E.S1]	           0	
 11	  1	              [E.S2]	           0	
 12	  1	               [E.P]	           0	
 13	  *	                r(E)	        0.85	    8.5e-007	      850000	    0.668208	     72.8044	     83905.3	     15466.5	    -186.893	     188.229	          99	
 14	  1	                r(S)	           0	
 15	  *	             r(E.S1)	        0.83	    8.3e-007	      830000	    0.862064	     94.0553	     84020.8	     15487.8	    -241.446	      243.17	          99	
 16	  *	             r(E.S2)	        0.75	    7.5e-007	      750000	    0.852282	     93.0112	     84041.7	     15491.7	    -238.766	      240.47	          99	
 17	  *	              r(E.P)	        0.81	    8.1e-007	      810000	    0.789413	     86.1504	     84041.9	     15491.7	    -221.154	     222.733	          99	
 18	  1	              offset	     0.38165	
 19	  1	               delay	           0	
 20	  1	          incubation	           0	
 21	  1	                [E]i	           0	
 22	  1	                [S]i	           0	
 23	  1	             [E.S1]i	           0	
 24	  1	             [E.S2]i	           0	
 25	  1	              [E.P]i	           0	
 26	  1	            dilution	           0	
 27	  1	           intensive	           0	
 28	  2	                time	           0	
 29	  2	             restart	           0	
 30	  2	                 [E]	           1	    0.909091	         1.1	    0.938403	      102.41	     84041.9	     15491.7	    -262.893	      264.77	          99	
 31	  2	                 [S]	           1	    0.909091	         1.1	    0.938534	     102.424	       84042	     15491.7	     -262.93	     264.807	          99	
 32	  2	              [E.S1]	           0	
 33	  2	              [E.S2]	           0	
 34	  2	               [E.P]	           0	
 35	  2	                r(S)	           0	
 36	  2	              offset	    0.357428	
 37	  2	               delay	           0	
 38	  2	          incubation	           0	
 39	  2	                [E]i	           0	
 40	  2	                [S]i	           0	
 41	  2	             [E.S1]i	           0	
 42	  2	             [E.S2]i	           0	
 43	  2	              [E.P]i	           0	
 44	  2	            dilution	           0	
 45	  2	           intensive	           0	
 46	  3	                time	           0	
 47	  3	             restart	           0	
 48	  3	                 [E]	           1	    0.909091	         1.1	    0.927853	     101.259	     84041.9	     15491.7	    -259.937	     261.793	          99	
 49	  3	                 [S]	         1.5	     1.36364	        1.65	     1.36364	     148.671	     83959.5	     15476.5	    -381.646	     384.373	          99	
 50	  3	              [E.S1]	           0	
 51	  3	              [E.S2]	           0	
 52	  3	               [E.P]	           0	
 53	  3	                r(S)	           0	
 54	  3	              offset	    0.316368	
 55	  3	               delay	           0	
 56	  3	          incubation	           0	
 57	  3	                [E]i	           0	
 58	  3	                [S]i	           0	
 59	  3	             [E.S1]i	           0	
 60	  3	             [E.S2]i	           0	
 61	  3	              [E.P]i	           0	
 62	  3	            dilution	           0	
 63	  3	           intensive	           0	
 64	  4	                time	           0	
 65	  4	             restart	           0	
 66	  4	                 [E]	           1	    0.909091	         1.1	    0.921177	      100.53	     84041.9	     15491.7	    -258.067	      259.91	          99	
 67	  4	                 [S]	           2	     1.81818	         2.2	     1.83054	     207.736	     87392.6	     16109.3	    -533.345	     537.006	          99	
 68	  4	              [E.S1]	           0	
 69	  4	              [E.S2]	           0	
 70	  4	               [E.P]	           0	
 71	  4	                r(S)	           0	
 72	  4	              offset	    0.278203	
 73	  4	               delay	           0	
 74	  4	          incubation	           0	
 75	  4	                [E]i	           0	
 76	  4	                [S]i	           0	
 77	  4	             [E.S1]i	           0	
 78	  4	             [E.S2]i	           0	
 79	  4	              [E.P]i	           0	
 80	  4	            dilution	           0	
 81	  4	           intensive	           0	
 82	  5	                time	           0	
 83	  5	             restart	           0	
 84	  5	                 [E]	           1	    0.909091	         1.1	    0.916618	     100.033	     84041.9	     15491.7	     -256.79	     258.623	          99	
 85	  5	                 [S]	         2.5	     2.27273	        2.75	     2.43342	     488.473	      154585	     28495.1	    -1255.99	     1260.85	          99	
 86	  5	              [E.S1]	           0	
 87	  5	              [E.S2]	           0	
 88	  5	               [E.P]	           0	
 89	  5	                r(S)	           0	
 90	  5	              offset	    0.239307	
 91	  5	               delay	           0	
 92	  5	          incubation	           0	
 93	  5	                [E]i	           0	
 94	  5	                [S]i	           0	
 95	  5	             [E.S1]i	           0	
 96	  5	             [E.S2]i	           0	
 97	  5	              [E.P]i	           0	
 98	  5	            dilution	           0	
 99	  5	           intensive	           0	
100	  6	                time	           0	
101	  6	             restart	           0	
102	  6	                 [E]	           1	    0.909091	         1.1	    0.913511	     99.6934	     84041.9	     15491.7	     -255.92	     257.747	          99	
103	  6	                 [S]	           3	     2.72727	         3.3	     3.08492	     778.312	      194291	     35814.3	    -2002.03	      2008.2	          99	
104	  6	              [E.S1]	           0	
105	  6	              [E.S2]	           0	
106	  6	               [E.P]	           0	
107	  6	                r(S)	           0	
108	  6	              offset	    0.202509	
109	  6	               delay	           0	
110	  6	          incubation	           0	
111	  6	                [E]i	           0	
112	  6	                [S]i	           0	
113	  6	             [E.S1]i	           0	
114	  6	             [E.S2]i	           0	
115	  6	              [E.P]i	           0	
116	  6	            dilution	           0	
117	  6	           intensive	           0	
118	  7	                time	           0	
119	  7	             restart	           0	
120	  7	                 [E]	           1	    0.909091	         1.1	    0.909091	     99.2111	     84041.9	     15491.7	    -254.681	       256.5	          99	
121	  7	                 [S]	           4	     3.63636	         4.4	     3.99696	     1366.37	      263257	       48527	    -3516.08	     3524.07	          99	
122	  7	              [E.S1]	           0	
123	  7	              [E.S2]	           0	
124	  7	               [E.P]	           0	
125	  7	                r(S)	           0	
126	  7	              offset	    0.172856	
127	  7	               delay	           0	
128	  7	          incubation	           0	
129	  7	                [E]i	           0	
130	  7	                [S]i	           0	
131	  7	             [E.S1]i	           0	
132	  7	             [E.S2]i	           0	
133	  7	              [E.P]i	           0	
134	  7	            dilution	           0	
135	  7	           intensive	           0	
