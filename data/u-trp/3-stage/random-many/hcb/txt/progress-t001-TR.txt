
===================================================
Least squares Minimization - Trust Region Algorithm
===================================================

Optimized parameters:

     1 	 k.1
     2 	 k.-1
     3 	 k.2
     4 	 k.-2
     5 	 k.cat
     6 	 r(E)
     7 	 r(E.S1)
     8 	 r(E.S2)
     9 	 r(E.P)
    10 	 [E]#1
    11 	 [S]#1
    12 	 [E]#2
    13 	 [S]#2
    14 	 [E]#3
    15 	 [S]#3
    16 	 [E]#4
    17 	 [S]#4
    18 	 [E]#5
    19 	 [S]#5
    20 	 [E]#6
    21 	 [S]#6
    22 	 [E]#7
    23 	 [S]#7

:: NL2SOL :: DNLAFB
:: Analytic Jacobian; Constrained

________________________________________________________________________

:: Initial Estimate

     i	     Param p(i)	            Min	            Max
     1	      0.0001449	         1e-006	         1e+006
     2	           1485	         1e-006	         1e+006
     3	      0.0004468	         1e-006	         1e+006
     4	         0.5217	         1e-006	         1e+006
     5	         0.2276	         1e-006	         1e+006
     6	           0.85	       8.5e-007	         850000
     7	           0.83	       8.3e-007	         830000
     8	           0.75	       7.5e-007	         750000
     9	           0.81	       8.1e-007	         810000
    10	              1	       0.909091	            1.1
    11	            0.5	       0.454545	           0.55
    12	              1	       0.909091	            1.1
    13	              1	       0.909091	            1.1
    14	              1	       0.909091	            1.1
    15	            1.5	        1.36364	           1.65
    16	              1	       0.909091	            1.1
    17	              2	        1.81818	            2.2
    18	              1	       0.909091	            1.1
    19	            2.5	        2.27273	           2.75
    20	              1	       0.909091	            1.1
    21	              3	        2.72727	            3.3
    22	              1	       0.909091	            1.1
    23	              4	        3.63636	            4.4

:: Iterations

     i	 FEval	   F(i) = SSQ	   Param( 1)	   Param( 2)	   Param( 3)	   Param( 4)	   Param( 5)	   Param( 6)	   Param( 7)	   Param( 8)	   Param( 9)	   Param(10)	   Param(11)	   Param(12)	   Param(13)	   Param(14)	   Param(15)	   Param(16)	   Param(17)	   Param(18)	   Param(19)	   Param(20)	   Param(21)	   Param(22)	   Param(23)	
     0	     1	       160.29	    0.0001449	         1485	    0.0004468	       0.5217	       0.2276	         0.85	         0.83	         0.75	         0.81	            1	          0.5	            1	            1	            1	          1.5	            1	            2	            1	          2.5	            1	            3	            1	            4	
     1	     2	       2.4745	       1.8183	         1485	    0.0060011	      0.52171	      0.22759	      0.74915	      0.81674	      0.74995	      0.81007	       1.0144	      0.50001	      0.99761	            1	      0.98775	          1.5	      0.98153	            2	       0.9773	          2.5	      0.97441	            3	      0.97028	            4	
     2	     3	       2.4315	       1.2684	         1485	       1.0509	      0.51402	      0.24913	      0.80027	     8.3e-007	      0.63426	       0.9441	       0.9479	      0.49314	       0.9316	      0.99152	       0.9225	       1.4911	      0.91707	       1.9904	      0.91353	        2.491	      0.91157	       2.9923	      0.90909	       3.9903	
     3	     4	       2.3865	       1.4784	         1485	       1.4974	      0.43895	      0.62694	      0.79996	     8.3e-007	      0.63426	       0.9819	      0.95025	      0.52893	      0.93339	       1.0393	      0.92385	       1.5417	      0.91805	       2.0385	      0.91385	       2.5315	      0.91208	       3.0293	      0.90909	       4.0183	
     4	     5	       2.2744	        1.855	         1485	       1.9469	      0.28623	      0.65855	       0.8013	     8.3e-007	     7.5e-007	       1.1215	         0.95	      0.54585	       0.9331	       1.0417	      0.92357	       1.6057	      0.91783	       2.1404	      0.91396	       2.6169	      0.91193	       3.1146	      0.90909	        4.029	
     5	     6	       2.2508	       2.9211	         1485	       3.0084	      0.33826	      0.99413	      0.80483	     8.3e-007	     7.5e-007	       0.8321	      0.94785	      0.54585	      0.93133	       1.0417	      0.92221	       1.6057	      0.91688	       2.1404	      0.91366	       2.6169	      0.91156	       3.1146	      0.90909	       3.9769	
     6	     7	       2.1212	       3.1779	         1485	       3.7579	      0.24771	       1.1608	      0.80719	     8.3e-007	     7.5e-007	      0.85286	      0.94644	      0.54585	      0.93065	       1.0417	      0.92181	       1.6057	      0.91641	       2.1404	      0.91395	       2.6169	      0.91242	       3.2797	      0.91016	       3.9972	
     7	     8	        2.028	       3.6348	         1485	       4.5765	      0.26732	       1.2827	       0.8098	     8.3e-007	     7.5e-007	      0.85209	      0.94424	      0.54585	      0.92871	       1.0417	      0.92039	       1.6057	      0.91568	       2.1404	       0.9136	       2.6169	      0.91224	       3.2797	      0.91013	       3.9184	
     8	     9	       1.9255	       4.1885	         1485	        5.436	       0.2682	       1.4943	      0.81283	     8.3e-007	     7.5e-007	      0.84679	      0.94154	      0.54585	      0.92646	       1.0417	      0.91873	       1.6057	      0.91462	       2.1404	        0.913	       2.6169	      0.91201	       3.2797	      0.90952	       3.7346	
     9	    10	       1.8238	       4.8043	         1485	       6.3243	      0.30231	       1.7692	        0.816	     8.3e-007	     7.5e-007	      0.83845	      0.93869	      0.54585	      0.92414	       1.0417	      0.91701	       1.6057	      0.91347	       2.1404	       0.9123	       2.6169	      0.91193	       3.2797	      0.90952	       3.7346	
    10	    11	       1.7241	       5.4405	         1485	       7.2178	      0.35812	       2.0717	      0.81863	     8.3e-007	     7.5e-007	      0.83099	      0.93647	      0.54585	       0.9225	       1.0417	      0.91599	       1.6057	        0.913	       2.1404	      0.91224	       2.6169	      0.91258	       3.2797	      0.91027	       3.7346	
    11	    12	       1.6287	       6.0796	         1485	       8.1062	      0.42692	       2.3798	      0.82061	     8.3e-007	     7.5e-007	      0.82487	      0.93496	      0.54585	      0.92158	       1.0417	      0.91568	       1.6057	      0.91321	       2.1404	      0.91293	       2.6308	      0.91389	       3.2797	      0.91171	       3.7346	
    12	    13	       1.5396	       6.7055	         1485	       8.9768	      0.49948	       2.6735	      0.82152	     8.3e-007	     7.5e-007	      0.81933	      0.93466	      0.54585	      0.92185	       1.0417	      0.91655	       1.6057	      0.91458	       2.1404	      0.91478	       2.6427	      0.91629	       3.2797	      0.91413	       3.7149	
    13	    14	       1.4587	       7.3021	         1485	         9.81	      0.57449	        2.939	      0.82114	     8.3e-007	     7.5e-007	      0.81374	      0.93577	      0.54585	      0.92352	       1.0417	      0.91878	       1.6057	      0.91729	       2.1404	      0.91793	       2.6515	      0.91995	       3.2797	      0.91779	       3.6924	
    14	    15	       1.3864	       7.8546	         1485	       10.585	      0.65312	       3.1656	      0.81965	     8.3e-007	     7.5e-007	      0.80803	       0.9381	      0.54585	      0.92636	       1.0417	      0.92215	       1.6057	      0.92111	       2.1404	      0.92207	       2.6617	      0.92467	       3.2797	      0.92254	       3.6689	
    15	    16	       1.3333	       8.2769	         1485	       11.195	      0.74121	       3.3048	      0.81735	     8.3e-007	     7.5e-007	      0.80259	      0.94118	      0.54585	      0.92985	       1.0417	      0.92608	       1.6057	       0.9254	       2.1404	      0.92672	       2.6718	      0.92967	       3.2797	      0.92778	       3.6689	
    16	    17	       1.2967	       8.5642	         1485	        11.62	      0.78846	       3.4115	      0.81516	     8.3e-007	     7.5e-007	      0.79826	      0.94394	      0.54585	       0.9329	       1.0417	      0.92945	       1.6057	      0.92906	       2.1404	      0.93075	       2.6794	      0.93383	       3.2797	      0.93217	       3.6689	
    17	    18	       1.2752	       8.7249	         1485	       11.873	      0.80996	       3.4674	      0.81364	     8.3e-007	     7.5e-007	      0.79562	      0.94574	      0.54585	       0.9349	       1.0417	      0.93167	       1.6057	      0.93148	       2.1404	      0.93338	       2.6794	       0.9366	       3.2797	      0.93513	       3.6689	
    18	    19	       1.2657	       8.7821	         1485	       11.987	      0.81278	       3.4799	      0.81291	     8.3e-007	     7.5e-007	       0.7946	       0.9465	      0.54585	      0.93578	       1.0417	      0.93269	       1.6057	      0.93271	       2.1404	       0.9346	       2.6794	      0.93801	       3.2797	      0.93657	       3.6689	
    19	    20	       1.2551	        8.845	         1485	       12.055	      0.84167	        3.547	      0.81272	     8.3e-007	     7.5e-007	      0.79443	      0.94663	      0.54585	      0.93598	       1.0417	      0.93298	       1.6057	      0.93314	       2.1404	      0.93497	       2.6794	      0.93859	       3.2862	      0.93723	       3.6689	
    20	    22	       1.2311	       8.9833	         1485	       12.212	      0.86553	       3.6335	      0.81329	     8.3e-007	     7.5e-007	      0.79362	      0.94549	      0.54585	      0.93526	       1.0417	      0.93271	       1.6392	      0.93326	       2.1409	      0.93615	         2.75	      0.93882	       3.2985	       0.9379	       3.6689	
    21	    23	       1.2179	       9.1018	         1485	       12.358	      0.88095	       3.7126	       0.8127	     8.3e-007	     7.5e-007	      0.79254	      0.94633	      0.54585	      0.93612	       1.0417	      0.93383	       1.6392	       0.9342	       2.1409	      0.93713	         2.75	      0.93992	       3.2985	      0.93919	       3.6689	
    22	    24	       1.2069	       9.2048	         1485	       12.482	      0.88926	       3.7748	      0.81222	     8.3e-007	     7.5e-007	      0.79158	        0.947	      0.54585	      0.93684	       1.0417	       0.9347	       1.6392	        0.935	       2.1409	      0.93795	         2.75	      0.94086	       3.2985	      0.94026	       3.6689	
    23	    25	       1.1976	       9.2949	         1485	       12.587	      0.89376	       3.8255	      0.81185	     8.3e-007	     7.5e-007	      0.79075	      0.94753	      0.54585	      0.93742	       1.0417	      0.93539	       1.6392	      0.93568	       2.1409	      0.93865	         2.75	      0.94165	       3.2985	      0.94115	       3.6689	
    24	    26	       1.1897	       9.3737	         1485	       12.676	      0.89618	       3.8676	      0.81156	     8.3e-007	     7.5e-007	      0.79003	      0.94795	      0.54585	       0.9379	       1.0417	      0.93595	       1.6392	      0.93626	       2.1409	      0.93924	         2.75	      0.94234	       3.2985	      0.94189	       3.6689	
    25	    27	       1.1829	       9.4429	         1485	       12.752	      0.89739	       3.9034	      0.81132	     8.3e-007	     7.5e-007	      0.78941	       0.9483	      0.54585	       0.9383	       1.0417	      0.93641	       1.6392	      0.93675	       2.1409	      0.93974	         2.75	      0.94292	       3.2985	      0.94252	       3.6689	
    26	    28	        1.177	       9.5039	         1485	       12.817	      0.89788	       3.9342	      0.81113	     8.3e-007	     7.5e-007	      0.78887	      0.94857	      0.54585	      0.93862	       1.0417	      0.93679	       1.6392	      0.93717	       2.1409	      0.94017	         2.75	      0.94343	       3.2985	      0.94306	       3.6689	
    27	    29	       1.1718	       9.5579	         1485	       12.873	       0.8979	       3.9608	      0.81098	     8.3e-007	     7.5e-007	       0.7884	       0.9488	      0.54585	       0.9389	       1.0417	      0.93712	       1.6392	      0.93754	       2.1409	      0.94054	         2.75	      0.94386	       3.2985	      0.94353	       3.6689	
    28	    30	       1.1672	        9.606	         1485	       12.921	       0.8976	       3.9841	      0.81086	     8.3e-007	     7.5e-007	      0.78799	      0.94898	      0.54585	      0.93912	       1.0417	      0.93743	       1.6392	      0.93782	       2.1409	      0.94087	         2.75	      0.94425	       3.2985	      0.94393	       3.6689	
    29	    31	       1.1631	        9.649	         1485	       12.964	      0.89705	       4.0046	      0.81076	     8.3e-007	     7.5e-007	      0.78764	      0.94913	      0.54585	      0.93932	       1.0417	       0.9377	       1.6392	      0.93805	       2.1409	      0.94116	         2.75	      0.94459	       3.2985	      0.94429	       3.6689	
    30	    32	       1.1594	       9.6878	         1485	       13.002	      0.89631	       4.0228	      0.81068	     8.3e-007	     7.5e-007	      0.78732	      0.94926	      0.54585	      0.93948	       1.0417	      0.93793	       1.6392	      0.93826	       2.1409	      0.94142	         2.75	      0.94489	       3.2985	      0.94461	       3.6689	
    31	    33	       1.1561	       9.7229	         1485	       13.036	      0.89537	       4.0389	      0.81061	     8.3e-007	     7.5e-007	      0.78704	      0.94935	      0.54585	      0.93962	       1.0417	      0.93813	       1.6392	      0.93844	       2.1409	      0.94164	         2.75	      0.94516	       3.2985	      0.94489	       3.6689	
    32	    35	       1.1484	       9.7427	         1485	       13.053	      0.89336	       4.0455	      0.81086	     8.3e-007	     7.5e-007	      0.78675	      0.94919	      0.54585	      0.93921	       1.0974	      0.93829	       1.6392	      0.93901	       2.1409	      0.94127	         2.75	      0.94505	       3.2985	      0.94488	       3.6496	
    33	    36	       1.1393	       9.7711	         1485	       13.078	      0.89113	       4.0558	      0.81076	    0.0042004	     7.5e-007	      0.78657	       0.9493	         0.55	      0.93977	          1.1	      0.93847	       1.6438	      0.93973	       2.1968	       0.9415	         2.75	       0.9453	       3.2986	      0.94506	       3.6496	
    34	    38	       1.1293	       9.8627	         1485	        13.18	      0.89025	       4.0997	      0.81055	     8.3e-007	     7.5e-007	      0.78598	      0.94955	         0.55	      0.94045	          1.1	      0.93922	         1.65	       0.9403	          2.2	      0.94217	         2.75	        0.946	        3.299	       0.9457	       3.6479	
    35	    39	       1.1174	       9.9746	         1485	       13.323	      0.88669	       4.1633	      0.80967	     8.3e-007	     7.5e-007	      0.78461	       0.9508	         0.55	      0.94199	          1.1	      0.94032	         1.65	       0.9417	          2.2	      0.94374	         2.75	       0.9476	        3.299	      0.94725	       3.6479	
    36	    41	        1.102	        10.13	         1485	       13.539	       0.8732	       4.1825	      0.80773	     8.3e-007	     7.5e-007	      0.78177	      0.95349	         0.55	      0.94491	          1.1	       0.9431	         1.65	      0.94445	          2.2	      0.94695	         2.75	      0.95079	       3.2967	      0.95045	       3.6479	
    37	    43	       1.0838	       10.325	         1485	       13.801	      0.86745	       4.2318	       0.8047	     8.3e-007	     7.5e-007	       0.7777	      0.95752	         0.55	      0.94914	          1.1	      0.94738	         1.65	       0.9488	          2.2	       0.9516	         2.75	      0.95533	       3.2845	      0.95515	       3.6479	
    38	    45	       1.0621	       10.556	         1485	        14.11	      0.86782	       4.3133	      0.80048	     8.3e-007	     7.5e-007	      0.77234	      0.96303	         0.55	      0.95486	          1.1	      0.95323	         1.65	      0.95479	          2.2	      0.95781	         2.75	       0.9614	        3.267	      0.96148	       3.6479	
    39	    47	       1.0371	       10.822	         1485	       14.465	       0.8686	       4.4246	      0.79503	     8.3e-007	     7.5e-007	      0.76573	       0.9701	         0.55	      0.96215	          1.1	      0.96071	         1.65	      0.96245	          2.2	      0.96568	         2.75	      0.96903	       3.2411	      0.96952	       3.6479	
    40	    49	       1.0088	       11.123	         1485	       14.871	      0.86422	       4.5648	      0.78821	     8.3e-007	     7.5e-007	      0.75773	      0.97898	         0.55	      0.97127	          1.1	      0.97007	         1.65	      0.97202	          2.2	      0.97545	         2.75	       0.9785	       3.2084	       0.9795	       3.6479	
    41	    51	      0.97684	       11.467	         1485	       15.341	      0.84773	       4.7397	      0.77976	     8.3e-007	     7.5e-007	      0.74805	      0.99013	         0.55	       0.9827	          1.1	      0.98178	         1.65	      0.98397	          2.2	      0.98762	         2.75	      0.99028	         3.17	      0.99193	       3.6479	
    42	    52	      0.94118	       11.852	       1484.9	       15.877	      0.80642	       4.9724	      0.77545	    0.0070472	     7.5e-007	      0.74207	      0.99634	         0.55	      0.98924	          1.1	       0.9886	         1.65	      0.99102	          2.2	      0.99484	         2.75	      0.99702	       3.1291	      0.99934	       3.6479	
    43	    53	      0.90072	         12.3	       1484.9	       16.496	      0.73162	       5.2576	      0.76397	     0.061381	     7.5e-007	      0.73032	       1.0116	         0.55	       1.0045	          1.1	        1.004	         1.65	       1.0064	          2.2	       1.0102	         2.75	       1.0118	       3.0878	       1.0145	       3.6364	
    44	    54	      0.84791	       12.897	       1484.9	       17.333	      0.56317	       5.7165	      0.76229	      0.20957	     7.5e-007	      0.72958	       1.0141	         0.55	       1.0067	          1.1	       1.0058	         1.65	       1.0078	          2.2	        1.011	         2.75	       1.0115	       3.0387	       1.0145	       3.6364	
    45	    56	      0.83944	       13.118	       1484.9	       17.499	      0.52531	       5.8074	      0.73191	      0.21324	     7.5e-007	      0.70053	        1.053	         0.55	       1.0461	          1.1	       1.0457	         1.65	       1.0484	          2.2	       1.0525	         2.75	       1.0537	       3.0536	       1.0571	       3.6364	
    46	    57	      0.82411	       13.163	       1484.9	       17.547	      0.53726	       5.8747	      0.73128	      0.21886	     7.5e-007	       0.7003	       1.0545	         0.55	       1.0475	          1.1	       1.0471	         1.65	       1.0498	          2.2	       1.0538	         2.75	       1.0552	       3.0644	       1.0584	       3.6364	
    47	    58	      0.80791	       13.343	       1484.9	       17.683	       0.4828	       5.9264	      0.71616	      0.22343	     7.5e-007	      0.68502	       1.0775	         0.55	       1.0705	          1.1	       1.0702	         1.65	        1.073	          2.2	       1.0772	         2.75	       1.0788	       3.0767	       1.0819	       3.6364	
    48	    61	      0.67299	       16.243	       1484.9	       19.881	      0.15984	       7.4511	      0.70218	      0.67611	     7.5e-007	      0.67409	       1.0984	         0.55	       1.0904	          1.1	       1.0889	         1.65	       1.0904	          2.2	       1.0931	         2.75	       1.0923	       2.9832	       1.0953	       3.6364	
    49	    62	      0.53948	       18.974	       1484.9	       22.334	       1e-006	       8.8462	      0.70111	      0.72537	     7.5e-007	      0.67318	          1.1	         0.55	       1.0934	          1.1	       1.0918	         1.65	       1.0929	          2.2	       1.0952	         2.75	       1.0926	       2.8269	       1.0966	       3.6364	
    50	    63	      0.48349	       24.414	       1484.8	       25.612	       1e-006	       10.655	      0.70046	       1.1714	     7.5e-007	      0.67794	          1.1	         0.55	       1.0908	          1.1	       1.0872	         1.65	        1.086	          2.2	        1.086	         2.75	       1.0817	       2.7273	       1.0837	       3.6364	
    51	    67	       0.4521	       24.414	       1484.8	       25.612	   0.00019207	       10.655	      0.69819	       1.1713	     7.5e-007	      0.67783	          1.1	         0.55	       1.0908	          1.1	        1.087	         1.65	       1.0856	          2.2	       1.0855	         2.75	       1.0813	       2.7273	       1.0831	       3.6364	
    52	    68	       0.4468	       24.414	       1484.8	       25.612	   0.00012184	       10.655	      0.69887	        1.171	     7.5e-007	      0.67877	          1.1	         0.55	       1.0907	          1.1	       1.0864	         1.65	       1.0847	          2.2	       1.0844	         2.75	       1.0805	       2.7273	       1.0818	       3.6364	
    53	    69	      0.44485	       24.414	       1484.8	       25.612	  7.6639e-005	       10.655	      0.69935	       1.1705	     7.5e-007	      0.67947	          1.1	         0.55	         1.09	          1.1	       1.0855	         1.65	       1.0837	          2.2	       1.0833	         2.75	       1.0795	       2.7273	       1.0807	       3.6364	
    54	    70	      0.44432	       24.415	       1484.8	       25.612	    0.0016383	       10.655	      0.69972	       1.1696	     7.5e-007	      0.67997	          1.1	         0.55	       1.0893	          1.1	       1.0848	         1.65	       1.0831	          2.2	       1.0826	       2.7499	       1.0788	       2.7274	         1.08	       3.6364	
    55	    76	      0.43698	       24.536	       1484.8	        25.71	   0.00038111	       10.535	      0.70083	       1.0717	     7.5e-007	      0.67734	          1.1	         0.55	       1.0908	          1.1	       1.0876	         1.65	       1.0869	          2.2	       1.0871	       2.7083	       1.0835	       2.7273	       1.0858	       3.6364	
    56	    77	      0.43179	       24.725	       1484.8	       25.825	       1e-006	       10.361	      0.70183	       1.0063	     7.5e-007	      0.67563	          1.1	         0.55	       1.0921	          1.1	       1.0897	         1.65	       1.0898	          2.2	       1.0893	       2.5997	       1.0869	       2.7273	       1.0901	       3.6364	
    57	    79	      0.42518	       25.737	       1484.8	       26.611	       1e-006	       10.541	      0.70212	       1.0383	     7.5e-007	      0.67553	          1.1	         0.55	       1.0923	          1.1	         1.09	         1.65	         1.09	          2.2	       1.0885	       2.5181	       1.0871	       2.7273	       1.0902	       3.6364	
    58	    80	      0.42503	       25.737	       1484.8	       26.611	       1e-006	        10.54	      0.70203	       1.0356	    0.0034656	      0.67548	          1.1	         0.55	       1.0923	          1.1	         1.09	         1.65	         1.09	          2.2	       1.0884	       2.5185	        1.087	       2.7273	       1.0902	       3.6364	
    59	    82	      0.41788	       26.631	       1484.8	       27.398	       1e-006	       9.9726	      0.70216	       1.0016	      0.08248	      0.67565	          1.1	         0.55	       1.0923	          1.1	       1.0899	         1.65	       1.0898	          2.2	       1.0884	       2.5416	       1.0867	       2.7273	       1.0895	       3.6364	
    60	    84	       0.4172	       26.668	       1484.8	       27.433	       1e-006	       9.9649	      0.70256	      0.99572	     0.081016	      0.67574	       1.0995	         0.55	        1.092	          1.1	       1.0897	         1.65	       1.0896	          2.2	       1.0883	       2.5414	       1.0866	       2.7273	       1.0895	       3.6364	
    61	    86	      0.41677	       26.688	       1484.8	       27.451	   0.00010335	       9.9501	      0.71104	       0.9992	      0.08167	      0.68361	       1.0865	         0.55	       1.0791	          1.1	        1.077	         1.65	       1.0769	          2.2	       1.0756	       2.5417	        1.074	       2.7273	       1.0769	       3.6364	
    62	    87	      0.41665	       26.701	       1484.8	       27.459	       1e-006	       9.9491	      0.71135	       1.0001	     0.081553	        0.684	        1.086	         0.55	       1.0787	          1.1	       1.0765	         1.65	       1.0765	          2.2	       1.0752	        2.542	       1.0735	       2.7273	       1.0764	       3.6364	
    63	    88	      0.41649	       26.731	       1484.8	       27.486	    0.0023843	       9.9297	       0.7228	       1.0127	      0.08666	      0.69493	       1.0687	         0.55	       1.0615	          1.1	       1.0594	         1.65	       1.0594	          2.2	        1.058	        2.539	       1.0564	       2.7273	       1.0593	       3.6364	
    64	    89	      0.41602	       26.745	       1484.8	       27.494	       1e-006	         9.93	      0.72331	       1.0163	     0.086722	       0.6956	       1.0681	         0.55	       1.0608	          1.1	       1.0586	         1.65	       1.0586	          2.2	       1.0573	       2.5396	       1.0557	       2.7273	       1.0585	       3.6364	
    65	    90	      0.41571	        26.76	       1484.8	       27.508	    0.0019728	       9.9205	      0.72972	       1.0221	     0.089804	      0.70161	       1.0588	         0.55	       1.0516	          1.1	       1.0495	         1.65	       1.0494	          2.2	       1.0482	       2.5411	       1.0466	       2.7273	       1.0493	       3.6364	
    66	    91	      0.41536	       26.779	       1484.8	       27.525	    0.0017325	       9.9099	      0.73478	       1.0314	      0.09206	      0.70669	       1.0516	         0.55	       1.0444	          1.1	       1.0422	         1.65	       1.0421	          2.2	       1.0408	       2.5408	       1.0392	       2.7273	       1.0419	       3.6364	
    67	    92	      0.41525	        26.79	       1484.8	       27.534	       1e-006	       9.9072	      0.73524	       1.0328	     0.092574	      0.70717	       1.0509	         0.55	       1.0438	          1.1	       1.0415	         1.65	       1.0414	          2.2	       1.0401	       2.5407	       1.0385	       2.7273	       1.0412	       3.6364	
    68	    93	      0.41502	       26.819	       1484.8	        27.56	   0.00068565	       9.8926	       0.7453	       1.0465	     0.097122	      0.71692	       1.0366	         0.55	       1.0295	          1.1	       1.0273	         1.65	       1.0271	          2.2	       1.0258	       2.5394	       1.0242	       2.7273	       1.0269	       3.6364	
    69	    94	      0.41476	       26.832	       1484.8	       27.569	       1e-006	       9.8937	       0.7455	       1.0489	     0.097055	      0.71725	       1.0363	         0.55	       1.0292	          1.1	        1.027	         1.65	       1.0268	          2.2	       1.0255	       2.5393	       1.0239	       2.7273	       1.0265	       3.6364	
    70	    95	      0.41443	       26.852	       1484.8	       27.586	       1e-006	       9.8846	      0.75233	       1.0574	      0.10032	      0.72381	        1.027	         0.55	       1.0199	          1.1	       1.0177	         1.65	       1.0175	          2.2	       1.0162	       2.5391	       1.0146	       2.7273	       1.0172	       3.6364	
    71	   100	      0.40607	       28.029	       1484.7	       28.637	       1e-006	       9.5308	      0.75903	       1.0618	      0.16702	      0.73071	       1.0179	         0.55	       1.0109	          1.1	       1.0084	         1.65	        1.008	          2.2	       1.0064	       2.5341	       1.0048	       2.7273	       1.0069	       3.6364	
    72	   103	      0.40584	       28.034	       1484.7	       28.641	       1e-006	        9.528	      0.75679	       1.0604	      0.16264	      0.72869	       1.0209	         0.55	       1.0137	          1.1	       1.0113	         1.65	       1.0108	          2.2	       1.0093	       2.5341	       1.0077	       2.7273	       1.0098	       3.6364	
    73	   104	      0.40576	       28.037	       1484.7	       28.644	    0.0049474	       9.5269	      0.75948	       1.0546	      0.16175	      0.73067	       1.0176	         0.55	       1.0105	          1.1	       1.0083	         1.65	        1.008	          2.2	       1.0064	       2.5329	       1.0049	       2.7273	       1.0073	       3.6364	
    74	   105	      0.40562	       28.039	       1484.7	       28.645	   0.00069816	       9.5261	      0.76043	       1.0542	      0.16247	      0.73166	       1.0163	         0.55	       1.0094	          1.1	       1.0071	         1.65	       1.0067	          2.2	       1.0053	       2.5328	       1.0037	       2.7273	       1.0059	       3.6364	
    75	   123	      0.40562	       28.039	       1484.7	       28.645	   0.00069816	       9.5261	      0.76043	       1.0542	      0.16247	      0.73166	       1.0163	         0.55	       1.0094	          1.1	       1.0071	         1.65	       1.0067	          2.2	       1.0053	       2.5328	       1.0037	       2.7273	       1.0059	       3.6364	

:: False convergence

       Squares	        0.40562
     Func Eval	            123
     Grad Eval	             75

:: Nonlinear parameters

     i	   Final p(i)	  Gradient(i)
     1	      28.0391	   -0.0017973
     2	      1484.74	  3.2438e-005
     3	      28.6455	   -0.0016142
     4	  0.000698162	    -0.015816
     5	      9.52611	   0.00074586
     6	      0.76043	    -0.008602
     7	      1.05423	   0.00016467
     8	     0.162468	   -0.0011231
     9	     0.731656	   -0.0067181
    10	      1.01632	  -0.00026563
    11	         0.55	    -0.078138
    12	      1.00941	  -0.00015921
    13	          1.1	    -0.064614
    14	      1.00707	  -0.00020604
    15	         1.65	     -0.04903
    16	      1.00675	  -0.00022574
    17	          2.2	    -0.022524
    18	      1.00527	  -0.00022097
    19	      2.53276	   0.00013316
    20	      1.00372	  -0.00022859
    21	      2.72727	    0.0043978
    22	      1.00593	 -9.0168e-005
    23	      3.63636	     0.047218
