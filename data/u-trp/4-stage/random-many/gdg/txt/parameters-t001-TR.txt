
PARAMETERS AND CONFIDENCE INTERVALS

NO.	SET	           PARAMETER	     INITIAL	    LIMIT-LO	    LIMIT-HI	         FIT	      STDERR	         CV%	      F(SDI)	   CONLIN-LO	   CONLIN-HI	   CONLIN-P%	

  1	  1	                time	           0	
  2	  1	             restart	           0	
  3	  *	                 k.1	   0.0006165	      1e-006	      1e+006	     5.18315	     589.933	     84064.2	     34000.3	    -1151.18	     1161.55	          95	
  4	  *	                k.-1	      0.3525	      1e-006	      1e+006	1.11423e-006	   0.0011875	      787154	      318370	 -0.00232657	  0.00232879	          95	
  5	  *	                 k.2	       277.9	      1e-006	      1e+006	     273.707	     3610.88	     9743.82	     3940.96	    -6804.19	      7351.6	          95	
  6	  *	                k.-2	  5.819e-005	      1e-006	      1e+006	     4.83248	     3582.26	      547505	      221442	    -7016.95	     7026.62	          95	
  7	  *	               k.cat	  1.828e-005	      1e-006	      1e+006	     1.80897	     23.8342	     9731.33	      3935.9	    -44.9098	     48.5278	          95	
  8	  *	               k.off	       11.17	      1e-006	      1e+006	    0.826365	    0.142971	     127.784	     51.6831	     0.54612	     1.10661	          95	
  9	  *	              k.-off	         138	      1e-006	      1e+006	     87.1934	     9928.68	     84102.7	     34015.9	    -19374.6	       19549	          95	
 10	  1	                 [E]	           1	    0.909091	         1.1	    0.954902	     108.689	     84067.3	     34001.6	    -212.092	     214.002	          95	
 11	  1	                 [S]	         0.5	    0.454545	        0.55	        0.55	     62.6017	       84067	     34001.5	    -122.159	     123.259	          95	
 12	  1	              [E.S1]	           0	
 13	  1	              [E.S2]	           0	
 14	  1	               [E.P]	           0	
 15	  1	                 [P]	           0	
 16	  *	                r(E)	        0.18	    1.8e-007	      180000	    0.185965	     21.1668	     84067.3	     34001.6	    -41.3043	     41.6762	          95	
 17	  1	                r(S)	           0	
 18	  *	             r(E.S1)	        0.14	    1.4e-007	      140000	    0.157596	     17.9385	     84070.3	     34002.8	    -35.0046	     35.3198	          95	
 19	  *	             r(E.S2)	       0.079	    7.9e-008	       79000	   0.0932863	     10.6131	     84028.5	     33985.9	    -20.7101	     20.8966	          95	
 20	  *	              r(E.P)	        0.13	    1.3e-007	      130000	    0.138582	     15.7736	     84067.4	     34001.6	    -30.7802	     31.0574	          95	
 21	  1	                r(P)	           0	
 22	  1	              offset	        0.99	
 23	  1	               delay	           0	
 24	  1	          incubation	           0	
 25	  1	                [E]i	           0	
 26	  1	                [S]i	           0	
 27	  1	             [E.S1]i	           0	
 28	  1	             [E.S2]i	           0	
 29	  1	              [E.P]i	           0	
 30	  1	                [P]i	           0	
 31	  1	            dilution	           0	
 32	  1	           intensive	           0	
 33	  2	                time	           0	
 34	  2	             restart	           0	
 35	  2	                 [E]	           1	    0.909091	         1.1	    0.941937	     107.213	     84067.2	     34001.6	    -209.212	     211.096	          95	
 36	  2	                 [S]	           1	    0.909091	         1.1	         1.1	     125.201	     84065.2	     34000.8	    -244.313	     246.513	          95	
 37	  2	              [E.S1]	           0	
 38	  2	              [E.S2]	           0	
 39	  2	               [E.P]	           0	
 40	  2	                 [P]	           0	
 41	  2	                r(S)	           0	
 42	  2	                r(P)	           0	
 43	  2	              offset	        0.97	
 44	  2	               delay	           0	
 45	  2	          incubation	           0	
 46	  2	                [E]i	           0	
 47	  2	                [S]i	           0	
 48	  2	             [E.S1]i	           0	
 49	  2	             [E.S2]i	           0	
 50	  2	              [E.P]i	           0	
 51	  2	                [P]i	           0	
 52	  2	            dilution	           0	
 53	  2	           intensive	           0	
 54	  3	                time	           0	
 55	  3	             restart	           0	
 56	  3	                 [E]	           1	    0.909091	         1.1	     0.92187	     104.929	     84067.2	     34001.6	    -204.755	     206.599	          95	
 57	  3	                 [S]	         1.5	     1.36364	        1.65	        1.65	     187.802	     84065.5	     34000.9	    -366.471	     369.771	          95	
 58	  3	              [E.S1]	           0	
 59	  3	              [E.S2]	           0	
 60	  3	               [E.P]	           0	
 61	  3	                 [P]	           0	
 62	  3	                r(S)	           0	
 63	  3	                r(P)	           0	
 64	  3	              offset	        0.93	
 65	  3	               delay	           0	
 66	  3	          incubation	           0	
 67	  3	                [E]i	           0	
 68	  3	                [S]i	           0	
 69	  3	             [E.S1]i	           0	
 70	  3	             [E.S2]i	           0	
 71	  3	              [E.P]i	           0	
 72	  3	                [P]i	           0	
 73	  3	            dilution	           0	
 74	  3	           intensive	           0	
 75	  4	                time	           0	
 76	  4	             restart	           0	
 77	  4	                 [E]	           1	    0.909091	         1.1	    0.920001	     104.716	     84067.4	     34001.6	     -204.34	      206.18	          95	
 78	  4	                 [S]	           2	     1.81818	         2.2	     2.18369	     248.548	     84066.1	     34001.1	     -485.01	     489.378	          95	
 79	  4	              [E.S1]	           0	
 80	  4	              [E.S2]	           0	
 81	  4	               [E.P]	           0	
 82	  4	                 [P]	           0	
 83	  4	                r(S)	           0	
 84	  4	                r(P)	           0	
 85	  4	              offset	        0.89	
 86	  4	               delay	           0	
 87	  4	          incubation	           0	
 88	  4	                [E]i	           0	
 89	  4	                [S]i	           0	
 90	  4	             [E.S1]i	           0	
 91	  4	             [E.S2]i	           0	
 92	  4	              [E.P]i	           0	
 93	  4	                [P]i	           0	
 94	  4	            dilution	           0	
 95	  4	           intensive	           0	
 96	  5	                time	           0	
 97	  5	             restart	           0	
 98	  5	                 [E]	           1	    0.909091	         1.1	    0.909091	     103.474	     84067.3	     34001.6	    -201.917	     203.735	          95	
 99	  5	                 [S]	         2.5	     2.27273	        2.75	      2.4455	     278.346	     84065.8	       34001	    -543.157	     548.048	          95	
100	  5	              [E.S1]	           0	
101	  5	              [E.S2]	           0	
102	  5	               [E.P]	           0	
103	  5	                 [P]	           0	
104	  5	                r(S)	           0	
105	  5	                r(P)	           0	
106	  5	              offset	        0.85	
107	  5	               delay	           0	
108	  5	          incubation	           0	
109	  5	                [E]i	           0	
110	  5	                [S]i	           0	
111	  5	             [E.S1]i	           0	
112	  5	             [E.S2]i	           0	
113	  5	              [E.P]i	           0	
114	  5	                [P]i	           0	
115	  5	            dilution	           0	
116	  5	           intensive	           0	
117	  6	                time	           0	
118	  6	             restart	           0	
119	  6	                 [E]	           1	    0.909091	         1.1	    0.922034	     104.948	     84067.3	     34001.6	    -204.792	     206.636	          95	
120	  6	                 [S]	           3	     2.72727	         3.3	     2.72727	     310.417	     84065.6	     34000.9	    -605.739	     611.193	          95	
121	  6	              [E.S1]	           0	
122	  6	              [E.S2]	           0	
123	  6	               [E.P]	           0	
124	  6	                 [P]	           0	
125	  6	                r(S)	           0	
126	  6	                r(P)	           0	
127	  6	              offset	        0.81	
128	  6	               delay	           0	
129	  6	          incubation	           0	
130	  6	                [E]i	           0	
131	  6	                [S]i	           0	
132	  6	             [E.S1]i	           0	
133	  6	             [E.S2]i	           0	
134	  6	              [E.P]i	           0	
135	  6	                [P]i	           0	
136	  6	            dilution	           0	
137	  6	           intensive	           0	
138	  7	                time	           0	
139	  7	             restart	           0	
140	  7	                 [E]	           1	    0.909091	         1.1	    0.915534	     104.208	     84067.2	     34001.6	    -203.348	     205.179	          95	
141	  7	                 [S]	           4	     3.63636	         4.4	     3.63636	     413.888	     84065.4	     34000.8	    -807.649	     814.922	          95	
142	  7	              [E.S1]	           0	
143	  7	              [E.S2]	           0	
144	  7	               [E.P]	           0	
145	  7	                 [P]	           0	
146	  7	                r(S)	           0	
147	  7	                r(P)	           0	
148	  7	              offset	        0.78	
149	  7	               delay	           0	
150	  7	          incubation	           0	
151	  7	                [E]i	           0	
152	  7	                [S]i	           0	
153	  7	             [E.S1]i	           0	
154	  7	             [E.S2]i	           0	
155	  7	              [E.P]i	           0	
156	  7	                [P]i	           0	
157	  7	            dilution	           0	
158	  7	           intensive	           0	
