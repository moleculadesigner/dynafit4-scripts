
PARAMETERS AND CONFIDENCE INTERVALS

NO.	SET	           PARAMETER	     INITIAL	    LIMIT-LO	    LIMIT-HI	         FIT	      STDERR	         CV%	      F(SDI)	   CONLIN-LO	   CONLIN-HI	   CONLIN-P%	

  1	  1	                time	           0	
  2	  1	             restart	           0	
  3	  *	                 k.1	      442000	      1e-006	      1e+006	      442000	2.16556e+007	     36186.7	     8573.99	-4.20064e+007	4.28904e+007	          95	
  4	  *	                k.-1	     0.04039	      1e-006	      1e+006	 0.000614098	    0.106887	      128555	     30459.4	   -0.208901	    0.210129	          95	
  5	  *	                 k.2	       27880	      1e-006	      1e+006	       27880	      996782	     26406.4	     6256.66	-1.92597e+006	1.98173e+006	          95	
  6	  *	                k.-2	       1.027	      1e-006	      1e+006	     7.43818	     696.961	       69206	     16397.5	    -1358.71	     1373.59	          95	
  7	  *	               k.cat	       15.36	      1e-006	      1e+006	     14.3783	    0.440445	     22.6248	     5.36066	      13.515	     15.2417	          95	
  8	  *	               k.off	     0.05484	      1e-006	      1e+006	     1.34747	   0.0782336	     42.8823	     10.1604	     1.19412	     1.50082	          95	
  9	  *	              k.-off	      0.3616	      1e-006	      1e+006	    0.108284	     5.30642	     36194.3	     8575.77	    -10.2931	     10.5097	          95	
 10	  1	                 [E]	           1	    0.909091	         1.1	         1.1	     53.9179	     36202.8	     8577.79	    -104.588	     106.788	          95	
 11	  1	                 [S]	         0.5	    0.454545	        0.55	        0.55	     26.9589	     36202.7	     8577.76	    -52.2936	     53.3936	          95	
 12	  1	              [E.S1]	           0	
 13	  1	              [E.S2]	           0	
 14	  1	               [E.P]	           0	
 15	  1	                 [P]	           0	
 16	  *	                r(E)	        0.18	    1.8e-007	      180000	    0.139808	     6.85288	     36202.8	     8577.79	    -13.2929	     13.5725	          95	
 17	  1	                r(S)	           0	
 18	  *	             r(E.S1)	        0.14	    1.4e-007	      140000	    0.191538	     9.39477	     36227.1	     8583.56	    -18.2237	     18.6068	          95	
 19	  *	             r(E.S2)	       0.079	    7.9e-008	       79000	    0.185888	     9.11158	     36203.1	     8577.85	    -17.6742	      18.046	          95	
 20	  *	              r(E.P)	        0.13	    1.3e-007	      130000	    0.114243	     5.59976	     36202.8	     8577.78	    -10.8622	     11.0907	          95	
 21	  1	                r(P)	           0	
 22	  1	              offset	        0.99	
 23	  1	               delay	           0	
 24	  1	          incubation	           0	
 25	  1	                [E]i	           0	
 26	  1	                [S]i	           0	
 27	  1	             [E.S1]i	           0	
 28	  1	             [E.S2]i	           0	
 29	  1	              [E.P]i	           0	
 30	  1	                [P]i	           0	
 31	  1	            dilution	           0	
 32	  1	           intensive	           0	
 33	  2	                time	           0	
 34	  2	             restart	           0	
 35	  2	                 [E]	           1	    0.909091	         1.1	    0.997461	     48.8918	     36202.8	     8577.79	    -94.8382	     96.8331	          95	
 36	  2	                 [S]	           1	    0.909091	         1.1	         1.1	     53.9181	     36202.9	     8577.82	    -104.588	     106.788	          95	
 37	  2	              [E.S1]	           0	
 38	  2	              [E.S2]	           0	
 39	  2	               [E.P]	           0	
 40	  2	                 [P]	           0	
 41	  2	                r(S)	           0	
 42	  2	                r(P)	           0	
 43	  2	              offset	        0.97	
 44	  2	               delay	           0	
 45	  2	          incubation	           0	
 46	  2	                [E]i	           0	
 47	  2	                [S]i	           0	
 48	  2	             [E.S1]i	           0	
 49	  2	             [E.S2]i	           0	
 50	  2	              [E.P]i	           0	
 51	  2	                [P]i	           0	
 52	  2	            dilution	           0	
 53	  2	           intensive	           0	
 54	  3	                time	           0	
 55	  3	             restart	           0	
 56	  3	                 [E]	           1	    0.909091	         1.1	    0.944125	     46.2776	     36202.9	     8577.82	    -89.7673	     91.6556	          95	
 57	  3	                 [S]	         1.5	     1.36364	        1.65	     1.42275	     69.7387	     36203.2	     8577.88	    -135.276	     138.122	          95	
 58	  3	              [E.S1]	           0	
 59	  3	              [E.S2]	           0	
 60	  3	               [E.P]	           0	
 61	  3	                 [P]	           0	
 62	  3	                r(S)	           0	
 63	  3	                r(P)	           0	
 64	  3	              offset	        0.93	
 65	  3	               delay	           0	
 66	  3	          incubation	           0	
 67	  3	                [E]i	           0	
 68	  3	                [S]i	           0	
 69	  3	             [E.S1]i	           0	
 70	  3	             [E.S2]i	           0	
 71	  3	              [E.P]i	           0	
 72	  3	                [P]i	           0	
 73	  3	            dilution	           0	
 74	  3	           intensive	           0	
 75	  4	                time	           0	
 76	  4	             restart	           0	
 77	  4	                 [E]	           1	    0.909091	         1.1	    0.926249	     45.4014	     36202.9	     8577.81	    -88.0676	     89.9201	          95	
 78	  4	                 [S]	           2	     1.81818	         2.2	     2.09474	     102.675	     36202.5	     8577.72	    -199.165	     203.355	          95	
 79	  4	              [E.S1]	           0	
 80	  4	              [E.S2]	           0	
 81	  4	               [E.P]	           0	
 82	  4	                 [P]	           0	
 83	  4	                r(S)	           0	
 84	  4	                r(P)	           0	
 85	  4	              offset	        0.89	
 86	  4	               delay	           0	
 87	  4	          incubation	           0	
 88	  4	                [E]i	           0	
 89	  4	                [S]i	           0	
 90	  4	             [E.S1]i	           0	
 91	  4	             [E.S2]i	           0	
 92	  4	              [E.P]i	           0	
 93	  4	                [P]i	           0	
 94	  4	            dilution	           0	
 95	  4	           intensive	           0	
 96	  5	                time	           0	
 97	  5	             restart	           0	
 98	  5	                 [E]	           1	    0.909091	         1.1	    0.909091	     44.5604	     36202.9	     8577.81	    -86.4362	     88.2544	          95	
 99	  5	                 [S]	         2.5	     2.27273	        2.75	     2.27273	     111.401	     36202.8	     8577.79	     -216.09	     220.635	          95	
100	  5	              [E.S1]	           0	
101	  5	              [E.S2]	           0	
102	  5	               [E.P]	           0	
103	  5	                 [P]	           0	
104	  5	                r(S)	           0	
105	  5	                r(P)	           0	
106	  5	              offset	        0.85	
107	  5	               delay	           0	
108	  5	          incubation	           0	
109	  5	                [E]i	           0	
110	  5	                [S]i	           0	
111	  5	             [E.S1]i	           0	
112	  5	             [E.S2]i	           0	
113	  5	              [E.P]i	           0	
114	  5	                [P]i	           0	
115	  5	            dilution	           0	
116	  5	           intensive	           0	
117	  6	                time	           0	
118	  6	             restart	           0	
119	  6	                 [E]	           1	    0.909091	         1.1	      0.9142	     44.8109	       36203	     8577.83	    -86.9222	     88.7506	          95	
120	  6	                 [S]	           3	     2.72727	         3.3	     2.72727	     133.682	     36203.2	      8577.9	    -259.311	     264.766	          95	
121	  6	              [E.S1]	           0	
122	  6	              [E.S2]	           0	
123	  6	               [E.P]	           0	
124	  6	                 [P]	           0	
125	  6	                r(S)	           0	
126	  6	                r(P)	           0	
127	  6	              offset	        0.81	
128	  6	               delay	           0	
129	  6	          incubation	           0	
130	  6	                [E]i	           0	
131	  6	                [S]i	           0	
132	  6	             [E.S1]i	           0	
133	  6	             [E.S2]i	           0	
134	  6	              [E.P]i	           0	
135	  6	                [P]i	           0	
136	  6	            dilution	           0	
137	  6	           intensive	           0	
138	  7	                time	           0	
139	  7	             restart	           0	
140	  7	                 [E]	           1	    0.909091	         1.1	    0.909091	     44.5603	     36202.9	     8577.81	    -86.4362	     88.2543	          95	
141	  7	                 [S]	           4	     3.63636	         4.4	     3.63636	     178.242	       36203	     8577.83	    -345.746	     353.018	          95	
142	  7	              [E.S1]	           0	
143	  7	              [E.S2]	           0	
144	  7	               [E.P]	           0	
145	  7	                 [P]	           0	
146	  7	                r(S)	           0	
147	  7	                r(P)	           0	
148	  7	              offset	        0.78	
149	  7	               delay	           0	
150	  7	          incubation	           0	
151	  7	                [E]i	           0	
152	  7	                [S]i	           0	
153	  7	             [E.S1]i	           0	
154	  7	             [E.S2]i	           0	
155	  7	              [E.P]i	           0	
156	  7	                [P]i	           0	
157	  7	            dilution	           0	
158	  7	           intensive	           0	
