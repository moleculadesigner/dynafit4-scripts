
PARAMETERS AND CONFIDENCE INTERVALS

NO.	SET	           PARAMETER	     INITIAL	    LIMIT-LO	    LIMIT-HI	         FIT	      STDERR	         CV%	      F(SDI)	   CONLIN-LO	   CONLIN-HI	   CONLIN-P%	

  1	  1	                time	           0	
  2	  1	             restart	           0	
  3	  *	                 k.1	   0.0008097	      1e-006	      1e+006	     5.22609	      47.922	     6772.67	     2755.18	    -88.7086	     99.1608	          95	
  4	  *	                k.-1	  5.015e-006	      1e-006	      1e+006	    0.358692	   0.0465984	     95.9515	     39.0339	    0.267351	    0.450032	          95	
  5	  *	                 k.2	      0.1263	      1e-006	      1e+006	   0.0175809	  0.00191847	     80.5964	     32.7873	   0.0138204	   0.0213413	          95	
  6	  *	                k.-2	  3.058e-005	      1e-006	      1e+006	 0.000232674	  0.00642162	     20384.4	     8292.54	  -0.0123547	   0.0128201	          95	
  7	  *	               k.cat	        6356	      1e-006	      1e+006	        6356	      149778	     17404.7	     7080.38	     -287233	      299945	          95	
  8	  *	               k.off	  1.688e-005	      1e-006	      1e+006	     1.59554	   0.0235721	     10.9117	     4.43898	     1.54933	     1.64174	          95	
  9	  *	              k.-off	  2.639e-005	      1e-006	      1e+006	      1e-006	2.40703e-005	       17778	     7232.24	-4.61816e-005	4.81816e-005	          95	
 10	  1	                 [E]	           1	    0.909091	         1.1	    0.936718	     8.59181	     6774.51	     2755.93	    -15.9046	      17.778	          95	
 11	  1	                 [S]	         0.5	    0.454545	        0.55	        0.55	     5.04397	     6773.48	     2755.51	    -9.33698	      10.437	          95	
 12	  1	              [E.S1]	           0	
 13	  1	              [E.S2]	           0	
 14	  1	               [E.P]	           0	
 15	  1	                 [P]	           0	
 16	  *	                r(E)	        0.18	    1.8e-007	      180000	    0.189868	     1.74149	     6774.41	     2755.89	    -3.22373	     3.60347	          95	
 17	  1	                r(S)	           0	
 18	  *	             r(E.S1)	        0.14	    1.4e-007	      140000	   0.0948027	      1.0804	     8417.19	     3424.18	    -2.02296	     2.21256	          95	
 19	  *	             r(E.S2)	       0.079	    7.9e-008	       79000	   0.0765605	      201307	1.94203e+009	7.90033e+008	     -394593	      394593	          95	
 20	  *	              r(E.P)	        0.13	    1.3e-007	      130000	     3.72409	     34.1682	     6776.46	     2756.72	    -63.2509	     70.6991	          95	
 21	  1	                r(P)	           0	
 22	  1	              offset	        0.99	
 23	  1	               delay	           0	
 24	  1	          incubation	           0	
 25	  1	                [E]i	           0	
 26	  1	                [S]i	           0	
 27	  1	             [E.S1]i	           0	
 28	  1	             [E.S2]i	           0	
 29	  1	              [E.P]i	           0	
 30	  1	                [P]i	           0	
 31	  1	            dilution	           0	
 32	  1	           intensive	           0	
 33	  2	                time	           0	
 34	  2	             restart	           0	
 35	  2	                 [E]	           1	    0.909091	         1.1	    0.924613	     8.48081	     6774.53	     2755.93	    -15.6991	     17.5483	          95	
 36	  2	                 [S]	           1	    0.909091	         1.1	         1.1	     10.0858	     6772.05	     2754.93	    -18.6698	     20.8698	          95	
 37	  2	              [E.S1]	           0	
 38	  2	              [E.S2]	           0	
 39	  2	               [E.P]	           0	
 40	  2	                 [P]	           0	
 41	  2	                r(S)	           0	
 42	  2	                r(P)	           0	
 43	  2	              offset	        0.97	
 44	  2	               delay	           0	
 45	  2	          incubation	           0	
 46	  2	                [E]i	           0	
 47	  2	                [S]i	           0	
 48	  2	             [E.S1]i	           0	
 49	  2	             [E.S2]i	           0	
 50	  2	              [E.P]i	           0	
 51	  2	                [P]i	           0	
 52	  2	            dilution	           0	
 53	  2	           intensive	           0	
 54	  3	                time	           0	
 55	  3	             restart	           0	
 56	  3	                 [E]	           1	    0.909091	         1.1	    0.913429	     8.37848	     6774.73	     2756.02	    -15.5097	     17.3366	          95	
 57	  3	                 [S]	         1.5	     1.36364	        1.65	        1.65	     15.1295	     6772.38	     2755.06	    -28.0061	     31.3061	          95	
 58	  3	              [E.S1]	           0	
 59	  3	              [E.S2]	           0	
 60	  3	               [E.P]	           0	
 61	  3	                 [P]	           0	
 62	  3	                r(S)	           0	
 63	  3	                r(P)	           0	
 64	  3	              offset	        0.93	
 65	  3	               delay	           0	
 66	  3	          incubation	           0	
 67	  3	                [E]i	           0	
 68	  3	                [S]i	           0	
 69	  3	             [E.S1]i	           0	
 70	  3	             [E.S2]i	           0	
 71	  3	              [E.P]i	           0	
 72	  3	                [P]i	           0	
 73	  3	            dilution	           0	
 74	  3	           intensive	           0	
 75	  4	                time	           0	
 76	  4	             restart	           0	
 77	  4	                 [E]	           1	    0.909091	         1.1	    0.916245	     8.40442	     6774.83	     2756.06	    -15.5577	     17.3902	          95	
 78	  4	                 [S]	           2	     1.81818	         2.2	         2.2	     20.1767	     6773.76	     2755.62	    -37.3496	     41.7496	          95	
 79	  4	              [E.S1]	           0	
 80	  4	              [E.S2]	           0	
 81	  4	               [E.P]	           0	
 82	  4	                 [P]	           0	
 83	  4	                r(S)	           0	
 84	  4	                r(P)	           0	
 85	  4	              offset	        0.89	
 86	  4	               delay	           0	
 87	  4	          incubation	           0	
 88	  4	                [E]i	           0	
 89	  4	                [S]i	           0	
 90	  4	             [E.S1]i	           0	
 91	  4	             [E.S2]i	           0	
 92	  4	              [E.P]i	           0	
 93	  4	                [P]i	           0	
 94	  4	            dilution	           0	
 95	  4	           intensive	           0	
 96	  5	                time	           0	
 97	  5	             restart	           0	
 98	  5	                 [E]	           1	    0.909091	         1.1	    0.909091	     8.33876	     6774.79	     2756.04	    -15.4362	     17.2544	          95	
 99	  5	                 [S]	         2.5	     2.27273	        2.75	     2.53628	     23.2597	     6773.43	     2755.49	    -43.0563	     48.1289	          95	
100	  5	              [E.S1]	           0	
101	  5	              [E.S2]	           0	
102	  5	               [E.P]	           0	
103	  5	                 [P]	           0	
104	  5	                r(S)	           0	
105	  5	                r(P)	           0	
106	  5	              offset	        0.85	
107	  5	               delay	           0	
108	  5	          incubation	           0	
109	  5	                [E]i	           0	
110	  5	                [S]i	           0	
111	  5	             [E.S1]i	           0	
112	  5	             [E.S2]i	           0	
113	  5	              [E.P]i	           0	
114	  5	                [P]i	           0	
115	  5	            dilution	           0	
116	  5	           intensive	           0	
117	  6	                time	           0	
118	  6	             restart	           0	
119	  6	                 [E]	           1	    0.909091	         1.1	    0.920802	     8.44626	     6774.85	     2756.07	    -15.6352	     17.4768	          95	
120	  6	                 [S]	           3	     2.72727	         3.3	     2.76745	     25.3804	     6773.61	     2755.56	    -46.9821	      52.517	          95	
121	  6	              [E.S1]	           0	
122	  6	              [E.S2]	           0	
123	  6	               [E.P]	           0	
124	  6	                 [P]	           0	
125	  6	                r(S)	           0	
126	  6	                r(P)	           0	
127	  6	              offset	        0.81	
128	  6	               delay	           0	
129	  6	          incubation	           0	
130	  6	                [E]i	           0	
131	  6	                [S]i	           0	
132	  6	             [E.S1]i	           0	
133	  6	             [E.S2]i	           0	
134	  6	              [E.P]i	           0	
135	  6	                [P]i	           0	
136	  6	            dilution	           0	
137	  6	           intensive	           0	
138	  7	                time	           0	
139	  7	             restart	           0	
140	  7	                 [E]	           1	    0.909091	         1.1	    0.914422	     8.38764	     6774.78	     2756.04	    -15.5267	     17.3555	          95	
141	  7	                 [S]	           4	     3.63636	         4.4	     3.63636	     33.3359	      6770.9	     2754.46	    -61.7072	       68.98	          95	
142	  7	              [E.S1]	           0	
143	  7	              [E.S2]	           0	
144	  7	               [E.P]	           0	
145	  7	                 [P]	           0	
146	  7	                r(S)	           0	
147	  7	                r(P)	           0	
148	  7	              offset	        0.78	
149	  7	               delay	           0	
150	  7	          incubation	           0	
151	  7	                [E]i	           0	
152	  7	                [S]i	           0	
153	  7	             [E.S1]i	           0	
154	  7	             [E.S2]i	           0	
155	  7	              [E.P]i	           0	
156	  7	                [P]i	           0	
157	  7	            dilution	           0	
158	  7	           intensive	           0	
