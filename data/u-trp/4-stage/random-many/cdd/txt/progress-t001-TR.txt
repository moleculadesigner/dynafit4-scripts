
===================================================
Least squares Minimization - Trust Region Algorithm
===================================================

Optimized parameters:

     1 	 k.1
     2 	 k.-1
     3 	 k.2
     4 	 k.-2
     5 	 k.cat
     6 	 k.off
     7 	 k.-off
     8 	 r(E)
     9 	 r(E.S1)
    10 	 r(E.S2)
    11 	 r(E.P)
    12 	 [E]#1
    13 	 [S]#1
    14 	 [E]#2
    15 	 [S]#2
    16 	 [E]#3
    17 	 [S]#3
    18 	 [E]#4
    19 	 [S]#4
    20 	 [E]#5
    21 	 [S]#5
    22 	 [E]#6
    23 	 [S]#6
    24 	 [E]#7
    25 	 [S]#7

:: NL2SOL :: DNLAFB
:: Analytic Jacobian; Constrained

________________________________________________________________________

:: Initial Estimate

     i	     Param p(i)	            Min	            Max
     1	           8.05	         1e-006	         1e+006
     2	     1.375e-006	         1e-006	         1e+006
     3	       0.001541	         1e-006	         1e+006
     4	       0.006614	         1e-006	         1e+006
     5	          3.642	         1e-006	         1e+006
     6	          14.46	         1e-006	         1e+006
     7	         335500	         1e-006	         1e+006
     8	           0.18	       1.8e-007	         180000
     9	           0.14	       1.4e-007	         140000
    10	          0.079	       7.9e-008	          79000
    11	           0.13	       1.3e-007	         130000
    12	              1	       0.909091	            1.1
    13	            0.5	       0.454545	           0.55
    14	              1	       0.909091	            1.1
    15	              1	       0.909091	            1.1
    16	              1	       0.909091	            1.1
    17	            1.5	        1.36364	           1.65
    18	              1	       0.909091	            1.1
    19	              2	        1.81818	            2.2
    20	              1	       0.909091	            1.1
    21	            2.5	        2.27273	           2.75
    22	              1	       0.909091	            1.1
    23	              3	        2.72727	            3.3
    24	              1	       0.909091	            1.1
    25	              4	        3.63636	            4.4

:: Iterations

     i	 FEval	   F(i) = SSQ	   Param( 1)	   Param( 2)	   Param( 3)	   Param( 4)	   Param( 5)	   Param( 6)	   Param( 7)	   Param( 8)	   Param( 9)	   Param(10)	   Param(11)	   Param(12)	   Param(13)	   Param(14)	   Param(15)	   Param(16)	   Param(17)	   Param(18)	   Param(19)	   Param(20)	   Param(21)	   Param(22)	   Param(23)	   Param(24)	   Param(25)	
     0	     1	       6.8709	         8.05	   1.375e-006	     0.001541	     0.006614	        3.642	        14.46	   3.355e+005	         0.18	         0.14	        0.079	         0.13	            1	          0.5	            1	            1	            1	          1.5	            1	            2	            1	          2.5	            1	            3	            1	            4	
     1	     2	      0.72925	       9.2825	  9.5068e-006	       1e-006	       1e-006	       3.6368	       14.461	   3.355e+005	      0.16833	      0.11485	     7.9e-008	      0.12601	       1.0302	      0.50088	       1.0207	       1.0001	       1.0096	        1.503	       1.0061	       2.0023	       1.0062	       2.4775	       1.0139	       2.9874	       1.0007	        3.998	
     2	     3	      0.64669	        10.45	       0.1527	       1e-006	     0.036578	       3.6368	       14.461	   3.355e+005	      0.17291	      0.11401	     7.9e-008	      0.12684	       1.0207	      0.50088	       1.0105	       1.0001	        1.009	        1.503	       1.0115	       2.0023	      0.99797	       2.4775	       1.0102	       2.9473	       1.0007	       3.9803	
     3	     5	      0.52259	       10.459	      0.25622	      0.08471	     0.036581	       3.6368	       14.461	   3.355e+005	      0.17483	      0.11031	     7.9e-008	      0.12694	       1.0284	         0.55	       1.0321	      0.99144	        1.009	       1.5545	      0.99899	       2.0266	      0.98556	       2.4773	       1.0101	        2.951	      0.99248	       3.9716	
     4	     6	      0.37137	       10.444	        0.397	      0.13905	     0.042834	       3.6772	       14.461	   3.355e+005	      0.17794	      0.10936	    0.0033938	      0.13521	       1.0162	         0.55	       1.0129	      0.98346	       1.0053	       1.5828	      0.99748	       2.0373	      0.98885	       2.4721	        1.007	       2.9341	      0.99353	       3.9468	
     5	     7	      0.36044	       10.416	      0.59221	      0.28779	      0.08346	       3.8053	       14.461	   3.355e+005	      0.17955	      0.10883	     0.084119	      0.12313	       1.0089	         0.55	       1.0044	      0.98094	       1.0046	       1.6161	      0.99671	       2.0414	      0.98824	       2.4398	       1.0048	        2.869	      0.99303	       3.8527	
     6	     8	      0.25596	       10.393	      0.61358	      0.64673	     0.085595	       3.8566	       14.461	   3.355e+005	      0.18056	      0.10792	     0.099205	      0.12518	       1.0046	         0.55	      0.99922	      0.97174	       1.0025	       1.6195	      0.99438	       2.0217	      0.98542	       2.3789	       1.0012	       2.7615	      0.99023	       3.6857	
     7	     9	      0.19882	       10.354	      0.43631	      0.94026	     0.022288	       3.8814	       14.462	   3.355e+005	      0.17962	      0.10773	     0.089476	      0.12638	       1.0104	         0.55	       1.0022	      0.95566	       1.0064	       1.6206	      0.99719	       2.0129	      0.98631	       2.3255	       1.0015	       2.7273	      0.99018	       3.6364	
     8	    10	      0.14995	       9.9935	     0.048917	       1.9783	     0.042271	       4.0587	       14.465	   3.355e+005	      0.17511	      0.10946	     0.066909	      0.12406	       1.0328	         0.55	       1.0131	      0.90909	       1.0256	         1.65	        1.014	       2.0822	      0.99971	       2.3612	       1.0131	       2.7273	      0.99869	       3.6364	
     9	    11	      0.12101	       9.4648	     0.056979	       2.5792	      0.26698	       4.7139	       14.471	   3.355e+005	      0.17436	      0.11417	     0.056902	      0.12535	       1.0322	         0.55	       1.0117	      0.90909	       1.0173	         1.65	       1.0055	        2.125	      0.99169	       2.4538	       1.0022	       2.7273	       0.9871	       3.6364	
    10	    12	      0.11638	       9.4938	       1e-006	       2.6488	      0.51261	       5.3016	       14.482	   3.355e+005	      0.17516	      0.11859	     0.045862	      0.12679	       1.0258	         0.55	       1.0041	      0.90909	        1.007	       1.6462	      0.99488	       2.1341	      0.98147	       2.4917	      0.99125	       2.7273	       0.9758	       3.6364	
    11	    14	      0.11271	       10.373	       1e-006	       2.7068	      0.93634	       5.8095	       14.495	   3.355e+005	      0.17628	      0.12496	     0.028724	      0.12801	       1.0198	         0.55	       0.9986	      0.90909	      0.99753	       1.6098	      0.98544	       2.1039	      0.97223	       2.4798	      0.98208	       2.7273	      0.96609	       3.6364	
    12	    15	      0.10953	       11.849	       1e-006	       2.7562	       1.4342	       6.1513	       14.522	   3.355e+005	      0.17816	       0.1327	    0.0082388	      0.12959	        1.011	         0.55	      0.99127	      0.90909	      0.98532	       1.5612	      0.97335	       2.0644	      0.96042	       2.4647	      0.97099	       2.7795	      0.95343	       3.6364	
    13	    16	      0.10745	       13.596	   0.00016321	       2.9047	       1.8519	       5.8626	       14.571	   3.355e+005	      0.17822	      0.13729	     7.9e-008	      0.12939	       1.0125	         0.55	      0.99382	      0.90909	      0.98675	        1.529	      0.97451	       2.0394	      0.96194	       2.4694	      0.97319	       2.8307	      0.95343	       3.6364	
    14	    17	      0.10657	       15.409	    0.0021807	       3.2412	       2.5997	       5.4653	       16.783	   3.355e+005	       0.1827	      0.14483	     7.9e-008	      0.13304	      0.99039	         0.55	      0.97466	      0.90909	        0.959	        1.451	      0.94713	       1.9539	      0.93481	       2.3877	      0.94517	       2.7391	      0.92732	       3.6364	
    15	    18	      0.10588	       16.925	      0.44135	       3.3934	       2.9532	        4.845	       16.958	   3.355e+005	      0.18332	      0.14732	     7.9e-008	      0.13318	      0.98879	         0.55	      0.97167	      0.90909	      0.95948	       1.4629	       0.9476	       1.9722	      0.93652	       2.4605	       0.9464	       2.8239	      0.92732	       3.6364	
    16	    20	      0.10488	       16.973	      0.57752	       3.4654	       2.9069	       4.8966	       16.958	   3.355e+005	      0.18679	      0.15027	    0.0012449	      0.13602	       0.9717	         0.55	      0.95422	      0.90909	      0.93821	       1.4309	      0.92843	       1.9595	      0.91693	       2.4281	       0.9272	       2.8052	      0.90909	       3.6364	
    17	    21	      0.10479	        17.03	       0.6046	       3.4629	       2.8367	       4.9183	        16.96	   3.355e+005	      0.18693	      0.15027	    0.0016191	      0.13607	      0.97136	         0.55	      0.95384	      0.90909	      0.93781	       1.4266	      0.92837	       1.9605	      0.91681	       2.4256	      0.92717	       2.8081	      0.90909	       3.6364	
    18	    22	      0.10478	       17.077	       0.6097	       3.4561	       2.7992	       4.9303	       16.963	   3.355e+005	      0.18699	       0.1503	    0.0016003	      0.13609	      0.97124	         0.55	      0.95374	      0.90909	      0.93769	       1.4251	      0.92836	       1.9606	       0.9168	       2.4255	      0.92717	       2.8094	      0.90909	       3.6364	
    19	    24	      0.10474	       17.433	      0.63849	       3.4756	       2.7894	       4.9084	       16.974	   3.355e+005	      0.18706	      0.15084	    0.0013424	      0.13608	      0.97114	         0.55	      0.95389	      0.90909	      0.93761	       1.4194	      0.92839	       1.9565	      0.91686	       2.4243	      0.92725	        2.812	      0.90909	       3.6364	
    20	    25	      0.10473	        17.77	      0.67033	       3.5792	       2.8242	       4.7576	       17.003	   3.355e+005	      0.18713	      0.15123	    0.0051471	      0.13606	       0.9711	         0.55	      0.95409	      0.90909	      0.93756	       1.4138	      0.92846	       1.9526	      0.91695	       2.4242	      0.92735	       2.8162	      0.90909	       3.6364	
    21	    27	      0.10473	       17.821	       0.6774	        3.604	       2.8289	       4.7287	       17.012	   3.355e+005	      0.18714	      0.15129	    0.0059271	      0.13606	      0.97109	         0.55	       0.9541	      0.90909	      0.93756	       1.4133	      0.92848	       1.9527	      0.91699	       2.4257	      0.92737	       2.8181	      0.90909	       3.6364	
    22	    29	      0.10472	       17.861	      0.68238	       3.6341	       2.8358	       4.6894	       17.018	   3.355e+005	      0.18715	      0.15134	    0.0070765	      0.13606	      0.97109	         0.55	      0.95413	      0.90909	      0.93757	       1.4127	      0.92849	       1.9523	        0.917	       2.4257	      0.92739	       2.8186	      0.90909	       3.6364	
    23	    31	      0.10472	       17.898	      0.68996	       3.6789	       2.8481	       4.6323	       17.033	   3.355e+005	      0.18716	      0.15138	    0.0087496	      0.13606	      0.97108	         0.55	      0.95415	      0.90909	      0.93756	       1.4122	      0.92849	        1.952	      0.91701	        2.426	       0.9274	       2.8192	      0.90909	       3.6364	
    24	    32	      0.10472	       17.935	      0.70073	       3.7801	       2.8762	       4.5088	       17.054	   3.355e+005	      0.18716	      0.15143	     0.012493	      0.13606	      0.97108	         0.55	      0.95418	      0.90909	      0.93757	       1.4117	       0.9285	       1.9515	      0.91701	       2.4257	       0.9274	       2.8193	      0.90909	       3.6364	
    25	    33	      0.10472	       17.961	      0.71501	       3.8879	       2.8953	       4.3864	       17.074	   3.355e+005	      0.18717	      0.15146	     0.016214	      0.13606	      0.97107	         0.55	      0.95417	      0.90909	      0.93757	       1.4117	       0.9285	       1.9515	      0.91702	       2.4259	      0.92741	       2.8198	      0.90909	       3.6364	
    26	    34	      0.10472	       17.986	      0.72764	       3.9889	       2.9067	       4.2787	       17.093	   3.355e+005	      0.18717	      0.15149	     0.019498	      0.13606	      0.97107	         0.55	      0.95418	      0.90909	      0.93757	       1.4115	       0.9285	       1.9513	      0.91702	       2.4259	      0.92741	       2.8201	      0.90909	       3.6364	
    27	    35	      0.10472	       18.006	      0.73984	       4.0965	        2.916	       4.1696	        17.11	   3.355e+005	      0.18717	      0.15152	      0.02283	      0.13606	      0.97107	         0.55	       0.9542	      0.90909	      0.93758	       1.4114	       0.9285	       1.9511	      0.91703	       2.4257	      0.92742	       2.8202	      0.90909	       3.6364	
    28	    36	      0.10471	       18.031	      0.75579	        4.215	       2.9181	       4.0554	       17.127	   3.355e+005	      0.18717	      0.15155	     0.026317	      0.13606	      0.97106	         0.55	       0.9542	      0.90909	      0.93758	       1.4114	      0.92851	        1.951	      0.91703	       2.4258	      0.92742	       2.8205	      0.90909	       3.6364	
    29	    37	      0.10471	       18.051	        0.769	       4.3305	       2.9127	       3.9509	       17.144	   3.355e+005	      0.18718	      0.15158	     0.029548	      0.13605	      0.97107	         0.55	      0.95422	      0.90909	      0.93759	       1.4113	      0.92851	       1.9508	      0.91703	       2.4255	      0.92742	       2.8204	      0.90909	       3.6364	
    30	    38	      0.10471	       18.077	      0.78609	       4.4563	       2.9019	       3.8428	       17.163	   3.355e+005	      0.18718	      0.15161	     0.032866	      0.13605	      0.97107	         0.55	      0.95423	      0.90909	       0.9376	       1.4112	      0.92851	       1.9505	      0.91703	       2.4254	      0.92743	       2.8205	      0.90909	       3.6364	
    31	    39	       0.1047	       18.107	      0.80505	       4.5863	       2.8857	        3.738	       17.181	   3.355e+005	      0.18718	      0.15165	     0.036065	      0.13605	      0.97106	         0.55	      0.95424	      0.90909	       0.9376	       1.4112	      0.92851	       1.9503	      0.91703	       2.4253	      0.92743	       2.8207	      0.90909	       3.6364	
    32	    40	       0.1047	        18.14	      0.82595	       4.7312	       2.8616	       3.6275	       17.198	   3.355e+005	      0.18719	      0.15169	     0.039446	      0.13605	      0.97106	         0.55	      0.95425	      0.90909	      0.93761	        1.411	      0.92852	       1.9501	      0.91704	       2.4252	      0.92744	        2.821	      0.90909	       3.6364	
    33	    41	       0.1047	       18.174	      0.84906	       4.8778	       2.8292	       3.5232	       17.218	   3.355e+005	      0.18719	      0.15174	     0.042644	      0.13605	      0.97106	         0.55	      0.95426	      0.90909	      0.93762	        1.411	      0.92852	       1.9499	      0.91704	       2.4251	      0.92744	       2.8213	      0.90909	       3.6364	
    34	    42	      0.10469	       18.211	      0.87228	       5.0214	       2.7927	       3.4274	       17.235	   3.355e+005	       0.1872	      0.15178	     0.045577	      0.13605	      0.97106	         0.55	      0.95428	      0.90909	      0.93763	       1.4108	      0.92852	       1.9496	      0.91704	       2.4249	      0.92745	       2.8214	      0.90909	       3.6364	
    35	    43	      0.10469	        18.25	      0.89925	       5.1786	       2.7456	       3.3281	       17.255	   3.355e+005	       0.1872	      0.15183	     0.048636	      0.13605	      0.97106	         0.55	       0.9543	      0.90909	      0.93764	       1.4108	      0.92853	       1.9494	      0.91705	       2.4248	      0.92745	       2.8217	      0.90909	       3.6364	
    36	    44	      0.10469	       18.298	      0.93148	       5.3513	       2.6896	       3.2261	       17.273	   3.355e+005	      0.18721	       0.1519	     0.051755	      0.13605	      0.97105	         0.55	      0.95431	      0.90909	      0.93765	       1.4107	      0.92853	       1.9491	      0.91705	       2.4247	      0.92746	       2.8222	      0.90909	       3.6364	
    37	    45	      0.10468	       18.348	      0.96215	       5.5251	       2.6273	       3.1309	       17.295	   3.355e+005	      0.18721	      0.15196	     0.054689	      0.13605	      0.97106	         0.55	      0.95435	      0.90909	      0.93767	       1.4105	      0.92854	       1.9486	      0.91706	       2.4243	      0.92747	       2.8221	      0.90909	       3.6364	
    38	    46	      0.10468	       18.404	       1.0003	       5.7094	       2.5553	       3.0363	       17.315	   3.355e+005	      0.18722	      0.15203	     0.057595	      0.13604	      0.97106	         0.55	      0.95437	      0.90909	      0.93769	       1.4104	      0.92854	       1.9481	      0.91706	        2.424	      0.92747	       2.8224	      0.90909	       3.6364	
    39	    47	      0.10467	        18.47	       1.0444	         5.91	         2.47	       2.9403	       17.336	   3.355e+005	      0.18722	      0.15212	     0.060555	      0.13604	      0.97106	         0.55	       0.9544	      0.90909	      0.93771	       1.4102	      0.92855	       1.9477	      0.91707	       2.4237	      0.92748	       2.8227	      0.90909	       3.6364	
    40	    48	      0.10466	       18.542	       1.0905	       6.1165	       2.3784	        2.849	       17.358	   3.355e+005	      0.18723	      0.15221	     0.063371	      0.13604	      0.97107	         0.55	      0.95446	      0.90909	      0.93774	       1.4099	      0.92856	        1.947	      0.91707	       2.4232	      0.92749	       2.8228	      0.90909	       3.6364	
    41	    49	      0.10466	       18.626	        1.148	       6.3394	       2.2724	       2.7574	       17.382	   3.355e+005	      0.18724	      0.15231	     0.066202	      0.13604	      0.97107	         0.55	      0.95449	      0.90909	      0.93776	       1.4098	      0.92856	       1.9464	      0.91708	       2.4227	       0.9275	       2.8231	      0.90909	       3.6364	
    42	    50	      0.10465	       18.718	       1.2105	       6.5593	        2.164	       2.6745	       17.406	   3.355e+005	      0.18724	      0.15243	     0.068759	      0.13603	      0.97107	         0.55	      0.95454	      0.90909	      0.93779	       1.4096	      0.92857	       1.9457	      0.91709	       2.4223	      0.92751	       2.8235	      0.90909	       3.6364	
    43	    51	      0.10464	       18.816	       1.2754	       6.7692	       2.0565	       2.6014	        17.43	   3.355e+005	      0.18725	      0.15255	     0.071029	      0.13603	      0.97108	         0.55	      0.95459	      0.90909	      0.93782	       1.4093	      0.92858	       1.9449	      0.91709	       2.4217	      0.92753	       2.8238	      0.90909	       3.6364	
    44	    52	      0.10464	       18.916	       1.3419	       6.9699	       1.9514	       2.5361	       17.454	   3.355e+005	      0.18726	      0.15267	     0.073058	      0.13603	      0.97109	         0.55	      0.95465	      0.90909	      0.93786	        1.409	      0.92859	       1.9441	       0.9171	        2.421	      0.92754	        2.824	      0.90909	       3.6364	
    45	    53	      0.10463	       19.021	       1.4114	       7.1624	       1.8484	       2.4776	       17.479	   3.355e+005	      0.18727	      0.15279	     0.074887	      0.13602	       0.9711	         0.55	      0.95471	      0.90909	       0.9379	       1.4087	       0.9286	       1.9432	      0.91711	       2.4203	      0.92755	       2.8242	      0.90909	       3.6364	
    46	    55	      0.10463	       19.143	       1.4904	       7.3687	       1.7386	       2.4187	       17.508	   3.355e+005	      0.18728	      0.15294	     0.076724	      0.13602	      0.97111	         0.55	       0.9548	      0.90909	      0.93794	       1.4082	      0.92861	       1.9421	      0.91712	       2.4194	      0.92756	       2.8243	      0.90909	       3.6364	
    47	    56	      0.10463	       19.404	        1.659	       7.7675	       1.5232	       2.3111	       17.603	   3.355e+005	       0.1873	      0.15325	     0.080089	      0.13601	      0.97114	         0.55	      0.95495	      0.90909	      0.93803	       1.4074	      0.92864	       1.9399	      0.91714	       2.4179	      0.92759	       2.8246	      0.90909	       3.6364	
    48	    57	      0.10461	       19.559	       1.7649	       7.9373	       1.4307	       2.2782	       17.651	   3.355e+005	      0.18731	      0.15343	     0.081133	      0.13601	      0.97114	         0.55	      0.95502	      0.90909	      0.93807	        1.407	      0.92865	       1.9387	      0.91715	       2.4169	      0.92761	       2.8252	      0.90909	       3.6364	
    49	    59	      0.10461	       19.873	         1.96	         8.28	       1.2447	       2.2026	       18.641	   3.355e+005	      0.18735	      0.15379	     0.083528	      0.13601	      0.97113	         0.55	      0.95516	      0.90909	      0.93811	       1.4055	      0.92865	       1.9359	      0.91715	        2.415	      0.92763	       2.8259	      0.90909	       3.6364	
    50	    60	      0.10461	       20.019	       2.0625	       8.3914	       1.1843	       2.1864	       19.749	   3.355e+005	      0.18737	      0.15394	     0.084077	      0.13601	      0.97107	         0.55	      0.95516	      0.90909	      0.93807	       1.4047	      0.92861	       1.9346	      0.91713	       2.4142	      0.92763	       2.8269	      0.90909	       3.6364	
    51	    63	      0.10459	       19.791	       2.0549	       8.3097	       1.1618	       2.2116	       37.927	   3.355e+005	      0.18754	      0.15364	     0.083832	      0.13618	      0.97018	         0.55	      0.95409	      0.90909	      0.93701	       1.4024	      0.92791	       1.9333	      0.91669	       2.4142	      0.92736	       2.8288	      0.90909	       3.6364	
    52	    64	      0.10459	       19.772	       2.0905	       8.3537	       1.1172	       2.2105	       49.266	  3.3549e+005	       0.1876	      0.15361	     0.084143	      0.13622	      0.96991	         0.55	      0.95379	      0.90909	      0.93666	       1.4008	       0.9277	       1.9329	      0.91655	       2.4137	      0.92727	       2.8283	      0.90909	       3.6364	
    53	    65	      0.10458	       19.752	       2.1684	       8.4408	       1.0336	       2.2029	       62.983	  3.3548e+005	      0.18769	      0.15356	     0.084784	      0.13629	      0.96948	         0.55	      0.95331	      0.90909	      0.93617	       1.3998	      0.92736	       1.9316	      0.91634	       2.4131	      0.92713	       2.8282	      0.90909	       3.6364	
    54	    66	      0.10458	       19.729	       2.1648	        8.423	       1.0371	       2.2086	       67.044	  3.3548e+005	      0.18771	      0.15353	     0.084675	      0.13631	      0.96937	         0.55	      0.95319	      0.90909	      0.93603	       1.3993	      0.92729	       1.9318	      0.91629	       2.4133	       0.9271	       2.8286	      0.90909	       3.6364	
    55	    67	      0.10458	       19.701	       2.1529	       8.4058	       1.0434	       2.2125	       68.799	  3.3548e+005	      0.18772	       0.1535	     0.084583	      0.13632	      0.96933	         0.55	      0.95312	      0.90909	      0.93596	       1.3991	      0.92726	        1.932	      0.91627	       2.4135	      0.92708	       2.8287	      0.90909	       3.6364	
    56	    75	      0.10458	         19.7	       2.1511	       8.4033	       1.0443	        2.213	       68.799	  3.3548e+005	      0.18772	       0.1535	      0.08457	      0.13632	      0.96933	         0.55	      0.95312	      0.90909	      0.93596	       1.3991	      0.92726	       1.9321	      0.91627	       2.4136	      0.92708	       2.8287	      0.90909	       3.6364	
    57	    76	      0.10458	       19.698	       2.1509	        8.404	       1.0435	       2.2128	         68.8	  3.3548e+005	      0.18772	       0.1535	     0.084578	      0.13632	      0.96933	         0.55	      0.95312	      0.90909	      0.93596	       1.3991	      0.92726	       1.9321	      0.91627	       2.4136	      0.92708	       2.8287	      0.90909	       3.6364	
    58	    77	      0.10458	       19.697	       2.1498	       8.4034	       1.0431	       2.2129	         68.8	  3.3548e+005	      0.18772	       0.1535	     0.084581	      0.13632	      0.96933	         0.55	      0.95312	      0.90909	      0.93597	       1.3991	      0.92726	       1.9321	      0.91627	       2.4135	      0.92708	       2.8286	      0.90909	       3.6364	
    59	    82	      0.10458	       19.697	       2.1498	       8.4034	       1.0431	       2.2129	         68.8	  3.3548e+005	      0.18772	       0.1535	      0.08458	      0.13632	      0.96933	         0.55	      0.95312	      0.90909	      0.93597	       1.3991	      0.92726	       1.9321	      0.91627	       2.4135	      0.92708	       2.8285	      0.90909	       3.6364	
    60	    87	      0.10458	       19.697	       2.1498	       8.4034	       1.0431	       2.2129	         68.8	  3.3548e+005	      0.18772	       0.1535	      0.08458	      0.13632	      0.96933	         0.55	      0.95312	      0.90909	      0.93597	       1.3991	      0.92726	       1.9321	      0.91627	       2.4135	      0.92708	       2.8285	      0.90909	       3.6364	

:: False convergence

       Squares	       0.104583
     Func Eval	             87
     Grad Eval	             60

:: Nonlinear parameters

     i	   Final p(i)	  Gradient(i)
     1	      19.6968	  4.8014e-008
     2	      2.14983	 -4.4785e-007
     3	       8.4034	 -1.3721e-007
     4	      1.04312	 -4.6778e-007
     5	      2.21292	  4.3977e-007
     6	      68.7996	  -7.142e-009
     7	       335482	 -1.4164e-013
     8	     0.187719	  1.6911e-006
     9	     0.153496	  6.9557e-006
    10	    0.0845805	 -3.5004e-006
    11	     0.136317	 -3.3252e-005
    12	     0.969329	  2.8187e-007
    13	         0.55	    -0.035368
    14	     0.953123	 -2.0373e-006
    15	     0.909091	     0.010164
    16	     0.935965	 -7.9271e-009
    17	      1.39913	 -1.8838e-007
    18	     0.927257	 -1.1503e-007
    19	      1.93208	  4.1966e-008
    20	     0.916267	 -6.9109e-007
    21	      2.41351	  1.1559e-007
    22	     0.927082	 -1.0963e-006
    23	      2.82855	 -5.7816e-009
    24	     0.909091	     0.010123
    25	      3.63636	   0.00027774
