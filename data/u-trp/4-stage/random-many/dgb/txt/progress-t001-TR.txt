
===================================================
Least squares Minimization - Trust Region Algorithm
===================================================

Optimized parameters:

     1 	 k.1
     2 	 k.-1
     3 	 k.2
     4 	 k.-2
     5 	 k.cat
     6 	 k.off
     7 	 k.-off
     8 	 r(E)
     9 	 r(E.S1)
    10 	 r(E.S2)
    11 	 r(E.P)
    12 	 [E]#1
    13 	 [S]#1
    14 	 [E]#2
    15 	 [S]#2
    16 	 [E]#3
    17 	 [S]#3
    18 	 [E]#4
    19 	 [S]#4
    20 	 [E]#5
    21 	 [S]#5
    22 	 [E]#6
    23 	 [S]#6
    24 	 [E]#7
    25 	 [S]#7

:: NL2SOL :: DNLAFB
:: Analytic Jacobian; Constrained

________________________________________________________________________

:: Initial Estimate

     i	     Param p(i)	            Min	            Max
     1	         0.4061	         1e-006	         1e+006
     2	          26.75	         1e-006	         1e+006
     3	          342.9	         1e-006	         1e+006
     4	         349900	         1e-006	         1e+006
     5	     8.189e-005	         1e-006	         1e+006
     6	       0.002937	         1e-006	         1e+006
     7	         0.8939	         1e-006	         1e+006
     8	           0.18	       1.8e-007	         180000
     9	           0.14	       1.4e-007	         140000
    10	          0.079	       7.9e-008	          79000
    11	           0.13	       1.3e-007	         130000
    12	              1	       0.909091	            1.1
    13	            0.5	       0.454545	           0.55
    14	              1	       0.909091	            1.1
    15	              1	       0.909091	            1.1
    16	              1	       0.909091	            1.1
    17	            1.5	        1.36364	           1.65
    18	              1	       0.909091	            1.1
    19	              2	        1.81818	            2.2
    20	              1	       0.909091	            1.1
    21	            2.5	        2.27273	           2.75
    22	              1	       0.909091	            1.1
    23	              3	        2.72727	            3.3
    24	              1	       0.909091	            1.1
    25	              4	        3.63636	            4.4

:: Iterations

     i	 FEval	   F(i) = SSQ	   Param( 1)	   Param( 2)	   Param( 3)	   Param( 4)	   Param( 5)	   Param( 6)	   Param( 7)	   Param( 8)	   Param( 9)	   Param(10)	   Param(11)	   Param(12)	   Param(13)	   Param(14)	   Param(15)	   Param(16)	   Param(17)	   Param(18)	   Param(19)	   Param(20)	   Param(21)	   Param(22)	   Param(23)	   Param(24)	   Param(25)	
     0	     1	       36.925	       0.4061	        26.75	        342.9	   3.499e+005	   8.189e-005	     0.002937	       0.8939	         0.18	         0.14	        0.079	         0.13	            1	          0.5	            1	            1	            1	          1.5	            1	            2	            1	          2.5	            1	            3	            1	            4	
     1	     2	        12.27	       2.0453	       26.712	        342.9	   3.499e+005	      0.15211	    0.0027824	      0.89389	      0.17967	     1.4e-007	     0.078672	      0.13002	      0.98407	         0.55	      0.97433	          1.1	      0.97434	       1.6333	      0.98073	       2.0541	      0.99301	       2.5242	      0.99734	       2.9728	       1.0119	       3.9029	
     2	     3	       1.3775	       4.1533	       22.455	        342.9	   3.499e+005	      0.07454	       1e-006	      0.89846	      0.17525	     1.4e-007	      0.12381	       1.6534	      0.96266	         0.55	      0.92381	          1.1	      0.92444	         1.65	      0.97589	       2.1074	      0.96515	       2.5337	       0.9924	       2.9599	       1.0145	       3.8834	
     3	     4	       1.1283	        5.017	       18.223	        342.9	   3.499e+005	       2.5579	   0.00035638	      0.89916	      0.19357	     1.4e-007	     0.078766	       1.8321	      0.90909	         0.55	      0.90909	          1.1	      0.90909	         1.65	      0.91119	          2.2	      0.92279	       2.5304	      0.97193	       2.8938	       1.0257	       3.8562	
     4	     6	      0.73392	       4.0324	       14.945	       342.91	   3.499e+005	       1.8601	     0.034277	       0.8971	      0.18273	     1.4e-007	      0.11872	       2.6454	      0.90909	         0.55	      0.90909	          1.1	      0.90909	         1.65	      0.94712	          2.2	      0.98226	       2.6695	      0.98196	       2.7273	        1.044	       3.6364	
     5	     7	      0.64124	       3.5611	       13.189	       342.92	   3.499e+005	       2.8185	      0.39849	      0.73876	      0.18082	     1.4e-007	     0.085759	        3.191	      0.92545	         0.55	      0.90909	          1.1	      0.90909	         1.65	       0.9523	          2.2	       0.9708	       2.5559	      0.99519	       2.7273	       1.0529	       3.6364	
     6	     8	      0.55494	       3.7333	       9.5621	       342.93	   3.499e+005	        3.919	      0.65069	       1e-006	      0.18144	     0.026049	     0.085831	       3.8761	      0.93015	         0.55	      0.90909	          1.1	      0.90909	         1.65	      0.95462	          2.2	      0.95398	       2.3971	      0.99503	       2.7273	       1.0507	       3.6364	
     7	    11	      0.52623	        3.768	       9.5276	       342.93	   3.499e+005	       3.9534	       0.4521	   0.00079987	      0.18197	      0.02576	      0.08583	       3.9114	       0.9358	         0.55	      0.90909	          1.1	      0.91859	         1.65	      0.96081	          2.2	      0.95366	       2.6079	      0.98906	       2.7273	       1.0837	       3.6364	
     8	    12	      0.47465	        3.834	       9.4172	       342.93	   3.499e+005	       4.0428	      0.40593	       1e-006	       0.1815	      0.02404	     0.085828	       3.9704	       0.9391	         0.55	      0.90909	          1.1	      0.92089	         1.65	      0.96317	          2.2	      0.98058	       2.5665	       1.0067	       2.7273	       1.0805	       3.6364	
     9	    15	      0.41781	       4.5919	       6.6726	       342.94	   3.499e+005	       4.6924	       0.5804	       1e-006	      0.18396	     0.056499	     0.085862	       4.6473	      0.93413	         0.55	      0.90909	          1.1	       0.9183	         1.65	      0.95524	          2.2	      0.95157	       2.3808	      0.99265	       2.7273	       1.0404	       3.6364	
    10	    16	      0.30755	       4.7515	       5.1777	       342.96	   3.499e+005	       5.1517	      0.69296	       1e-006	      0.18509	     0.061105	     0.085884	       5.1446	      0.93406	         0.55	      0.90909	          1.1	      0.92105	         1.65	      0.95351	          2.2	      0.94085	        2.277	      0.98494	       2.7273	        1.026	       3.6364	
    11	    18	      0.21565	       4.7044	       2.4881	       342.97	   3.499e+005	       6.0216	      0.95536	   0.00017495	      0.18351	     0.077184	      0.08591	       6.0361	        0.948	         0.55	      0.92345	          1.1	      0.92802	         1.65	      0.95152	          2.2	      0.94085	        2.277	       0.9747	       2.7273	      0.99464	       3.6364	
    12	    19	      0.20127	       4.7751	       2.2816	       342.97	   3.499e+005	       6.0856	      0.98997	       1e-006	      0.18279	     0.079383	     0.085912	       6.1019	      0.95515	         0.55	      0.93281	          1.1	      0.93427	         1.65	      0.95547	       2.1927	      0.94249	        2.277	       0.9762	       2.7273	      0.99464	       3.6364	
    13	    20	      0.19253	       4.8749	       2.1234	       342.97	   3.499e+005	       6.1781	       1.0111	   0.00017316	      0.18403	     0.081147	     0.085918	       6.2151	       0.9516	         0.55	      0.92919	          1.1	      0.93124	         1.65	      0.95151	          2.2	       0.9405	        2.345	      0.96845	       2.7548	      0.98425	       3.6364	
    14	    21	      0.18741	       4.9306	        2.014	       342.98	   3.499e+005	       6.2374	       1.0415	       1e-006	      0.18377	     0.081729	     0.085924	       6.3204	      0.95463	         0.55	      0.93248	          1.1	      0.93472	         1.65	      0.95345	          2.2	      0.94541	       2.3959	      0.96983	        2.781	      0.98379	       3.6364	
    15	    24	      0.15441	        5.266	      0.28112	       343.02	   3.499e+005	       8.3074	       1.4994	       1e-006	      0.18549	     0.093591	     0.086121	       7.0798	      0.95603	         0.55	      0.93787	          1.1	      0.93312	         1.65	      0.94261	          2.2	      0.93305	       2.5233	      0.94849	       2.7659	      0.95171	       3.6364	
    16	    25	       0.1386	       5.2533	      0.40368	       343.01	   3.499e+005	       8.0988	        1.598	       1e-006	      0.18487	     0.091131	     0.087565	        7.973	      0.96131	         0.55	      0.94638	          1.1	       0.9374	         1.65	      0.94637	          2.2	      0.93802	       2.5238	      0.94901	       2.7541	      0.95171	       3.6364	
    17	    27	       0.1368	       5.2688	      0.39632	       343.01	   3.499e+005	       8.0252	       1.5965	       1e-006	      0.18413	     0.091187	     0.087567	       7.9622	      0.96525	         0.55	      0.95201	          1.1	      0.94057	         1.65	      0.94951	          2.2	      0.94103	       2.5317	      0.95212	       2.7487	      0.94995	       3.6364	
    18	    28	      0.13545	       5.2422	       0.4324	       343.01	   3.499e+005	       8.0317	       1.5665	       1e-006	      0.18992	     0.094366	     0.087576	       8.0193	      0.93626	         0.55	      0.92291	          1.1	      0.91069	         1.65	      0.91721	          2.2	      0.90909	       2.5191	       0.9205	       2.7519	      0.91744	       3.6364	
    19	    29	      0.13494	       5.2337	       0.3858	          343	   3.499e+005	       7.8345	       1.5778	       1e-006	      0.18947	     0.094395	     0.087661	       8.3137	       0.9394	         0.55	      0.92676	          1.1	      0.91532	         1.65	      0.91835	          2.2	      0.90994	       2.5105	      0.92233	       2.7463	      0.91744	       3.6364	
    20	    30	      0.13491	        5.228	      0.38617	          343	   3.499e+005	        7.756	       1.5801	       1e-006	      0.18963	     0.094426	     0.087694	        8.432	      0.93868	         0.55	      0.92602	          1.1	      0.91453	         1.65	       0.9176	          2.2	      0.90922	       2.5113	      0.92159	       2.7474	      0.91658	       3.6364	
    21	    31	      0.13491	       5.2262	      0.38683	          343	   3.499e+005	       7.7432	       1.5803	       1e-006	      0.18965	     0.094425	     0.087703	       8.4505	      0.93856	         0.55	       0.9259	          1.1	       0.9144	         1.65	      0.91746	          2.2	      0.90909	       2.5113	      0.92146	       2.7473	      0.91645	       3.6364	
    22	    32	      0.13488	       5.2202	       0.3831	          343	   3.499e+005	       7.6797	       1.5791	       1e-006	      0.18977	     0.094568	     0.087727	       8.5153	      0.93792	         0.55	      0.92522	          1.1	      0.91364	         1.65	      0.91657	          2.2	      0.90909	       2.5273	      0.92063	       2.7503	      0.91503	       3.6364	
    23	    56	      0.13488	       5.2202	       0.3831	          343	   3.499e+005	       7.6797	       1.5791	       1e-006	      0.18977	     0.094568	     0.087727	       8.5153	      0.93792	         0.55	      0.92522	          1.1	      0.91364	         1.65	      0.91657	          2.2	      0.90909	       2.5273	      0.92063	       2.7503	      0.91503	       3.6364	

:: False convergence

       Squares	       0.134883
     Func Eval	             56
     Grad Eval	             23

:: Nonlinear parameters

     i	   Final p(i)	  Gradient(i)
     1	      5.22022	 -5.7186e-006
     2	     0.383103	 -9.8064e-005
     3	          343	 -6.8052e-007
     4	       349900	  6.7254e-010
     5	      7.67967	 -3.0457e-005
     6	      1.57911	   0.00015613
     7	       1e-006	      -4.0621
     8	     0.189774	   -0.0012015
     9	    0.0945681	    -0.009956
    10	    0.0877272	 -9.7651e-006
    11	      8.51528	 -5.6563e-005
    12	      0.93792	   -0.0002108
    13	         0.55	    -0.043078
    14	     0.925215	  -0.00026323
    15	          1.1	   -0.0079031
    16	     0.913644	  -0.00024799
    17	         1.65	   -0.0072766
    18	     0.916568	  -0.00027331
    19	          2.2	    -0.001868
    20	     0.909091	     0.012594
    21	      2.52728	 -1.2545e-005
    22	     0.920627	  -0.00027779
    23	      2.75026	 -1.8965e-006
    24	     0.915028	   -0.0003479
    25	      3.63636	     0.010149
