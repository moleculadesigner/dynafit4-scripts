
===================================================
Least squares Minimization - Trust Region Algorithm
===================================================

Optimized parameters:

     1 	 k.1
     2 	 k.-1
     3 	 k.2
     4 	 k.-2
     5 	 k.cat
     6 	 k.off
     7 	 k.-off
     8 	 r(E)
     9 	 r(E.S1)
    10 	 r(E.S2)
    11 	 r(E.P)
    12 	 [E]#1
    13 	 [S]#1
    14 	 [E]#2
    15 	 [S]#2
    16 	 [E]#3
    17 	 [S]#3
    18 	 [E]#4
    19 	 [S]#4
    20 	 [E]#5
    21 	 [S]#5
    22 	 [E]#6
    23 	 [S]#6
    24 	 [E]#7
    25 	 [S]#7

:: NL2SOL :: DNLAFB
:: Analytic Jacobian; Constrained

________________________________________________________________________

:: Initial Estimate

     i	     Param p(i)	            Min	            Max
     1	         304500	         1e-006	         1e+006
     2	          68240	         1e-006	         1e+006
     3	          83150	         1e-006	         1e+006
     4	          375.1	         1e-006	         1e+006
     5	          25720	         1e-006	         1e+006
     6	           1.34	         1e-006	         1e+006
     7	       0.002255	         1e-006	         1e+006
     8	           0.18	       1.8e-007	         180000
     9	           0.14	       1.4e-007	         140000
    10	          0.079	       7.9e-008	          79000
    11	           0.13	       1.3e-007	         130000
    12	              1	       0.909091	            1.1
    13	            0.5	       0.454545	           0.55
    14	              1	       0.909091	            1.1
    15	              1	       0.909091	            1.1
    16	              1	       0.909091	            1.1
    17	            1.5	        1.36364	           1.65
    18	              1	       0.909091	            1.1
    19	              2	        1.81818	            2.2
    20	              1	       0.909091	            1.1
    21	            2.5	        2.27273	           2.75
    22	              1	       0.909091	            1.1
    23	              3	        2.72727	            3.3
    24	              1	       0.909091	            1.1
    25	              4	        3.63636	            4.4

:: Iterations

     i	 FEval	   F(i) = SSQ	   Param( 1)	   Param( 2)	   Param( 3)	   Param( 4)	   Param( 5)	   Param( 6)	   Param( 7)	   Param( 8)	   Param( 9)	   Param(10)	   Param(11)	   Param(12)	   Param(13)	   Param(14)	   Param(15)	   Param(16)	   Param(17)	   Param(18)	   Param(19)	   Param(20)	   Param(21)	   Param(22)	   Param(23)	   Param(24)	   Param(25)	
     0	     1	       11.037	   3.045e+005	        68240	        83150	        375.1	        25720	         1.34	     0.002255	         0.18	         0.14	        0.079	         0.13	            1	          0.5	            1	            1	            1	          1.5	            1	            2	            1	          2.5	            1	            3	            1	            4	
     1	     2	       2.9774	   3.045e+005	        68240	        83150	        375.1	        25720	       1.3587	     0.042914	      0.12918	     0.091821	      0.15963	       0.1217	          1.1	      0.45455	       1.0293	       0.9569	       1.0244	       1.5542	       1.0081	       1.9633	      0.98524	       2.4662	      0.99167	       2.9795	        1.015	       3.9859	
     2	     3	        2.671	   3.045e+005	        68240	        83150	        375.1	        25720	        0.498	      0.58665	      0.13562	      0.12897	      0.20595	      0.12346	          1.1	      0.45455	       1.0416	      0.90909	       1.0002	       1.6125	      0.98448	       2.0386	       0.9595	       2.3899	      0.96865	       2.8668	       1.0061	       3.9405	
     3	     4	       2.4592	   3.045e+005	        68240	        83150	        375.1	        25720	      0.24594	      0.78982	       0.1422	      0.11371	      0.23273	      0.12668	          1.1	      0.45455	        1.026	      0.90909	      0.98007	       1.5605	      0.95406	       1.8182	      0.93662	       2.2727	      0.94394	       2.7273	      0.93757	       3.6364	
     4	     5	       2.4272	   3.045e+005	        68240	        83150	        375.1	        25720	      0.15598	        0.911	      0.14347	      0.10725	      0.24511	      0.12925	          1.1	      0.45455	       1.0184	      0.90909	      0.96302	       1.3636	      0.93941	       1.8182	      0.92111	       2.2727	       0.9273	       2.7273	      0.91066	       3.6364	
     5	     6	       2.3997	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	        1.383	       0.1488	     0.028952	      0.26907	      0.12786	          1.1	      0.45455	       1.0364	      0.90909	      0.98019	       1.3636	      0.95572	       1.8182	      0.93615	       2.2727	      0.93791	       2.7273	      0.90909	       3.6364	
     6	     7	       2.3949	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	       1.3355	      0.14708	     0.025395	      0.26746	      0.12799	          1.1	      0.45455	       1.0315	      0.90909	      0.97741	       1.3636	      0.95017	       1.8182	      0.93071	       2.2727	      0.93601	       2.7273	      0.90909	       3.6364	
     7	    10	       2.3948	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	       1.3355	      0.14737	     0.015777	      0.26989	      0.12829	          1.1	      0.47506	       1.0295	      0.90909	      0.97485	       1.3636	      0.94817	       1.8218	      0.92821	       2.2738	      0.93391	       2.7273	      0.91074	       3.6367	
     8	    11	       2.3946	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	       1.3359	      0.14824	    0.0083626	      0.27149	      0.12805	          1.1	      0.49931	       1.0311	      0.90909	      0.97649	       1.3636	      0.94943	       1.8237	      0.92987	       2.2746	      0.93558	       2.7273	      0.91074	       3.6368	
     9	    13	       2.3946	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	       1.3362	      0.14892	   0.00082209	      0.27415	      0.12828	          1.1	      0.52404	       1.0293	      0.91025	      0.97473	       1.3636	      0.94771	       1.8258	      0.92819	       2.2755	      0.93388	       2.7273	      0.90909	        3.637	
    10	    14	       2.3946	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	       1.3367	        0.149	     1.4e-007	      0.27458	      0.12828	          1.1	      0.52573	       1.0306	      0.91952	      0.97474	       1.3636	      0.94773	       1.8433	      0.92821	       2.2831	       0.9339	       2.7273	      0.90909	       3.6385	
    11	    15	       2.3946	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	       1.3412	       0.1499	     1.4e-007	      0.27393	      0.12828	          1.1	         0.55	       1.0318	      0.93324	      0.97475	       1.3636	      0.94773	       1.8628	      0.92821	       2.2914	       0.9339	       2.7273	      0.90909	       3.6403	
    12	    16	       2.3945	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	       1.3339	      0.15004	     1.4e-007	      0.27297	      0.12826	          1.1	         0.55	        1.046	       1.0309	      0.97486	       1.3636	      0.94781	       1.9577	      0.92833	       2.3399	      0.93402	       2.7273	      0.90909	       3.6521	
    13	    18	       2.3945	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	       1.3339	      0.15008	     1.4e-007	      0.27253	      0.12828	       1.0994	         0.55	       1.0465	       1.0351	      0.97472	       1.3636	      0.94769	       1.9578	      0.92818	         2.34	      0.93388	       2.7273	      0.90909	       3.6521	
    14	    20	       2.3945	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	       1.3339	      0.15011	     1.4e-007	      0.27223	      0.12829	       1.0991	         0.55	       1.0471	       1.0404	      0.97463	       1.3636	      0.94762	       1.9579	      0.92809	       2.3401	      0.93379	       2.7273	      0.90909	       3.6521	
    15	    22	       2.3944	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	       1.3339	      0.15011	     1.4e-007	      0.27222	       0.1283	       1.0991	      0.54993	       1.0477	       1.0451	      0.97457	       1.3636	      0.94756	        1.958	      0.92804	       2.3401	      0.93373	       2.7273	      0.90909	       3.6521	
    16	    24	       2.3944	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	       1.3339	      0.15012	     1.4e-007	      0.27225	      0.12832	       1.0991	         0.55	       1.0478	       1.0484	      0.97441	       1.3636	       0.9474	        1.958	      0.92789	       2.3401	      0.93358	       2.7273	      0.90909	       3.6521	
    17	    27	       2.3944	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	       1.3339	      0.15011	     1.4e-007	      0.27225	      0.12831	       1.0991	         0.55	       1.0482	        1.048	      0.97445	       1.3636	      0.94744	        1.958	      0.92793	       2.3401	      0.93362	       2.7273	      0.90909	       3.6521	
    18	    29	       2.3943	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	       1.3339	      0.15011	     1.4e-007	      0.27225	      0.12831	       1.0991	         0.55	       1.0481	       1.0482	      0.97446	       1.3636	      0.94745	        1.958	      0.92794	       2.3401	      0.93363	       2.7273	      0.90909	       3.6521	
    19	    30	       2.3943	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	       1.3339	      0.15012	     1.4e-007	      0.27224	       0.1283	       1.0992	         0.55	       1.0482	       1.0482	      0.97457	       1.3636	      0.94756	        1.958	      0.92804	       2.3401	      0.93373	       2.7273	      0.90909	       3.6521	
    20	    31	       2.3943	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	       1.3339	      0.15012	     1.4e-007	      0.27223	      0.12829	       1.0992	         0.55	       1.0483	       1.0484	      0.97467	       1.3636	      0.94766	        1.958	      0.92814	       2.3401	      0.93383	       2.7273	      0.90909	       3.6521	
    21	    32	       2.3943	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	       1.3339	      0.15013	     1.4e-007	      0.27217	      0.12827	       1.0992	         0.55	       1.0485	       1.0485	      0.97477	       1.3636	      0.94775	       1.9585	      0.92823	       2.3404	      0.93393	       2.7273	      0.90909	       3.6522	
    22	    33	       2.3943	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	        1.334	      0.15016	     1.4e-007	      0.27178	      0.12815	       1.0995	         0.55	       1.0494	       1.0494	      0.97573	       1.3636	      0.94869	       1.9591	      0.92915	       2.3408	      0.93485	       2.7273	      0.90999	       3.6523	
    23	    37	       2.3943	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	        1.334	      0.15016	     1.4e-007	      0.27178	      0.12815	       1.0995	         0.55	       1.0494	       1.0494	      0.97573	       1.3636	      0.94869	       1.9591	      0.92915	       2.3408	      0.93485	       2.7273	      0.90999	       3.6523	
    24	    39	       2.3943	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	        1.334	      0.15018	     1.4e-007	      0.27179	      0.12815	       1.0995	         0.55	       1.0494	       1.0495	      0.97573	       1.3636	      0.94869	       1.9591	      0.92915	       2.3408	      0.93485	       2.7273	      0.90999	       3.6523	
    25	    41	       2.3943	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	        1.334	      0.15018	     1.4e-007	      0.27179	      0.12815	       1.0995	         0.55	       1.0494	       1.0495	      0.97573	       1.3636	      0.94869	       1.9591	      0.92915	       2.3408	      0.93485	       2.7273	      0.90999	       3.6523	
    26	    43	       2.3943	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	        1.334	      0.15018	     1.4e-007	      0.27179	      0.12815	       1.0995	         0.55	       1.0494	       1.0494	      0.97573	       1.3636	      0.94869	       1.9591	      0.92915	       2.3408	      0.93485	       2.7273	      0.90999	       3.6523	
    27	    45	       2.3943	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	        1.334	      0.15018	     1.4e-007	      0.27179	      0.12815	       1.0995	         0.55	       1.0494	       1.0494	      0.97573	       1.3636	      0.94869	       1.9591	      0.92915	       2.3408	      0.93485	       2.7273	      0.90999	       3.6523	
    28	    48	       2.3943	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	        1.334	      0.15018	     1.4e-007	      0.27179	      0.12815	       1.0995	         0.55	       1.0494	       1.0494	      0.97573	       1.3636	      0.94869	       1.9591	      0.92915	       2.3408	      0.93485	       2.7273	      0.90999	       3.6523	
    29	    49	       2.3943	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	        1.334	      0.15018	     1.4e-007	      0.27179	      0.12815	       1.0995	         0.55	       1.0494	       1.0494	      0.97573	       1.3636	      0.94869	       1.9591	      0.92915	       2.3408	      0.93485	       2.7273	      0.90999	       3.6523	
    30	    50	       2.3943	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	        1.334	      0.15018	     1.4e-007	      0.27179	      0.12815	       1.0995	         0.55	       1.0494	       1.0494	      0.97573	       1.3636	      0.94869	       1.9591	      0.92915	       2.3408	      0.93485	       2.7273	      0.90999	       3.6523	
    31	    51	       2.3943	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	        1.334	      0.15018	     1.4e-007	      0.27179	      0.12815	       1.0995	         0.55	       1.0494	       1.0494	      0.97573	       1.3636	      0.94869	       1.9591	      0.92915	       2.3408	      0.93485	       2.7273	      0.90999	       3.6523	
    32	    52	       2.3943	   3.045e+005	        68240	        83150	        375.1	        25720	       1e-006	        1.334	      0.15018	     1.4e-007	      0.27179	      0.12815	       1.0995	         0.55	       1.0494	       1.0494	      0.97573	       1.3636	      0.94869	       1.9591	      0.92915	       2.3408	      0.93485	       2.7273	      0.90999	       3.6523	

:: False convergence

       Squares	        2.39432
     Func Eval	             52
     Grad Eval	             33

:: Nonlinear parameters

     i	   Final p(i)	  Gradient(i)
     1	       304500	  6.5724e-010
     2	        68240	 -1.4354e-009
     3	        83150	  2.7373e-009
     4	        375.1	 -1.4666e-008
     5	        25720	  1.9896e-008
     6	       1e-006	      0.18928
     7	      1.33397	 -6.6191e-009
     8	     0.150179	   -0.0016094
     9	     1.4e-007	   0.00029789
    10	     0.271787	   -0.0002428
    11	     0.128149	   0.00010249
    12	      1.09953	   0.00059577
    13	         0.55	  -0.00048563
    14	      1.04945	   -0.0012781
    15	      1.04945	  -0.00010892
    16	     0.975731	    0.0001976
    17	      1.36364	  3.3859e-005
    18	      0.94869	   0.00043066
    19	      1.95913	  -3.168e-005
    20	     0.929147	   0.00014167
    21	      2.34076	 -1.7992e-005
    22	     0.934846	   0.00014451
    23	      2.72727	  2.7436e-005
    24	     0.909993	   0.00011512
    25	       3.6523	 -4.5149e-006
