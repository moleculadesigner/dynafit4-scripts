
PARAMETERS AND CONFIDENCE INTERVALS

NO.	SET	           PARAMETER	     INITIAL	    LIMIT-LO	    LIMIT-HI	         FIT	      STDERR	         CV%	      F(SDI)	   CONLIN-LO	   CONLIN-HI	   CONLIN-P%	

  1	  1	                time	           0	
  2	  1	             restart	           0	
  3	  *	                 k.1	    0.000127	      1e-006	      1e+006	     5.22452	     72.1932	     10205.9	     4169.53	    -136.285	     146.735	          95	
  4	  *	                k.-1	       1.008	      1e-006	      1e+006	    0.386761	   0.0560017	     106.945	     43.6913	    0.276989	    0.496533	          95	
  5	  *	                 k.2	    0.001564	      1e-006	      1e+006	     6.59925	     128.156	     14343.2	     5859.79	    -244.607	     257.806	          95	
  6	  *	                k.-2	       56650	      1e-006	      1e+006	       56650	1.11911e+006	     14590.7	     5960.88	-2.13699e+006	2.25029e+006	          95	
  7	  *	               k.cat	       53.07	      1e-006	      1e+006	     53.3013	     1256.85	       17416	     7115.13	    -2410.33	     2516.93	          95	
  8	  *	               k.off	       9.766	      1e-006	      1e+006	     1.57575	   0.0236439	     11.0824	     4.52761	      1.5294	     1.62209	          95	
  9	  *	              k.-off	    0.009484	      1e-006	      1e+006	4.72844e-005	  0.00119042	     18594.4	     7596.58	 -0.00228612	  0.00238069	          95	
 10	  1	                 [E]	           1	    0.909091	         1.1	    0.938135	     12.9692	     10210.6	     4171.43	    -24.4836	     26.3599	          95	
 11	  1	                 [S]	         0.5	    0.454545	        0.55	        0.55	     7.60247	     10209.2	     4170.89	     -14.352	      15.452	          95	
 12	  1	              [E.S1]	           0	
 13	  1	              [E.S2]	           0	
 14	  1	               [E.P]	           0	
 15	  1	                 [P]	           0	
 16	  *	                r(E)	        0.18	    1.8e-007	      180000	     0.18976	     2.62337	     10210.7	     4171.49	    -4.95246	     5.33198	          95	
 17	  1	                r(S)	           0	
 18	  *	             r(E.S1)	        0.14	    1.4e-007	      140000	   0.0945697	     6.10709	     47696.2	     19485.8	    -11.8763	     12.0654	          95	
 19	  *	             r(E.S2)	       0.079	    7.9e-008	       79000	   0.0897579	     53341.8	4.38932e+008	1.79321e+008	     -104558	      104558	          95	
 20	  *	              r(E.P)	        0.13	    1.3e-007	      130000	     10.2737	     142.175	     10221.1	     4175.72	    -268.411	     288.959	          95	
 21	  1	                r(P)	           0	
 22	  1	              offset	        0.99	
 23	  1	               delay	           0	
 24	  1	          incubation	           0	
 25	  1	                [E]i	           0	
 26	  1	                [S]i	           0	
 27	  1	             [E.S1]i	           0	
 28	  1	             [E.S2]i	           0	
 29	  1	              [E.P]i	           0	
 30	  1	                [P]i	           0	
 31	  1	            dilution	           0	
 32	  1	           intensive	           0	
 33	  2	                time	           0	
 34	  2	             restart	           0	
 35	  2	                 [E]	           1	    0.909091	         1.1	    0.925275	     12.7911	     10210.3	      4171.3	    -24.1472	     25.9978	          95	
 36	  2	                 [S]	           1	    0.909091	         1.1	         1.1	     15.2016	       10207	     4169.99	    -28.6976	     30.8976	          95	
 37	  2	              [E.S1]	           0	
 38	  2	              [E.S2]	           0	
 39	  2	               [E.P]	           0	
 40	  2	                 [P]	           0	
 41	  2	                r(S)	           0	
 42	  2	                r(P)	           0	
 43	  2	              offset	        0.97	
 44	  2	               delay	           0	
 45	  2	          incubation	           0	
 46	  2	                [E]i	           0	
 47	  2	                [S]i	           0	
 48	  2	             [E.S1]i	           0	
 49	  2	             [E.S2]i	           0	
 50	  2	              [E.P]i	           0	
 51	  2	                [P]i	           0	
 52	  2	            dilution	           0	
 53	  2	           intensive	           0	
 54	  3	                time	           0	
 55	  3	             restart	           0	
 56	  3	                 [E]	           1	    0.909091	         1.1	    0.913673	     12.6306	     10210.2	     4171.27	    -23.8442	     25.6716	          95	
 57	  3	                 [S]	         1.5	     1.36364	        1.65	        1.65	     22.8059	     10208.6	     4170.61	    -43.0532	     46.3532	          95	
 58	  3	              [E.S1]	           0	
 59	  3	              [E.S2]	           0	
 60	  3	               [E.P]	           0	
 61	  3	                 [P]	           0	
 62	  3	                r(S)	           0	
 63	  3	                r(P)	           0	
 64	  3	              offset	        0.93	
 65	  3	               delay	           0	
 66	  3	          incubation	           0	
 67	  3	                [E]i	           0	
 68	  3	                [S]i	           0	
 69	  3	             [E.S1]i	           0	
 70	  3	             [E.S2]i	           0	
 71	  3	              [E.P]i	           0	
 72	  3	                [P]i	           0	
 73	  3	            dilution	           0	
 74	  3	           intensive	           0	
 75	  4	                time	           0	
 76	  4	             restart	           0	
 77	  4	                 [E]	           1	    0.909091	         1.1	    0.916671	     12.6716	     10209.8	     4171.13	    -23.9216	      25.755	          95	
 78	  4	                 [S]	           2	     1.81818	         2.2	         2.2	     30.4025	     10206.8	     4169.88	    -57.3938	     61.7938	          95	
 79	  4	              [E.S1]	           0	
 80	  4	              [E.S2]	           0	
 81	  4	               [E.P]	           0	
 82	  4	                 [P]	           0	
 83	  4	                r(S)	           0	
 84	  4	                r(P)	           0	
 85	  4	              offset	        0.89	
 86	  4	               delay	           0	
 87	  4	          incubation	           0	
 88	  4	                [E]i	           0	
 89	  4	                [S]i	           0	
 90	  4	             [E.S1]i	           0	
 91	  4	             [E.S2]i	           0	
 92	  4	              [E.P]i	           0	
 93	  4	                [P]i	           0	
 94	  4	            dilution	           0	
 95	  4	           intensive	           0	
 96	  5	                time	           0	
 97	  5	             restart	           0	
 98	  5	                 [E]	           1	    0.909091	         1.1	    0.909091	     12.5666	     10209.6	     4171.05	    -23.7233	     25.5415	          95	
 99	  5	                 [S]	         2.5	     2.27273	        2.75	     2.52528	     34.8966	     10206.4	     4169.74	    -65.8775	      70.928	          95	
100	  5	              [E.S1]	           0	
101	  5	              [E.S2]	           0	
102	  5	               [E.P]	           0	
103	  5	                 [P]	           0	
104	  5	                r(S)	           0	
105	  5	                r(P)	           0	
106	  5	              offset	        0.85	
107	  5	               delay	           0	
108	  5	          incubation	           0	
109	  5	                [E]i	           0	
110	  5	                [S]i	           0	
111	  5	             [E.S1]i	           0	
112	  5	             [E.S2]i	           0	
113	  5	              [E.P]i	           0	
114	  5	                [P]i	           0	
115	  5	            dilution	           0	
116	  5	           intensive	           0	
117	  6	                time	           0	
118	  6	             restart	           0	
119	  6	                 [E]	           1	    0.909091	         1.1	    0.920679	     12.7266	     10209.6	     4171.02	    -24.0255	     25.8669	          95	
120	  6	                 [S]	           3	     2.72727	         3.3	     2.74955	     37.9968	     10206.7	     4169.87	    -71.7302	     77.2293	          95	
121	  6	              [E.S1]	           0	
122	  6	              [E.S2]	           0	
123	  6	               [E.P]	           0	
124	  6	                 [P]	           0	
125	  6	                r(S)	           0	
126	  6	                r(P)	           0	
127	  6	              offset	        0.81	
128	  6	               delay	           0	
129	  6	          incubation	           0	
130	  6	                [E]i	           0	
131	  6	                [S]i	           0	
132	  6	             [E.S1]i	           0	
133	  6	             [E.S2]i	           0	
134	  6	              [E.P]i	           0	
135	  6	                [P]i	           0	
136	  6	            dilution	           0	
137	  6	           intensive	           0	
138	  7	                time	           0	
139	  7	             restart	           0	
140	  7	                 [E]	           1	    0.909091	         1.1	    0.915124	     12.6495	     10209.3	     4170.92	      -23.88	     25.7102	          95	
141	  7	                 [S]	           4	     3.63636	         4.4	     3.63636	      50.246	     10205.5	     4169.37	    -94.8537	     102.126	          95	
142	  7	              [E.S1]	           0	
143	  7	              [E.S2]	           0	
144	  7	               [E.P]	           0	
145	  7	                 [P]	           0	
146	  7	                r(S)	           0	
147	  7	                r(P)	           0	
148	  7	              offset	        0.78	
149	  7	               delay	           0	
150	  7	          incubation	           0	
151	  7	                [E]i	           0	
152	  7	                [S]i	           0	
153	  7	             [E.S1]i	           0	
154	  7	             [E.S2]i	           0	
155	  7	              [E.P]i	           0	
156	  7	                [P]i	           0	
157	  7	            dilution	           0	
158	  7	           intensive	           0	
