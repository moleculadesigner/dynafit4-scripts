
===================================================
Least squares Minimization - Trust Region Algorithm
===================================================

Optimized parameters:

     1 	 k.1
     2 	 k.-1
     3 	 k.2
     4 	 k.-2
     5 	 k.cat
     6 	 k.off
     7 	 k.-off
     8 	 r(E)
     9 	 r(E.S1)
    10 	 r(E.S2)
    11 	 r(E.P)
    12 	 [E]#1
    13 	 [S]#1
    14 	 [E]#2
    15 	 [S]#2
    16 	 [E]#3
    17 	 [S]#3
    18 	 [E]#4
    19 	 [S]#4
    20 	 [E]#5
    21 	 [S]#5
    22 	 [E]#6
    23 	 [S]#6
    24 	 [E]#7
    25 	 [S]#7

:: NL2SOL :: DNLAFB
:: Analytic Jacobian; Constrained

________________________________________________________________________

:: Initial Estimate

     i	     Param p(i)	            Min	            Max
     1	       0.006645	         1e-006	         1e+006
     2	        0.04146	         1e-006	         1e+006
     3	          25960	         1e-006	         1e+006
     4	      0.0001233	         1e-006	         1e+006
     5	        0.09253	         1e-006	         1e+006
     6	          50490	         1e-006	         1e+006
     7	     8.303e-006	         1e-006	         1e+006
     8	           0.18	       1.8e-007	         180000
     9	           0.14	       1.4e-007	         140000
    10	          0.079	       7.9e-008	          79000
    11	           0.13	       1.3e-007	         130000
    12	              1	       0.909091	            1.1
    13	            0.5	       0.454545	           0.55
    14	              1	       0.909091	            1.1
    15	              1	       0.909091	            1.1
    16	              1	       0.909091	            1.1
    17	            1.5	        1.36364	           1.65
    18	              1	       0.909091	            1.1
    19	              2	        1.81818	            2.2
    20	              1	       0.909091	            1.1
    21	            2.5	        2.27273	           2.75
    22	              1	       0.909091	            1.1
    23	              3	        2.72727	            3.3
    24	              1	       0.909091	            1.1
    25	              4	        3.63636	            4.4

:: Iterations

     i	 FEval	   F(i) = SSQ	   Param( 1)	   Param( 2)	   Param( 3)	   Param( 4)	   Param( 5)	   Param( 6)	   Param( 7)	   Param( 8)	   Param( 9)	   Param(10)	   Param(11)	   Param(12)	   Param(13)	   Param(14)	   Param(15)	   Param(16)	   Param(17)	   Param(18)	   Param(19)	   Param(20)	   Param(21)	   Param(22)	   Param(23)	   Param(24)	   Param(25)	
     0	     1	       36.464	     0.006645	      0.04146	        25960	    0.0001233	      0.09253	        50490	   8.303e-006	         0.18	         0.14	        0.079	         0.13	            1	          0.5	            1	            1	            1	          1.5	            1	            2	            1	          2.5	            1	            3	            1	            4	
     1	     2	       22.122	      0.07014	     0.041463	        25960	  4.1101e-005	      0.69172	        50490	    0.0011285	       0.1562	      0.13527	      0.25937	         0.13	          1.1	         0.55	       1.0425	          1.1	       1.0082	       1.5779	      0.99457	       1.8891	      0.99262	       2.2861	      0.99671	       2.9703	      0.98972	       3.9505	
     2	     3	        7.629	       1e-006	     0.041918	        25960	    0.0056262	      0.53122	        50490	     0.087747	      0.15195	      0.12748	     0.086219	         0.13	          1.1	         0.55	       1.0031	          1.1	      0.95356	       1.5779	      0.93603	       1.8891	      0.93498	       2.2994	      0.93262	          3.3	      0.93196	       4.2885	
     3	     4	        2.685	     0.045674	     0.041918	        25960	     0.005649	      0.89593	        50490	     0.087743	      0.13888	      0.12741	     0.086254	      0.13002	          1.1	         0.55	       1.0052	          1.1	      0.94476	       1.5843	      0.92369	       1.8874	      0.92292	        2.293	      0.91557	          3.3	      0.93196	       4.2981	
     4	     7	       2.5638	      0.11196	     0.041918	        25960	    0.0056486	      0.90929	        50490	     0.087743	      0.14007	       0.1274	      0.10344	      0.13002	          1.1	         0.55	       1.0007	          1.1	      0.93791	       1.5851	      0.91744	       1.8878	      0.90909	       2.2931	      0.91829	          3.3	      0.93933	       4.2974	
     5	     8	       2.1462	      0.17673	     0.041911	        25960	    0.0056189	      0.91785	        50490	     0.087745	      0.13966	      0.12738	     0.091946	      0.13002	          1.1	         0.55	      0.99576	          1.1	      0.93562	       1.5865	      0.91652	       1.8884	      0.90909	       2.2934	      0.92377	          3.3	      0.93863	       4.2968	
     6	     9	       1.8222	      0.41393	      0.04191	        25960	    0.0056167	      0.98215	        50490	     0.087745	      0.14207	      0.12735	     0.099557	      0.13003	          1.1	         0.55	      0.98722	          1.1	      0.93428	       1.5955	      0.91765	       1.8928	      0.90909	       2.2939	      0.94208	          3.3	      0.94206	       4.2927	
     7	    10	       1.5055	      0.64517	     0.041881	        25960	    0.0053697	       1.0637	        50490	     0.087758	      0.14585	      0.12735	     0.090267	      0.13002	       1.0789	         0.55	      0.97521	          1.1	      0.92882	       1.6172	      0.91467	       1.9049	      0.90909	       2.2968	       0.9481	          3.3	      0.95094	       4.2881	
     8	    11	        1.421	      0.80273	      0.04187	        25960	    0.0056576	       1.1273	        50490	     0.087744	      0.14712	      0.12838	     0.092241	      0.12994	       1.0736	         0.55	      0.97239	          1.1	      0.92875	         1.65	      0.91497	       1.9489	      0.90909	       2.3165	      0.94642	          3.3	      0.94853	       4.2886	
     9	    13	       1.3417	       1.0453	     0.039529	        25960	       1e-006	       1.1701	        50490	      0.08886	      0.14805	       0.1355	     0.096292	       0.1295	       1.0708	         0.55	      0.97203	          1.1	      0.92958	         1.65	      0.92027	       2.0679	      0.90999	       2.3618	      0.94547	          3.3	      0.94853	       4.3463	
    10	    14	       1.2746	       1.3934	     0.041862	        25960	       1e-006	       1.1557	        50490	     0.089031	      0.14909	       0.1599	      0.10141	      0.12834	       1.0668	         0.55	      0.97105	          1.1	      0.92994	         1.65	      0.92697	          2.2	      0.91127	       2.4293	      0.94433	          3.3	      0.94752	          4.4	
    11	    15	       1.2384	       1.6974	     0.041872	        25960	       1e-006	       1.1275	        50490	     0.089909	      0.15031	      0.19189	      0.10357	      0.12722	       1.0603	         0.55	      0.96774	          1.1	       0.9282	         1.65	      0.92487	          2.2	      0.91121	       2.4704	      0.94276	          3.3	       0.9451	          4.4	
    12	    17	       1.1843	       2.7764	     0.041422	        25960	    0.0023782	      0.81075	        50490	     0.091085	      0.15253	      0.25887	      0.11454	      0.12508	       1.0512	         0.55	      0.96653	          1.1	      0.93149	         1.65	      0.92687	          2.2	      0.92034	       2.5389	       0.9449	          3.3	      0.94278	          4.4	
    13	    19	       1.1356	       3.3126	     0.041713	        25960	    0.0042865	      0.68059	        50490	     0.091504	      0.14896	      0.28355	      0.10586	      0.12442	       1.0821	         0.55	       1.0006	          1.1	      0.96651	         1.65	      0.96372	          2.2	      0.95738	       2.5804	      0.98568	          3.3	      0.98545	          4.4	
    14	    20	       1.0242	       3.7245	     0.037659	        25960	      0.16185	      0.53433	        50490	     0.088408	      0.15092	      0.83303	      0.10963	      0.11646	       1.0759	         0.55	      0.99845	          1.1	      0.96604	         1.65	      0.96439	          2.2	      0.95775	       2.5668	      0.98556	          3.3	      0.98545	          4.4	
    15	    21	      0.92641	       4.5923	     0.043715	        25960	      0.16854	      0.41551	        50490	     0.098386	      0.15079	        2.088	      0.10923	      0.10281	       1.0873	         0.55	       1.0185	          1.1	      0.98552	         1.65	      0.98569	          2.2	      0.98321	       2.6633	       1.0093	          3.3	       1.0093	          4.4	
    16	    22	      0.79078	       6.6883	       1e-006	        25960	      0.19227	        0.307	        50490	      0.15865	      0.15796	       7.9335	      0.10957	     0.036025	       1.0644	         0.55	       1.0234	          1.1	      0.99241	         1.65	      0.99798	          2.2	      0.99316	       2.6959	       1.0196	          3.3	       1.0222	          4.4	
    17	    23	      0.67828	       7.8284	       1e-006	        25960	     0.062356	      0.24816	        50490	       2.7179	      0.15307	       20.194	      0.10902	     1.3e-007	       1.0822	         0.55	       1.0413	          1.1	       1.0147	         1.65	       1.0172	          2.2	       1.0079	       2.5268	       1.0355	       3.2326	       1.0336	          4.4	
    18	    24	      0.64764	       8.5931	       1e-006	        25960	       1e-006	      0.20857	        50490	       28.478	      0.15287	       21.478	      0.10935	     1.3e-007	       1.0872	         0.55	       1.0413	          1.1	       1.0233	         1.65	       1.0256	          2.2	       1.0083	       2.2959	       1.0384	       3.2326	       1.0336	          4.4	
    19	    25	      0.62732	       8.9187	    0.0023226	        25959	    0.0022559	      0.18825	        50490	       125.47	       0.1523	        21.62	      0.10845	     1.3e-007	          1.1	         0.55	       1.0566	          1.1	       1.0387	         1.65	       1.0405	          2.2	       1.0186	       2.2959	       1.0463	       3.2326	       1.0336	          4.4	
    20	    28	      0.62472	       8.9188	    0.0040999	        25959	       1e-006	      0.18532	        50490	       125.47	      0.15195	        21.62	      0.10854	     1.3e-007	          1.1	         0.55	        1.057	          1.1	       1.0381	         1.65	       1.0398	          2.2	       1.0191	       2.2959	       1.0459	       3.2326	       1.0319	       4.3999	
    21	    29	      0.61815	        8.919	     0.004147	        25959	       1e-006	      0.17376	        50490	       125.47	       0.1522	        21.62	      0.10897	     1.3e-007	          1.1	         0.55	       1.0575	          1.1	       1.0366	         1.65	       1.0378	          2.2	       1.0195	       2.2943	       1.0421	        3.232	       1.0239	       4.3992	
    22	    30	      0.61424	       8.9251	    0.0041023	        25959	       1e-006	      0.16312	        50490	       125.47	      0.15251	       21.621	      0.10938	     1.3e-007	          1.1	         0.55	       1.0569	          1.1	       1.0359	         1.65	       1.0353	       2.1762	       1.0173	       2.2727	       1.0382	        3.227	       1.0191	       4.3889	
    23	    31	      0.61223	       8.9577	    0.0034742	        25959	   0.00064912	      0.13048	        50490	       125.47	      0.15319	       21.622	      0.11043	     1.3e-007	          1.1	         0.55	         1.06	          1.1	       1.0344	         1.65	       1.0265	       2.0905	       1.0149	       2.2727	       1.0288	       3.2162	       1.0024	       4.3477	
    24	    32	      0.60823	       9.0049	    0.0040847	        25959	   0.00042302	      0.13729	        50490	       125.47	      0.15473	       21.621	      0.11104	     1.3e-007	       1.0893	         0.55	        1.048	          1.1	       1.0275	         1.65	        1.019	       2.0211	       1.0061	       2.2727	       1.0234	       3.2052	        1.002	       4.3015	
    25	    34	      0.60261	       9.3356	    0.0045735	        25959	   0.00033941	      0.13021	        50490	       125.47	       0.1557	       21.606	      0.11167	     1.3e-007	       1.0847	         0.55	       1.0456	          1.1	       1.0264	         1.65	       1.0117	        1.878	       1.0013	       2.2748	       1.0189	       3.1392	       0.9993	       4.0597	
    26	    35	      0.59551	       10.179	    0.0055852	        25959	   0.00023502	      0.11727	        50490	       125.42	      0.15697	       20.726	      0.11204	     1.3e-007	       1.0817	         0.55	        1.048	          1.1	       1.0302	         1.65	       1.0088	       1.8182	        1.002	        2.279	       1.0179	       3.0155	      0.99964	       3.9361	
    27	    36	      0.59249	       10.675	       1e-006	        25959	       1e-006	      0.11659	        50490	       125.43	      0.15875	       20.469	      0.11307	     1.3e-007	       1.0716	         0.55	       1.0384	          1.1	       1.0203	         1.65	       1.0018	       1.8182	      0.99525	       2.4101	       1.0086	       3.0152	      0.98955	       3.9358	
    28	    37	      0.58709	       11.411	    0.0040731	        25959	       1e-006	      0.11141	        50490	       125.52	      0.16398	       17.798	      0.11481	     1.3e-007	       1.0458	         0.55	       1.0212	          1.1	        1.007	         1.65	      0.99032	       1.8182	        0.982	       2.4097	      0.99221	       2.7273	      0.97101	       3.6364	
    29	    42	      0.58696	       11.411	    0.0040718	        25959	    0.0044327	      0.10998	        50490	       125.52	      0.16398	       17.798	      0.11497	     1.3e-007	       1.0453	         0.55	       1.0202	       1.0998	       1.0087	         1.65	      0.98931	       1.8182	      0.98179	       2.4097	      0.99251	       2.7273	      0.97138	       3.6364	
    30	    43	       0.5861	       11.411	     0.004073	        25959	    0.0044568	      0.10844	        50490	       125.52	      0.16431	       17.798	      0.11495	     1.3e-007	       1.0452	         0.55	       1.0218	       1.0999	       1.0077	         1.65	      0.98889	       1.8182	      0.98127	       2.4097	      0.99175	       2.7273	      0.97215	       3.6364	
    31	    44	      0.58581	       11.411	    0.0040133	        25959	    0.0045939	      0.10701	        50490	       125.52	      0.16434	       17.798	      0.11499	     1.3e-007	       1.0451	         0.55	       1.0224	          1.1	       1.0074	       1.6496	      0.98865	       1.8182	      0.98083	       2.4098	      0.99126	       2.7273	      0.97223	       3.6364	
    32	    45	      0.58532	       11.411	    0.0043403	        25959	    0.0051361	      0.10416	        50490	       125.52	      0.16466	       17.798	      0.11526	     1.3e-007	       1.0435	         0.55	       1.0221	          1.1	       1.0053	       1.6445	      0.98673	       1.8182	      0.97845	       2.4105	      0.98858	       2.7273	      0.97069	       3.6364	
    33	    47	      0.58399	       11.413	    0.0054753	        25959	    0.0061801	     0.096371	        50490	       125.52	      0.16688	       17.795	      0.11682	     1.3e-007	       1.0321	         0.55	       1.0133	          1.1	      0.99076	       1.6024	      0.97414	       1.8182	      0.96522	       2.4208	       0.9748	       2.7273	      0.95886	       3.6364	
    34	    48	      0.58381	       11.439	    0.0085733	        25959	     0.008529	      0.07661	        50490	       125.52	      0.17321	       17.784	      0.12128	     1.3e-007	      0.99963	         0.55	      0.98733	          1.1	      0.95007	       1.4816	      0.93835	       1.8201	       0.9285	        2.453	      0.93666	       2.7273	      0.92495	       3.6364	
    35	    49	       0.5818	       11.454	    0.0079295	        25959	    0.0070799	     0.086759	        50490	       125.52	       0.1726	       17.774	      0.12069	     1.3e-007	        1.003	         0.55	      0.98805	          1.1	      0.95557	       1.5012	      0.94369	       1.8182	      0.93458	       2.4548	      0.94309	       2.7273	      0.92951	       3.6364	
    36	    50	      0.58102	       11.503	    0.0081633	        25959	    0.0065559	     0.088319	        50490	       125.52	      0.17347	        17.75	      0.12129	     1.3e-007	      0.99864	         0.55	      0.98318	          1.1	      0.95105	        1.486	      0.93959	       1.8182	       0.9309	       2.4529	       0.9395	       2.7273	      0.92505	       3.6364	
    37	    51	       0.5783	       11.536	     0.013283	        25959	    0.0024787	      0.08901	        50490	       125.52	      0.17645	       17.737	      0.12332	     1.3e-007	      0.98303	         0.55	      0.96665	          1.1	      0.93664	       1.4716	      0.92691	       1.9109	      0.91637	       2.4543	       0.9251	       2.7273	      0.90909	       3.6364	
    38	    52	      0.57635	        11.55	     0.026376	        25959	       1e-006	      0.08569	        50490	       125.52	      0.17665	       17.586	      0.12337	     1.3e-007	      0.98291	         0.55	       0.9668	          1.1	      0.93605	       1.4511	      0.93141	        2.112	      0.91636	       2.4503	      0.92516	       2.7273	      0.90909	       3.6364	
    39	    54	      0.57607	       11.619	    0.0040667	        25959	    0.0030319	     0.081738	        50490	       125.52	      0.17691	        17.41	      0.12344	     1.3e-007	      0.98191	         0.55	      0.96935	          1.1	      0.93538	       1.4283	      0.93141	       2.1567	       0.9163	       2.4518	      0.92582	       2.7273	      0.90909	       3.6364	
    40	    55	      0.57574	       11.664	      0.00178	        25959	    0.0030907	     0.081151	        50490	       125.52	      0.17726	       17.217	      0.12347	     1.3e-007	      0.98159	         0.55	      0.96748	          1.1	      0.93546	       1.4321	      0.93139	       2.1569	      0.91619	       2.4521	        0.925	       2.7273	      0.90909	       3.6364	
    41	    56	      0.57562	       11.674	       1e-006	        25959	       1e-006	     0.080605	        50490	       125.52	      0.17735	       17.058	      0.12347	     1.3e-007	      0.98152	         0.55	      0.96744	          1.1	      0.93566	       1.4344	      0.93136	       2.1517	      0.91622	       2.4498	      0.92506	       2.7273	      0.90909	       3.6364	
    42	    84	      0.57562	       11.674	  2.6403e-005	        25959	       1e-006	     0.080605	        50490	       125.52	      0.17735	       17.058	      0.12347	     1.3e-007	      0.98151	         0.55	      0.96744	          1.1	      0.93566	       1.4344	      0.93136	       2.1517	      0.91622	       2.4498	      0.92506	       2.7273	      0.90909	       3.6364	
    43	    85	      0.57562	       11.674	  2.6403e-005	        25959	  1.0008e-006	     0.080605	        50490	       125.52	      0.17735	       17.058	      0.12347	     1.3e-007	      0.98151	         0.55	      0.96744	          1.1	      0.93566	       1.4344	      0.93136	       2.1517	      0.91622	       2.4498	      0.92506	       2.7273	      0.90909	       3.6364	
    44	    86	      0.57562	       11.674	  2.6403e-005	        25959	  1.0008e-006	     0.080605	        50490	       125.52	      0.17735	       17.058	      0.12347	     1.3e-007	      0.98151	         0.55	      0.96744	          1.1	      0.93566	       1.4344	      0.93136	       2.1517	      0.91622	       2.4498	      0.92506	       2.7273	      0.90909	       3.6364	

:: False convergence

       Squares	       0.575623
     Func Eval	             86
     Grad Eval	             44

:: Nonlinear parameters

     i	   Final p(i)	  Gradient(i)
     1	      11.6744	 -3.4869e-005
     2	 2.64027e-005	    -0.015962
     3	      25959.3	  -2.113e-007
     4	 1.00078e-006	      -1.3226
     5	    0.0806046	    0.0077539
     6	      50489.9	    2.74e-009
     7	      125.524	 -1.0285e-006
     8	     0.177349	    -0.012797
     9	      17.0577	   0.00031571
    10	      0.12347	     -0.00686
    11	     1.3e-007	   0.00071814
    12	     0.981515	    0.0034194
    13	         0.55	    -0.050921
    14	     0.967445	   -0.0016328
    15	          1.1	   -0.0072295
    16	     0.935658	  -0.00051534
    17	      1.43439	  -0.00068908
    18	     0.931355	   -0.0010199
    19	      2.15172	  -9.333e-006
    20	     0.916223	  -0.00091386
    21	      2.44977	  -3.729e-006
    22	     0.925062	   -0.0005477
    23	      2.72727	   0.00093459
    24	     0.909091	     0.024604
    25	      3.63636	    0.0040661
